﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tagg.Domain;
using Tagg.Services;
using Tagg.Services.Communication;
using Sentry;
using Sentry.Extensibility;
using Sentry.Protocol;

namespace JobEngineTest
{
    public class JobWorker
    {
        private TaggUnitOfWork _work;
        private TaggMessageService _messages;
        private int _cheatFactorMinutes;

        public JobWorker(
            TaggUnitOfWork work,
            TaggMessageService messages)
        {
            _work = work;
            _messages = messages;
            _cheatFactorMinutes = 10;
        }

        public bool ProcessOffers()
        {
            try
            {
                SentrySdk.AddBreadcrumb("Finding ready Queues");
                var readyQueues = _work.Offers.GetByStartTime(DateTime.UtcNow.AddMinutes(_cheatFactorMinutes));
                foreach (var q in readyQueues.Where(x => x.OfferQueue.Count > 0))
                {
                    SentrySdk.AddBreadcrumb($"queue: {q.Id}, jobId: {q.JobId}");
                    try
                    {
                        processQueue(q);
                    }
                    catch (Exception ex)
                    {
                        SentrySdk.CaptureException(ex);
                    }

                }

                //if (readyQueues.Count > 0)
                //{
                //    SentrySdk.CaptureMessage("Staging Queues Processed");
                //}
                return true;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return false;
            }
        }

        // TODO : Process past jobs that have not be accepted (and maybe need to do something with cancelled jobs too).
        public ProcessJobsResult ProcessEndedJobs()
        {
            var result = new ProcessJobsResult();
            try
            {
                // TODO : Add all jobs to the result (in case some are not processed)
                // kvp of the job and the processed result??
                var endedJobs = _work.Jobs.GetByMaxEndDate(DateTime.UtcNow)
                    .Where(j => j.Status == JobStatus.Accepted || (!j.SubNeeded && j.Status == JobStatus.Open));

                foreach (var job in endedJobs)
                {
                    SentrySdk.AddBreadcrumb($"Ending Job: {job.Id}");
                    job.Status = JobStatus.Complete;
                    _work.Jobs.UpdateStatus(job.Id, JobStatus.Complete, job.SubstituteId);
                    _work.JobRatings.CreateEmptyRating(job);
                    result.ProcessedJobs.Add(job);
                }
                return result;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                result.Exception = ex;
                result.Succeeded = false;
                return result;
            }
        }

        public bool CloseExpiredChats()
        {
            try
            {
                var exceptionHappened = false;

                var expiredOffers = _work.Jobs
                    .Query(j => (j.Status == JobStatus.Accepted || j.Status == JobStatus.Cancelled)
                    && j.Offers.Any(z => z.Status != JobOfferStatus.Accepted))
                    .ToList();

                var expiredJobs = _work.Jobs.Query(j => j.Status == JobStatus.Open && j.StartDate < DateTime.UtcNow).ToList();
                expiredOffers.AddRange(expiredJobs);

                foreach (var expired in expiredOffers)
                {
                    var logData = new Dictionary<string, string> { { "JobId", expired.Id }, { "SubId", expired.SubstituteId } };
                    SentrySdk.AddBreadcrumb($"Closing chat for job {expired.Id}", null, null, logData);
                    foreach (var offer in expired.Offers.Where(o => o.Status != JobOfferStatus.Sent))
                    {
                        try
                        {
                            offer.Status = JobOfferStatus.Expired;
                            var channelDeletResult = _messages.DeleteChatChannel(expired.Id, offer.SubId);
                        }
                        catch (Exception ex)
                        {
                            SentrySdk.AddBreadcrumb("Exception while closing chat", null, null, logData, BreadcrumbLevel.Error);
                            exceptionHappened = true;
                        }

                    }
                    _work.Jobs.Update(expired);

                }

                return exceptionHappened;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return false;
            }
        }

        #region Helpers
        private void processQueue(JobOfferQueue queue)
        {
            // TODO: Comment this line
            //DateTime checkDate = DateTime.UtcNow.AddMinutes(1);
            // TODO: Uncomment this line
            DateTime checkDate = DateTime.UtcNow.AddMinutes(_cheatFactorMinutes);
            var failedQueue = new Queue<PendingJobOffer>();

            while (queue.OfferQueue.Count > 0 && queue.OfferQueue.Peek().SendOn <= checkDate)
            {
                var logData = new Dictionary<string, string> { { "QueueId", queue.Id }, { "JobId", queue.JobId }, { "StartTime", queue.StartTime.ToString("O") } };
                SentrySdk.AddBreadcrumb($"Processing Queue: {queue.Id}", null, null, logData);
                PendingJobOffer offer = queue.OfferQueue.Dequeue();

                var offerResult = processOffer(offer, queue.JobId);
                if (offerResult.Status == JobOfferStatus.Failed)
                {
                    var errorData = new Dictionary<string, string>
                    {
                        { "QueueId", queue.Id },
                        { "SendTo", offer.SendTo },
                        { "SendOn", offer.SendOn.ToString("O") },
                        { "Sent", offer.Sent.ToString() }
                    };
                    SentrySdk.AddBreadcrumb($"Offer failed", "error", "error", errorData, BreadcrumbLevel.Error);
                    failedQueue.Enqueue(offer);
                }
            }

            // If any offers fail to send, add to temp queue to keep them at the top
            // then add the remaining
            if (failedQueue.Count > 0)
            {
                while (queue.OfferQueue.Count > 0)
                {
                    failedQueue.Enqueue(queue.OfferQueue.Dequeue());
                }
                queue.OfferQueue = failedQueue;
            }

            // Set queue to complete if no more offers queued
            // (used to prevent all old queues from returning to readyQueues in DoWork )
            queue.Status = queue.OfferQueue.Count > 0 ? TaggStatus.Active : TaggStatus.Complete;
            _work.Offers.Update(queue);

        }

        private JobOffer processOffer(PendingJobOffer offer, string jobId)
        {
            bool subHasJob = false;
            var offerToAdd = new JobOffer
            {
                SubId = offer.SendTo,
                SentOn = DateTime.UtcNow
            };

            try
            {
                var jobDetails = _work.Jobs.GetDetailsSingle(jobId);
                var testStartDate = jobDetails.StartDate.Date;
                var testEndDate = testStartDate.AddDays(1);
                subHasJob = _work.Jobs
                    .Query(j => j.StartDate >= testStartDate && j.EndDate <= testEndDate && j.SubstituteId == offer.SendTo && j.Status == JobStatus.Accepted)
                    .Any();

                if (!subHasJob)
                {
                    SentrySdk.AddBreadcrumb($"Sending offers for Job:{jobId}");

                    // Send notifications
                    BatchSendResult sendResult = _messages.SendJobOffers(jobDetails, new List<string> { offer.SendTo });
                    if (sendResult.AllSucceeded)
                    {
                        SentrySdk.AddBreadcrumb($"Successfully sent all offers for Job:{jobId}");
                        offerToAdd.Status = JobOfferStatus.Sent;
                    }
                    else
                    {
                        var failedOffers = new Dictionary<string, string>();
                        foreach (var item in sendResult.FailedIds)
                        {
                            failedOffers.Add(item, "SubId");
                        }
                        SentrySdk.AddBreadcrumb($"Offers failed for Job:{jobId}", "OfferError", "OfferError", failedOffers, BreadcrumbLevel.Warning);
                    }
                    offerToAdd.SMSId = sendResult.SMSId;
                }

                
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
            }
            finally
            {
                if (!subHasJob)
                {
                    var updateResult = _work.Jobs.AddOffer(jobId, offerToAdd);
                }
            }
            return offerToAdd;
        }
        #endregion

    }

    public class ProcessJobsResult
    {
        public ProcessJobsResult()
        {
            this.Succeeded = true;
            this.ProcessedJobs = new List<Job>();
        }
        public bool Succeeded { get; set; }
        public List<Job> ProcessedJobs { get; set; }
        public Exception Exception { get; set; }
    }
}
