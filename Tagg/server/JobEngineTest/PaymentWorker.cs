﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tagg.Domain;
using Tagg.Domain.ApiModels;
using Tagg.Services;
using Tagg.Services.Communication;
using Sentry;

namespace JobEngineTest
{
    public class PaymentWorker
    {
        private TaggUnitOfWork _work;
        private TaggMessageService _messages;
        private PaymentService _payments;

        public PaymentWorker(
            TaggUnitOfWork work,
            TaggMessageService messages,
            IConfiguration config)
        {
            _work = work;
            _messages = messages;
            _payments = new PaymentService(config);
        }

        public ProcessTransactionsResult CreateTransactions()
        {
            var result = new ProcessTransactionsResult();
            try
            {
                var endedJobs = _work.Jobs.GetByMaxEndDate(DateTime.UtcNow)
                    .Where(j => j.SubNeeded && j.Status == JobStatus.Complete && j.TransactionStatus == JobTransactionStatus.Upcoming);

                foreach (var job in endedJobs)
                {
                    
                    var customerId = _work.Schools.GetCustomerId(job.SchoolId);
                    var destinationId = _work.Subs.GetPaymentId(job.SubstituteId);

                    SentrySdk.AddBreadcrumb($"Creating transaction for Job:{job.Id}", null, null, new Dictionary<string, string> { { "JobId", job.Id }, {"customerId",customerId},{"destinationId",destinationId } });

                    // TODO : Handle school not found or customerId nullOrWhiteSpace
                    if (!string.IsNullOrWhiteSpace(customerId) && !string.IsNullOrWhiteSpace(destinationId))
                    {
                        var trx = new Transaction
                        {
                            JobId = job.Id,
                            JobNumber = "abcde",
                            Status = TransactionStatus.Hold,
                            CustomerId = customerId,
                            DestinationId = destinationId,
                            ProcessOn = DateTime.UtcNow.AddDays(7),
                            PayRateUsd = job.PayAmount,
                            TotalAmountUsd = job.ChargeAmount,
                            Description = $"Transaction for Job {job.Id}",
                        };
                        var createResult = _work.Transactions.Add(trx);
                        // TODO : Need to do something if transaction isn't created successfully
                        if (createResult.Succeeded)
                        {
                            SentrySdk.AddBreadcrumb($"Transaction created successfully for Job:{job.Id}");
                            _work.Jobs.UpdateTransactionStatus(job.Id, JobTransactionStatus.Created);
                        }
                        else
                        {
                            SentrySdk.AddBreadcrumb($"Transaction failed for Job:{job.Id}", level: Sentry.Protocol.BreadcrumbLevel.Error);
                        }
                    }
                    else
                    {
                        SentrySdk.AddBreadcrumb("customerId or destinationId null");
                    }
                    
                }
                return result;

            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                result.Exception = ex;
                result.Succeeded = false;
                return result;
            }
        }

        public ProcessChargesResult ProcessCharges()
        {
            var result = new ProcessChargesResult();
            try
            {
                var readyTransactions = _work.Transactions.GetQueryable()
                    .Where(t => t.Status == TransactionStatus.Hold && t.ProcessOn <= DateTime.UtcNow)
                    .ToList();
                foreach (var trx in readyTransactions)
                {
                    SentrySdk.AddBreadcrumb($"Processing charges for trx:{trx.Id}");
                    var dto = new ConnectChargeDto
                    {
                        TotalAmountCents = TransactionHelper.UsdDoubleToIntCents(trx.TotalAmountUsd),
                        Currency = "usd",
                        CustomerId = trx.CustomerId,
                        Description = trx.Description,
                        DestinationId = trx.DestinationId,
                        DestinationAmountCents = TransactionHelper.UsdDoubleToIntCents(trx.PayRateUsd)
                    };
                    var chargeResult = _payments.Customers.CreateCharge(dto);
                    if (chargeResult.Succeeded)
                    {
                        SentrySdk.AddBreadcrumb($"Charge created with status: {chargeResult.Status}");
                        var trxUpdate = _work.Transactions.UpdateStatus(trx.Id, TransactionStatus.Pending, chargeResult.Status);
                        result.LogResult(chargeResult.Status, trx.JobId);
                    }
                    else
                    {
                        SentrySdk.AddBreadcrumb($"Charge failed: {chargeResult.Status}");
                        var trxUpdate = _work.Transactions.UpdateStatus(trx.Id, TransactionStatus.Failed, chargeResult.Status);
                        result.LogResult(chargeResult.Status, trx.JobId);
                    }
                    
                }
                return result;

            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                result.Exception = ex;
                result.Succeeded = false;
                return result;
            }
        }
    }

    public class ProcessChargesResult
    {
        public ProcessChargesResult()
        {
            this.Succeeded = true;
            this.FailedCharges = new List<string>();
        }
        public bool Succeeded { get; set; }
        public List<string> FailedCharges { get; set; }
        public Exception Exception { get; set; }

        public void LogResult(string status, string jobId)
        {
            if (status != "pending")
            {
                this.FailedCharges.Add(jobId);
                this.Succeeded = false;
            }
        }

    }

    public class ProcessTransactionsResult
    {
        public bool Succeeded { get; set; }
        public Exception Exception { get; set; }
    }
}
