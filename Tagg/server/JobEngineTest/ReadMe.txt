﻿**Summary**
JobEngine runs as a WebJob (deployed to Azure WebJobs within the tagg-api app service)

**How To Publish/Deploy**
NOTE: PUBLISH JOBENGINE AFTER PUBLISHING THE API. CURRENTLY, PUBLISHING THE API WILL DELETE THE WEBJOB. 
1. Make sure appsettings.json contains production settings (currently does not have different appsettings for different environments)
2. Publish JobEngine c# console app project (this project) to a folder profile (right click on project > publish > ...)
3. Make sure the publish directory (bin/Release/PublishOutput) has file named run.cmd with the following command: dotnet JobEngineTest.dll
4. After publish, select all files in the PublishOutput and compress into a .zip file
5. In Azure Portal: App Services > tagg-api > WebJobs > Add
-- Name: JobEngine (but doesn't matter what you call it)
-- File Upload: Browse to the zip file created in step 4
-- Type: Triggered
-- CRON Expression: 0 0/2 * * * * (this configures the job to run every 2 minutes)