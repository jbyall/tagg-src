﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using MongoDB.Driver.Core.Configuration;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using Tagg.Domain;
using Tagg.Services;
using Tagg.Services.Communication;
using Sentry;

namespace JobEngineTest
{
    class Program
    {
        static void Main(string[] args)
        {
            // SENTRY CONFIG
            var sentryConfig = new SentryOptions
            {
                Dsn = new Dsn("https://34d0a172db334a8e995094744333404b@sentry.io/1315199"),
                AttachStacktrace = true,
                Release = "Production"
            };
            using (SentrySdk.Init(sentryConfig))
            {
                SentrySdk.AddBreadcrumb($"JobEngine Trigger at {DateTime.UtcNow.ToString("O")}");
                var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false, true)
                .Build();

                // MONGODB CONFIG
                var mongoSettings = MongoClientSettings.FromUrl(new MongoUrl(config.GetConnectionString("MongoConnection")));
                mongoSettings.MaxConnectionIdleTime = TimeSpan.FromSeconds(60);
                Action<Socket> socketConfigurator = s => SocketConfigurator(s);
                mongoSettings.ClusterConfigurator = cb => cb.ConfigureTcp(tcp => tcp.With(socketConfigurator: socketConfigurator));
                
                var client = new MongoClient(mongoSettings);

                // SERVICES CONFIG
                var work = new TaggUnitOfWork(client, config);
                var email = new EmailService(config, work);
                var sms = new SmsService(config);
                var chat = new ChatService(config, work);
                var phone = new PhoneService(config, work);
                var messageService = new TaggMessageService(work, email, sms, chat, phone, config);

                doWork(work, messageService, config, email);

            }
        }

        static void doWork(TaggUnitOfWork work, TaggMessageService messageService, IConfiguration config, EmailService email)
        {
            try
            {
                var jobWorker = new JobWorker(work, messageService);
                var paymentWorker = new PaymentWorker(work, messageService, config);

                // Send queued offers to subs
                SentrySdk.AddBreadcrumb($"Calling ProcessOffers at {DateTime.Now}");
                var offersResult = jobWorker.ProcessOffers();

                // Close completed jobs
                SentrySdk.AddBreadcrumb($"Calling ProcessEndedJobs at {DateTime.Now}");
                var endedJobsResult = jobWorker.ProcessEndedJobs();

                // Queue transactions
                SentrySdk.AddBreadcrumb($"Calling CreateTransactions at {DateTime.Now}");
                var trxResult = paymentWorker.CreateTransactions();

                // Create transactions in payment service
                SentrySdk.AddBreadcrumb($"Calling ProcessCharges at {DateTime.Now}");
                var chargesResult = paymentWorker.ProcessCharges();

                //SentrySdk.CaptureMessage($"Job engine completed at {DateTime.Now}");
                //if (!offersResult || !chargesResult.Succeeded)
                //{
                //    sendErrorNotification(email, offersResult, chargesResult);
                //}
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
            }



            //SentrySdk.CaptureMessage("Job Engine Run");
        }

        private static bool sendErrorNotification(EmailService email, bool offersResult, ProcessChargesResult chargesResult)
        {
            var sendResult = email.Send("john.bond@vitalize.team",
                "jobengine@taggeducation.com",
                $"JobEngine failed at: {DateTime.Now.ToShortTimeString()}",
                $"Job failure report: Offers Succeeded - {offersResult}, Charges Result - {chargesResult.Succeeded}");
            if (sendResult.IsSuccessStatusCode)
            {
                return true;
            }
            return false;
        }

        public static void SocketConfigurator(Socket s)
        {
            var keepAliveValues = new KeepAliveValues()
            {
                OnOff = 1,
                KeepAliveTime = 120 * 1000,   // 120 seconds in milliseconds
                KeepAliveInterval = 10 * 1000 // 10 seconds in milliseconds
            };

            s.IOControl(IOControlCode.KeepAliveValues, keepAliveValues.ToBytes(), null);
        }
    }

    internal struct KeepAliveValues
    {
        public ulong OnOff { get; set; }
        public ulong KeepAliveTime { get; set; }
        public ulong KeepAliveInterval { get; set; }

        public byte[] ToBytes()
        {
            var bytes = new byte[24];
            Array.Copy(BitConverter.GetBytes(OnOff), 0, bytes, 0, 8);
            Array.Copy(BitConverter.GetBytes(KeepAliveTime), 0, bytes, 8, 8);
            Array.Copy(BitConverter.GetBytes(KeepAliveInterval), 0, bytes, 16, 8);
            return bytes;
        }
    }
}
