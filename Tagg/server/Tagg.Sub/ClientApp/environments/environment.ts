﻿export const environment = {
    production: false,
    apiUrl: 'https://localhost:44380/api/',
    loginUrl: 'https://localhost:44380/account/CreateToken/'
    //apiUrl: 'https://tagg-demo-api.azurewebsites.net/api/',
    //loginUrl: 'https://tagg-demo-api.azurewebsites.net/account/CreateToken/'
};
