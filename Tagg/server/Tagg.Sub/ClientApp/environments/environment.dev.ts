﻿export const environment = {
    production: false,
    apiUrl: 'https://localhost:44380/api/',
    loginUrl: 'https://localhost:44380/account/CreateToken/'
};
