﻿export const environment = {
    production: true,
    apiUrl: 'https://tagg-demo-api.azurewebsites.net/api/',
    loginUrl: 'https://tagg-demo-api.azurewebsites.net/account/CreateToken/'
};
