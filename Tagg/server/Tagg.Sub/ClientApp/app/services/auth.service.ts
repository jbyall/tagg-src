﻿// For dependency injection
import { Injectable } from '@angular/core';

// For calls to API/web service
import { HttpClient, HttpErrorResponse, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';

import * as Models from "../shared/models";
import { environment } from '../../environments/environment';

@Injectable()
export class AuthService {
    currentUser: Models.IUser;
    redirectUrl: string;
    tokenData: Models.TokenResponse;
    _loginUri = environment.loginUrl;


    constructor(private http: HttpClient) { }

    isLoggedIn(): boolean {
        return !! this.currentUser;
    }

    public login(creds: Models.LoginModel) {
        console.log("auth.service login");
        return this.http.post<Models.TokenResponse>(this._loginUri, creds)
            .do(resp => {
                this.tokenData = resp;
                this.currentUser = new Models.IUser(1, "jbond@mail.com", true);
            })
            .map(response => {
                return true;
            })
            .catch(this.handleError);
    }

    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error || "Server error");
    }

    logout(): void {
        this.currentUser = null;
    }
}
