﻿// For dependency injection
import { Injectable } from '@angular/core';

// For calls to API/web service
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

// For exception handling
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import * as Models from "../shared/models";
import { environment } from '../../environments/environment';

@Injectable()
export class LocationService {
    
    private _loginUri = environment.loginUrl;

    private _locationCheckUrl = environment.apiUrl + "location/check/";
    private _registerUrl = environment.apiUrl + "register";

    constructor(private http: HttpClient) { }

    checkLocation(postalCode: number): Observable<Models.DistanceResult[]> {
        return this.http.get<Models.DistanceResult[]>(this._locationCheckUrl + postalCode.toString())
    }

    register(model: Models.RegisterModel): Observable<boolean>{
        alert(this._registerUrl);
        return this.http.post<boolean>(this._registerUrl, model);
    }
}