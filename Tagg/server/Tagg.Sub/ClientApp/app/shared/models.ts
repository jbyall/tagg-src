﻿export class IUser {
    constructor(
        public id: number,
        public userName: string,
        public isAdmin: boolean,
    ){}
    
}


// Result from distance calculation
export class DistanceResult {
    constructor(
        public text: string,
        public value: number,
        public serviceLocation: Location,
        public inServiceArea: boolean
    ){}
}

//
export class Location {
    public latitude: number;
    public longitude: number;
}

export class LoginModel {
    constructor(
        public email: string,
        public password: string
    ){}
}

export class TokenResponse {
    constructor(
        public token: string = "",
        public expiration: Date = new Date()
    ) { }
}

// Model for register form
export class RegisterModel {
    constructor(
        public firstName: string = '',
        public lastName: string = '',
        public email: string = '',
        public password: string = '',
        public confirmPassword: string = '',
        public postalCode: string = '',
    ) { }
}