﻿import { AbstractControl, ValidationErrors, Validator, ValidatorFn } from '@angular/forms';
export class PasswordMatch implements Validator{
    validate(c: AbstractControl): ValidationErrors {
        let password = c.get('password').value;
        let confirmPassword = c.get('confirmPassword');
        return password === confirmPassword ? null : { 'passwordMatch': { value: confirmPassword } };
    }
}

