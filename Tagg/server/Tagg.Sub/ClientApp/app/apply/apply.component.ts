import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl, ValidatorFn } from "@angular/forms";
import { LocationService } from "../services/location.service";
import * as Models from "../shared/models";
import { ActivatedRoute, Router } from "@angular/router";
import { PasswordMatch } from "../shared/validators";
import 'rxjs/add/operator/debounceTime';

@Component({
    selector:'app-apply',
    templateUrl: './apply.component.html',
    styleUrls: ['../app.component.css'],
    encapsulation: ViewEncapsulation.None
})
export class ApplyComponent implements OnInit{
    isLinear = false;
    accountForm: FormGroup;
    profileForm: FormGroup;
    preferencesForm: FormGroup;
    contactForm: FormGroup;
    //subjects: SlideModel[] = [];

    constructor(private fb: FormBuilder, private locationService: LocationService, private _route: ActivatedRoute, private _router: Router) {
        
    }

    public subjects: SlideModel[]=[];

    ngOnInit() {
        this.accountForm = this.fb.group({
            street1: ['', Validators.required],
            street2: ['', Validators.required],
            city: ['', Validators.required],
            state: ['', Validators.required],
            zip: ['', Validators.required],
        });

        this.profileForm = this.fb.group({
            education: ['', Validators.required],
            hasTranscripts: ['', Validators.required],
            hasLicense: ['', Validators.required],
            resume: ['', Validators.required],
        });

        this.preferencesForm = this.fb.group({
            minPay: ['', Validators.required],
            maxDistance: ['', Validators.required],
        });

        this.contactForm = this.fb.group({
            phone: ['', Validators.required]
        })

        this.populateSubjects();
    }

    populateSubjects() {
        this.subjects.push(new SlideModel('Pre-K'));
        this.subjects.push(new SlideModel('1st Grade', true));
        this.subjects.push(new SlideModel('2nd Grade'));
        this.subjects.push(new SlideModel('3rd Grade'));
        this.subjects.push(new SlideModel('4th Grade'));
        this.subjects.push(new SlideModel('5th/6th Grade'));
    }
}

export class SlideModel {
    constructor(
        public display: string = '',
        public checked: boolean = false
    ){}
}