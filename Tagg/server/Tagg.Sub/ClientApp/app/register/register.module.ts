﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from "@angular/common";
//import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RegisterFormComponent } from "./register-form.component";
import { RegisterAreaComponent } from "./register-area.component";
import { RegisterComponent } from "./register.component";
import { LocationService } from "../services/location.service";
import { ConfirmEmailComponent } from "./confirm-email.component";


@NgModule({
    imports: [
        CommonModule,
        //HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            {
                path: '', component: RegisterComponent,
                children: [
                    { path: 'area', component: RegisterAreaComponent },
                    { path: 'confirm', component: ConfirmEmailComponent }
                ]
            },
            { path: ':id', component: RegisterFormComponent },
        ])
    ],
    declarations: [
        RegisterFormComponent,
        RegisterAreaComponent,
        RegisterComponent,
        ConfirmEmailComponent
    ],
    providers: [
        LocationService,
    ]
})
export class RegisterModule { }