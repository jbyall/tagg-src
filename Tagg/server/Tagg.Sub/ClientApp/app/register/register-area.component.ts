import { Component, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { LocationService } from "../services/location.service";
import * as Models from "../shared/models";
import { Router } from "@angular/router";

@Component({
    selector: 'register-area',
    templateUrl: './register-area.component.html',
    styleUrls: ['./register-area.component.css']
})
export class RegisterAreaComponent {
    locationResult: Models.DistanceResult[];
    postalCode: number;
    inServiceArea: boolean;
    errorMessage: string;

    constructor(private fb: FormBuilder, private locationService: LocationService, private _router: Router) {
        
    }

    onPostalCheck() {
        var locationResult: boolean;

        this.locationService.checkLocation(this.postalCode)
            .subscribe(resp => {
                console.log(resp);
                this.locationResult = resp;
                for (var i = 0; i < this.locationResult.length; i++) {
                    if (this.locationResult[i].inServiceArea) {
                        this._router.navigate(['/register', this.postalCode]);
                    }
                }
                if (this.inServiceArea) {

                }
            }, error => this.errorMessage = "Could not determine your location.");
    }
}

