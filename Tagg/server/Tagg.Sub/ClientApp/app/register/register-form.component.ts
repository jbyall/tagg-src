import { Component, Input, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl, ValidatorFn } from "@angular/forms";
import { LocationService } from "../services/location.service";
import * as Models from "../shared/models";
import { ActivatedRoute, Router } from "@angular/router";
import { PasswordMatch } from "../shared/validators";
import 'rxjs/add/operator/debounceTime';

@Component({
    selector: 'register-form',
    templateUrl: './register-form.component.html',
    styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {
    model: Models.RegisterModel;
    registerForm: FormGroup;
    locationResult: Models.DistanceResult[];
    emailErrorMessage: string;
    confirmPasswordErrorMessage: string;
    showSuccess: boolean = false;

    constructor(private fb: FormBuilder, private locationService: LocationService, private _route: ActivatedRoute, private _router: Router) {
        this.registerForm = this.fb.group({
            firstName: ['', Validators.required], // [{value: '', disabled:true/false}, Validators or [array of validation rules]] or 
            lastName: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            passwordGroup: this.fb.group({
                password: ['', Validators.required],
                confirmPassword: ['', [Validators.required]]
            }, { validator: passwordMatchValidator })
        });
    }

    validationMessages = {
        email: 'Please enter a valid email address',
        match: 'Passwords do not match'
    }

    get firstName() { return this.registerForm.get('firstName') };
    get lastName() { return this.registerForm.get('lastName') };
    get email() { return this.registerForm.get('email') };
    get password() { return this.registerForm.get('passwordGroup').get('password') };
    get confirmPassword() { return this.registerForm.get('passwordGroup').get('confirmPassword') };
    get passwordGroup() { return this.registerForm.get('passwordGroup') };


    ngOnInit() {
        const emailControl = this.email;
        emailControl.valueChanges.debounceTime(1000).subscribe(value => this.setEmailMessage(emailControl));

        const pwGroupControl = this.passwordGroup;
        pwGroupControl.valueChanges.debounceTime(1000).subscribe(value => this.setConfirmPasswordMessage(pwGroupControl));
    }

    onRegister() {
        if (this.registerForm.dirty && this.registerForm.valid) {
            let p = new Models.RegisterModel();
            Object.assign(p, this.registerForm.value, this.registerForm.value.passwordGroup);
            this.locationService.register(p)
                .subscribe(resp => {
                    this.showSuccess = true;
                });
        }
    }

    setEmailMessage(control: AbstractControl): void {
        this.emailErrorMessage = '';
        if (!control.valid && (control.touched || control.dirty)) {
            this.emailErrorMessage = Object.keys(control.errors).map(key =>
                this.validationMessages[key]).join(' ');
        }
    }

    setConfirmPasswordMessage(control: AbstractControl): void {
        this.confirmPasswordErrorMessage = '';
        const confirmControl = control.get('confirmPassword');
        if ((confirmControl.touched || confirmControl.dirty) && !control.valid) {
            this.confirmPasswordErrorMessage = Object.keys(control.errors).map(key => {
                return this.validationMessages[key]
            }).join(' ');
        }
        else {
            this.confirmPasswordErrorMessage = '';
        }
    }
}


export function passwordMatchValidator(c: AbstractControl): { [key: string]: boolean } | null {
    let passwordControl = c.get('password');
    let confirmPasswordControl = c.get('confirmPassword');

    if (passwordControl.pristine || confirmPasswordControl.pristine) {
        return null;
    }

    if (passwordControl.value === confirmPasswordControl.value) {
        return null;
    }

    return { 'match': true };
}

