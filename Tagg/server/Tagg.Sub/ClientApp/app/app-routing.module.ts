﻿// Modules
import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";
// Services
import { AuthGuard } from "./services/auth.guard";
// Components
import { HomeComponent } from './layout/home.component';
import { LoginComponent } from "./login/login.component";
import { PageNotFoundComponent } from "./layout/page-not-found.component";


@NgModule({
    imports: [
        RouterModule.forRoot([
            { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
            { path: 'login', component: LoginComponent },
            {
                path: 'register',
                loadChildren: './register/register.module#RegisterModule'
            },
            {
                path: 'apply',
                //canActivate: [AuthGuard],
                loadChildren: './apply/apply.module#ApplyModule'
            },
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: '**', component: PageNotFoundComponent }
        ])
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
