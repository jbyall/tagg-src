// Modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from "./app-routing.module";
import { MatStepperModule } from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Services
import { LocationService } from "./services/location.service";
import { AuthService } from "./services/auth.service";
import { AuthGuard } from "./services/auth.guard";

// Components
import { NavMenuComponent } from './layout/navmenu.component';
import { HomeComponent } from './layout/home.component';
import { LoginComponent } from "./login/login.component";
import { TopBarComponent } from './layout/topbar.component';
import { AppComponent } from "./app.component";
import { PageNotFoundComponent } from "./layout/page-not-found.component";



@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        AppRoutingModule,
        BrowserAnimationsModule
    ],
    declarations: [
        AppComponent,
        NavMenuComponent,
        TopBarComponent,
        LoginComponent,
        HomeComponent,
        PageNotFoundComponent,
    ],
    providers: [
        AuthGuard,
        AuthService,
        LocationService,
    ]
})
export class AppModuleShared {
}
