﻿import { Component } from '@angular/core';

@Component({
    template: `
    <h1>404 - Sorry we got lost and couldn't find our way to the page you were looking for.</h1>
    `
})
export class PageNotFoundComponent { }