import { Component } from '@angular/core';
import { LocationService } from "../services/location.service";
import { Router } from "@angular/router";

@Component({
    selector: 'home',
    templateUrl: './home.component.html'
})
export class HomeComponent {

    constructor(private _service: LocationService, private _router: Router) {
        //if (this._service.loginRequired) {
        //    this._router.navigate(["login"], {queryParams: {returnUrl: state.url});
        //}
    }

}
