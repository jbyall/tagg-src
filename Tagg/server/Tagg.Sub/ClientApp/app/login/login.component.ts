import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
import { LocationService } from "../services/location.service";
import * as Models from "../shared/models";
import { AuthService } from "../services/auth.service";


@Component({
    selector: 'login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})
export class LoginComponent {
    loginForm: FormGroup;
    loginModel: Models.LoginModel;
    errorMessage: string;


    constructor(private fb: FormBuilder, private authService: AuthService, private router: Router) {
        this.createForm();
    }

    createForm() {
        this.loginForm = this.fb.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });
    }

    onSubmit() {
        if (this.loginForm.dirty && this.loginForm.valid) {
            // Assign form values to the model
            const val = this.loginForm.value;
            let p = Object.assign({}, this.loginModel, this.loginForm.value)

            // Call login from the service
            this.authService.login(p)
                .subscribe(resp => {
                    if (resp && this.authService.redirectUrl) {
                        // Navigate to the original redirectUrl
                        this.router.navigateByUrl(this.authService.redirectUrl);
                    }
                    else {
                        // If no url, go to home
                        this.router.navigate(['/']);
                    }
                },
                error => {
                    this.errorMessage = "Invalid login";
                });

        }


    }
}
