﻿using System;
using System.Collections.Generic;
using System.Text;
using Tagg.Domain;
using Twilio.Rest.Chat.V2.Service;

namespace ChatEngine
{
    public class JobChannel
    {
        public JobChannel(Job job, ChannelResource channel)
        {
            this.Channels = new List<ChannelResource>();
            this.Channels.Add(channel);
            this.Job = job;
        }

        public JobChannel(ChannelResource channel)
        {
            this.Channels = new List<ChannelResource>();
            this.Channels.Add(channel);
        }

        public void AddChannel(ChannelResource channel)
        {
            this.Channels.Add(channel);
        }

        public Job Job { get; set; }
        public List<ChannelResource> Channels { get; set; }
    }
}
