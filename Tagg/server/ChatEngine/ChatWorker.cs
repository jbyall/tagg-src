﻿using System;
using System.Collections.Generic;
using System.Text;
using Tagg.Domain;
using Tagg.Services;
using Twilio.Base;
using Twilio.Rest.Chat.V2.Service;
using System.Linq;
using Sentry;

namespace ChatEngine
{
    public class ChatWorker
    {
        private TaggUnitOfWork _work;
        private ChatService _chat;

        public ChatWorker(TaggUnitOfWork work, ChatService chat)
        {
            _work = work;
            _chat = chat;
        }

        /// <summary>
        /// Fetches all channels from chat service and filters based on job status and member type
        /// </summary>
        /// <returns></returns>
        public List<JobChannel> GetJobChannels()
        {
            var result = new Dictionary<string, JobChannel>();
            ResourceSet<ChannelResource> channels = _chat.ListChannels();
            foreach (var channel in channels)
            {
                var jobId = getJobIdFromChannelName(channel.UniqueName);
                if (!string.IsNullOrWhiteSpace(jobId))
                {
                    JobChannel log;
                    if (result.TryGetValue(jobId, out log))
                    {
                        log.AddChannel(channel);
                        result[jobId] = log;
                    }
                    else
                    {
                        var job = _work.Jobs.GetById(jobId);
                        var logToAdd = new JobChannel(job, channel);
                        result.Add(jobId, logToAdd);
                    }
                }
            }

            return result.Values.ToList();
        }

        public bool DeleteExpiredOfferChannels(List<JobChannel> jobChannels)
        {
            try
            {
                var accepted = jobChannels
                    .Where(jc => jc.Job != null && jc.Job.Status == JobStatus.Accepted)
                    .ToList();

                foreach (var a in accepted)
                {
                    if (a.Channels.Any(c => c.MembersCount > 1))
                    {
                        foreach (var item in a.Channels.Where(c => c.MembersCount < 2))
                        {
                            try
                            {
                                if (!item.UniqueName.ToLower().Contains(a.Job.SubstituteId.ToLower()))
                                {
                                    _chat.DeleteChannel(item.UniqueName);
                                }
                            }
                            catch (Exception ex) { }
                        }
                    }
                    else
                    {
                        Console.WriteLine($"Accepted job has only single member channels: {a.Job.Id}");
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                Console.WriteLine($"Something happened while deleting expired offers: {ex.Message}");
                return false;
            }
        }

        /// <summary>
        /// Removes channels that had a valid JobId, but no associated job
        /// </summary>
        //public bool DeleteGhostChannels(List<JobChannel> jobChannels)
        //{
        //    try
        //    {
        //        var ghostChannels = jobChannels.Where(jc => jc.Job == null);
        //        foreach (var ghost in ghostChannels)
        //        {
        //            foreach (var channel in ghost.Channels)
        //            {
        //                _chat.DeleteChannel(channel.UniqueName);
        //            }
        //        }

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        SentrySdk.CaptureException(ex);
        //        Console.WriteLine($"Something happened while deleting ghosts: {ex.Message}");
        //        return false;
        //    }
        //}

        public bool DeleteCancelledJobChannels(List<JobChannel> jobChannels)
        {
            // Get all channels:
            // 1. With a job
            // 2. Status == Cancelled
            // 3. Start date was x days in the past
            var cancelledChannels = jobChannels
                .Where(jc => jc.Job != null && jc.Job.Status == JobStatus.Cancelled 
                    && jc.Job.StartDate < DateTime.UtcNow.AddDays(-5))
                .ToList();

            foreach (var cancelled in cancelledChannels)
            {
                foreach (var channel in cancelled.Channels)
                {
                    _chat.DeleteChannel(channel.UniqueName);
                }
            }
            return true;
        }

        public bool DeleteUnusedChannels(List<JobChannel> jobChannels)
        {
            // Get all channels:
            // 1. With only 1 member
            // 3. Start date was x days in the past
            var unusedChannels = jobChannels
                .Where(jc => jc.Job != null && jc.Job.Status == JobStatus.Complete
                    && jc.Job.StartDate < DateTime.UtcNow.AddDays(-20))
                .ToList();

            foreach (var cancelled in unusedChannels)
            {
                foreach (var channel in cancelled.Channels)
                {
                    if (channel.MembersCount < 2 && channel.MessagesCount < 2)
                    {
                        _chat.DeleteChannel(channel.UniqueName);
                    }
                }
            }
            return true;
        }



        private string getJobIdFromChannelName(string channelName)
        {
            var result = string.Empty;
            var channelParts = channelName.Split('-');
            result = channelParts.Length == 2 ? channelParts[0] : result;
            return result;
        }

    }
}
