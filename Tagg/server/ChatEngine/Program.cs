﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using MongoDB.Driver.Core.Configuration;
using System;
using System.Diagnostics;
using System.Linq;
using System.Net.Sockets;
using System.Threading.Tasks;
using Tagg.Domain;
using Tagg.Services;
using Tagg.Services.Communication;
using Sentry;
using Twilio.Base;
using Twilio.Rest.Chat.V2.Service;
using System.Collections.Generic;

namespace ChatEngine
{
    class Program
    {
        static void Main(string[] args)
        {
            // SENTRY CONFIG
            var sentryConfig = new SentryOptions
            {
                Dsn = new Dsn("https://34d0a172db334a8e995094744333404b@sentry.io/1315199"),
                AttachStacktrace = true,
                Release = "Production"
            };
            using (SentrySdk.Init(sentryConfig))
            {
                SentrySdk.AddBreadcrumb($"JobEngine Trigger at {DateTime.UtcNow.ToString("O")}");
                var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", false, true)
                .Build();

                // MONGODB CONFIG
                var mongoSettings = MongoClientSettings.FromUrl(new MongoUrl(config.GetConnectionString("MongoConnection")));
                mongoSettings.MaxConnectionIdleTime = TimeSpan.FromSeconds(60);
                Action<Socket> socketConfigurator = s => SocketConfigurator(s);
                mongoSettings.ClusterConfigurator = cb => cb.ConfigureTcp(tcp => tcp.With(socketConfigurator: socketConfigurator));

                var client = new MongoClient(mongoSettings);

                // SERVICES CONFIG
                var work = new TaggUnitOfWork(client, config);
                var chat = new ChatService(config, work);

                doWork(work, config, chat);

            }
        }


        static void doWork(TaggUnitOfWork work, IConfiguration config, ChatService chat)
        {
            try
            {
                var sw = new Stopwatch();
                sw.Start();
                var worker = new ChatWorker(work, chat);
                var jobChannels = worker.GetJobChannels();
                var cancelledResult = worker.DeleteCancelledJobChannels(jobChannels);
                var expiredResult = worker.DeleteExpiredOfferChannels(jobChannels);
                var unusedResult = worker.DeleteUnusedChannels(jobChannels);
                sw.Stop();
                var time = sw.ElapsedMilliseconds;
                if (time > 60000 * 8)
                {
                    SentrySdk.CaptureMessage($"Chat Engine took long: {time}ms at {DateTime.UtcNow}");
                }
                //var ghostJobsResult = worker.DeleteGhostChannels(jobChannels);
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
            }
        }

        public static void SocketConfigurator(Socket s)
        {
            var keepAliveValues = new KeepAliveValues()
            {
                OnOff = 1,
                KeepAliveTime = 120 * 1000,   // 120 seconds in milliseconds
                KeepAliveInterval = 10 * 1000 // 10 seconds in milliseconds
            };

            s.IOControl(IOControlCode.KeepAliveValues, keepAliveValues.ToBytes(), null);
        }
    }

    internal struct KeepAliveValues
    {
        public ulong OnOff { get; set; }
        public ulong KeepAliveTime { get; set; }
        public ulong KeepAliveInterval { get; set; }

        public byte[] ToBytes()
        {
            var bytes = new byte[24];
            Array.Copy(BitConverter.GetBytes(OnOff), 0, bytes, 0, 8);
            Array.Copy(BitConverter.GetBytes(KeepAliveTime), 0, bytes, 8, 8);
            Array.Copy(BitConverter.GetBytes(KeepAliveInterval), 0, bytes, 16, 8);
            return bytes;
        }
    }
}
