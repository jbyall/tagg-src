﻿using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Tagg.Domain
{
    public class LookupRepo : Repository<Lookups>
    {
        public LookupRepo(IMongoDatabase db)
            :base(db)
        {
            this.setCollection(Collections.Lookups);
        }

        public List<Lookup> GetStates()
        {
            return this
                .Collection
                .AsQueryable()
                .First()
                .States;
        }

        public List<Lookup> GetEducationLevels()
        {
            return this.Collection
                .AsQueryable()
                .FirstOrDefault()
                .EducationLevels;
        }

        public List<Lookup> GetSchoolTypes()
        {
            return this.Collection
                .AsQueryable()
                .FirstOrDefault()
                .SchoolTypes;
        }
        public List<Lookup> GetSubjects()
        {
            return this.Collection
                .AsQueryable()
                .FirstOrDefault()
                .Subjects;
        }
    }
}
