﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Linq;
using MongoDB.Driver.GeoJsonObjectModel;
using System.Linq.Expressions;
using Microsoft.Extensions.Configuration;
using Tagg.Domain.ApiModels;
using Tagg.Domain.Helpers;

namespace Tagg.Domain
{
    public class TeacherRepo : Repository<Teacher>
    {
        private IMongoDatabase _db;
        private IMongoCollection<District> _districtsCollection;
        private IMongoCollection<School> _schoolsCollection;

        public TeacherRepo(IMongoDatabase db)
            : base(db)
        {
            this.setCollection(Collections.Teachers);
            _db = db;
            _districtsCollection = db.GetCollection<District>(Collections.Districts);
            _schoolsCollection = db.GetCollection<School>(Collections.Schools);

            this.Schools = _schoolsCollection.AsQueryable();
        }

        public IQueryable<School> Schools { get; set; }

        private IQueryable<Teacher> getBySchoolIdQueryable(string schoolId)
        {
            return this.Collection.AsQueryable()
                .Where(t => t.SchoolId == schoolId);
        }

        public List<Teacher> GetAllBySchoolId(string schoolId)
        {
            return this.getBySchoolIdQueryable(schoolId)
                .ToList();
        }

        public int CountBySchool(string schoolId)
        {
            return this.getBySchoolIdQueryable(schoolId)
                .Count();
        }


        public EntityResult<Teacher> Update(Teacher teacher, bool updateFavorites = false)
        {

            var existingTeacher = this.GetById(teacher.Id);

            if (!updateFavorites)
            {
                teacher.Favorites = existingTeacher?.Favorites;
            }

            return base.Update(teacher);
        }

        public EntityResult UpdatePhotoId(string teacherId, string photoId)
        {
            var result = new EntityResult();
            var teacher = this.Collection.AsQueryable().FirstOrDefault(t => t.Id == teacherId);
            if (teacher != null)
            {
                teacher.Picture = photoId;
                var updateResult = this.Update(teacher);
                if (updateResult.Succeeded)
                {
                    return result;
                }
            }
            result.AddError($"Failed to update teacher picture for {teacherId}");
            return result;
        }

        public EntityResult UpdateEmail(string teacherId, string newEmail)
        {
            var result = new EntityResult();
            var teacher = this.Collection.AsQueryable().FirstOrDefault(t => t.Id == teacherId);
            if (teacher != null)
            {
                teacher.Email = newEmail;
                var updateResult = this.Update(teacher);
                if (updateResult.Succeeded)
                {
                    return result;
                }
            }
            result.AddError($"Failed to update teacher picture for {teacherId}");
            return result;
        }

        public TeacherSchoolResult GetWithSchool(string teacherId)
        {
            var result = new TeacherSchoolResult();

            var query = from t in this.Collection.AsQueryable().Where(t => t.Id == teacherId)
                        join s in _schoolsCollection.AsQueryable() on t.SchoolId equals s.Id
                        select new { Teacher = t, School = s };

            var queryResult = query.FirstOrDefault();
            if (queryResult != null)
            {
                result = new TeacherSchoolResult { School = queryResult.School, Teacher = queryResult.Teacher };
            }
            return result;
        }

    }
}
