﻿using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Tagg.Domain.ApiModels;

namespace Tagg.Domain
{
    public class TransactionRepo : Repository<Transaction>
    {
        private IMongoCollection<Job> _jobs;
        private IMongoCollection<Substitute> _subs;
        private IMongoCollection<School> _schools;
        private IMongoCollection<Teacher> _teachers;

        public TransactionRepo(IMongoDatabase db)
            :base(db)
        {
            this.setCollection(Collections.Transactions);
            _jobs = db.GetCollection<Job>(Collections.Jobs);
            _subs = db.GetCollection<Substitute>(Collections.Subs);
            _schools = db.GetCollection<School>(Collections.Schools);
            _teachers = db.GetCollection<Teacher>(Collections.Teachers);
		}

        public EntityResult<Transaction> UpdateStatus(string id, TransactionStatus status, string externalStatus = "")
        {
            var update = Builders<Transaction>.Update
                .Set(t => t.Status, status)
                .Set(t => t.ExternalStatus, externalStatus);

            var result = this.Collection.FindOneAndUpdate(t => t.Id == id, update);
            return new EntityResult<Transaction>(result);
        }

        public List<TransactionDto> GetByCustomerId(string customerId, int take)
        {
            var queryResult = this.QueryDetailList(t => t.CustomerId == customerId, maxResults: take);
            var result = queryResult == null ? new List<TransactionDto>() : queryResult.Transactions;
            return result;
        }

		public TransactionDtoContainer GetDetailList(int maxResults = 100, int skip = 0, string sort = null, string direction = null)
		{
			try
			{
				var allTransactionsQuery = from trx in this.Collection.AsQueryable()
							 join jb in _jobs.AsQueryable() on trx.JobId equals jb.Id
							 join sb in _subs.AsQueryable() on jb.SubstituteId equals sb.Id
                             join t in _teachers.AsQueryable() on jb.TeacherId equals t.Id
							 select new TransactionDto
							 {
								 Id = trx.Id,
								 JobId = trx.JobId,
								 JobNumber = trx.JobId,
								 CustomerId = trx.CustomerId,
								 JobDate = jb != null ? jb.StartDate : DateTime.MinValue,
								 PayRateUsd = trx.PayRateUsd,
								 ProcessOn = trx.ProcessOn,
								 Status = trx.Status,
								 SubName = sb.FirstName + " " + sb.LastName,
								 TotalAmountUsd = trx.TotalAmountUsd,
								 TeacherId = jb.TeacherId,
                                 TeacherName = t.FirstName + " " + t.LastName
							 };

				int totalCount = allTransactionsQuery.Count();

				var sortedTransactions = this.applyTransactionSort(allTransactionsQuery, sort, direction);

				List<TransactionDto> transactionDetails = sortedTransactions
					.Skip(skip)
					.Take(maxResults)
					.ToList();

				return new TransactionDtoContainer(transactionDetails, totalCount);
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        public TransactionDtoContainer QueryDetailList(Expression<Func<Transaction, bool>> predicate, int maxResults = 100, int skip = 0, string sort = null, string direction = null)
        {
            try
            {
                var allTransactionsQuery = from trx in this.Collection.AsQueryable().Where(predicate)
                                           join jb in _jobs.AsQueryable() on trx.JobId equals jb.Id
                                           join sb in _subs.AsQueryable() on jb.SubstituteId equals sb.Id
                                           join t in _teachers.AsQueryable() on jb.TeacherId equals t.Id
                                           select new TransactionDto
                                           {
                                               Id = trx.Id,
                                               JobId = trx.JobId,
                                               JobNumber = trx.JobId,
                                               CustomerId = trx.CustomerId,
                                               JobDate = jb != null ? jb.StartDate : DateTime.MinValue,
                                               PayRateUsd = trx.PayRateUsd,
                                               ProcessOn = trx.ProcessOn,
                                               Status = trx.Status,
                                               SubName = sb.FirstName + " " + sb.LastName,
                                               TotalAmountUsd = trx.TotalAmountUsd,
                                               TeacherId = jb.TeacherId,
                                               TeacherName = t.FirstName + " " + t.LastName
                                           };

                int totalCount = allTransactionsQuery.Count();

                var sortedTransactions = this.applyTransactionSort(allTransactionsQuery, sort, direction);

                List<TransactionDto> transactionDetails = sortedTransactions
                    .Skip(skip)
                    .Take(maxResults)
                    .ToList();

                return new TransactionDtoContainer(transactionDetails, totalCount);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



        public IQueryable<TransactionDto> applyTransactionSort(IQueryable<TransactionDto> query, string sort, string direction)
		{
			bool ascend = direction == null ? false : direction.ToLower().Equals("asc");

			switch (sort)
			{
				case null:
				case "":
					return query;
				case "processOn":
					if (ascend) return query.OrderBy(x => x.ProcessOn);
					else return query.OrderByDescending(x => x.ProcessOn);
				case "jobDate":
					if (ascend) return query.OrderBy(x => x.JobDate);
					else return query.OrderByDescending(x => x.JobDate);
				case "payRateUsd":
					if (ascend) return query.OrderBy(x => x.PayRateUsd);
					else return query.OrderByDescending(x => x.PayRateUsd);
				//case "teacherName":
				//	if (ascend) return query.OrderBy(x => x.TeacherName);
				//	else return query.OrderByDescending(x => x.TeacherName);
				case "totalAmountUsd":
					if (ascend) return query.OrderBy(x => x.TotalAmountUsd);
					else return query.OrderByDescending(x => x.TotalAmountUsd);
				case "subName":
					if (ascend) return query.OrderBy(x => x.SubName);
					else return query.OrderByDescending(x => x.SubName);
				case "status":
					if (ascend) return query.OrderBy(x => x.Status);
					else return query.OrderByDescending(x => x.Status);
			}

			return query;
		}
	}

}
