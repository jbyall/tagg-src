﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class ErrorRepo : Repository<ErrorLog>
    {
        public ErrorRepo(IMongoDatabase db)
            : base(db)
        {
            this.setCollection(Collections.Errors);
        }
    }
}
