﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GeoJsonObjectModel;
using MongoDB.Driver.GridFS;
using MongoDB.Driver.Linq;
using Tagg.Domain.ApiModels;

namespace Tagg.Domain
{
    public class SubstituteRepo : Repository<Substitute>
    {
        private GridFSBucket _bucket;
        private IMongoCollection<Job> _jobs;

        public SubstituteRepo(IMongoDatabase db)
            : base(db)
        {
            this.setCollection(Collections.Subs);
            _bucket = new GridFSBucket(Db);
            _jobs = db.GetCollection<Job>(Collections.Jobs);
        }

        public Substitute GetByEmail(string email)
        {
            return this.Collection
                .AsQueryable()
                .FirstOrDefault(s => s.Email.ToLower() == email.ToLower());
        }

        public Substitute GetByIdWithRating(string subId)
        {
            try
            {
                Substitute result = null;
                var ratingQuery = from s in this.Collection.AsQueryable().Where(s => s.Id == subId)
                                  join j in _jobs.AsQueryable() on s.Id equals j.SubstituteId into jobsJoin
                                  select new { Sub = s, Rating = jobsJoin };

                var singleResult = ratingQuery.FirstOrDefault();
                if (singleResult != null)
                {
                    result = singleResult.Sub;
                    result.RatingCount = singleResult.Rating.Count(j => j.Status == JobStatus.Complete && j.Rating > 0);
                    result.Rating = result.RatingCount >= 3 ? singleResult.Rating.Where(j => j.Status == JobStatus.Complete && j.Rating > 0).Average(r => r.Rating) : 3;
                }
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        new public EntityResult<Substitute> Update(Substitute sub)
        {
            var existingSub = this.GetById(sub.Id);
            if (existingSub != null)
            {
                sub.Availability = existingSub.Availability;
                sub.Exclusions = existingSub.Exclusions;
                return this.UpdateBase(sub);
            }
            return new EntityResult<Substitute>("Not Found");
        }

        public EntityResult<Substitute> UpdateAvailability(string subId, DateTime availDate)
        {
            var sub = this.GetById(subId);
            if (sub != null)
            {
                var existingAvail = sub.Availability.FirstOrDefault(a => a.StartDate.Date == availDate.Date);
                if (existingAvail != null)
                {
                    sub.Availability.Remove(existingAvail);
                }
                else
                {
                    sub.Availability.Add(new UserAvailability { StartDate = availDate, EndDate = availDate });
                }

                return this.UpdateBase(sub);
            }
            return new EntityResult<Substitute>("Failed to update availability");
        }

        public IQueryable<Substitute> GetByStatus(string status)
        {
            TaggStatus statusEnum;
            if (Enum.TryParse<TaggStatus>(status, out statusEnum))
            {
                return this.Collection.AsQueryable().Where(s => s.Status == statusEnum);
            }

            // If we can't parse the enum, return empty list
            return new List<Substitute>().AsQueryable();
        }

        public IQueryable<Substitute> GetByStatus(TaggStatus status)
        {
            return this.Collection.AsQueryable().Where(s => s.Status == status);
        }

        public EntityResult<Substitute> ExcludeFromSchool(string subId, string schoolId)
        {
            if (!string.IsNullOrWhiteSpace(subId) && !string.IsNullOrWhiteSpace(schoolId))
            {
                var addExclusionUpdate = Builders<Substitute>.Update.AddToSet(s => s.Exclusions, schoolId);
                var result = this.Collection.FindOneAndUpdate(s => s.Id == subId, addExclusionUpdate);
                return new EntityResult<Substitute>(result);
            }
            return new EntityResult<Substitute>("Sub id and school id required");
        }

        public EntityResult<Substitute> RemoveExclusion(string subId, string schoolId)
        {
            if (!string.IsNullOrWhiteSpace(subId) && !string.IsNullOrWhiteSpace(schoolId))
            {
                var addExclusionUpdate = Builders<Substitute>.Update.Pull(s => s.Exclusions, schoolId);
                var result = this.Collection.FindOneAndUpdate(s => s.Id == subId, addExclusionUpdate);
                return new EntityResult<Substitute>(result);
            }
            return new EntityResult<Substitute>("Sub id and school id required");
        }

        /// <summary>
        /// Returns subs that
        /// 1. Are available 
        /// 2. Are within maxDistance
        /// 3. Are not excluded for school
        /// 4. Are enabled
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="maxDistance"></param>
        /// <returns></returns>
        public List<Substitute> GetForSchool(string schoolId, GeoJsonPoint<GeoJson2DGeographicCoordinates> schoolLocation, int maxDistance = 100)
        {
            var builder = Builders<Substitute>.Filter;
            var filter = builder.GeoWithinCenterSphere(s => s.Location,
                    schoolLocation.Coordinates.Longitude,
                    schoolLocation.Coordinates.Latitude,
                    maxDistance / 3963.2);

            filter &= builder.Where(s => s.Enabled == true);
            filter &= builder.Where(s => s.Status == TaggStatus.Active);

            return this.Collection.Find(filter).ToList();
        }

        /// <summary>
        /// Returns count of subs that
        /// 1. Are available 
        /// 2. Are within maxDistance
        /// 3. Are not excluded for school
        /// 4. Are enabled
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="maxDistance"></param>
        /// <returns></returns>
        public long GetCountForSchool(string schoolId, GeoJsonPoint<GeoJson2DGeographicCoordinates> schoolLocation, int maxDistance = 100)
        {
            if (schoolLocation == null)
            {
                return 0;
            }
            var builder = Builders<Substitute>.Filter;
            var filter = builder.GeoWithinCenterSphere(j => j.Location,
                    schoolLocation.Coordinates.Longitude,
                    schoolLocation.Coordinates.Latitude,
                    maxDistance / 3963.2);

            filter &= builder.Where(s => s.Enabled == true);
            filter &= builder.Where(s => s.Status == TaggStatus.Active);
            filter &= builder.Where(s => !s.Exclusions.Any(x => x == schoolId));

            return this.Collection.Find(filter).Count();
        }

        public List<Substitute> Search(SubSearchCriteria search, int maxDistance = 100, string schoolId = "")
        {
            try
            {
                var builder = Builders<Substitute>.Filter;
                //var filter = Builders<Substitute>.Filter.Empty;

                var filter = builder.GeoWithinCenterSphere(s => s.Location,
                    search.Location.Coordinates.Longitude,
                    search.Location.Coordinates.Latitude,
                    maxDistance / 3963.2);

                if (search.EnabledOnly)
                {
                    // Subs that are enabled
                    filter &= builder.Where(s => s.Enabled == search.EnabledOnly);
                }
                if (search.ActiveOnly)
                {
                    // Subs with Active status
                    filter &= builder.Where(s => s.Status == TaggStatus.Active);
                }
                if (search.Subjects.Count > 0)
                {
                    // If there are multiple subjects for some reason, find subs that have all of them
                    // see https://docs.mongodb.com/manual/tutorial/query-arrays/
                    filter &= builder.All(s => s.Subjects, search.Subjects);
                }
                if (!string.IsNullOrWhiteSpace(search.SchoolId))
                {
                    // Subs that are not excluded from specified school
                    filter &= builder.Where(s => !s.Exclusions.Any(x => x == search.SchoolId));
                }
                if (!string.IsNullOrWhiteSpace(search.TeacherId))
                {
                    // Subs that are not excluded from specified school
                    filter &= builder.Where(s => !s.Exclusions.Any(x => x == search.TeacherId));
                }
                if (search.DateAvaliable.HasValue)
                {
                    // Subs that aren't marked "unavailable" for the specified "DateAvailable"
                    // TODO : Determine if we should use local time or utc
                    var testDate = search.DateAvaliable.Value.ToLocalTime().Date;
                    filter &= builder.Where(s => !s.Availability.Any(a => a.StartDate == testDate));
                }

                filter &= builder.Where(s => s.MinPayAmount <= search.PayRate);

                var baseQuery = this.Collection.Find(filter).ToList();

                // Distance fix
                var resultsAdjustedByDistance = new List<Substitute>();
                foreach (Substitute sub in baseQuery)
                {
                    var builder2 = Builders<Substitute>.Filter;

                    var filter2 = builder.GeoWithinCenterSphere(s => s.Location,
                        search.Location.Coordinates.Longitude,
                        search.Location.Coordinates.Latitude,
                        sub.MaxDistance / 3963.2);

                    filter2 &= builder.Where(s => s.Id == sub.Id);

                    var queryresult = this.Collection.Find(filter2).ToList();

                    if (queryresult.Count > 0) resultsAdjustedByDistance.Add(queryresult.FirstOrDefault());
                }

                var ratingQuery = from s in resultsAdjustedByDistance.AsQueryable()
                                  join j in _jobs.AsQueryable() on s.Id equals j.SubstituteId into jobsJoin
                                  select new { Sub = s, Rating = jobsJoin };

                var result = new List<Substitute>();
                foreach (var rating in ratingQuery)
                {

                    var subToAdd = rating.Sub;
                    subToAdd.RatingCount = rating.Rating.Count(r => r.Status == JobStatus.Complete && r.Rating > 0);
                    subToAdd.Rating = subToAdd.RatingCount >= 3 ? rating.Rating.Where(r => r.Status == JobStatus.Complete && r.Rating > 0).Average(r => r.Rating) : 3;
                    subToAdd.SubbedAtSchool = rating.Rating.Any(x => x.SchoolId == schoolId && x.Status == JobStatus.Complete);
                    result.Add(subToAdd);
                }

                

                return result;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public Substitute GetDetails(string subId, string schoolId = null)
        {
            var ratingJoinQuery = from s in this.Collection.AsQueryable().Where(s => s.Id == subId)
                                  join j in _jobs.AsQueryable() on s.Id equals j.SubstituteId into jobsJoin
                                  select new { Sub = s, Rating = jobsJoin };

            var rating = ratingJoinQuery.FirstOrDefault();
            Substitute result = rating.Sub;
            result = rating.Sub;
            result.RatingCount = rating.Rating.Count(j => j.Status == JobStatus.Complete && j.Rating > 0);
            result.Rating = result.RatingCount >= 3 ? rating.Rating.Where(j => j.Status == JobStatus.Complete && j.Rating > 0).Average(r => r.Rating) : 3;
            result.SubbedAtSchool = string.IsNullOrWhiteSpace(schoolId) ? false : rating.Rating.Any(x => x.SchoolId == schoolId && x.Status == JobStatus.Complete);

            return result;

        }

        public EntityResult Deactivate(string userId)
        {
            var update = Builders<Substitute>.Update
                .Set(s => s.Deactivated, true)
                .Set(s => s.Enabled, false);

            var result = this.Collection.FindOneAndUpdate(s => s.Id == userId, update);
            if (result != null)
            {
                return new EntityResult();
            }
            return new EntityResult("Not found");
        }

        public EntityResult Disable(string subId)
        {
            try
            {
                var update = Builders<Substitute>.Update
                        .Set(s => s.Enabled, false)
                        .Set(s => s.Status, TaggStatus.Disabled);

                var updateResult = this.Collection.FindOneAndUpdate(s => s.Id == subId, update);
                return new EntityResult();
            }
            catch (Exception ex)
            {
                return new EntityResult(ex.Message);
            }
        }

        public EntityResult Reactivate(string userId)
        {
            var update = Builders<Substitute>.Update
                .Set(s => s.Deactivated, false);

            var result = this.Collection.FindOneAndUpdate(s => s.Id == userId, update);
            if (result != null)
            {
                return new EntityResult();
            }
            return new EntityResult("Not found");
        }

        // TODO: Change this to use subId
        public string GetPaymentId(string subId)
        {
            var sub = this.Collection.AsQueryable().FirstOrDefault(s => s.Id == subId);

            return sub == null ? "" : sub.PaymentId;
        }

        public EntityResult UpdatePhotoId(string substituteId, string photoId)
        {
            var update = Builders<Substitute>.Update
                .Set(s => s.Picture, photoId);
            var result = this.Collection.FindOneAndUpdate(s => s.Id == substituteId, update);
            if (result != null)
            {
                return new EntityResult();
            }
            return new EntityResult($"failed to update subsitute picture for {substituteId}");
        }

        public EntityResult UpdateEmail(string substituteId, string newEmail)
        {
            var update = Builders<Substitute>.Update
                .Set(s => s.Email, newEmail);
            var result = this.Collection.FindOneAndUpdate(s => s.Id == substituteId, update);
            if (result != null)
            {
                return new EntityResult();
            }
            return new EntityResult($"failed to update subsitute email for {substituteId}");
        }

        public string UpdateResume(string substituteId, Stream resumeStream, string fileName, string contentType)
        {
            var sub = this.GetById(substituteId);
            var existingResumeId = sub?.ResumeFile;

            if (sub != null)
            {
                if (!string.IsNullOrWhiteSpace(existingResumeId))
                {
                    this.DeleteFile(existingResumeId);
                }
                var metaData = new Dictionary<string, string>
                {
                    { "contentType", contentType },
                    { "userId", substituteId },
                };

                var options = new GridFSUploadOptions { Metadata = new BsonDocument(metaData) };
                var resumeFileId = _bucket.UploadFromStream(fileName, resumeStream, options);
                this.updateResumeId(sub.Id, resumeFileId.ToString());

                return resumeFileId.ToString();
            }

            return string.Empty;
        }

        public FileResultDto GetResume(string resumeId)
        {
            var result = new FileResultDto();
            try
            {
                var stream = _bucket.OpenDownloadStream(new ObjectId(resumeId));
                result.ContentType = stream.FileInfo.Metadata["contentType"].AsString;
                result.Stream = stream;
                return result;
            }
            catch (Exception ex)
            {
                result.Succeeded = false;
                return result;
            }
        }

        public void DeleteFile(string fileId)
        {
            try
            {
                _bucket.Delete(new ObjectId(fileId));
            }
            // TODO : add log entries for things like this
            // this should only happen if file is not found
            catch (Exception ex)
            {
                return;
            }
        }

        private bool updateResumeId(string subId, string resumeFileId)
        {
            var update = Builders<Substitute>.Update.Set(s => s.ResumeFile, resumeFileId);
            var result = this.Collection.FindOneAndUpdate(s => s.Id == subId, update);
            return result != null;
        }

    }

    public class SubSearchCriteria
    {
        public DateTime? DateAvaliable { get; set; }
        public string SchoolId { get; set; }
        public string TeacherId { get; set; }
        public List<Tagg.Domain.GradeSubject> Subjects { get; set; }
        public double PayRate { get; set; }
        public bool EnabledOnly { get; set; }
        public bool ActiveOnly { get; set; }
        public GeoJsonPoint<GeoJson2DGeographicCoordinates> Location { get; set; }

        public SubSearchCriteria()
        {
            Subjects = new List<GradeSubject>();
        }
    }
}
