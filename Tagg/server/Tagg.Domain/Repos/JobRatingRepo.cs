﻿using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tagg.Domain
{
    public class JobRatingRepo : Repository<JobRating>
    {
        private IMongoQueryable<School> _schools;
        private IMongoQueryable<Substitute> _subs;
        private IMongoQueryable<Job> _jobs;
        private IQueryable<Teacher> _teachers;

        public JobRatingRepo(IMongoDatabase db)
            : base(db)
        {
            this.setCollection(Collections.JobRatings);
            _schools = Db.GetCollection<School>(Collections.Schools).AsQueryable();
            _subs = Db.GetCollection<Substitute>(Collections.Subs).AsQueryable();
            _jobs = Db.GetCollection<Job>(Collections.Jobs).AsQueryable();
        }


        public IQueryable<JobRating> GetIncompleteBySubId(string subId)
        {
            return this.Collection.AsQueryable()
                .Where(r => r.SubstituteId == subId && !r.SubCompleted);
        }

        public IQueryable<JobRating> GetIncompleteByTeacherId(string teacherId)
        {
            return this.Collection.AsQueryable()
                .Where(r => r.SchoolReviewerId == teacherId && !r.SchoolCompleted);
        }

        public EntityResult<JobRating> TeacherSubmission(JobRating submission)
        {
            var filter = Builders<JobRating>.Update
                .Set(r => r.SubScores, submission.SubScores)
                .Set(r => r.SchoolReviewerId, submission.SchoolReviewerId)
                .Set(r => r.Modified, DateTime.UtcNow)
                .Set(r => r.SchoolCompleted, true);

            var result = this.Collection.FindOneAndUpdate(r => r.JobId == submission.JobId, filter);
            return new EntityResult<JobRating>(result);
        }

        public EntityResult<JobRating> SubSubmission(JobRating submission)
        {
            var filter = Builders<JobRating>.Update
                .Set(r => r.SchoolScores, submission.SchoolScores)
                .Set(r => r.Modified, DateTime.UtcNow)
                .Set(r => r.SubCompleted, true)
                .Set(r => r.SubComments, submission.SubComments);

            var result = this.Collection.FindOneAndUpdate(r => r.JobId == submission.JobId, filter);
            return new EntityResult<JobRating>(result);
        }

        public EntityResult CreateEmptyRating(Job job)
        {
            try
            {
                var newRating = new JobRating
                {
                    JobId = job.Id,
                    SchoolCompleted = false,
                    SubCompleted = false,
                    SubstituteId = job.SubstituteId,
                    TeacherId = job.TeacherId,
                    SchoolReviewerId = job.CreatedBy
                };

                this.Add(newRating);

                return new EntityResult();
            }
            catch (Exception ex)
            {
                return new EntityResult("Failed to create empty job rating.");
            }
        }
    }
}
