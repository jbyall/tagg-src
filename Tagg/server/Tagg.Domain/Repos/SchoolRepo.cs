﻿//using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using MongoDB.Bson;
using MongoDB.Driver;
using Tagg.Domain.ApiModels;
using Tagg.Domain.Helpers;

namespace Tagg.Domain
{
    public class SchoolRepo : Repository<School>
    {
        private IMongoCollection<District> _districts;
        private IMongoCollection<Blackout> _blackouts;
        private IMongoCollection<Teacher> _teachers;

        public SchoolRepo(IMongoDatabase db)
            : base(db)
        {
            this.setCollection(Collections.Schools);
            _districts = this.Db.GetCollection<District>(Collections.Districts);
            _blackouts = this.Db.GetCollection<Blackout>(Collections.Blackouts);
            _teachers = this.Db.GetCollection<Teacher>(Collections.Teachers);
        }

        public List<SchoolTeachersResult> GetByAdminId(string adminId)
        {
            return this.QueryWithTeachers(s => s.Admins.Any(x => x.Id == adminId));
        }

        public IQueryable<School> GetByAdminIdQueryable(string adminId)
        {
            return this.Collection
                .AsQueryable()
                .Where(s => s.Admins.Any(x => x.Id == adminId));
        }

        public List<School> GetByDistrictId(string districtId)
        {
            return this.Collection
                .Find(Builders<School>.Filter
                .Where(s => s.DistrictId == districtId))
                .ToList();
        }

        public List<District> GetAllDistricts()
        {
            return _districts.AsQueryable().ToList();
        }

        public List<UserBase> GetSchoolAdmins(string schoolId)
        {
            return this.Collection.AsQueryable()
                .Where(s => s.Id == schoolId)
                .FirstOrDefault()
                .Admins;
        }

        public District GetDistrict(string districtId)
        {
            return _districts.AsQueryable().FirstOrDefault(d => d.Id == districtId);
        }

        public string GetDistrictIdBySchoolId(string schoolId)
        {
            return this.Collection.AsQueryable()
                .Where(s => s.Id == schoolId)
                .Select(s => s.DistrictId)
                .FirstOrDefault();
        }

        public EntityResult<School> Add(School school, string districtId)
        {
            if (!string.IsNullOrWhiteSpace(districtId) && _districts.AsQueryable().Any(d => d.Id == districtId))
            {
                school.DistrictId = districtId;
                this.Collection.InsertOne(school);
                return new EntityResult<School>(school);
            }

            return new EntityResult<School>("Invalid district id");
        }

        public EntityResult<District> AddDistrict(District district)
        {
            try
            {
                if (DistrictExists(district))
                {
                    return new EntityResult<District>(_districts.AsQueryable().FirstOrDefault(d => d.Id == district.Id || d.Name.ToLower() == district.Name.ToLower()));
                }
                _districts.InsertOne(district);
                return new EntityResult<District>(district);
            }
            catch (Exception ex)
            {
                return new EntityResult<District>(ex.Message);
            }
        }

        public EntityResult AddSchoolAdmin(string schoolId, UserBase newAdmin)
        {
            var result = new EntityResult();
            var addDefinition = Builders<School>.Update.AddToSet(s => s.Admins, newAdmin);
            var addResult = this.Collection.FindOneAndUpdate(s => s.Id == schoolId, addDefinition);
            return addResult == null ? new EntityResult("Failed to update") : result;
        }

        public EntityResult AddDistrictAdmin(string districtId, UserBase newAdmin)
        {
            bool somethingFailed = false;
            var district = this.GetByDistrictId(districtId);
            foreach (var school in district)
            {
                var addResult = this.AddSchoolAdmin(school.Id, newAdmin);
                if (!addResult.Succeeded)
                {
                    somethingFailed = true;
                }
            }

            return somethingFailed ? new EntityResult("Something failed") : new EntityResult();

        }

        public EntityResult DisableSchoolAdmin(string schoolId, string adminId)
        {
            var result = new EntityResult();
            var filter = Builders<School>.Filter
                .Where(s => s.Id == schoolId
                && s.Admins.Any(a => a.Id == adminId));

            var update = Builders<School>.Update.Set(s => s.Admins[-1].Enabled, false);
            var updateResult = this.Collection.UpdateOne(filter, update);

            if (updateResult.IsAcknowledged)
            {
                return result;
            }
            return new EntityResult("Failed to disabled admin");
        }

        public EntityResult EnableSchoolAdmin(string schoolId, string adminId)
        {
            var result = new EntityResult();
            var filter = Builders<School>.Filter
                .Where(s => s.Id == schoolId
                && s.Admins.Any(a => a.Id == adminId));

            var update = Builders<School>.Update.Set(s => s.Admins[-1].Enabled, true);
            var updateResult = this.Collection.UpdateOne(filter, update);

            if (updateResult.IsAcknowledged)
            {
                return result;
            }
            return new EntityResult("Failed to disabled admin");
        }

        public bool DistrictExists(District district)
        {
            return _districts.AsQueryable().Any(d => d.Id == district.Id || d.Name.ToLower() == district.Name.ToLower());
        }

        public EntityResult AddCustomerId(string schoolId, string customerId)
        {
            var updateBuilder = Builders<School>.Update
                .Set(s => s.CustomerId, customerId);
            var partialUpdateResult = this.Collection.FindOneAndUpdate(s => s.Id == schoolId, updateBuilder);
            if (partialUpdateResult != null)
            {
                return new EntityResult();
            }
            return new EntityResult($"Could not add customer id to school: {schoolId}");
        }

        new public EntityResult<School> Update(School update)
        {
            var updateBuilder = Builders<School>.Update
                .Set(s => s.Name, update.Name)
                .Set(s => s.SchoolType, update.SchoolType)
                .Set(s => s.Url, update.Url)
                .Set(s => s.ContactEmail, update.ContactEmail)
                .Set(s => s.ContactPhone, update.ContactPhone)
                .Set(s => s.Address, update.Address)
                .Set(s => s.Location, update.Location)
                .Set(s => s.PayRate, update.PayRate)
                .Set(s => s.StartTime, update.StartTime)
                .Set(s => s.EndTime, update.EndTime)
                .Set(s => s.Summary, update.Summary)
                .Set(s => s.Grades, update.Grades);

            var partialUpdateResult = this.Collection.FindOneAndUpdate(s => s.Id == update.Id, updateBuilder);
            return new EntityResult<School>(partialUpdateResult);

            //var existingSchool = this.Collection.AsQueryable()
            //    .FirstOrDefault(s => s.Id == update.Id);

            //if (existingSchool != null)
            //{
            //    update.Contacts = existingSchool.Contacts;
            //    update.Teachers = existingSchool.Teachers;
            //    update.DistrictId = existingSchool.DistrictId;
            //    update.Created = existingSchool.Created;
            //    update.Grades = existingSchool.Grades;
            //    update.Admins = existingSchool.Admins;
            //    update.Modified = DateTime.UtcNow;

            //    var updateResult = this.Collection.FindOneAndReplace(s => s.Id == update.Id, update);
            //    return new EntityResult<School>(updateResult);
            //}
            //return new EntityResult<School>("Not found");


        }

        public EntityResult UpdateAccountStatus(string schoolId, AccountStatus status)
        {
            var updateBuilder = Builders<School>.Update
                .Set(s => s.AccountStatus, status);
            if (status == AccountStatus.Verified)
            {
                updateBuilder.Set(s => s.Status, TaggStatus.Active);
            }
            var updateResult = this.Collection.FindOneAndUpdate(s => s.Id == schoolId, updateBuilder);
            if (updateResult != null)
            {
                return new EntityResult();
            }
            return new EntityResult($"Could not update account status for {schoolId}");
        }

        public string GetCustomerId(string schoolId)
        {
            var school = this.Collection.AsQueryable().FirstOrDefault(s => s.Id == schoolId);

            return school == null ? "" : school.CustomerId;
        }

        public School GetByTeacherId(string teacherId)
        {
            return (from t in _teachers.AsQueryable().Where(t => t.Id == teacherId)
                    join s in this.Collection.AsQueryable() on t.SchoolId equals s.Id
                    select s).FirstOrDefault();

        }

        public List<AutocompleteSchool> Autocomplete(string term, int limit = 100)
        {
            try
            {
                var builder = Builders<School>.Filter;

                var regexFilter = Regex.Escape(term);
                var bsonRegex = new BsonRegularExpression(regexFilter, "i");
                var filter = builder.Regex(x => x.Name, bsonRegex);

                var result = this.Collection.Find(filter)
                    .Project<AutocompleteSchool>("{Name:1}")
                    .Limit(limit);

                return result.ToList();
            }
            catch (Exception ex)
            {
                return new List<AutocompleteSchool>();
            }
        }

        public List<AutocompleteSchool> AutocompleteDistrict(string term, int limit = 100)
        {
            try
            {
                var builder = Builders<District>.Filter;

                var regexFilter = Regex.Escape(term);
                var bsonRegex = new BsonRegularExpression(regexFilter, "i");
                var filter = builder.Regex(x => x.Name, bsonRegex);

                var result = _districts.Find(filter)
                    .Project<AutocompleteSchool>("{Name:1}")
                    .Limit(limit);

                return result.ToList();
            }
            catch (Exception ex)
            {
                return new List<AutocompleteSchool>();
            }
        }

        public List<string> GetUserIds(string schoolId)
        {
            var result = new List<string>();
            var school = this.GetById(schoolId);
            if (school != null)
            {
                result.AddRange(_teachers.AsQueryable().Select(s => s.Id));
                result.AddRange(school.Admins.Select(s => s.Id));
            }
            return result;
        }

        public List<string> GetUserIdsDistrict(string districtId)
        {
            var result = new List<string>();
            var district = this.GetByDistrictId(districtId);
            foreach (var school in district)
            {
                result.AddRange(GetUserIds(school.Id));
            }
            return result;
        }

        public List<Blackout> GetBlackouts(string schoolId, DateTime? startDate, DateTime? endDate)
        {
            var result = _blackouts.AsQueryable()
                .Where(b => b.StartDate >= startDate && b.StartDate <= endDate)
                .Where(b => b.SchoolId == schoolId)
                .ToList();

            return result;
        }

        public EntityResult<Blackout> AddBlackout(string schoolId, DateTime startDate)
        {
            try
            {
                var blackoutStartDay = startDate.ToUniversalTime().Date;
                var filter = Builders<Blackout>.Filter.Where(b => b.StartDate == blackoutStartDay && b.SchoolId == schoolId);
                var updateDef = Builders<Blackout>.Update
                    .Set(x => x.StartDate, blackoutStartDay)
                    .Set(x => x.Modified, DateTime.UtcNow)
                    .SetOnInsert(x => x.Created, DateTime.UtcNow);

                var updateOptions = new FindOneAndUpdateOptions<Blackout, Blackout>
                {
                    IsUpsert = true,
                    ReturnDocument = ReturnDocument.After
                };

                var insertion = new Blackout { StartDate = blackoutStartDay, SchoolId = schoolId, Created = DateTime.UtcNow, Modified = DateTime.UtcNow };
                var result = _blackouts.FindOneAndUpdate(filter, updateDef, updateOptions);
                if (result != null)
                {
                    return new EntityResult<Blackout>(result);
                }

                return new EntityResult<Blackout>("Could not insert blackout");
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public EntityResult RemoveBlackout(string schoolId, DateTime startDate)
        {
            var blackoutStartDate = startDate.ToUniversalTime().Date;
            var deleteFilter = Builders<Blackout>.Filter.Where(b => b.SchoolId == schoolId && b.StartDate == blackoutStartDate);
            var deleteResult = _blackouts.DeleteOne(deleteFilter);
            if (deleteResult.DeletedCount > 0)
            {
                return new EntityResult();
            }
            return new EntityResult("Failed to delete");
        }

        public SchoolTeachersResult GetWithTeachers(string schoolId)
        {
            try
            {
                var query = from s in this.Collection.AsQueryable().Where(s => s.Id == schoolId)
                            join t in _teachers.AsQueryable() on s.Id equals t.SchoolId into tJoin
                            select new { School = s, Teachers = tJoin };

                var result = query.FirstOrDefault();
                if (result != null)
                {
                    return new SchoolTeachersResult { School = result.School, Teachers = result.Teachers.ToList() };
                }
                return new SchoolTeachersResult();
            }
            catch (Exception ex)
            {
                return new SchoolTeachersResult();
            }
        }

        public List<SchoolTeachersResult> QueryWithTeachers(Expression<Func<School, bool>> predicate)
        {
            var result = new List<SchoolTeachersResult>();

            try
            {
                var query = from s in this.Collection.AsQueryable().Where(predicate)
                            join t in _teachers.AsQueryable() on s.Id equals t.SchoolId into tJoin
                            select new { School = s, Teachers = tJoin };

                var queryList = query.ToList();
                
                if (queryList != null)
                {
                    result = queryList
                        .Select(x => new SchoolTeachersResult { School = x.School, Teachers = x.Teachers.ToList() })
                        .ToList();
                }
                return result;
                
            }
            catch (Exception ex)
            {
                return result;
            }
        }
    }
}
