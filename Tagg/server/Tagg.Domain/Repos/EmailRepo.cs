﻿using MongoDB.Driver;
using System.Linq;

namespace Tagg.Domain
{
    public class EmailRepo : Repository<EmailLog>
    {
        public EmailRepo(IMongoDatabase db)
            :base(db)
        {
            this.setCollection("emails");
        }

    }

    public class EmailTemplateRepo : Repository<EmailTemplate>
    {
        public EmailTemplateRepo(IMongoDatabase db)
            :base(db)
        {
            this.setCollection("emailTemplates");
        }

        public IEmailTemplate GetByType(EmailTemplateType templateType)
        {
            return this.Collection
                .AsQueryable()
                .FirstOrDefault(t => t.TemplateType == templateType);
        }
    }
}
