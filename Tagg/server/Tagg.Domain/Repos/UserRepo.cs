﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using MongoDB.Driver.GridFS;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Tagg.Domain.ApiModels;
using MongoDB.Bson;
using System.Text.RegularExpressions;

namespace Tagg.Domain
{
    public class UserRepo : Repository<MongoUser>
    {
        private UserManager<MongoUser> _userManager;
        private SubstituteRepo _subs;
        private TeacherRepo _teachers;
        private IMongoCollection<Job> _jobs;
        private GridFSBucket _bucket;

        public UserRepo(IMongoDatabase db, UserManager<MongoUser> userManager, SubstituteRepo subs, TeacherRepo teachers)
            : base(db)
        {
            this.setCollection(Collections.Users);
            _userManager = userManager;
            _subs = subs;
            _teachers = teachers;
            _jobs = db.GetCollection<Job>(Collections.Jobs);
            _bucket = new GridFSBucket(Db);
        }

        public string UpdatePhoto(string userId, Stream stream, string fileName, string contentType)
        {
            var user = this.GetById(userId);
            var userType = UserHelper.GetUserTypeFromClaim(user.Claims);
            if (user != null)
            {
                DeletePhoto(user.Id, userType);
                var metaData = new Dictionary<string, string>
                {
                    { "contentType", contentType },
                    { "userId", userId },
                };

                var options = new GridFSUploadOptions { Metadata = new BsonDocument(metaData) };
                var fileId = _bucket.UploadFromStream(fileName, stream, options);
                switch (userType)
                {
                    case UserType.Substitute:
                        _subs.UpdatePhotoId(user.Id, fileId.ToString());
                        break;
                    case UserType.Teacher:
                        _teachers.UpdatePhotoId(user.Id, fileId.ToString());
                        break;
                    case UserType.SchoolAdmin:
                    case UserType.DistrictAdmin:
                    case UserType.SystemAdmin:
                    default:
                        break;
                }
                return fileId.ToString();
            }
            return string.Empty;
        }

        public void DeletePhoto(string userId, UserType userType)
        {
            try
            {
                string pictureId = string.Empty;
                switch (userType)
                {
                    case UserType.Substitute:
                        var sub = _subs.GetById(userId);
                        pictureId = sub?.Picture;
                        break;
                    case UserType.Teacher:
                        var teacher = _teachers.GetById(userId);
                        pictureId = teacher?.Picture;
                        break;
                    case UserType.SchoolAdmin:
                    case UserType.DistrictAdmin:
                    case UserType.SystemAdmin:
                    default:
                        break;
                }
                if (!string.IsNullOrWhiteSpace(pictureId))
                {
                    _bucket.Delete(new ObjectId(pictureId));
                }
            }
            // TODO : add log entries for things like this
            // this should only happen if file is not found
            catch (Exception ex)
            {
                return;
            }

        }

        public EntityResult<MongoUser> Lockout(string userId, DateTime endDate)
        {
            var user = this.GetById(userId);
            if (user != null)
            {
                user.LockoutEnabled = true;
                user.LockoutEndDateUtc = endDate;

                return this.Update(user);
            }
            return new EntityResult<MongoUser>("Not found");
        }

        public EntityResult<MongoUser> Unlock(string userId)
        {
            var user = this.GetById(userId);
            if (user != null)
            {
                user.LockoutEndDateUtc = null;

                return this.Update(user);
            }
            return new EntityResult<MongoUser>("Not found");
        }

        //new public async Task<MongoUser> Update(MongoUser user)
        //{
        //    var existingUser = await _userManager.FindByIdAsync(user.Id);

        //    foreach (var c in existingUser.Claims)
        //    {
        //        switch (c.Type)
        //        {
        //            case TaggClaimTypes.FirstName:
        //                c.Value = user.PhoneNumber;
        //                break;
        //            case TaggClaimTypes.LastName:
        //                c.Value = 
        //            default:
        //                break;
        //        }
        //    }
        //    var firstNameClaim = existingUser.Claims.FirstOrDefault(c => c.Type == TaggClaimTypes.FirstName);
        //    var lastNameClaim = existingUser.Claims.FirstOrDefault(c => c.Type == TaggClaimTypes.LastName);
        //    var phoneClaim = existingUser.Claims.FirstOrDefault(c => c.Type == TaggClaimTypes.PhoneNumber);

        //    var update = await _userManager.
            
        //}

        public UserPhotoResultDto GetPhoto(string photoId)
        {
            var result = new UserPhotoResultDto();
            try
            {

                var stream = _bucket.OpenDownloadStream(new ObjectId(photoId));
                result.ContentType = stream.FileInfo.Metadata["contentType"].AsString;
                result.Stream = stream;
                return result;
            }
            catch (Exception ex)
            {
                // TODO : As a fallback, return a default file stored on the server.
                result.Succeeded = false;
                return result;
            }
        }

        public MongoUser GetByEmail(string email)
        {
            return this.Collection.Find(Builders<MongoUser>.Filter
                .Where(u => u.NormalizedEmail == email.ToUpper()))
                .FirstOrDefault();
        }

        public async Task<EntityResult<MongoUser>> CreateAsync(UserCreateDto dto)
        {
            var user = new MongoUser { UserName = dto.Email, Email = dto.Email, PhoneNumber = dto.PhoneNumber, FirstName = dto.FirstName, LastName = dto.LastName };
            if (await this.IsValidUser(user))
            {
                user.Claims.AddRange(getClaimsForUser(dto));
                var result = await _userManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    return new EntityResult<MongoUser>(await _userManager.FindByEmailAsync(user.Email));
                }
            }
            return new EntityResult<MongoUser>("Invalid user.");
        }

        /// <summary>
        /// Adds claims to user
        /// </summary>
        /// <param name="claims">Dictionary with claim value as key, and claim type as dictionary value</param>
        /// <returns></returns>
        public async Task<EntityResult<MongoUser>> AddClaimsAsync(string email, List<Claim> claims)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user != null)
            {
                var result = await _userManager.AddClaimsAsync(user, claims);
                if (result.Succeeded)
                {
                    return new EntityResult<MongoUser>(user);
                }
            }
            return new EntityResult<MongoUser>($"Failed to add claims to {email}");

        }

        public async Task<EntityResult> AddClaimAsync(string email, Claim claim)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user != null)
            {
                var result = await _userManager.AddClaimAsync(user, claim);
                return result.Succeeded ? new EntityResult() : new EntityResult($"Failed to add claim to {email}");
            }
            return new EntityResult($"{email} not found.");
        }

        public async Task<EntityResult> UpdateClaim(MongoUser user, string claimType, string newClaimValue)
        {
            try
            {
                var existingClaims = user.Claims.Where(c => c.Type == claimType).ToList();
                foreach (var c in existingClaims)
                {
                    var newClaim = new Claim(c.Type, newClaimValue);
                    var claimUpdateResult = await _userManager.ReplaceClaimAsync(user, new Claim(c.Type, c.Value), newClaim);
                }
                return new EntityResult();
            }
            catch (Exception ex)
            {
                return new EntityResult(ex.Message);
            }
        }

        public async Task<string> GetFullNameById(string userId)
        {
            try
            {
                //var user = await _userManager.FindByIdAsync(userId);
                var user = this.GetById(userId);
                if (user != null)
                {
                    var firstNameClaim = user.Claims.FirstOrDefault(c => c.Type == TaggClaimTypes.FirstName);
                    var lastNameClaim = user.Claims.FirstOrDefault(c => c.Type == TaggClaimTypes.LastName);
                    return $"{firstNameClaim?.Value} {lastNameClaim?.Value}";
                }
                return string.Empty;
            }
            catch (Exception ex)
            {
                return string.Empty;
            }
        }

        public async Task<bool> IsValidUser(MongoUser checkMe)
        {
            var validator = _userManager.UserValidators.FirstOrDefault();
            var result = await validator.ValidateAsync(_userManager, checkMe);
            return result.Succeeded;
        }

        // Not Implemented
        private SubUserDto GetSubUser(string id)
        {
            var query = from u in this.Collection.AsQueryable()
                        join s in _subs.Collection.AsQueryable() on u.Id equals s.Id into subJoin
                        from r in subJoin.DefaultIfEmpty()
                        select new SubUserDto { User = u, Sub = r };

            return query.FirstOrDefault();
        }

        public UserBase GetUserBaseFromPrinciple(string userId)
        {
            var result = new UserBase();
            var user = this.GetById(userId);
            if (user != null)
            {
                result.FirstName = user.Claims.FirstOrDefault(c => c.Type == TaggClaimTypes.FirstName)?.Value;
                result.LastName = user.Claims.FirstOrDefault(c => c.Type == TaggClaimTypes.LastName)?.Value;
                result.Id = user.Id;
            }

            return result;
        }

        public List<AutocompleteUser> Autocomplete(string term, int limit = 100)
        {
            try
            {
                var builder = Builders<MongoUser>.Filter;
                var filter = builder.Empty;

                var termParts = term.Split(" ");
                switch (termParts.Length)
                {
                    case 1:
                        var regexFilter = Regex.Escape(term);
                        var bsonRegex = new BsonRegularExpression(regexFilter, "i");
                        filter = builder.Regex(x => x.FirstName, bsonRegex);
                        filter |= builder.Regex(x => x.LastName, bsonRegex);
                        break;
                    case 2:
                        var regexOne = Regex.Escape(termParts[0]);
                        var bsonOne = new BsonRegularExpression(regexOne, "i");
                        var regexTwo = Regex.Escape(termParts[1]);
                        var bsonTwo = new BsonRegularExpression(regexTwo, "i");
                        filter = builder.Regex(x => x.FirstName, bsonOne);
                        filter &= builder.Regex(x => x.LastName, bsonTwo);
                        break;
                    case 0:
                    default:
                        break;
                }

                var result = this.Collection.Find(filter)
                    .Project<AutocompleteUser>("{FirstName: 1, LastName: 1}")
                    .Limit(limit);

                return result.ToList();
            }
            catch (Exception ex)
            {
                return new List<AutocompleteUser>();
            }
        }

        private List<MongoUserClaim> getClaimsForUser(UserCreateDto dto)
        {
            var claims = new List<Claim>();
            switch (dto.UserType)
            {
                case UserType.Teacher:
                    claims.Add(new Claim(TaggClaimTypes.ApiAccess, ApiAccessLevels.Teacher));
                    break;
                case UserType.SchoolAdmin:
                    claims.Add(new Claim(TaggClaimTypes.ApiAccess, ApiAccessLevels.SchoolAdmin));
                    break;
                case UserType.DistrictAdmin:
                    claims.Add(new Claim(TaggClaimTypes.ApiAccess, ApiAccessLevels.DistrictAdmin));
                    break;
                case UserType.SystemAdmin:
                    claims.Add(new Claim(TaggClaimTypes.ApiAccess, ApiAccessLevels.SystemAdmin));
                    break;
                case UserType.Substitute:
                // TODO : Need default case? Put sub access level as default for placeholder
                default:
                    claims.Add(new Claim(TaggClaimTypes.ApiAccess, ApiAccessLevels.Substitute));
                    break;
            }
            claims.Add(new Claim(TaggClaimTypes.FirstName, dto.FirstName));
            claims.Add(new Claim(TaggClaimTypes.LastName, dto.LastName));
            claims.Add(new Claim(TaggClaimTypes.Email, dto.Email));
            if (!string.IsNullOrWhiteSpace(dto.SchoolId))
            {
                claims.Add(new Claim(TaggClaimTypes.School, dto.SchoolId));
            }

            return claims.Select(c => new MongoUserClaim(c)).ToList();
        }

        new public EntityResult<MongoUser> Delete(string userId)
        {
            var user = this.GetById(userId);
            if (user != null)
            {
                var userType = UserHelper.GetUserTypeFromClaim(user.Claims);

                // Attempt to delete the child object (i.e. teacher, sub, etc.)
                bool deleteChildResult = deleteUserChild(user);
                if (deleteChildResult)
                {
                    var deleteResult = this.Collection.DeleteOne(u => u.Id == user.Id);
                    if (deleteResult.IsAcknowledged && deleteResult.DeletedCount > 0)
                    {
                        return new EntityResult<MongoUser>();
                    }
                }
            }
            return new EntityResult<MongoUser>("Could not delete user");
        }

        private bool deleteUserChild(MongoUser user)
        {
            UserType userType = UserHelper.GetUserTypeFromClaim(user.Claims);
            bool jobsExist = true;

            switch (userType)
            {
                case UserType.Substitute:
                    jobsExist = _jobs.AsQueryable().Any(j => j.SubstituteId == user.Id);
                    if (!jobsExist)
                    {
                        var subDeleteResult = _subs.Delete(user.Id);
                        return subDeleteResult.Succeeded;
                    }
                    return false;
                case UserType.Teacher:
                    jobsExist = _jobs.AsQueryable().Any(j => j.TeacherId == user.Id);
                    if (!jobsExist)
                    {
                        var teacherDeleteResult = _teachers.Delete(user.Id);
                        return teacherDeleteResult.Succeeded;
                    }
                    return false;
                // TODO : Implement delete for these user types
                case UserType.SchoolAdmin:
                case UserType.DistrictAdmin:
                case UserType.SystemAdmin:
                default:
                    return false;
            }
        }

    }
}
