﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class InquiriesRepo : Repository<SchoolInquiry>
    {
        public InquiriesRepo(IMongoDatabase db) : base(db)
        {
            this.setCollection(Collections.Inquiries);
        }
    }
}
