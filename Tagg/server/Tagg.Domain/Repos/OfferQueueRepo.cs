﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MongoDB.Driver.Linq;
using Tagg.Domain.ApiModels;

namespace Tagg.Domain
{
    public class OfferQueueRepo : Repository<JobOfferQueue>
    {
        private JobRepo _jobs;

        public OfferQueueRepo(JobRepo jobs, IMongoDatabase db)
            : base(db)
        {
            this.setCollection(Collections.OfferQueue);
            _jobs = jobs;
        }


        public EntityResult<JobOfferQueue> CreateOfferQueue(Job job, JobOfferDetailsDto details)
        {
            var queue = new JobOfferQueue();
            queue.JobId = job.Id;

            var minHoursForAll = 18;
            var maxHoursForExclusive = 12;
            var jobStart = details.Job.StartDate.ToUniversalTime();
            var jobStartsInHours = (jobStart - DateTime.UtcNow).TotalHours;
            bool hasSelectedSubs = details.Selected?.Count > 0;
            bool hasFavoriteSubs = details.Favorites?.Count > 0;

            var hoursForSelected = jobStartsInHours;
            var hoursForFavorites = hasSelectedSubs ? Math.Max(Math.Min(minHoursForAll, hoursForSelected), (hoursForSelected - maxHoursForExclusive)) : jobStartsInHours;
            var hoursForOthers = hasFavoriteSubs ? Math.Max(Math.Min(minHoursForAll, hoursForFavorites), (hoursForFavorites - maxHoursForExclusive)) : hoursForFavorites;

            DateTime selectedSendOn = DateTime.UtcNow;
            DateTime favSendOn = jobStart.AddHours(-hoursForFavorites);
            DateTime otherSendOn = jobStart.AddHours(-hoursForOthers);

            foreach (var sub in details.Selected)
            {
                queue.OfferQueue.Enqueue(new PendingJobOffer
                {
                    SendTo = sub,
                    SendOn = selectedSendOn
                });
            }

            foreach (var sub in details.Favorites)
            {
                queue.OfferQueue.Enqueue(new PendingJobOffer
                {
                    SendTo = sub,
                    SendOn = favSendOn
                });
            }

            foreach (var sub in details.Others)
            {
                queue.OfferQueue.Enqueue(new PendingJobOffer
                {
                    SendTo = sub,
                    SendOn = otherSendOn
                });
            }
            queue.StartTime = queue.OfferQueue.Min(q => q.SendOn);
            queue.Status = TaggStatus.Active;
            return this.Add(queue);
        }

        public List<JobOfferQueue> GetOpenQueues()
        {
            return this.Collection.AsQueryable()
                .Where(q => q.OfferQueue.Count > 0)
                .ToList();
        }

        //Returns all queues 
        public List<JobOfferQueue> GetByStartTime(DateTime startTime, TaggStatus excludeStatus = TaggStatus.Complete)
        {
            return this.Collection.AsQueryable()
                .Where(q => q.StartTime <= startTime && q.Status != excludeStatus && q.Status != TaggStatus.Disabled)
                .ToList();
        }

        public EntityResult<JobOfferQueue> CloseQueue(string jobId)
        {
            var queue = this.Collection.AsQueryable()
                .FirstOrDefault(q => q.JobId == jobId);

            if (queue != null)
            {
                queue.OfferQueue.Clear();
                queue.Status = TaggStatus.Complete;
                return this.Update(queue);
            }
            return new EntityResult<JobOfferQueue>("Queue not found");
        }

        public EntityResult<JobOfferQueue> DisableQueue(string jobId)
        {
            var queue = this.Collection.AsQueryable()
                .FirstOrDefault(q => q.JobId == jobId);

            if (queue != null)
            {
                queue.Status = TaggStatus.Disabled;
                return this.Update(queue);
            }
            return new EntityResult<JobOfferQueue>("Queue not found");

        }

        public JobOfferQueue GetByJobId(string jobId)
        {
            return this.Collection.AsQueryable().FirstOrDefault(q => q.JobId == jobId);
        }

        public EntityResult<JobOfferQueue> ReopenQueue(Job job)
        {
            var result = new EntityResult<JobOfferQueue>("Could not reopen offer queue");
            try
            {
                var offersToResend = new List<JobOffer>();
                
                // Reset properties on job
                foreach (var offer in job.Offers.Where(o => o.Status != JobOfferStatus.Declined && o.Status != JobOfferStatus.Cancelled))
                {
                    if (offer.SubId == job.SubstituteId)
                    {
                        offer.Status = JobOfferStatus.Cancelled;
                    }
                    else
                    {
                        offer.Status = JobOfferStatus.Expired;
                        offersToResend.Add(offer);
                    }
                }
                job.SubstituteId = null;
                job.Status = JobStatus.Open;
                var jobUpdateResult = _jobs.Update(job);

                // Re-up offer queue to send offers again
                var queueToReopen = this.GetByJobId(job.Id);
                if (queueToReopen != null && queueToReopen.Status == TaggStatus.Disabled)
                {
                    queueToReopen.Status = TaggStatus.Active;
                    foreach (var sent in offersToResend)
                    {
                        queueToReopen.OfferQueue.Enqueue(new PendingJobOffer
                        {
                            SendTo = sent.SubId,
                            SendOn = sent.SentOn
                        });
                    }

                    var offersSorted = queueToReopen.OfferQueue.OrderBy(x => x.SendOn).ToList();
                    DateTime? sendOnTracker = offersSorted.Min(x => x.SendOn);
                    if (sendOnTracker.HasValue)
                    {
                        queueToReopen.OfferQueue.Clear();
                        foreach (var item in offersSorted)
                        {
                            // Set send on to now for next upcoming batch 
                            item.SendOn = item.SendOn == sendOnTracker ? DateTime.Now : item.SendOn;
                            queueToReopen.OfferQueue.Enqueue(item);
                        }
                    }
                    result = this.Update(queueToReopen);
                }
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

    }
}
