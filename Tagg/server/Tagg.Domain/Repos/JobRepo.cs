﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using MongoDB.Driver.Linq;
using Tagg.Domain.ApiModels;

namespace Tagg.Domain
{
    public class JobRepo : Repository<Job>
    {
        private IMongoQueryable<School> _schools;
        private IMongoQueryable<Substitute> _subs;
        private IQueryable<Teacher> _teachers;
        private GridFSBucket _bucket;

        public JobRepo(IMongoDatabase db)
            : base(db)
        {
            this.setCollection(Collections.Jobs);
            _schools = Db.GetCollection<School>(Collections.Schools).AsQueryable();
            _subs = Db.GetCollection<Substitute>(Collections.Subs).AsQueryable();
            _teachers = Db.GetCollection<Teacher>(Collections.Teachers).AsQueryable();
            _bucket = new GridFSBucket(Db);
        }

        /// <summary>
        /// Returns all jobs that have sub listed in the offers array
        /// within date range (if provided)
        /// </summary>
        public List<Job> GetSubOffers(Substitute sub, DateTime minDate, DateTime maxDate)
        {
            // Get offers that have not been declined and have not been previously accepted
            var filter = this.Collection.AsQueryable()
                .Where(j => j.Status == JobStatus.Open)
                .Where(j => j.Offers.Any(x => x.SubId == sub.Id && x.Status != JobOfferStatus.Declined && x.Status != JobOfferStatus.Cancelled))
                .Where(j => j.StartDate >= minDate && j.StartDate <= maxDate);

            return filter.ToList();
        }

        /// <summary>
        /// Returns all jobs that have sub listed on the job
        /// within date range (if provided)
        /// </summary>
        public List<Job> GetSubJobs(Substitute sub, DateTime? minDate = null, DateTime? maxDate = null)
        {
            var filterDefinition = Builders<Job>.Filter
                .Where(j => j.SubstituteId == sub.Id);

            if (minDate != null && minDate.HasValue)
            {
                filterDefinition &= Builders<Job>.Filter.Where(j => j.StartDate >= minDate);
            }

            if (maxDate != null && maxDate.HasValue)
            {
                filterDefinition &= Builders<Job>.Filter.Where(j => j.StartDate <= maxDate);
            }

            var result = this.Collection.Find(filterDefinition).ToList();
            return result;
        }

        public List<Job> Search(JobSearchCriteria search)
        {
            // TODO : DateTime fix?
            var builder = Builders<Job>.Filter;
            var filter = Builders<Job>.Filter.Empty;


            if (search.StartDate.HasValue && search.EndDate.HasValue)
            {
                filter &= builder.Where(j => j.StartDate > search.StartDate.Value.Date);
                filter &= builder.Where(j => j.StartDate < search.StartDate.Value.AddDays(1).Date);
            }
            else if (search.StartDate.HasValue)
            {
                filter &= builder.Where(j => j.StartDate > search.StartDate.Value.Date);
            }
            else if (search.EndDate.HasValue)
            {
                filter &= builder.Where(j => j.EndDate < search.EndDate.Value.Date);
            }

            if (!string.IsNullOrWhiteSpace(search.SubstituteId))
            {
                filter &= builder.Where(j => j.SubstituteId == search.SubstituteId);
            }

            if (!string.IsNullOrWhiteSpace(search.TeacherId))
            {
                filter &= builder.Where(j => j.TeacherId == search.TeacherId);
            }

            return this.Collection.Find(filter).ToList();

        }

        public IQueryable<Job> GetByMaxEndDate(DateTime endDate)
        {
            return this.Collection.AsQueryable()
                .Where(j => j.EndDate <= endDate);
        }

        public List<Job> GetByMaxEndDate(DateTime endDate, JobStatus status)
        {
            var filter = Builders<Job>.Filter
                .Where(j => j.EndDate <= endDate && j.Status == status);
            return this.Collection.Find(filter).ToList();
        }

        public IQueryable<JobDetailsDto> GetJobsDetailsQuery()
        {
            var query = from j in this.Collection.AsQueryable()
                        join t in _teachers.AsQueryable() on j.TeacherId equals t.Id
                        join sch in _schools on j.SchoolId equals sch.Id
                        join sub in _subs on j.SubstituteId equals sub.Id into subJoin
                        from sub in subJoin.DefaultIfEmpty()
                        select new JobDetailsDto
                        {
                            Id = j.Id,
                            SchoolName = sch.Name,
                            StartDate = j.StartDate,
                            EndDate = j.EndDate,
                            ChargeAmount = j.ChargeAmount,
                            PayAmount = j.PayAmount,
                            SchoolAddress = sch.Address,
                            SchoolId = sch.Id,
                            Status = j.Status,
                            Subject = j.Subject,
                            SubstituteId = j.SubstituteId,
                            TeacherId = j.TeacherId,
                            SubstituteName = sub.FirstName + " " + sub.LastName,
                            TeacherName = t.FirstName + " " + t.LastName,
                            Offers = j.Offers,
                            SubNeeded = j.SubNeeded,
                            LessonPlanId = j.LessonPlanId,
                            Rating = j.Rating,
                            CreatedBy = j.CreatedBy
                        };

            return query;
        }

        public List<JobDetailsDto> GetBySchoolId(string schoolId, int maxResults = 100, int skip = 0)
        {
            var result = new List<JobDetailsDto>();
            try
            {
                result = GetJobsDetailsQuery()
                .Where(x => x.SchoolId == schoolId)
                .OrderByDescending(x => x.StartDate)
                .Skip(skip)
                .Take(maxResults)
                .ToList();

                //result = projectJobDetails(query).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        public List<JobDetailsDto> GetBySchoolId(string schoolId, DateTime minDate, DateTime maxDate)
        {
            var result = new List<JobDetailsDto>();
            try
            {
                result = GetJobsDetailsQuery()
                .Where(x => x.SchoolId == schoolId && x.StartDate >= minDate && x.StartDate <= maxDate)
                .OrderByDescending(x => x.StartDate)
                .ToList();

                //result = projectJobDetails(query).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        public JobDetailsDtoContainer GetDetailsAll(int maxResults = 100, int skip = 0, string sort = null, string direction = null)
        {
            try
            {
                IQueryable<JobDetailsDto> allJobsQuery = GetJobsDetailsQuery();

                int totalCount = allJobsQuery.Count();

                allJobsQuery = this.applyJobsSort(allJobsQuery, sort, direction);
                List<JobDetailsDto> jobsDetails = allJobsQuery
                    .Skip(skip)
                    .Take(maxResults)
                    .ToList();

                //result = projectJobDetails(query).ToList();

                return new JobDetailsDtoContainer(jobsDetails, totalCount);
            }
            catch (Exception ex)
            {
                return new JobDetailsDtoContainer(new List<JobDetailsDto>(), 0);
                throw;
            }
        }

        public IQueryable<JobDetailsDto> applyJobsSort(IQueryable<JobDetailsDto> query, string sort, string direction)
        {
            bool ascend = direction == null ? false : direction.ToLower().Equals("asc");

            switch (sort)
            {
                case null:
                case "":
                    return query;
                case "startDate":
                    if (ascend) return query.OrderBy(x => x.StartDate);
                    else return query.OrderByDescending(x => x.StartDate);
                case "schoolName":
                    if (ascend) return query.OrderBy(x => x.SchoolName);
                    else return query.OrderByDescending(x => x.SchoolName);
                case "substituteName":
                    if (ascend) return query.OrderBy(x => x.SubstituteName);
                    else return query.OrderByDescending(x => x.SubstituteName);
                case "teacherName":
                    if (ascend) return query.OrderBy(x => x.TeacherName);
                    else return query.OrderByDescending(x => x.TeacherName);
                case "subject":
                    if (ascend) return query.OrderBy(x => x.Subject);
                    else return query.OrderByDescending(x => x.Subject);
                case "status":
                    if (ascend) return query.OrderBy(x => x.Status);
                    else return query.OrderByDescending(x => x.Status);
            }

            return query;
        }

        public JobDetailsDto GetDetailsSingle(string jobId)
        {
            var result = new List<JobDetailsDto>();
            try
            {
                result = GetJobsDetailsQuery()
                .Where(x => x.Id == jobId)
                .ToList();

                //result = projectJobDetails(query).ToList();
                return result.FirstOrDefault();
            }
            catch (Exception ex)
            {
                return result.FirstOrDefault();
            }
        }

        public List<JobDetailsDto> GetBySubId(string subId, DateTime minDate, DateTime maxDate, int maxResults = 100, int skip = 0)
        {

            var result = new List<JobDetailsDto>();
            try
            {
                result = GetJobsDetailsQuery()
                .Where(x => x.SubstituteId == subId)
                .Where(j => j.StartDate > minDate.Date && j.StartDate < maxDate.AddDays(1).Date)
                .Where(j => j.Status != JobStatus.Cancelled)
                .OrderByDescending(x => x.StartDate)
                .Skip(skip)
                .Take(maxResults)
                .ToList();

                //result = projectJobDetails(queryStage1).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }

        }

        public List<JobDetailsDto> GetByTeacherId(string teacherId, int maxResults = 100, int skip = 0)
        {
            var result = new List<JobDetailsDto>();
            try
            {
                result = GetJobsDetailsQuery()
                .Where(x => x.TeacherId == teacherId)
                .OrderByDescending(x => x.StartDate)
                .Skip(skip)
                .Take(maxResults)
                .ToList();

                //result = projectJobDetails(queryStage1).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        public IQueryable<JobDetailsDto> GetDetailsQueryableByTeacherId(string teacherId)
        {
            var result = GetJobsDetailsQuery();
            try
            {
                result = projectJobDetailsQueryable(result.Where(x => x.TeacherId == teacherId));
                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        public List<JobDetailsDto> GetOffersBySubId(string subId, DateTime minDate)
        {
            var result = new List<JobDetailsDto>();
            try
            {
                result = GetJobsDetailsQuery()
                    .Where(x => x.StartDate >= minDate)
                .Where(x => x.Status == JobStatus.Open)
                .Where(x => x.Offers.Any(z => z.SubId == subId && z.Status != JobOfferStatus.Declined && z.Status != JobOfferStatus.Cancelled))
                .OrderByDescending(x => x.StartDate)
                .ToList();

                return result;
            }
            catch (Exception ex)
            {
                return result;
            }
        }

        public List<JobDetailsDto> GetJobDetails(string schoolId, int limit = 100)
        {
            var school = _schools.Where(s => s.Id == schoolId).FirstOrDefault();
            var query = from j in this.Collection.AsQueryable()
                        join sch in _schools on j.SchoolId equals sch.Id
                        join sub in _subs on j.SubstituteId equals sub.Id into subJoin
                        from sub in subJoin.DefaultIfEmpty()
                        select new JobDetailsDto
                        {
                            Id = j.Id,
                            SchoolName = sch.Name,
                            StartDate = j.StartDate,
                            EndDate = j.EndDate,
                            PayAmount = j.PayAmount,
                            ChargeAmount = j.ChargeAmount,
                            SchoolAddress = sch.Address,
                            SchoolId = sch.Id,
                            Status = j.Status,
                            Subject = j.Subject,
                            SubstituteId = j.SubstituteId,
                            TeacherId = j.TeacherId,
                            SubstituteName = sub.FirstName + " " + sub.LastName,
                            LessonPlanId = j.LessonPlanId,
                            SubNeeded = j.SubNeeded,
                            CreatedBy = j.CreatedBy
                        };
            try
            {
                //var test = query.ToList();
                var result = query
                    .Where(x => x.SchoolId == schoolId && x.SubNeeded)
                    .Take(limit)
                    .ToList();

                foreach (var detail in result)
                {
                    var teacher = _teachers.FirstOrDefault(t => t.Id == detail.TeacherId);
                    detail.TeacherId = teacher?.Id;
                    detail.TeacherName = teacher?.FullName;
                }
                return result;
            }
            catch (Exception ex)
            {

                throw;
            }
        }

        public bool UpdateStatus(string jobId, JobStatus status, string subId)
        {
            var statusUpdate = Builders<Job>.Update
                .Set(j => j.Status, status)
                .Set(j => j.SubstituteId, subId);

            var result = this.Collection.FindOneAndUpdate(j => j.Id == jobId, statusUpdate);
            return result != null;
        }

        public bool UpdateTransactionStatus(string jobId, JobTransactionStatus status)
        {
            var statusUpdate = Builders<Job>.Update
                .Set(j => j.TransactionStatus, status);

            var result = this.Collection.FindOneAndUpdate(j => j.Id == jobId, statusUpdate);
            return result != null;
        }

        public EntityResult<Job> AddOffer(string jobId, JobOffer offer)
        {
            var addDefinition = Builders<Job>.Update.AddToSet(j => j.Offers, offer);
            var result = this.Collection.FindOneAndUpdate(j => j.Id == jobId, addDefinition);
            return new EntityResult<Job>(result);
        }

        public EntityResult<Job> Cancel(string jobId)
        {
            try
            {
                var update = Builders<Job>.Update.Set(j => j.Status, JobStatus.Cancelled);
                var result = this.Collection.FindOneAndUpdate(j => j.Id == jobId, update);
                return new EntityResult<Job>(result);
            }
            catch (Exception ex)
            {
                return new EntityResult<Job>($"Failed to cancel job: {jobId}");
            }
        }

        public string UpdateLessonPlan(string jobId, Stream lessonPlanStream, string fileName, string contentType)
        {
            var job = this.GetById(jobId);
            var existingFileId = job?.LessonPlanId;

            if (job != null)
            {
                if (!string.IsNullOrWhiteSpace(existingFileId))
                {
                    this.DeleteFile(existingFileId);
                }
                var metaData = new Dictionary<string, string>
                {
                    {"contentType", contentType },
                    { "jobId", jobId }
                };

                var options = new GridFSUploadOptions { Metadata = new BsonDocument(metaData) };
                var lessonPlanId = _bucket.UploadFromStream(fileName, lessonPlanStream, options);
                this.updateLessonPlanId(jobId, lessonPlanId.ToString());

                return lessonPlanId.ToString();
            }
            return string.Empty;
        }

        public FileResultDto GetLessonPlan(string lessonPlanId)
        {
            var result = new FileResultDto();
            try
            {
                var stream = _bucket.OpenDownloadStream(new ObjectId(lessonPlanId));
                result.ContentType = stream.FileInfo.Metadata["contentType"].AsString;
                result.Stream = stream;
                return result;
            }
            catch (Exception ex)
            {
                result.Succeeded = false;
                return result;
            }
        }

        public void DeleteFile(string fileId)
        {
            try
            {
                _bucket.Delete(new ObjectId(fileId));
            }
            // TODO : add log entries for things like this
            // this should only happen if file is not found
            catch (Exception ex)
            {
                return;
            }
        }

        public EntityResult AddRating(string jobId, double rating)
        {
            var update = Builders<Job>.Update
                .Set(j => j.Rating, rating);

            var result = this.Collection.FindOneAndUpdate(j => j.Id == jobId, update);
            if (result != null)
            {
                return new EntityResult();
            }
            return new EntityResult("Failed to update job rating");
        }

        public EntityResult Decline(string jobId, string subId)
        {
            try
            {
                var job = this.Collection.AsQueryable().FirstOrDefault(j => j.Id == jobId);
                if (job != null)
                {
                    job.Offers.FirstOrDefault(o => o.SubId == subId).Status = JobOfferStatus.Declined;
                    var update = this.Collection.FindOneAndReplace(j => j.Id == job.Id, job);
                    return new EntityResult();
                }
                return new EntityResult("Job offer not found");
            }
            catch (Exception ex)
            {
                return new EntityResult("Job offer not found");
                throw;
            }
        }

        private bool updateLessonPlanId(string jobId, string lessonPlanId)
        {
            var update = Builders<Job>.Update.Set(j => j.LessonPlanId, lessonPlanId);
            var result = this.Collection.FindOneAndUpdate(j => j.Id == jobId, update);
            return result != null;
        }

        //private IQueryable<JobDetailsDto> projectJobDetails(List<JobDetailsDto> detailsQuery)
        //{
        //    var projectStage = from j in detailsQuery.AsQueryable()
        //                      join teach in _teachers on j.TeacherId equals teach.Id
        //                      select new JobDetailsDto
        //                      {
        //                          Id = j.Id,
        //                          SchoolName = j.SchoolName,
        //                          StartDate = j.StartDate,
        //                          EndDate = j.EndDate,
        //                          ChargeAmount = j.ChargeAmount,
        //                          PayAmount = j.PayAmount,
        //                          SchoolAddress = j.SchoolAddress,
        //                          SchoolId = j.SchoolId,
        //                          Status = j.Status,
        //                          Subject = j.Subject,
        //                          SubstituteId = j.SubstituteId,
        //                          TeacherId = j.TeacherId,
        //                          TeacherName = teach.FirstName + " " + teach.LastName,
        //                          SubstituteName = j.SubstituteName,
        //                          SubNeeded = j.SubNeeded,
        //                          LessonPlanId = j.LessonPlanId,
        //                          CreatedBy = j.CreatedBy
        //                      };
        //    return projectStage;
        //}

        //private List<JobDetailsDto> addTeacherNameToJobDetails(List<JobDetailsDto> detailsList)
        //{
        //	detailsList.ForEach(d => d.TeacherName = _teachersList.Where(t => t.Id == d.TeacherId).Select(t => t.FirstName + " " + t.LastName).FirstOrDefault());

        //	return detailsList;
        //}


        private IQueryable<JobDetailsDto> projectJobDetailsQueryable(IQueryable<JobDetailsDto> detailsQuery)
        {
            var projectStage = from j in detailsQuery
                               join teach in _teachers on j.TeacherId equals teach.Id
                               select new JobDetailsDto
                               {
                                   Id = j.Id,
                                   SchoolName = j.SchoolName,
                                   StartDate = j.StartDate,
                                   EndDate = j.EndDate,
                                   ChargeAmount = j.ChargeAmount,
                                   PayAmount = j.PayAmount,
                                   SchoolAddress = j.SchoolAddress,
                                   SchoolId = j.SchoolId,
                                   Status = j.Status,
                                   Subject = j.Subject,
                                   SubstituteId = j.SubstituteId,
                                   TeacherId = j.TeacherId,
                                   TeacherName = teach.FirstName + " " + teach.LastName,
                                   SubstituteName = j.SubstituteName,
                                   SubNeeded = j.SubNeeded,
                                   LessonPlanId = j.LessonPlanId,
                                   CreatedBy = j.CreatedBy
                               };
            return projectStage;
        }
    }

    public class JobSearchCriteria
    {
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string SubstituteId { get; set; }
        public string TeacherId { get; set; }
    }
}
