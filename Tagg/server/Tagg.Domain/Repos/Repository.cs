﻿using MongoDB.Bson;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using MongoDB.Driver.Linq;


namespace Tagg.Domain
{
    public class Repository<T> where T : TaggEntity
    {
        public Repository(IMongoDatabase db)
        {
            Db = db;
        }

        public IMongoCollection<T> Collection { get; set; }
        public IMongoDatabase Db { get; set; }

        internal void setCollection(string name)
        {
            this.Collection = Db.GetCollection<T>(name);
        }

        #region read
        public bool Exists(string id)
        {
            return this.Collection.Count(Builders<T>.Filter.Where(o => o.Id == id)) > 0;
        }
        public T GetById(string id)
        {
            return this.Collection
                .AsQueryable()
                .FirstOrDefault(e => e.Id == id);
        }

        public List<T> GetAll()
        {
            return this.Collection.AsQueryable().ToList();
        }

        public IQueryable<T> GetQueryable()
        {
            this.Collection.Find<T>(o => o.Id == null);
            return this.Collection.AsQueryable();
        }

        public IQueryable<T> Query(Expression<Func<T, bool>> predicate)
        {
            return this.Collection
                .AsQueryable()
                .Where(predicate);
        }

        public int Count()
        {
            return this.Collection.AsQueryable().Count();
        }

        public int CountQuery(Expression<Func<T, bool>> predicate)
        {
            return this.Collection
                .AsQueryable()
                .Where(predicate)
                .Count();
        }
        #endregion

        #region create-update-delete
        public EntityResult<T> Add(T entity)
        {
            try
            {
                entity.Created = DateTime.UtcNow;
                entity.Modified = DateTime.UtcNow;
                this.Collection.InsertOne(entity);
                return new EntityResult<T>(entity);
            }
            catch (Exception ex)
            {
                return new EntityResult<T>(ex.Message);
            }
        }

        public EntityResult<T> Update(T entity)
        {
            try
            {
                entity.Modified = DateTime.UtcNow;
                var result = this.Collection.FindOneAndReplace(o => o.Id == entity.Id, entity);
                return new EntityResult<T>(entity);
            }
            catch (Exception ex)
            {
                return new EntityResult<T>(ex.Message);
            }
        }

        // Used for repos that need to override the normal update method, but still use this functionality.
        public EntityResult<T> UpdateBase(T entity)
        {
            try
            {
                entity.Modified = DateTime.UtcNow;
                var result = this.Collection.FindOneAndReplace(o => o.Id == entity.Id, entity);
                return new EntityResult<T>(result);
            }
            catch (Exception ex)
            {
                return new EntityResult<T>(ex.Message);
            }
        }

        public EntityResult<T> Delete(string id)
        {
            try
            {
                ObjectId objId;
                if (ObjectId.TryParse(id, out objId))
                {
                    var result = this.Collection.DeleteOne(new BsonDocument("_id", ObjectId.Parse(id)));
                    return result.IsAcknowledged ? new EntityResult<T>() : new EntityResult<T>("Delete failed");
                }
                return new EntityResult<T>("Invalid id");
            }
            catch (Exception ex)
            {
                return new EntityResult<T>(ex.Message);
            }
        }
        #endregion



    }
}
