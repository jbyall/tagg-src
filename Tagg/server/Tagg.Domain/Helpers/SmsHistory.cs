﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.Helpers
{
    public class SmsHistory
    {
        public string Sid { get; set; }
        public DateTime? DateSent { get; set; }
        public string To { get; set; }
    }
}
