﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class CalendarDay
    {
        public CalendarDay()
        {
            this.DayOfMonthIndex = 0;
        }
        public CalendarDay(DateTime day)
        {
            this.Date = day;
            this.Year = day.Year;
            this.MonthIndex = day.Month;
            this.DayOfMonthIndex = day.Day;
            this.DayOfWeekIndex = day.DayOfWeek;
            this.DayOfWeek = CalendarHelper.MapDayIndex(day.DayOfWeek);
            this.Events = new List<CalendarEvent>();
        }

        public DateTime Date { get; set; }
        public int Year { get; set; }
        public int MonthIndex { get; set; }
        public int DayOfMonthIndex { get; set; }
        public DayOfWeek DayOfWeekIndex { get; set; }
        public string DayOfWeek { get; set; }

        public List<CalendarEvent> Events { get; set; }
    }
}
