﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public static class TransactionHelper
    {
        public static double CalculateTotalAmountUsd(double payRateUsd)
        {
            var serviceFeeFactor = .25;
            var transactionFeeFactor = .0124;
            if (serviceFeeFactor * payRateUsd > 50)
            {
                serviceFeeFactor = 50 / payRateUsd;
            }

            var test = payRateUsd / (1 - serviceFeeFactor) / (1 - transactionFeeFactor);
            return test;

        }
        public static int UsdDoubleToIntCents(double totalCharge)
        {
            var cents = totalCharge * 100;
            return Convert.ToInt32(cents);
        }
    }
}
