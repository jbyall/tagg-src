﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class EntityResult<T> where T : TaggEntity
    {
        public EntityResult()
        {
            this.Errors = new List<string>();
            this.Succeeded = true;
        }

        public EntityResult(T entity)
            : this()
        {
            this.Entity = entity;
        }

        public EntityResult(string errorMessage)
            : this()
        {
            this.AddError(errorMessage);
        }

        public T Entity { get; set; }
        public bool Succeeded { get; set; }
        public List<string> Errors { get; private set; }

        public void AddError(string errorMessage)
        {
            this.Errors.Add(errorMessage);
            this.Succeeded = false;
        }

        public EntityResult ToGenericResult()
        {
            var result = new EntityResult();
            this.Errors.ForEach(e => result.AddError(e));
            return result;
        }
    }

    public class EntityResult
    {
        public EntityResult()
        {
            this.Errors = new List<string>();
            this.Succeeded = true;
        }

        public EntityResult(string errorMessage)
            : this()
        {
            this.AddError(errorMessage);
        }

        public bool Succeeded { get; set; }
        public List<string> Errors { get; private set; }

        public void AddError(string errorMessage)
        {
            this.Errors.Add(errorMessage);
            this.Succeeded = false;
        }
    }
}
