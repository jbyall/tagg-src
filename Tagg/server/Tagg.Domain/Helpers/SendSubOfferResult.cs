﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.Helpers
{
    public class SendSubOfferResult
    {
        public bool Succeeded { get; set; }
        public string SmsID { get; set; }
    }
}
