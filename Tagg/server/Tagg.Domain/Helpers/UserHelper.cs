﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Tagg.Domain
{
    public static class UserHelper
    {
        public static UserType GetUserTypeFromClaim(List<MongoUserClaim> claims)
        {
            var accessClaim = claims.FirstOrDefault(c => c.Type == TaggClaimTypes.ApiAccess);
            if (accessClaim != null)
            {
                switch (accessClaim.Value)
                {
                    case ApiAccessLevels.Substitute:
                        return UserType.Substitute;
                    case ApiAccessLevels.Teacher:
                        return UserType.Teacher;
                    case ApiAccessLevels.SchoolAdmin:
                        return UserType.SchoolAdmin;
                    case ApiAccessLevels.DistrictAdmin:
                        return UserType.DistrictAdmin;
                    case ApiAccessLevels.SystemAdmin:
                        return UserType.SystemAdmin;
                    default:
                        break;
                }
            }
            return UserType.Substitute;
        }

        public static UserType GetUserTypeFromClaim(IEnumerable<Claim> claims)
        {
            var accessClaim = claims.FirstOrDefault(c => c.Type == TaggClaimTypes.ApiAccess);
            if (accessClaim != null)
            {
                switch (accessClaim.Value)
                {
                    case ApiAccessLevels.Substitute:
                        return UserType.Substitute;
                    case ApiAccessLevels.Teacher:
                        return UserType.Teacher;
                    case ApiAccessLevels.SchoolAdmin:
                        return UserType.SchoolAdmin;
                    case ApiAccessLevels.DistrictAdmin:
                        return UserType.DistrictAdmin;
                    case ApiAccessLevels.SystemAdmin:
                        return UserType.SystemAdmin;
                    default:
                        break;
                }
            }
            return UserType.Substitute;
        }
    }
}
