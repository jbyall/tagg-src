﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Tagg.Domain
{
    public enum UserType
    {
        Substitute,
        Teacher,
        [Display(Name = "School Admin")]
        SchoolAdmin,
        [Display(Name = "District Admin")]
        DistrictAdmin,
        [Display(Name = "System Admin")]
        SystemAdmin
    }

    public enum JobStatus
    {
        Open,
        // Job has been accepted by a sub
        Accepted,
        // Job has been cancelled
        Cancelled,
        // Job is done
        Complete,
    }

    public enum JobTransactionStatus
    {
        Upcoming = 0,
        Created = 1,
        Cancelled = 2,
        Complete = 3
    }

    public enum JobOfferStatus
    {
        Sent,
        Accepted,
        Declined,
        Expired,
        Failed,
        Cancelled
    }

    public enum TaggStatus
    {
        Disabled = 0,
        Active = 1,
        Pending = 2,
        Complete = 3,
        New = 4,
    }

    public enum AccountStatus
    {
        New = 0,
        Verified = 1,
        Hold = 2
    }

    public enum TransactionStatus
    {
        Hold = 0,
        Pending = 1,
        Succeeded = 2,
        Failed = 3,
        Cancelled = 4
    }

    public enum MessageType
    {
        System = 0,
        UserUser = 1,
        JobOffer = 2
    }

    public enum EducationLevel
    {
        None = 0,
        AssociatesDegree = 1,
        BachelorsDegree = 2,
        DoctoralDegree = 3,
        MastersDegree = 4,
    }

    public enum GradeSubject
    {
        PreK = 0,
        Kinder = 1,
        FirstGrade = 2,
        SecondGrade = 3,
        ThirdGrade = 4,
        ForthGrade = 5,
        FifthSixthGrade = 6,
        MSMath = 7,
        MSEnglish = 8,
        MSSocialStudies = 9,
        MSScience = 10,
        MSElectives = 11,
        HSMath = 12,
        HSEnglish = 13,
        HSSocialStudies = 14,
        HSScience = 15,
        HSElecties = 16
    }

    public enum ContactMethod
    {
        Email=0,
        SMS=1,
        Phone=2
    }

    public enum EmailTemplateType
    {
        EmailConfirmation=0,
        TeacherWelcome=1,
        SubWelcome=2,
        SchoolSetup=3,
        SchoolInquiry=4,
        JobRequest=5,
        JobAcceptance=6,
        PasswordReset=7,
        ChangeEmail=8,
        JobCancelledBySub=9,
        JobCancelledByTeacher=10
    }

    public enum SendToOption
    {
        User = 1,
        School = 2,
        District = 3,
        AllUsers = 4
    }
}
