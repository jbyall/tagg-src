﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;

namespace Tagg.Domain
{
    public static class MongoIdentityBuilderExtensions
    {
        /// <summary>
        ///     This method only registers mongo stores, you also need to call AddIdentity.
        ///     Consider using AddIdentityWithMongoStores.
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="connectionString">Must contain the database name</param>
        public static IdentityBuilder RegisterMongoStores<TUser, TRole>(this IdentityBuilder builder, string connectionString, MongoClient client)
            where TRole : MongoRole
            where TUser : MongoUser
        {
            var url = new MongoUrl(connectionString);
            //var client = new MongoClient(url);
            if (url.DatabaseName == null)
            {
                throw new ArgumentException("Your connection string must contain a database name", connectionString);
            }
            var database = client.GetDatabase(url.DatabaseName);
            return builder.RegisterMongoStores(
                p => database.GetCollection<TUser>("users"),
                p => database.GetCollection<TRole>("roles"));
        }

        /// <summary>
        ///     If you want control over creating the users and roles collections, use this overload.
        ///     This method only registers mongo stores, you also need to call AddIdentity.
        /// </summary>
        /// <typeparam name="TUser"></typeparam>
        /// <typeparam name="TRole"></typeparam>
        /// <param name="builder"></param>
        /// <param name="usersCollectionFactory"></param>
        /// <param name="rolesCollectionFactory"></param>
        public static IdentityBuilder RegisterMongoStores<TUser, TRole>(this IdentityBuilder builder,
            Func<IServiceProvider, IMongoCollection<TUser>> usersCollectionFactory,
            Func<IServiceProvider, IMongoCollection<TRole>> rolesCollectionFactory)
            where TRole : MongoRole
            where TUser : MongoUser
        {
            if (typeof(TUser) != builder.UserType)
            {
                var message = "User type passed to RegisterMongoStores must match user type passed to AddIdentity. "
                              + $"You passed {builder.UserType} to AddIdentity and {typeof(TUser)} to RegisterMongoStores, "
                              + "these do not match.";
                throw new ArgumentException(message);
            }
            if (typeof(TRole) != builder.RoleType)
            {
                var message = "Role type passed to RegisterMongoStores must match role type passed to AddIdentity. "
                              + $"You passed {builder.RoleType} to AddIdentity and {typeof(TRole)} to RegisterMongoStores, "
                              + "these do not match.";
                throw new ArgumentException(message);
            }
            builder.Services.AddSingleton<IUserStore<TUser>>(p => new MongoUserStore<TUser>(usersCollectionFactory(p)));
            builder.Services.AddSingleton<IRoleStore<TRole>>(p => new MongoRoleStore<TRole>(rolesCollectionFactory(p)));
            return builder;
        }

        /// <summary>
        ///     This method registers identity services and MongoDB stores using the MongoUser and MongoRole types.
        /// </summary>
        /// <param name="services"></param>
        /// <param name="connectionString">Connection string must contain the database name</param>
        public static IdentityBuilder AddIdentityWithMongoStores(this IServiceCollection services, string connectionString, MongoClient client)
        {
            return services.AddIdentityWithMongoStoresUsingCustomTypes<MongoUser, MongoRole>(connectionString, client);
        }

        /// <summary>
        ///     This method allows you to customize the user and role type when registering identity services
        ///     and MongoDB stores.
        /// </summary>
        /// <typeparam name="TUser"></typeparam>
        /// <typeparam name="TRole"></typeparam>
        /// <param name="services"></param>
        /// <param name="connectionString">Connection string must contain the database name</param>
        public static IdentityBuilder AddIdentityWithMongoStoresUsingCustomTypes<TUser, TRole>(this IServiceCollection services, string connectionString, MongoClient client)
            where TUser : MongoUser
            where TRole : MongoRole
        {
            return services.AddIdentity<TUser, TRole>()
                .RegisterMongoStores<TUser, TRole>(connectionString, client);
        }
    }
    public class MongoRole
    {
        public MongoRole()
        {
            Id = ObjectId.GenerateNewId().ToString();
        }

        public MongoRole(string roleName) : this()
        {
            Name = roleName;
        }

        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }

        public string Name { get; set; }

        public string NormalizedName { get; set; }

        public override string ToString() => Name;
    }

   

    public class MongoUserClaim
    {
        public MongoUserClaim()
        {
        }

        public MongoUserClaim(Claim claim)
        {
            Type = claim.Type;
            Value = claim.Value;
        }

        /// <summary>
        /// Claim type
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Claim value
        /// </summary>
        public string Value { get; set; }

        public Claim ToSecurityClaim()
        {
            return new Claim(Type, Value);
        }
    }

    public class MongoUserLogin
    {
        public MongoUserLogin(string loginProvider, string providerKey, string providerDisplayName)
        {
            LoginProvider = loginProvider;
            ProviderDisplayName = providerDisplayName;
            ProviderKey = providerKey;
        }

        public MongoUserLogin(UserLoginInfo login)
        {
            LoginProvider = login.LoginProvider;
            ProviderDisplayName = login.ProviderDisplayName;
            ProviderKey = login.ProviderKey;
        }

        public string LoginProvider { get; set; }
        public string ProviderDisplayName { get; set; }
        public string ProviderKey { get; set; }

        public UserLoginInfo ToUserLoginInfo()
        {
            return new UserLoginInfo(LoginProvider, ProviderKey, ProviderDisplayName);
        }
    }

    public class MongoUserToken
    {
        /// <summary>
        /// The provider that the token came from.
        /// </summary>
        public string LoginProvider { get; set; }

        /// <summary>
        /// The name of the token.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The value of the token.
        /// </summary>
        public string Value { get; set; }
    }

    public class MongoRoleStore<TRole> : IQueryableRoleStore<TRole>
        where TRole : MongoRole
    {
        private readonly IMongoCollection<TRole> _Roles;

        public MongoRoleStore(IMongoCollection<TRole> roles)
        {
            _Roles = roles;
        }

        public virtual void Dispose()
        {
            // no need to dispose of anything, mongodb handles connection pooling automatically
        }

        public virtual async Task<IdentityResult> CreateAsync(TRole role, CancellationToken token)
        {
            await _Roles.InsertOneAsync(role, cancellationToken: token);
            return IdentityResult.Success;
        }

        public virtual async Task<IdentityResult> UpdateAsync(TRole role, CancellationToken token)
        {
            var result = await _Roles.ReplaceOneAsync(r => r.Id == role.Id, role, cancellationToken: token);
            return IdentityResult.Success;
        }

        public virtual async Task<IdentityResult> DeleteAsync(TRole role, CancellationToken token)
        {
            var result = await _Roles.DeleteOneAsync(r => r.Id == role.Id, token);
            return IdentityResult.Success;
        }

        public virtual async Task<string> GetRoleIdAsync(TRole role, CancellationToken cancellationToken)
            => role.Id;

        public virtual async Task<string> GetRoleNameAsync(TRole role, CancellationToken cancellationToken)
            => role.Name;

        public virtual async Task SetRoleNameAsync(TRole role, string roleName, CancellationToken cancellationToken)
            => role.Name = roleName;

        // note: can't test as of yet through integration testing because the Identity framework doesn't use this method internally anywhere
        public virtual async Task<string> GetNormalizedRoleNameAsync(TRole role, CancellationToken cancellationToken)
            => role.NormalizedName;

        public virtual async Task SetNormalizedRoleNameAsync(TRole role, string normalizedName, CancellationToken cancellationToken)
            => role.NormalizedName = normalizedName;

        public virtual Task<TRole> FindByIdAsync(string roleId, CancellationToken token)
            => _Roles.Find(r => r.Id == roleId)
                .FirstOrDefaultAsync(token);

        public virtual Task<TRole> FindByNameAsync(string normalizedName, CancellationToken token)
            => _Roles.Find(r => r.NormalizedName == normalizedName)
                .FirstOrDefaultAsync(token);

        public virtual IQueryable<TRole> Roles
            => _Roles.AsQueryable();
    }

    public class MongoUserStore<TUser> :
            IUserPasswordStore<TUser>,
            IUserRoleStore<TUser>,
            IUserLoginStore<TUser>,
            IUserSecurityStampStore<TUser>,
            IUserEmailStore<TUser>,
            IUserClaimStore<TUser>,
            IUserPhoneNumberStore<TUser>,
            IUserTwoFactorStore<TUser>,
            IUserLockoutStore<TUser>,
            IQueryableUserStore<TUser>,
            IUserAuthenticationTokenStore<TUser>
        where TUser : MongoUser
    {
        private readonly IMongoCollection<TUser> _Users;

        public MongoUserStore(IMongoCollection<TUser> users)
        {
            _Users = users;
        }

        public virtual void Dispose()
        {
            // no need to dispose of anything, mongodb handles connection pooling automatically
        }

        public virtual async Task<IdentityResult> CreateAsync(TUser user, CancellationToken token)
        {
            await _Users.InsertOneAsync(user, cancellationToken: token);
            return IdentityResult.Success;
        }

        public virtual async Task<IdentityResult> UpdateAsync(TUser user, CancellationToken token)
        {
            await _Users.ReplaceOneAsync(u => u.Id == user.Id, user, cancellationToken: token);
            return IdentityResult.Success;
        }

        public virtual async Task<IdentityResult> DeleteAsync(TUser user, CancellationToken token)
        {
            await _Users.DeleteOneAsync(u => u.Id == user.Id, token);
            return IdentityResult.Success;
        }

        public virtual async Task<string> GetUserIdAsync(TUser user, CancellationToken cancellationToken)
            => user.Id;

        public virtual async Task<string> GetUserNameAsync(TUser user, CancellationToken cancellationToken)
            => user.UserName;

        public virtual async Task SetUserNameAsync(TUser user, string userName, CancellationToken cancellationToken)
            => user.UserName = userName;

        // note: again this isn't used by Identity framework so no way to integration test it
        public virtual async Task<string> GetNormalizedUserNameAsync(TUser user, CancellationToken cancellationToken)
            => user.NormalizedUserName;

        public virtual async Task SetNormalizedUserNameAsync(TUser user, string normalizedUserName, CancellationToken cancellationToken)
            => user.NormalizedUserName = normalizedUserName;

        public virtual Task<TUser> FindByIdAsync(string userId, CancellationToken token)
            => IsObjectId(userId)
                ? _Users.Find(u => u.Id == userId).FirstOrDefaultAsync(token)
                : Task.FromResult<TUser>(null);

        private bool IsObjectId(string id)
        {
            ObjectId temp;
            return ObjectId.TryParse(id, out temp);
        }

        public virtual Task<TUser> FindByNameAsync(string normalizedUserName, CancellationToken token)
            => _Users.Find(u => u.NormalizedUserName == normalizedUserName).FirstOrDefaultAsync(token);

        public virtual async Task SetPasswordHashAsync(TUser user, string passwordHash, CancellationToken token)
            => user.PasswordHash = passwordHash;

        public virtual async Task<string> GetPasswordHashAsync(TUser user, CancellationToken token)
            => user.PasswordHash;

        public virtual async Task<bool> HasPasswordAsync(TUser user, CancellationToken token)
            => user.HasPassword();

        public virtual async Task AddToRoleAsync(TUser user, string normalizedRoleName, CancellationToken token)
            => user.AddRole(normalizedRoleName);

        public virtual async Task RemoveFromRoleAsync(TUser user, string normalizedRoleName, CancellationToken token)
            => user.RemoveRole(normalizedRoleName);

        // note: might have issue, I'm just storing Normalized only now, so I'm returning normalized here instead of not normalized.
        // EF provider returns not noramlized here
        // however, the rest of the API uses normalized (add/remove/isinrole) so maybe this approach is better anyways
        // note: could always map normalized to not if people complain
        public virtual async Task<IList<string>> GetRolesAsync(TUser user, CancellationToken token)
            => user.Roles;

        public virtual async Task<bool> IsInRoleAsync(TUser user, string normalizedRoleName, CancellationToken token)
            => user.Roles.Contains(normalizedRoleName);

        public virtual async Task<IList<TUser>> GetUsersInRoleAsync(string normalizedRoleName, CancellationToken token)
            => await _Users.Find(u => u.Roles.Contains(normalizedRoleName))
                .ToListAsync(token);

        public virtual async Task AddLoginAsync(TUser user, UserLoginInfo login, CancellationToken token)
            => user.AddLogin(login);

        public virtual async Task RemoveLoginAsync(TUser user, string loginProvider, string providerKey, CancellationToken cancellationToken = default(CancellationToken))
            => user.RemoveLogin(loginProvider, providerKey);

        public virtual async Task<IList<UserLoginInfo>> GetLoginsAsync(TUser user, CancellationToken token)
            => user.Logins
                .Select(l => l.ToUserLoginInfo())
                .ToList();

        public virtual Task<TUser> FindByLoginAsync(string loginProvider, string providerKey, CancellationToken cancellationToken = default(CancellationToken))
            => _Users
                .Find(u => u.Logins.Any(l => l.LoginProvider == loginProvider && l.ProviderKey == providerKey))
                .FirstOrDefaultAsync(cancellationToken);

        public virtual async Task SetSecurityStampAsync(TUser user, string stamp, CancellationToken token)
            => user.SecurityStamp = stamp;

        public virtual async Task<string> GetSecurityStampAsync(TUser user, CancellationToken token)
            => user.SecurityStamp;

        public virtual async Task<bool> GetEmailConfirmedAsync(TUser user, CancellationToken token)
            => user.EmailConfirmed;

        public virtual async Task SetEmailConfirmedAsync(TUser user, bool confirmed, CancellationToken token)
            => user.EmailConfirmed = confirmed;

        public virtual async Task SetEmailAsync(TUser user, string email, CancellationToken token)
            => user.Email = email;

        public virtual async Task<string> GetEmailAsync(TUser user, CancellationToken token)
            => user.Email;

        // note: no way to intergation test as this isn't used by Identity framework	
        public virtual async Task<string> GetNormalizedEmailAsync(TUser user, CancellationToken cancellationToken)
            => user.NormalizedEmail;

        public virtual async Task SetNormalizedEmailAsync(TUser user, string normalizedEmail, CancellationToken cancellationToken)
            => user.NormalizedEmail = normalizedEmail;

        public virtual Task<TUser> FindByEmailAsync(string normalizedEmail, CancellationToken token)
        {
            // note: I don't like that this now searches on normalized email :(... why not FindByNormalizedEmailAsync then?
            return _Users.Find(u => u.NormalizedEmail == normalizedEmail).FirstOrDefaultAsync(token);
        }

        public virtual async Task<IList<Claim>> GetClaimsAsync(TUser user, CancellationToken token)
            => user.Claims.Select(c => c.ToSecurityClaim()).ToList();

        public virtual Task AddClaimsAsync(TUser user, IEnumerable<Claim> claims, CancellationToken token)
        {
            foreach (var claim in claims)
            {
                user.AddClaim(claim);
            }
            return Task.FromResult(0);
        }

        public virtual Task RemoveClaimsAsync(TUser user, IEnumerable<Claim> claims, CancellationToken token)
        {
            foreach (var claim in claims)
            {
                user.RemoveClaim(claim);
            }
            return Task.FromResult(0);
        }

        public virtual async Task ReplaceClaimAsync(TUser user, Claim claim, Claim newClaim, CancellationToken cancellationToken = default(CancellationToken))
        {
            user.ReplaceClaim(claim, newClaim);
        }

        public virtual Task SetPhoneNumberAsync(TUser user, string phoneNumber, CancellationToken token)
        {
            user.PhoneNumber = phoneNumber;
            return Task.FromResult(0);
        }

        public virtual Task<string> GetPhoneNumberAsync(TUser user, CancellationToken token)
        {
            return Task.FromResult(user.PhoneNumber);
        }

        public virtual Task<bool> GetPhoneNumberConfirmedAsync(TUser user, CancellationToken token)
        {
            return Task.FromResult(user.PhoneNumberConfirmed);
        }

        public virtual Task SetPhoneNumberConfirmedAsync(TUser user, bool confirmed, CancellationToken token)
        {
            user.PhoneNumberConfirmed = confirmed;
            return Task.FromResult(0);
        }

        public virtual Task SetTwoFactorEnabledAsync(TUser user, bool enabled, CancellationToken token)
        {
            user.TwoFactorEnabled = enabled;
            return Task.FromResult(0);
        }

        public virtual Task<bool> GetTwoFactorEnabledAsync(TUser user, CancellationToken token)
        {
            return Task.FromResult(user.TwoFactorEnabled);
        }

        public virtual async Task<IList<TUser>> GetUsersForClaimAsync(Claim claim, CancellationToken cancellationToken = default(CancellationToken))
        {
            return await _Users
                .Find(u => u.Claims.Any(c => c.Type == claim.Type && c.Value == claim.Value))
                .ToListAsync(cancellationToken);
        }

        public virtual Task<DateTimeOffset?> GetLockoutEndDateAsync(TUser user, CancellationToken token)
        {
            DateTimeOffset? dateTimeOffset = user.LockoutEndDateUtc;
            return Task.FromResult(dateTimeOffset);
        }

        public virtual Task SetLockoutEndDateAsync(TUser user, DateTimeOffset? lockoutEnd, CancellationToken token)
        {
            user.LockoutEndDateUtc = lockoutEnd?.UtcDateTime;
            return Task.FromResult(0);
        }

        public virtual Task<int> IncrementAccessFailedCountAsync(TUser user, CancellationToken token)
        {
            user.AccessFailedCount++;
            return Task.FromResult(user.AccessFailedCount);
        }

        public virtual Task ResetAccessFailedCountAsync(TUser user, CancellationToken token)
        {
            user.AccessFailedCount = 0;
            return Task.FromResult(0);
        }

        public virtual async Task<int> GetAccessFailedCountAsync(TUser user, CancellationToken token)
            => user.AccessFailedCount;

        public virtual async Task<bool> GetLockoutEnabledAsync(TUser user, CancellationToken token)
            => user.LockoutEnabled;

        public virtual async Task SetLockoutEnabledAsync(TUser user, bool enabled, CancellationToken token)
            => user.LockoutEnabled = enabled;

        public virtual IQueryable<TUser> Users => _Users.AsQueryable();

        public virtual async Task SetTokenAsync(TUser user, string loginProvider, string name, string value, CancellationToken cancellationToken)
            => user.SetToken(loginProvider, name, value);

        public virtual async Task RemoveTokenAsync(TUser user, string loginProvider, string name, CancellationToken cancellationToken)
            => user.RemoveToken(loginProvider, name);

        public virtual async Task<string> GetTokenAsync(TUser user, string loginProvider, string name, CancellationToken cancellationToken)
            => user.GetTokenValue(loginProvider, name);
    }

    public static class IndexChecks
    {
        public static void EnsureUniqueIndexOnNormalizedUserName<TUser>(IMongoCollection<TUser> users)
            where TUser : MongoUser
        {
            var userName = Builders<TUser>.IndexKeys.Ascending(t => t.NormalizedUserName);
            var unique = new CreateIndexOptions { Unique = true };
            users.Indexes.CreateOneAsync(userName, unique);
        }

        public static void EnsureUniqueIndexOnNormalizedRoleName<TRole>(IMongoCollection<TRole> roles)
            where TRole : MongoRole
        {
            var roleName = Builders<TRole>.IndexKeys.Ascending(t => t.NormalizedName);
            var unique = new CreateIndexOptions { Unique = true };
            roles.Indexes.CreateOneAsync(roleName, unique);
        }

        public static void EnsureUniqueIndexOnNormalizedEmail<TUser>(IMongoCollection<TUser> users)
            where TUser : MongoUser
        {
            var email = Builders<TUser>.IndexKeys.Ascending(t => t.NormalizedEmail);
            var unique = new CreateIndexOptions { Unique = true };
            users.Indexes.CreateOneAsync(email, unique);
        }

        /// <summary>
        ///     ASP.NET Core Identity now searches on normalized fields so these indexes are no longer required, replace with
        ///     normalized checks.
        /// </summary>
        public static class OptionalIndexChecks
        {
            public static void EnsureUniqueIndexOnUserName<TUser>(IMongoCollection<TUser> users)
                where TUser : MongoUser
            {
                var userName = Builders<TUser>.IndexKeys.Ascending(t => t.UserName);
                var unique = new CreateIndexOptions { Unique = true };
                users.Indexes.CreateOneAsync(userName, unique);
            }

            public static void EnsureUniqueIndexOnRoleName<TRole>(IMongoCollection<TRole> roles)
                where TRole : MongoRole
            {
                var roleName = Builders<TRole>.IndexKeys.Ascending(t => t.Name);
                var unique = new CreateIndexOptions { Unique = true };
                roles.Indexes.CreateOneAsync(roleName, unique);
            }

            public static void EnsureUniqueIndexOnEmail<TUser>(IMongoCollection<TUser> users)
                where TUser : MongoUser
            {
                var email = Builders<TUser>.IndexKeys.Ascending(t => t.Email);
                var unique = new CreateIndexOptions { Unique = true };
                users.Indexes.CreateOneAsync(email, unique);
            }
        }
    }
}
