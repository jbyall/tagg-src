﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.Helpers
{
    public class TeacherSchoolResult
    {
        public School School { get; set; }
        public Teacher Teacher { get; set; }
    }

    public class SchoolTeachersResult
    {
        public School School { get; set; }
        public List<Teacher> Teachers { get; set; }
    }
}
