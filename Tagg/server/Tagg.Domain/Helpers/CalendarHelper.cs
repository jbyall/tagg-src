﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Tagg.Domain
{
    public class CalendarHelper
    {
        public static string MapDayIndex(DayOfWeek dayOfWeek)
        {
            switch (dayOfWeek)
            {
                case DayOfWeek.Sunday:
                    return "Su";
                case DayOfWeek.Monday:
                    return "M";
                case DayOfWeek.Tuesday:
                    return "T";
                case DayOfWeek.Wednesday:
                    return "W";
                case DayOfWeek.Thursday:
                    return "Th";
                case DayOfWeek.Friday:
                    return "F";
                case DayOfWeek.Saturday:
                    return "Sa";
                default:
                    return "";
            }
        }

        public static List<CalendarDay> GetCalendarMonth(int month, int year)
        {
            var result = new List<CalendarDay>();
            var loopDate = new DateTime(year, month, 1);
            result.AddRange(getPlaceholderDays(loopDate));

            // set the first day to the first monday
            while (loopDate.DayOfWeek == DayOfWeek.Sunday || loopDate.DayOfWeek == DayOfWeek.Saturday)
            {
                loopDate = loopDate.AddDays(1);
            }

            
            while (loopDate.Month == month)// || (loopDate.DayOfWeek != DayOfWeek.Saturday && loopDate.DayOfWeek != DayOfWeek.Sunday))
            {
                if (loopDate.DayOfWeek != DayOfWeek.Saturday && loopDate.DayOfWeek != DayOfWeek.Sunday)
                {
                    result.Add(new CalendarDay(loopDate));
                }
                loopDate = loopDate.AddDays(1);
            }

            return result;
        }

        private static IEnumerable<CalendarDay> getPlaceholderDays(DateTime startDate)
        {
            var emptyResult = new List<CalendarDay>();
            
            switch (startDate.DayOfWeek)
            {
                case DayOfWeek.Saturday:
                case DayOfWeek.Sunday:
                case DayOfWeek.Monday:
                    return emptyResult;
                case DayOfWeek.Tuesday:
                    return Enumerable.Repeat(new CalendarDay(), 1);
                case DayOfWeek.Wednesday:
                    return Enumerable.Repeat(new CalendarDay(), 2);
                case DayOfWeek.Thursday:
                    return Enumerable.Repeat(new CalendarDay(), 3);
                case DayOfWeek.Friday:
                    return Enumerable.Repeat(new CalendarDay(), 4);
                default:
                    return emptyResult;
            }
        }
    }
}
