﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Tagg.Domain
{
    public class SendResult : ISendResult
    {

        public SendResult(HttpStatusCode statusCode)
        {
            this.Status = statusCode;
        }
        public HttpStatusCode Status { get; private set; }
        public bool IsSuccessStatusCode
        {
            get { return ((int)this.Status >= 200) && ((int)this.Status <= 299); }
        }
    }

    public class BatchSendResult
    {
        public BatchSendResult()
        {
            this.FailedIds = new List<string>();
        }
        public bool AllSucceeded { get; set; } = true;
        public List<string> FailedIds { get; set; }
        public string SMSId { get; set; }
    }
}
