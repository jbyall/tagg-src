﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Tagg.Domain
{

    public static class CalendarEventType
    {
        public const string Job = "Job";
        public const string Availability = "Availability";
    }

    public static class MimeTypes
    {
        public const string CSV = "text/csv";
        public const string GIF = "image/gif";
        public const string JPEG = "image/jpeg";
        public const string MSDoc = "application/msword";
        public const string MSDocX = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
        public const string PDF = "application/pdf";
        public const string PNG = "image/png";
    }

    public static class Collections
    {
        public const string Blackouts = "blackouts";
        public const string Districts = "districts";
        public const string Emails = "emails";
        public const string EmailTemplates = "emailTemplates";
        public const string Errors = "errors";
        public const string FsChunks = "fs.chunks";
        public const string FsFiles = "fs.files";
        public const string Hooks = "hooks";
        public const string Inquiries = "inquiries";
        public const string Jobs = "jobs";
        public const string JobRatings = "jobRatings";
        public const string Lookups = "lookups";
        public const string OfferQueue = "offerqueue";
        public const string Schools = "schools";
        public const string Subs = "substitutes";
        public const string Teachers = "teachers";
        public const string Transactions = "transactions";
        public const string Users = "users";
    }

    public static class UserStatus
    {
        // Email has not been verified
        public const string Incomplete = "Incomplete";
        // Email has been verified
        public const string New = "New";
        // Profile considered complete and waiting for approval
        public const string Pending = "Pending";
        // Profile has been approved
        public const string Active = "Active";
        // Profile has been disabled
        public const string Disabled = "Disabled";
    }



    public static class LookupTypes
    {
        public const string Subject = "Subject";
        public const string EducationLevel = "EducationLevel";
        public const string State = "State";
        public const string ContactMethod = "ContactMethod";
        public const string SchoolType = "SchoolType";
    }

    public static class SchoolTypes
    {
        public const string Public = "Public";
        public const string PublicCharter = "PublicCharter";
        public const string Private = "Private";
        public const string Other = "Other";
    }

    public static class ContactMethods
    {
        public const string Email = "Email";
        public const string Phone = "Phone";
        public const string SMS = "SMS";
    }

    public static class TaggClaimTypes
    {
        public const string Email = "email";
        public const string FirstName = "given_name";
        public const string LastName = "family_name";
        public const string ProfileComplete = "profile_complete";
        public const string PhoneNumber = "phone_number";
        public const string ApiAccess = "api_access";
        public const string School = "school";
    }

    public static class ApiAccessLevels
    {
        public const string Substitute = "0x1";
        public const string Teacher = "0x2";
        public const string SchoolAdmin = "0x3";
        public const string DistrictAdmin = "0x33";
        public const string SystemAdmin = "0x4";
    }

    public static class AuthPolicies
    {
        public const string TaggUser = "TaggUser";
        public const string Substitute = "Substitute";
        public const string Teacher = "Teacher";
        public const string SchoolAdmin = "SchoolAdmin";
        public const string SystemAdmin = "SystemAdmin";
        public const string AnyAdmin = "AnyAdmin";
    }

    public static class EmailReplacements
    {
        public const string Date = "_DATE_";
        public const string School = "_SCHOOL_";
        public const string Teacher = "_TEACHER_";
        public const string Grade = "_GRADE_";
        public const string Time = "_TIME_";
        public const string Pay = "_PAY_";
        public const string AddressStreet = "_ADDRESS1_";
        public const string AddressCityState = "_ADDRESS2_";
        public const string Href = "_HREF_";
        public const string Sub = "_SUB_";

    }

    public static class SubjectLookup
    {
        public static string Get(GradeSubject subject)
        {
            switch (subject)
            {
                case GradeSubject.PreK:
                    return "Pre-K";
                case GradeSubject.Kinder:
                    return "Kindergarten";
                case GradeSubject.FirstGrade:
                    return "1st Grade";
                case GradeSubject.SecondGrade:
                    return "2nd Grade";
                case GradeSubject.ThirdGrade:
                    return "3rd Grade";
                case GradeSubject.ForthGrade:
                    return "4th Grade";
                case GradeSubject.FifthSixthGrade:
                    return "5th-6th Grade";
                case GradeSubject.MSMath:
                    return "6th-8th Grade Math";
                case GradeSubject.MSEnglish:
                    return "6th-8th Grade English";
                case GradeSubject.MSSocialStudies:
                    return "6th-8th Grade Social Studies";
                case GradeSubject.MSScience:
                    return "6th-8th Grade Science";
                case GradeSubject.MSElectives:
                    return "6th-8th Grade Electives";
                case GradeSubject.HSMath:
                    return "9th-12th Grade Math";
                case GradeSubject.HSEnglish:
                    return "9th-12th Grade English";
                case GradeSubject.HSSocialStudies:
                    return "9th-12th Grade Social Studies";
                case GradeSubject.HSScience:
                    return "9th-12th Grade Science";
                case GradeSubject.HSElecties:
                    return "9th-12th Grade Electives";
                default:
                    return "";
            }
        }
    }

}
