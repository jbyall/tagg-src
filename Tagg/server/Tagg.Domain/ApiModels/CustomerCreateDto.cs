﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class CustomerCreateDto
    {
        public string SourceTokenId { get; set; }
        public string Description { get; set; }
        public string SchoolId { get; set; }
    }
}
