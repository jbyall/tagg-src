﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class JobOfferDetailsDto
    {
        public JobOfferDetailsDto()
        {
            this.Selected = new List<string>();
            this.Favorites = new List<string>();
            this.Others = new List<string>();
        }
        public JobCreateDto Job { get; set; }
        public List<string> Selected { get; set; }
        public List<string> Favorites { get; set; }
        public List<string> Others { get; set; }
    }
}
