﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class ConnectChargeDto
    {
        public int DestinationAmountCents { get; set; }
        public int TotalAmountCents { get; set; }
        public string Currency { get; set; }
        public string CustomerId { get; set; }
        public string Description { get; set; }
        public string DestinationId { get; set; }
    }

    public class ChargeResponseDto
    {
        public string Id { get; set; }
        public string Status { get; set; }
        public string TransactionId { get; set; }
        public bool Succeeded { get; set; }
    }
}
