﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class AutocompleteUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
    }

    public class AutocompleteSchool
    {
        public string Name { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; }
    }
}
