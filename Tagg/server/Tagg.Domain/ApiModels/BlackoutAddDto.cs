﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class BlackoutAddDto
    {
        public DateTime Day { get; set; }
        public List<string> Schools { get; set; }
    }
}
