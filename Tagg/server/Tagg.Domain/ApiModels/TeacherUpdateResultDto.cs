﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class TeacherUpdateResultDto
    {
        public string PhoneNumber { get; set; }
        public bool VerificationRequired { get; set; }
        public TeacherDto Teacher { get; set; }
    }
}
