﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class SystemDashDto
    {
        public int InquiryCount { get; set; }
        public int ApplicationCount { get; set; }
    }
}
