﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class TeacherDto
    {
        public TeacherDto()
        {
            this.Jobs = new List<string>();
            this.Subjects = new List<GradeSubject>();
            this.Favorites = new List<string>();
        }
        public string Id { get; set; }
        public int UserType { get; set; }
        public string SchoolId { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Picture { get; set; }
        public string Bio { get; set; }
        public string Title { get; set; }
        public bool ReceivePhone { get; set; }
        public bool ReceiveSMS { get; set; }
        public bool ReceiveEmail { get; set; }
        public bool Enabled { get; set; }
        public double PayRate { get; set; }
        public TaggStatus Status { get; set; }
        public bool? PhoneVerified { get; set; }

        public AddressDto Address { get; set; }
        public string FullName { get; set; }
        public List<GradeSubject> Subjects { get; set; }
        public List<string> Jobs { get; set; }
        public List<string> Favorites { get; set; }
    }

    public class TeacherDetailDto : TeacherDto
    {
        public double HoursTaken { get; set; }
        public double HoursCovered { get; set; }
    }
}
