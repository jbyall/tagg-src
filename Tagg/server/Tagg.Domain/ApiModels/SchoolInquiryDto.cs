﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class SchoolInquiryDto
    {
        public string Id { get; set; }
        public string FirstName { get; set; }   // Teacher
        public string LastName { get; set; }    // Teacher
        public string Email { get; set; }       // Teacher
        public string PhoneNumber { get; set; } // Teacher
        public string SchoolName { get; set; }  // School
        public string DistrictName { get; set; } // District
        public string Title { get; set; }       // Teacher
        public string City { get; set; }        // School
        public string State { get; set; }       // School
        public string PostalCode { get; set; }  // School
        public TaggStatus Status { get; set; }
    }

    
}
