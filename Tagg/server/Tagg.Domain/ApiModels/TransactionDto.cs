﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
	public class TransactionDtoContainer
	{
		public TransactionDtoContainer(List<TransactionDto> transactions, int totalCount)
		{
			this.Transactions = transactions;
			this.TotalCount = totalCount;
		}

		public List<TransactionDto> Transactions { get; set; }
		public int TotalCount { get; set; }
	}

	public class TransactionDto
    {
        public string Id { get; set; }
        public string JobId { get; set; }
        public string JobNumber { get; set; }
        public TransactionStatus Status { get; set; }
        public string CustomerId { get; set; }
        public DateTime ProcessOn { get; set; }
        public DateTime JobDate { get; set; }
        public double PayRateUsd { get; set; }
        public double TotalAmountUsd { get; set; }
		public string TeacherId { get; set; }
        public string TeacherName { get; set; }
        public string SubName { get; set; }
    }
}
