﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class JobRatingDto
    {
        public JobRatingDto()
        {
            this.Ratings = new List<RatingDto>();
        }
        public string SubstituteId { get; set; }
        public string JobId { get; set; }
        public string SchoolReviewerId { get; set; }
        public string TeacherId { get; set; }
        public List<RatingDto> Ratings { get; set; }
        public string SubComments { get; set; }
    }

    public class RatingDto
    {
        public string Category { get; set; }
        public int Rating { get; set; }
    }

}
