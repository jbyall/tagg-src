﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class FileResultDto
    {
        public Stream Stream { get; set; }
        public string ContentType { get; set; }
        public bool Succeeded { get; set; } = true;
    }
}
