﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class UserCreateDto
    {
        public UserType UserType { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public bool SendInvitEmail { get; set; }
        public string SchoolId { get; set; }
    }
}
