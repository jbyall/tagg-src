﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class SchoolDto
    {
        public SchoolDto()
        {
            this.Contacts = new List<ContactDto>();
            this.Teachers = new List<TeacherDto>();
            this.Admins = new List<UserBase>();
        }
        public string Id { get; set; }
        public TaggStatus Status { get; set; }
        public string CustomerId { get; set; }
        public AccountStatus AccountStatus { get; set; }
        [Required]
        public string Name { get; set; }
        public string Url { get; set; }
        public string SchoolType { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        [Required]
        public AddressDto Address { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Summary { get; set; }
        public double PayRate { get; set; }
        public string Grades { get; set; }

        //[Required]
        public string DistrictId { get; set; }
        public List<ContactDto> Contacts { get; set; }
        public List<TeacherDto> Teachers { get; set; }
        public List<UserBase> Admins { get; set; }
    }

    public class SchoolDetailDto
    {
        public SchoolDetailDto()
        {

        }
        public SchoolDetailDto(SchoolDto school)
        {
            this.SchoolBase = school;
        }
        public SchoolDto SchoolBase { get; set; }
        public long availableSubCount { get; set; }
    }

    public class SchoolSelectDto
    {
        public string Name { get; set; }
        public string Id { get; set; }
    }
}
