﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class SchoolMetricsDto
    {
        /// <summary>
        /// Number of sub jobs this month
        /// </summary>
        public int? SubDaysMonth { get; set; }

        /// <summary>
        /// Percent of jobs filled for this school year to date
        /// </summary>
        public int? FilledSytdPercent { get; set; }

        /// <summary>
        /// Total days taken off by teachers this month
        /// </summary>
        public int? TeacherDaysMonth { get; set; }

        /// <summary>
        /// Total days taken off by teachers this school year to date
        /// </summary>
        public int? TeacherDaysSytd { get; set; }

        /// <summary>
        /// Days covered by sub this school year to date
        /// </summary>
        public int? SubDaysSytd { get; set; }

        /// <summary>
        /// Average sub rating given from all teachers at school
        /// </summary>
        public double? AvgRating { get; set; }

        /// <summary>
        /// Comma sep. string of sub name for most frequently used subs
        /// </summary>
        public string PopularSubs { get; set; }
    }
}
