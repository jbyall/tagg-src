﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class UserDto
    {
        public UserType UserType { get; set; }
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public string SchoolId { get; set; }
        public bool IsLockedOut { get; set; }
    }

    public class UserPhotoResultDto
    {
        public Stream Stream { get; set; }
        public string ContentType { get; set; }
        public bool Succeeded { get; set; } = true;
    }

    public class ImageUploadModel
    {
        public IFormFile ImageFile { get; set; }
    }
}
