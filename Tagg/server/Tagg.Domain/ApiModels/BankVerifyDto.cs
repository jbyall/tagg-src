﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class BankVerifyDto
    {
        public int AmountOne { get; set; }
        public int AmountTwo { get; set; }
        public string SchoolId { get; set; }
    }
}
