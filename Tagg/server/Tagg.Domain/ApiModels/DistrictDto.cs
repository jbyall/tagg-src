﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class DistrictDto
    {
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
