﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class SchoolAdminDto
    {
        public UserType UserType { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        public string Picture { get; set; }
        public string Title { get; set; }
        public bool Enabled { get; set; }
        public string Status { get; set; }
        public string FullName { get; set; }
        public AddressDto Address { get; set; }
    }

    public class SchoolAdminCreateDto
    {
        [Required]
        public string AdminUserId { get; set; }
    }
}
