﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class SubSearchDto
    {
        
        public DateTime Date { get; set; }
        public string SchoolId { get; set; }
        public string TeacherId { get; set; }
    }

    public class SubSearchResultDto
    {
        public SubSearchResultDto()
        {
            this.Favorites = new List<SubDisplayDto>();
            this.Others = new List<SubDisplayDto>();
        }
        public List<SubDisplayDto> Favorites { get; set; }
        public List<SubDisplayDto> Others { get; set; }
        public int Total { get; set; }
    }
}
