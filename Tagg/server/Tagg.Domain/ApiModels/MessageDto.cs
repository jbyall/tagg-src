﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class AppMessageDto
    {
        public string Id { get; set; }
        [Required]
        public string To { get; set; }
        [Required]
        public string From { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        public string Body { get; set; }
        public DateTime? ReadOn { get; set; }
        public DateTime Created { get; set; }
        //public List<Tuple<string, string>> Headers { get; set; }
    }

    public class MessageThreadDto
    {
        public MessageThreadDto()
        {
            this.Messages = new List<AppMessageDto>();
            this.Audience = new List<string>();
        }
        public string Id { get; set; }
        public string Subject { get; set; }
        public DateTime Created { get; set; }
        public List<AppMessageDto> Messages { get; set; }
        public List<string> Audience { get; set; }
        public string JobId { get; set; }
    }

    public class AdminMessageDto
    {
        [Required]
        public string From { get; set; }
        public SendToOption SendToOption { get; set; }
        [Required]
        public string Body { get; set; }
        [Required]
        public string Subject { get; set; }
        public string SendTo { get; set; }
    }
}
