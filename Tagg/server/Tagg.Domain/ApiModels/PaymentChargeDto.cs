﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class PaymentChargeDto
    {
        public int Amount { get; set; }
        public string Currency { get; set; }
        public string CustomerId { get; set; }
        public string Description { get; set; }
    }
}
