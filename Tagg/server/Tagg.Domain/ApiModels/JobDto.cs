﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class JobDto
    {
        public string Id { get; set; }
        [Required]
        public string TeacherEmail { get; set; }
        public string SubstituteEmail { get; set; }
        public bool SubNeeded { get; set; }

        public string Subject { get; set; }
        public string SchoolName { get; set; }
        public string TeacherName { get; set; }
        public double PayAmount { get; set; }
        public string LessonPlanId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public JobStatus Status { get; set; }
    }

    public class JobCreateDto
    {
        public string Id { get; set; }
        [Required]
        public string TeacherId { get; set; }
        public string SubstituteId { get; set; }
        public string SchoolId { get; set; }
        public string CreatedBy { get; set; }
        public bool SubNeeded { get; set; }
        public string Subject { get; set; }
        public double PayAmount { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public JobStatus Status { get; set; }
    }

	public class JobDetailsDtoContainer
	{
		public	JobDetailsDtoContainer(List<JobDetailsDto> jobs, int totalCount)
		{
			this.Jobs = jobs;
			this.TotalCount = totalCount;
		}

		public List<JobDetailsDto> Jobs { get; set; }
		public int TotalCount { get; set; }
	}

    public class JobDetailsDto
    {
        public JobDetailsDto()
        {
            this.Offers = new List<JobOffer>();
        }
        public JobDetailsDto(School school, Teacher teacher, Job job, Substitute sub = null)
        {
            this.Offers = new List<JobOffer>();
            this.Id = job.Id;
            this.SchoolId = school.Id;
            this.SchoolName = school.Name;
            this.SchoolAddress = school.Address;

            this.TeacherId = teacher.Id;
            this.TeacherName = teacher.FullName;

            this.SubstituteId = sub?.Id;
            this.SubstituteName = sub?.FullName;
            this.SubRating = sub?.Rating;
            this.RatingCount = sub?.RatingCount;
            this.SubbedAtSchool = sub?.SubbedAtSchool;

            this.ChargeAmount = job.ChargeAmount;
            this.Subject = job.Subject;
            this.PayAmount = job.PayAmount;
            this.Status = job.Status;
            this.StartDate = job.StartDate;
            this.EndDate = job.EndDate;
            this.LessonPlanId = job.LessonPlanId;

        }

        // Used for teacher job list
        public JobDetailsDto(Job job, string subName)
        {
            this.Offers = new List<JobOffer>();
            this.Id = job.Id;
            this.Subject = job.Subject;
            this.Status = job.Status;
            this.SubstituteName = subName;
            this.StartDate = job.StartDate;
            this.EndDate = job.EndDate;
        }

        public string Id { get; set; }
        public string SchoolId { get; set; }
        public string SchoolName { get; set; }
        public string LessonPlanId { get; set; }
        public Address SchoolAddress { get; set; }
        public string TeacherId { get; set; }
        public string CreatedBy { get; set; }
        public string TeacherName { get; set; }
        public string SubstituteId { get; set; }
        public string SubstituteName { get; set; }
        public GradeSubject Subject { get; set; }
        public double PayAmount { get; set; }
        public double ChargeAmount { get; set; }
        public double Rating { get; set; }
        public JobStatus Status { get; set; }
        public bool SubNeeded { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<JobOffer> Offers { get; set; }
        public double? SubRating { get; set; } = 3;
        public bool? SubbedAtSchool { get; set; } = false;
        public int? RatingCount { get; set; } = 0;
    }

    public class AllJobsDto
    {
        public AllJobsDto()
        {
            this.Accepted = new List<JobDetailsDto>();
            this.Offered = new List<JobDetailsDto>();
            this.Recent = new List<JobDetailsDto>();
        }
        public List<JobDetailsDto> Accepted { get; set; }
        public List<JobDetailsDto> Offered { get; set; }
        public List<JobDetailsDto> Recent { get; set; }
    }

    public class SchoolJobsDto
    {
        public SchoolJobsDto()
        {
            this.Today = new List<JobDetailsDto>();
            this.Upcoming = new List<JobDetailsDto>();
            this.Recent = new List<JobDetailsDto>();
        }

        public SchoolJobsDto(List<JobDetailsDto> allSchoolJobs, DateTime today)
        {
            Today = allSchoolJobs
                .Where(j => j.StartDate.Date == today.Date)
                .OrderBy(j => j.StartDate)
                .ToList();

            Upcoming = allSchoolJobs
                .Where(j => j.StartDate.Date > today.Date)
                .OrderBy(j => j.StartDate)
                .ToList();

            Recent = allSchoolJobs
                .Where(j => j.StartDate.Date < today.Date)
                .OrderByDescending(j => j.StartDate)
                .ToList();

            //Upcoming = allSchoolJobs
            //    .Where(j => j.StartDate.Date > DateTime.UtcNow.Date)
            //    .OrderBy(j => j.StartDate)
            //    .ToList();

            //Recent = allSchoolJobs
            //    .Where(j => j.StartDate.Date < DateTime.UtcNow.Date)
            //    .OrderBy(j => j.StartDate)
            //    .ToList();
        }

        public List<JobDetailsDto> Today { get; set; }
        public List<JobDetailsDto> Upcoming { get; set; }
        public List<JobDetailsDto> Recent { get; set; }
    }
}
