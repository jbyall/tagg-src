﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class SubCalendarDto
    {
        public SubCalendarDto()
        {
            this.Days = new List<CalendarDay>();
        }
        public List<CalendarDay> Days { get; set; }
    }
}
