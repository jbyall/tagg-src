﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class TeacherMetricsDto
    {
        public int TimeOff { get; set; }
        public int TimeCovered { get; set; }
        public int SubsHired { get; set; }
        public JobDetailsDto RecentJob { get; set; }
        public string FavoriteSub { get; set; }
    }
}
