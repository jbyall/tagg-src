﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class SubMetricsDto
    {
        public double EarningsLastMonth { get; set; }
        public double EarningsAvgPerMonth { get; set; }
        public double Rating { get; set; }
        public int DaysSubbing { get; set; }
        public int TotalSchools { get; set; }
        public string FavoriteSchool { get; set; }
        public GradeSubject FavoriteGrade { get; set; }
        public SubstituteDto Sub { get; set; }
    }
}
