﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class SubstituteDto
    {
        public SubstituteDto()
        {
            this.Subjects = new List<GradeSubject>();
            this.Exclusions = new List<string>();
        }
        // User Base
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Picture { get; set; }
        public string Bio { get; set; }
        public string Title { get; set; }
        public bool ReceivePhone { get; set; }
        public bool ReceiveSMS { get; set; }
        public bool ReceiveEmail { get; set; }
        public bool Enabled { get; set; }
        public TaggStatus Status { get; set; }
        public AddressDto Address { get; set; }
        public string FullName { get; set; }
        public string PaymentId { get; set; }

        // Substitute
        public EducationLevel EducationLevel { get; set; }
        public bool HasTeachingLicense { get; set; }
        public bool HasTranscripts { get; set; }
        public string ResumeFile { get; set; }
        public bool ApplicationComplete { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public string ApprovedBy { get; set; }
        public string LinkedInUrl { get; set; }
        public string Interests { get; set; }
        public int MinPayAmount { get; set; }
        public int MaxDistance { get; set; }
        public double Rating { get; set; }
        public int RatingCount { get; set; }
        public bool Deactivated { get; set; }

        public List<GradeSubject> Subjects { get; set; }
        public List<string> Exclusions { get; set; }
    }

    public class SubDisplayDto
    {
        public SubDisplayDto()
        {
            this.Subjects = new List<GradeSubject>();
            this.Exclusions = new List<string>();
        }
        // Not on SubstituteDto
        public bool IsFavorite { get; set; }

        // User Base
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Picture { get; set; }
        public string Bio { get; set; }
        public string Title { get; set; }
        public bool ReceivePhone { get; set; }
        public bool ReceiveSMS { get; set; }
        public bool ReceiveEmail { get; set; }
        public bool Enabled { get; set; }
        public TaggStatus Status { get; set; }
        public AddressDto Address { get; set; }
        public string FullName { get; set; }

        // Substitute
        public string EducationLevel { get; set; }
        public bool HasTeachingLicense { get; set; }
        public bool HasTranscripts { get; set; }
        public string ResumeFile { get; set; }
        public bool ApplicationComplete { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public string ApprovedBy { get; set; }
        public string LinkedInUrl { get; set; }
        public string Interests { get; set; }
        public int MinPayAmount { get; set; }
        public int MaxDistance { get; set; }
        public double Rating { get; set; }
        public bool SubbedAtSchool { get; set; }
        public int RatingCount { get; set; }

        public List<GradeSubject> Subjects { get; set; }
        public List<string> Exclusions { get; set; }
    }
}
