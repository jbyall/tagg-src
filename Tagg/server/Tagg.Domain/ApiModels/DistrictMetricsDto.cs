﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class DistrictMetricsDto
    {
        /// <summary>
        /// Days taken off by teachers this month
        /// </summary>
        public int TeacherDaysMonth { get; set; }
        /// <summary>
        /// Days taken off by teachers this school year to date
        /// </summary>
        public int TeacherDaysSytd { get; set; }
        /// <summary>
        /// Days covered by sub this month
        /// </summary>
        public int SubDaysMonth { get; set; }

        /// <summary>
        /// Days covered by sub this school year
        /// </summary>
        public int SubDaysSytd { get; set; }

        /// <summary>
        /// Number of jobs today with accepted offers
        /// </summary>
        public int JobsFilledToday { get; set; }

        /// <summary>
        /// Number of jobs today that don't have accepted offers
        /// </summary>
        public int JobsUnfilledToday { get; set; }

        /// <summary>
        /// Percent of all jobs filled this school year
        /// </summary>
        public int JobsFilledSytdPercent { get; set; }

        /// <summary>
        /// Average of all teacher ratings given at school
        /// </summary>
        public double AvgRating { get; set; }
    }
}
