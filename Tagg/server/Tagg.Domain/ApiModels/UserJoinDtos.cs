﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain.ApiModels
{
    public class SubUserDto
    {
        public Substitute Sub { get; set; }
        public MongoUser User { get; set; }
    }

    public class TeacherUserDto
    {
        public Teacher Teacher { get; set; }
        public MongoUser User { get; set; }
    }

}
