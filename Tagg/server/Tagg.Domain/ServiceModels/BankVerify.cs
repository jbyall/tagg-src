﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class BankVerify
    {
        public int AmountOne { get; set; }
        public int AmountTwo { get; set; }
        public string CustomerId { get; set; }
        public string BankAccountId { get; set; }
    }
}
