﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class Customer : ICustomer
    {
        public string Id { get; set; }
        public int AccountBalance { get; set; }
        public DateTime Created { get; set; }
        public string Currency { get; set; }
        public bool Delinquent { get; set; }
        public string Description { get; set; }
        public string Email { get; set; }
        public string PaymentSourceId { get; set; }
    }
}
