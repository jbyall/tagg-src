﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class TaggUnitOfWork
    {
        public TaggUnitOfWork(MongoClient client, IConfiguration config, UserManager<MongoUser> userManager = null)
        {
            var dbName = new MongoUrl(config.GetConnectionString("MongoConnection")).DatabaseName;
            var db = client.GetDatabase(dbName);

            this.DB = db;
            this.Emails = new EmailRepo(db);
            this.EmailTemplates = new EmailTemplateRepo(db);
            this.Errors = new ErrorRepo(db);
            this.Inquiries = new InquiriesRepo(db);
            this.Jobs = new JobRepo(db);
            this.JobRatings = new JobRatingRepo(db);
            this.Lookup = new LookupRepo(db);
            this.Offers = new OfferQueueRepo(this.Jobs, db);
            this.Subs = new SubstituteRepo(db);
            this.Schools = new SchoolRepo(db);
            this.Teachers = new TeacherRepo(db);
            this.Transactions = new TransactionRepo(db);
            this.Users = new UserRepo(db, userManager, this.Subs, this.Teachers);
        }


        public JobRepo Jobs { get; set; }
        public LookupRepo Lookup { get; set; }
        public SubstituteRepo Subs { get; set; }
        public SchoolRepo Schools { get; set; }
        public TeacherRepo Teachers { get; set; }
        public UserRepo Users { get; set; }
        public InquiriesRepo Inquiries { get; set; }
        public OfferQueueRepo Offers { get; set; }
        public TransactionRepo Transactions { get; set; }
        public ErrorRepo Errors { get; set; }
        public EmailRepo Emails { get; set; }
        public EmailTemplateRepo EmailTemplates { get; set; }
        public JobRatingRepo JobRatings { get; set; }
        // TODO : Delete these (used for testing only)
        public IMongoCollection<TestWebhookPost> Hooks { get; set; }
        public IMongoDatabase DB { get; private set; }


        //public void LogHook(string content)
        //{
        //    this.Hooks.InsertOne(new TestWebhookPost { Content = content });
        //}

    }

    public class TestWebhookPost
    {
        public DateTime ReceivedOn { get; set; } = DateTime.UtcNow;
        public string EventType { get; set; }
    }
}
