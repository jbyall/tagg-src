﻿using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Linq;
using MongoDB.Driver.GeoJsonObjectModel;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MongoDB.Bson;
using System.Diagnostics;
using Microsoft.AspNetCore.Identity;
using System.Threading.Tasks;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;

namespace Tagg.Domain
{
    public class TaggSeed
    {
        private IMongoDatabase _db;
        private UserManager<MongoUser> _userManager;
        private string _dbName;
        private MongoClient _client;
        private IMongoCollection<School> _schoolsCollection { get; set; }
        private IMongoCollection<District> _districtsCollection { get; set; }
        private IMongoCollection<Substitute> _subsCollection { get; set; }
        private IMongoCollection<Job> _jobsCollection { get; set; }
        private IMongoCollection<Lookups> _lookupsCollection { get; set; }
        private IMongoCollection<MongoUser> _usersCollection { get; set; }
        private IMongoCollection<EmailTemplate> _emailTemplatesCollection { get; set; }

        public TaggSeed(IConfiguration config, UserManager<MongoUser> userManager = null)
        {
            var connectionString = config["ConnectionStrings:MongoConnection"];
            _dbName = MongoUrl.Create(connectionString).DatabaseName;
            _client = new MongoClient(connectionString);
            _db = _client.GetDatabase(_dbName);
            _userManager = userManager;
            this._schoolsCollection = _db.GetCollection<School>("schools");
            this._districtsCollection = _db.GetCollection<District>("districts");
            this._subsCollection = _db.GetCollection<Substitute>("subs");
            this._jobsCollection = _db.GetCollection<Job>("jobs");
            this._lookupsCollection = _db.GetCollection<Lookups>("lookups");
            this._usersCollection = _db.GetCollection<MongoUser>("users");
            this._emailTemplatesCollection = _db.GetCollection<EmailTemplate>(Collections.EmailTemplates);

        }

        public async Task<bool> Seed(string defaultAdminEmail, bool refresh = false)
        {
            if (refresh)
            {
                this._db.DropCollection(Collections.Blackouts);
                this._db.DropCollection(Collections.Districts);
                this._db.DropCollection(Collections.Emails);
                this._db.DropCollection(Collections.EmailTemplates);
                this._db.DropCollection(Collections.FsChunks);
                this._db.DropCollection(Collections.FsFiles);
                this._db.DropCollection(Collections.Hooks);
                this._db.DropCollection(Collections.Inquiries);
                this._db.DropCollection(Collections.Jobs);
                this._db.DropCollection(Collections.JobRatings);
                this._db.DropCollection(Collections.Lookups);
                this._db.DropCollection(Collections.OfferQueue);
                this._db.DropCollection(Collections.Schools);
                this._db.DropCollection(Collections.Subs);
                this._db.DropCollection(Collections.Transactions);
                this._db.DropCollection(Collections.Users);
                this._db.DropCollection(Collections.Errors);
            }

            seedLookups();
            seedEmailTemplates();
            return await seedUsers(defaultAdminEmail);
        }

        public void RefreshEmails()
        {
            this._db.DropCollection(Collections.Emails);
            this.RefreshEmailTemplates();
        }

        public void RefreshEmailTemplates()
        {
            this.seedEmailTemplates(true);
        }

        private void seedEmailTemplates(bool refresh = false)
        {
            if (refresh)
            {
                this._db.DropCollection(Collections.EmailTemplates);
            }
            if (_emailTemplatesCollection.AsQueryable().Count() < 1)
            {
                var teachWelcomeTemplate = "c5b607b1-76a9-4b3f-a378-30b2ed8eceeb";
                var subWelcomeTemplate = "56c2d770-aceb-4f3b-ae09-a0aa3096f361";
                var schoolSetupTemplate = "b5f9a554-f0e0-4e45-84cb-5a98881eb3f2";
                var schoolInquiryTemplate = "55355048-df2a-47bb-b2b2-571efce528f6";
                var emailConfirmationTemplate = "b6bed05c-d87c-4ab9-ab91-b7150ef76c9b";

                var jobRequestTemplate = "826d6528-6e2a-4ea7-9d68-e0ecdb3463fb";
                var jobAcceptance = "795fbfc1-1066-4637-b895-a57b6b2f7897";
                var jobCancelledBySub = "33d6eedf-c1bb-47a7-aa8f-0b00cec59a6f";
                var jobCancelledByTeacher = "27f8b715-dcd0-49ee-8d70-edf659bca037";

                var passwordReset = "2f84e352-ffa7-489e-be85-7c144244a4c9";
                var changeEmail = "7ef9e01c-7232-419e-a1ef-919dd72d6517";
                

                var templates = new List<EmailTemplate>
                {
                    new EmailTemplate
                    {
                        Name = "EmailConfirmation",
                        ExternalId = emailConfirmationTemplate,
                        TemplateType = EmailTemplateType.EmailConfirmation,
                        Replacements = new Dictionary<string, string>
                        {
                            { EmailReplacements.Href ,"" }
                        },
                        From = "noreply@taggeducation.com",
                        Subject = "Tagg Email Confirmation"
                    },
                    new EmailTemplate
                    {
                        Name = "TeacherWelcome",
                        ExternalId = teachWelcomeTemplate,
                        TemplateType = EmailTemplateType.TeacherWelcome,
                        Replacements = new Dictionary<string, string>
                        {
                            {EmailReplacements.Href,"" }
                        },
                        From = "noreply@taggeducation.com",
                        Subject = "Tagg Teacher Welcome"
                    },
                    new EmailTemplate
                    {
                        Name = "SubWelcome",
                        ExternalId = subWelcomeTemplate,
                        TemplateType = EmailTemplateType.SubWelcome,
                        Replacements = new Dictionary<string, string>
                        {
                            {EmailReplacements.Href,"" }
                        },
                        From = "noreply@taggeducation.com",
                        Subject = "Tagg Sub Welcome"
                    },
                    new EmailTemplate
                    {
                        Name = "SchoolSetup",
                        ExternalId = schoolSetupTemplate,
                        TemplateType = EmailTemplateType.SchoolSetup,
                        Replacements = new Dictionary<string, string>
                        {
                            {EmailReplacements.Href,"" }
                        },
                        From = "noreply@taggeducation.com",
                        Subject = "Tagg School Setup"
                    },
                    new EmailTemplate
                    {
                        Name = "SchoolInquiry",
                        ExternalId = schoolInquiryTemplate,
                        TemplateType = EmailTemplateType.SchoolInquiry,
                        From = "noreply@taggeducation.com",
                        Subject = "Tagg School Inquiry"
                    },
                    new EmailTemplate
                    {
                        Name = "JobRequest",
                        ExternalId = jobRequestTemplate,
                        TemplateType = EmailTemplateType.JobRequest,
                        Replacements = new Dictionary<string, string>{
                            {EmailReplacements.Date,"" },
                            {EmailReplacements.School,"" },
                            {EmailReplacements.Teacher,"" },
                            {EmailReplacements.Grade,"" },
                            {EmailReplacements.Time,"" },
                            {EmailReplacements.Pay,"" },
                            {EmailReplacements.AddressStreet,"" },
                            {EmailReplacements.AddressCityState,"" },
                            {EmailReplacements.Href,"" },
                        },
                        From = "noreply@taggeducation.com",
                        Subject = "New Job Request"
                    },
                    new EmailTemplate
                    {
                        Name = "JobAcceptance",
                        ExternalId = jobAcceptance,
                        TemplateType = EmailTemplateType.JobAcceptance,
                        Replacements = new Dictionary<string, string>{
                            {EmailReplacements.Date,"" },
                            {EmailReplacements.School,"" },
                            {EmailReplacements.Teacher,"" },
                            {EmailReplacements.Grade,"" },
                            {EmailReplacements.Time,"" },
                            //{EmailReplacements.Pay,"" },
                            {EmailReplacements.AddressStreet,"" },
                            {EmailReplacements.AddressCityState,"" },
                            {EmailReplacements.Sub,"" },
                        },
                        From = "noreply@taggeducation.com"
                    },
                    new EmailTemplate
                    {
                        Name = "JobCancelledBySub",
                        ExternalId = jobCancelledBySub,
                        TemplateType = EmailTemplateType.JobCancelledBySub,
                        Replacements = new Dictionary<string, string>{
                            {EmailReplacements.Date,"" },
                            {EmailReplacements.School,"" },
                            {EmailReplacements.Teacher,"" },
                            {EmailReplacements.Grade,"" },
                            {EmailReplacements.Time,"" },
                            {EmailReplacements.AddressStreet,"" },
                            {EmailReplacements.AddressCityState,"" },
                            {EmailReplacements.Sub,"" },
                        },
                        From = "noreply@taggeducation.com",
                        Subject = "Your Sub Search Has Restarted"
                    },
                    new EmailTemplate
                    {
                        Name = "JobCancelledByTeacher",
                        ExternalId = jobCancelledByTeacher,
                        TemplateType = EmailTemplateType.JobCancelledByTeacher,
                        Replacements = new Dictionary<string, string>{
                            {EmailReplacements.Date,"" },
                            {EmailReplacements.School,"" },
                            {EmailReplacements.Teacher,"" },
                            {EmailReplacements.Grade,"" },
                            {EmailReplacements.Time,"" },
                            {EmailReplacements.AddressStreet,"" },
                            {EmailReplacements.AddressCityState,"" },
                            {EmailReplacements.Pay,"" },
                            {EmailReplacements.Href,"" },
                        },
                        From = "noreply@taggeducation.com",
                        Subject = "Your Job Has Been Cancelled"
                    },
                    new EmailTemplate
                    {
                        Name = "PasswordReset",
                        ExternalId = passwordReset,
                        TemplateType = EmailTemplateType.PasswordReset,
                        Replacements = new Dictionary<string, string>{
                            {EmailReplacements.Href,"" },
                        },
                        From = "noreply@taggeducation.com"
                    },
                    new EmailTemplate
                    {
                        Name = "ChangeEmail",
                        ExternalId = changeEmail,
                        TemplateType = EmailTemplateType.ChangeEmail,
                        Replacements = new Dictionary<string, string>{
                            {EmailReplacements.Href,"" },
                        },
                        From = "noreply@taggeducation.com"
                    }
                };

                _emailTemplatesCollection.InsertMany(templates);
            }

        }

        private void seedLookups(bool refresh = false)
        {
            if (refresh)
            {
                this._db.DropCollection(Collections.Lookups);
            }
            if (_lookupsCollection.AsQueryable().Count() == 0)
            {
                _lookupsCollection.InsertMany(new List<Lookups>
                {
                    new Lookups
                    {
                         States = new List<Lookup>
                         {
                            new Lookup {  Value = "Alabama", Key = "AL" },
                            new Lookup {  Value = "Alaska", Key = "AK" },
                            new Lookup {  Value = "Arizona", Key = "AZ" },
                            new Lookup {  Value = "Arkansas", Key = "AR" },
                            new Lookup {  Value = "California", Key = "CA" },
                            new Lookup {  Value = "Colorado", Key = "CO" },
                            new Lookup {  Value = "Connecticut", Key = "CT" },
                            new Lookup {  Value = "Delaware", Key = "DE" },
                            new Lookup {  Value = "District of Columbia", Key = "DC" },
                            new Lookup {  Value = "Florida", Key = "FL" },
                            new Lookup {  Value = "Georgia", Key = "GA" },
                            new Lookup {  Value = "Hawaii", Key = "HI" },
                            new Lookup {  Value = "Idaho", Key = "ID" },
                            new Lookup {  Value = "Illinois", Key = "IL" },
                            new Lookup {  Value = "Indiana", Key = "IN" },
                            new Lookup {  Value = "Iowa", Key = "IA" },
                            new Lookup {  Value = "Kansas", Key = "KS" },
                            new Lookup {  Value = "Kentucky", Key = "KY" },
                            new Lookup {  Value = "Louisiana", Key = "LA" },
                            new Lookup {  Value = "Maine", Key = "ME" },
                            new Lookup {  Value = "Montana", Key = "MT" },
                            new Lookup {  Value = "Nebraska", Key = "NE" },
                            new Lookup {  Value = "Nevada", Key = "NV" },
                            new Lookup {  Value = "New Hampshire", Key = "NH" },
                            new Lookup {  Value = "New Jersey", Key = "NJ" },
                            new Lookup {  Value = "New Mexico", Key = "NM" },
                            new Lookup {  Value = "New York", Key = "NY" },
                            new Lookup {  Value = "North Carolina", Key = "NC" },
                            new Lookup {  Value = "North Dakota", Key = "ND" },
                            new Lookup {  Value = "Ohio", Key = "OH" },
                            new Lookup {  Value = "Oklahoma", Key = "OK" },
                            new Lookup {  Value = "Oregon", Key = "OR" },
                            new Lookup {  Value = "Maryland", Key = "MD" },
                            new Lookup {  Value = "Massachusetts", Key = "MA" },
                            new Lookup {  Value = "Michigan", Key = "MI" },
                            new Lookup {  Value = "Minnesota", Key = "MN" },
                            new Lookup {  Value = "Mississippi", Key = "MS" },
                            new Lookup {  Value = "Missouri", Key = "MO" },
                            new Lookup {  Value = "Pennsylvania", Key = "PA" },
                            new Lookup {  Value = "Rhode Island", Key = "RI" },
                            new Lookup {  Value = "South Carolina", Key = "SC" },
                            new Lookup {  Value = "South Dakota", Key = "SD" },
                            new Lookup {  Value = "Tennessee", Key = "TN" },
                            new Lookup {  Value = "Texas", Key = "TX" },
                            new Lookup {  Value = "Utah", Key = "UT" },
                            new Lookup {  Value = "Vermont", Key = "VT" },
                            new Lookup {  Value = "Virginia", Key = "VA" },
                            new Lookup {  Value = "Washington", Key = "WA" },
                            new Lookup {  Value = "West Virginia", Key = "WV" },
                            new Lookup {  Value = "Wisconsin", Key = "WI" },
                            new Lookup {  Value = "Wyoming", Key = "WY" },
                         },
                          EducationLevels = new List<Lookup>
                          {
                            new Lookup { Key = "None", Value = "None"},
                            new Lookup { Key = "AS", Value = "Associates Degree"},
                            new Lookup { Key = "BS", Value = "Bachelors Degree"},
                            new Lookup { Key = "PHD", Value = "Doctoral Degree"},
                            new Lookup { Key = "Other", Value = "Other"},
                          },
                          SchoolTypes = new List<Lookup>
                          {
                              new Lookup { Key = SchoolTypes.Public, Value = "Public", Order = 1},
                              new Lookup { Key = SchoolTypes.PublicCharter, Value = "Public Charter", Order = 2},
                              new Lookup { Key = SchoolTypes.Private, Value = "Private", Order = 3},
                              new Lookup { Key = SchoolTypes.Other, Value = "Other", Order = 4},
                          },
                          Subjects = new List<Lookup>
                          {
                            new Lookup { Key = "Pre-K", Value = "Pre-K", Order = 1},
                            new Lookup { Key = "K", Value = "Kindergarten", Order = 2},
                            new Lookup { Key = "1st", Value = "1st Grade", Order = 3},
                            new Lookup { Key = "2nd", Value = "2nd Grade", Order = 4},
                            new Lookup { Key = "3rd", Value = "3rd Grade", Order = 5},
                            new Lookup { Key = "4th", Value = "4th Grade", Order = 6},
                            new Lookup { Key = "5th/6th", Value = "5th/6th Grade", Order = 7},
                            new Lookup { Key = "MSMath", Value = "Grades 6-8 Math", Order = 8},
                            new Lookup { Key = "MSEnglish", Value = "Grades 6-8 English", Order = 9},
                            new Lookup { Key = "MSSocialStudies", Value = "Grades 6-8 Social Studies", Order = 10},
                            new Lookup { Key = "MSScience", Value = "Grades 6-8 Science", Order = 11},
                            new Lookup { Key = "MSElectives", Value = "Grades 6-8 Electives", Order = 12},
                            new Lookup { Key = "HSMath", Value = "Grades 9-12 Math", Order = 13},
                            new Lookup { Key = "HSEnglish", Value = "Grades 9-12 English", Order = 14},
                            new Lookup { Key = "HSSocialStudies", Value = "Grades 9-12 Social Studies", Order = 15},
                            new Lookup { Key = "HSScience", Value = "Grades 9-12 Science", Order = 16},
                            new Lookup { Key = "HSElectives", Value = "Grades 9-12 Electives", Order = 17},
                          }
    }
                });
            }
        }

        private async Task<bool> seedUsers(string email)
        {

            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                var claims = new List<Claim>
                {
                    new Claim(TaggClaimTypes.Email, email),
                    new Claim(TaggClaimTypes.ApiAccess, ApiAccessLevels.SystemAdmin),
                    new Claim(TaggClaimTypes.FirstName, "John"),
                    new Claim(TaggClaimTypes.LastName, "Bond"),
                };

                user = new MongoUser { UserName = email, Email = email, PhoneNumber = "7202611925", FirstName = "John", LastName = "Bond" };
                foreach (var item in claims)
                {
                    user.AddClaim(item);
                }

                var key = "ZGV2QWRtaW4xMjM0";
                byte[] pd = Convert.FromBase64String(key);
                var test = Encoding.UTF8.GetString(pd);
                try
                {
                    var createResult = await _userManager.CreateAsync(user, Encoding.UTF8.GetString(pd));
                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }

            var adminUsers = new List<MongoUser>
            {
                new MongoUser{ UserName = "dmbaker90@gmail.com", Email = "dmbaker90@gmail.com", FirstName = "David", LastName = "Baker"},
                new MongoUser{ UserName = "emily@taggeducation.com", Email = "emily@taggeducation.com", FirstName = "Emily", LastName = "Berryman"},
                new MongoUser{ UserName = "trevor@taggeducation.com", Email = "trevor@taggeducation.com", FirstName = "Trevor", LastName = "Miller"},
                new MongoUser{ UserName = "andrew@taggeducation.com", Email = "andrew@taggeducation.com", FirstName = "Andrew", LastName = "Lundeen"},
                new MongoUser{ UserName = "matthew@taggeducation.com", Email = "matthew@taggeducation.com", FirstName = "Matthew", LastName = "Miller"},
            };
            foreach (var admin in adminUsers)
            {
                var mongoUser = await _userManager.FindByEmailAsync(admin.Email);
                if (mongoUser == null)
                {
                    var claims = new List<Claim>
                    {
                        new Claim(TaggClaimTypes.Email, admin.Email),
                        new Claim(TaggClaimTypes.FirstName, admin.FirstName),
                        new Claim(TaggClaimTypes.LastName, admin.LastName),
                        new Claim(TaggClaimTypes.ApiAccess, ApiAccessLevels.SystemAdmin),
                    };

                    foreach (var item in claims)
                    {
                        admin.AddClaim(item);
                    }

                    try
                    {
                        var createResult = await _userManager.CreateAsync(admin, "Password1234");
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                }
            }

            return true;

        }

    }
}
