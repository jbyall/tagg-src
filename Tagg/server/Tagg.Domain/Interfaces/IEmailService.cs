﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public interface IEmailService
    {
        IConfiguration Config { get; set; }

        ISendResult SendFromTemplate(IEmailTemplate template);
        ISendResult Send(string to, string from, string subject, string content, string cc = null);
        ISendResult Send(List<string> to, string from, string subject, string content, List<string> cc = null);

        //ISendResult SendEmail(IEmailTemplate email);
        //string SendEmailDelay(IEmailTemplate email, DateTime sendDateTime);
        //void CancelEmail(string batchId);
    }
}
