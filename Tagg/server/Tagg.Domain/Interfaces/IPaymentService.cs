﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public interface IPaymentService
    {
        ICustomerService Customers { get; set; }
        IAccountService Accounts { get; set; }
        IWebhooksService Webhooks { get; set; }
    }
}
