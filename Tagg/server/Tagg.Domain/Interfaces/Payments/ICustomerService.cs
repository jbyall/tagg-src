﻿using System;
using System.Collections.Generic;
using System.Text;
using Tagg.Domain.ApiModels;

namespace Tagg.Domain
{
    public interface ICustomerService
    {
        ICustomer Create(CustomerCreateDto customer);
        ICustomer GetById(string customerId);
        bool VerifyBank(BankVerify dto);
        ChargeResponseDto CreateCharge(ConnectChargeDto dto);
    }
}
