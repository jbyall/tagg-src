﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public interface ICustomer
    {
        string Id { get; set; }
        int AccountBalance { get; set; }
        DateTime Created { get; set; }
        string Currency { get; set; }
        bool Delinquent { get; set; }
        string Description { get; set; }
        string Email { get; set; }
        string PaymentSourceId { get; set; }
    }
}
