﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public interface IPaymentAccount
    {
        string Id { get; set; }
        string Email { get; set; }
    }
}
