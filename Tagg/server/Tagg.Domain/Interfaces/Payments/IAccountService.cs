﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public interface IAccountService
    {
        IPaymentAccount GetAccountData(string code, string secret);
        string GetAccountLogin(string accountId);
    }
}
