﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public interface IWebhooksService
    {
        void ProcessCallback(string json);
    }
}
