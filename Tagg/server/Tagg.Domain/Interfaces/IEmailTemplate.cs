﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Tagg.Domain
{
    public interface IEmailTemplate
    {
        string ExternalId { get; set; }
        string Name { get; set; }
        EmailTemplateType TemplateType { get; set; }
        Dictionary<string, string> Replacements { get; set; }
        string Subject { get; set; }
        List<string> To { get; set; }
        List<string> Cc { get; set; }
        string From { get; set; }
    }
}
