﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Tagg.Domain
{
    public interface ISendResult
    {
        HttpStatusCode Status { get; }
        bool IsSuccessStatusCode { get; }
    }
}
