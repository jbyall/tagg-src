﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public interface ILocationService
    {
        bool PostalCodeIsInServiceArea(string postalCode, int maxDistance = 100);
        ILocation GetLocationFromAddress(Address address);
    }

    
}
