﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public interface ILocation
    {
        double Latitude { get; set; }
        double Longitude { get; set; }
    }
}
