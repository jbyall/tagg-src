﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public interface ITaggMessage
    {
        string To { get; set; }
        string From { get; set; }
        string Subject { get; set; }
        string Body { get; set; }
        //List<Tuple<string,string>> Headers { get; set; }

    }
}
