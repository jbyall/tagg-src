﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Tagg.Domain.Helpers;

namespace Tagg.Domain
{
    public interface ISmsService
    {
        Task SendSmsAsync(string number, string message);
        Task<string> SendJobSmsAsync(string number, string message);
        Task<List<SmsHistory>> GetMessagesByAccountId(string accountSid);
    }
}
