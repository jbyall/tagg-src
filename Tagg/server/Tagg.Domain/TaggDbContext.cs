﻿//using Microsoft.Extensions.Configuration;
//using MongoDB.Bson;
//using MongoDB.Driver;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Security.Authentication;
//using System.Threading.Tasks;

//namespace Tagg.Domain
//{
//    public class TaggDbContext
//    {
//        //private MongoClientSettings _settings;
//        //private string _host;
//        private string _dbName;
//        private MongoClient _client;
//        private IMongoDatabase _db;


//        public TaggDbContext(string connectionString)
//        {
//            #region cosmosdb
//            //_host = "vitalize.documents.azure.com";
//            //_dbName = "tagg-test";
//            //var userName = "vitalize";
//            //var password = "SmahqWg7lBvNMI0WYh6Esdq1wGFipvZNkxChfbuPHCg3l15YbPLg2qanaA9RWGLIMN2oxVHspKV1T4B5yY37jA==";
//            //_settings = new MongoClientSettings();
//            //_settings.Server = new MongoServerAddress(_host, 10255);
//            //_settings.UseSsl = true;
//            //_settings.SslSettings = new SslSettings();
//            //_settings.SslSettings.EnabledSslProtocols = SslProtocols.Tls12;
//            //MongoIdentity identity = new MongoInternalIdentity(_dbName, userName);
//            //MongoIdentityEvidence evidence = new PasswordEvidence(password);


//            //_settings.Credentials = new List<MongoCredential>()
//            //{
//            //    new MongoCredential("SCRAM-SHA-1", identity, evidence)
//            //};
//            #endregion
//            _dbName = MongoUrl.Create(connectionString).DatabaseName;
//            _client = new MongoClient(connectionString);
//            _db = _client.GetDatabase(_dbName);

//            this.Schools = _db.GetCollection<School>(Collections.Schools);
//            this.Districts = _db.GetCollection<District>(Collections.Districts);
//            this.Teachers = _db.GetCollection<Teacher>(Collections.Teachers);
//            this.Subs = _db.GetCollection<Substitute>(Collections.Subs);
//            this.Lookups = _db.GetCollection<Lookups>(Collections.Lookups);
//        }

//        public IMongoCollection<School> Schools { get; set; }
//        public IMongoCollection<District> Districts { get; set; }
//        public IMongoCollection<Teacher> Teachers { get; set; }
//        public IMongoCollection<Substitute> Subs { get; set; }
//        public IMongoCollection<Lookups> Lookups { get; set; }
//        public IMongoCollection<Job> Jobs { get; set; }

//    }
//}

////using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
////using Microsoft.EntityFrameworkCore;
////using System;
////using System.Collections.Generic;
////using System.Text;

////namespace Tagg.Domain
////{
////    public class TaggDbContext : IdentityDbContext<ApplicationUser>
////    {
////        public DbSet<Address> Addresses { get; set; }
////        public DbSet<District> Districts { get; set; }
////        public DbSet<Lookup> Lookup { get; set; }
////        public DbSet<School> Schools { get; set; }
////        public DbSet<SchoolContact> SchoolContacts { get; set; }
////        public DbSet<Subject> Subjects { get; set; }
////        public DbSet<Substitute> Substitutes { get; set; }
////        public DbSet<Teacher> Teachers { get; set; }
////        public DbSet<UserProfile> UserProfiles { get; set; }

////        public TaggDbContext(DbContextOptions<TaggDbContext> options)
////            : base(options)
////        {
////        }

////        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
////        //{
////        //    optionsBuilder.UseSqlServer(
////        //        "Server=tcp:tagg-dev.database.windows.net,1433;Initial Catalog=TaggDev;Persist Security Info=False;User ID=taggDevUser;Password=Wc0VUjuUMJd;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30");

////        //}

////        protected override void OnModelCreating(ModelBuilder builder)
////        {
////            base.OnModelCreating(builder);
////            // Customize the ASP.NET Identity model and override the defaults if needed.
////            // For example, you can rename the ASP.NET Identity table names and more.
////            // Add your customizations after calling base.OnModelCreating(builder);
////            builder.Entity<UserProfile>()
////                .HasOne(p => p.User);

////            builder.Entity<UserProfile>()
////                .HasOne(p => p.Address)
////                .WithMany()
////                .HasForeignKey(p => p.AddressId);

////            builder.Entity<SubjectUserProfile>()
////                .HasKey(t => new { t.UserProfileId, t.SubjectId });

////            builder.Entity<SubjectUserProfile>()
////                .HasOne(up => up.Subject)
////                .WithMany(p => p.UserProfiles)
////                .HasForeignKey(up => up.SubjectId);

////            builder.Entity<SubjectUserProfile>()
////                .HasOne(su => su.UserProfile)
////                .WithMany(s => s.Subjects)
////                .HasForeignKey(up => up.UserProfileId);
////        }
////    }
////}
