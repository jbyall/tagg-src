﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class Transaction : TaggEntity
    {
        [BsonRepresentation(MongoDB.Bson.BsonType.ObjectId)]
        public string JobId { get; set; }
        public string JobNumber { get; set; }
        public TransactionStatus Status { get; set; }
        public string CustomerId { get; set; }
        public string DestinationId { get; set; }
        public DateTime ProcessOn { get; set; }
        public string ExternalStatus { get; set; }
        public string ExternalId { get; set; }
        public double PayRateUsd { get; set; }
        public double TotalAmountUsd { get; set; }
        public string Description { get; set; }
        public string Currency { get; set; } = "usd";

    }
}
