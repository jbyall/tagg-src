﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class Contact
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsPrimary { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Title { get; set; }
    }
}
