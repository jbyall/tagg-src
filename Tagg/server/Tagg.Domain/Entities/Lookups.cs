﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class Lookups : TaggEntity
    {
        public Lookups()
        {
            this.States = new List<Lookup>();
            this.EducationLevels = new List<Lookup>();
            this.SchoolTypes = new List<Lookup>();
            this.Subjects = new List<Lookup>();
        }
        public List<Lookup> States { get; set; }
        public List<Lookup> EducationLevels { get; set; }
        public List<Lookup> SchoolTypes { get; set; }
        public List<Lookup> Subjects { get; set; }

    }

    public class Lookup
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public int Order { get; set; }
    }
}
