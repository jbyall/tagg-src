﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class AppMessage : TaggEntity, ITaggMessage
    {
        public AppMessage()
        {
            this.Id = ObjectId.GenerateNewId().ToString();
        }

        public string ChannelId { get; set; }
        public string JobId { get; set; }

        public string To { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public DateTime? ReadOn { get; set; }
    }
}
