﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class ErrorLog : TaggEntity
    {
        public ErrorLog(Exception ex)
        {
            this.ErrorMessage = $"{ex}";
            this.StackTrace = ex.StackTrace;
        }

        public ErrorLog(string from, Exception ex)
        {
            this.ErrorMessage = $"{ex}";
            this.StackTrace = ex.StackTrace;
            this.From = from;
        }

        public string ErrorMessage { get; set; }
        public string StackTrace { get; set; }
        public string From { get; set; }
    }
}
