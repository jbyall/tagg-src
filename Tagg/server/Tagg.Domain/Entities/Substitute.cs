﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver.GeoJsonObjectModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class Substitute : UserBase
    {
        public Substitute()
        {
            this.Subjects = new List<GradeSubject>();
            this.Availability = new List<UserAvailability>();
            this.Exclusions = new List<string>();
        }
        public EducationLevel EducationLevel { get; set; }
        public bool HasTeachingLicense { get; set; }
        public bool HasTranscripts { get; set; }
        public string ResumeFile { get; set; }
        public bool ApplicationComplete { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public string ApprovedBy { get; set; }
        public string LinkedInUrl { get; set; }
        public string Interests { get; set; }
        public int MinPayAmount { get; set; }
        public int MaxDistance { get; set; }
        public string PaymentId { get; set; }
        [JsonIgnore]
        public GeoJsonPoint<GeoJson2DGeographicCoordinates> Location { get; set; } = null;
        public List<UserAvailability> Availability { get; set; }
        public List<GradeSubject> Subjects { get; set; }
        public List<string> Exclusions { get; set; }
        [BsonIgnore]
        public double Rating { get; set; }
        [BsonIgnore]
        public bool SubbedAtSchool { get; set; }
        [BsonIgnore]
        public int RatingCount { get; set; }
    }
}
