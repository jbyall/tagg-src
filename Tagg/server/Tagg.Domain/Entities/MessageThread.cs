﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class MessageThread : TaggEntity
    {
        public MessageThread()
        {
            this.Messages = new List<AppMessage>();
            this.Audience = new List<string>();
        }
        public MessageThread(string threadId, List<AppMessage> messages)
        {
            this.Messages = messages;
            this.Subject = Messages[0].Subject;
            this.Audience = new List<string> { Messages[0].To, Messages[0].From };
        }
        public string Subject { get; set; }
        public List<AppMessage> Messages { get; set; }
        public List<string> Audience { get; set; }
        public string JobId { get; set; }
    }
}
