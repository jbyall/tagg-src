﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class EmailLog : TaggEntity
    {
        public EmailLog()
        {
            this.To = new List<string>();
            this.Cc = new List<string>();
        }

        public List<string> To { get; set; }
        public List<string> Cc { get; set; }
        public string From { get; set; }
        public string Subject { get; set; }
        public string Content { get; set; }
    }
}
