﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;
using MongoDB.Driver.GeoJsonObjectModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tagg.Domain
{
    public class UserBase : TaggEntity
    {
        //public UserType UserType { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string Picture { get; set; }
        public string Bio { get; set; }
        public string Title { get; set; }
        public bool ReceivePhone { get; set; }
        public bool ReceiveSMS { get; set; }
        public bool ReceiveEmail { get; set; }
        public bool Enabled { get; set; }
        public TaggStatus Status { get; set; }
        public Address Address { get; set; }
        public bool Deactivated { get; set; } = false;

        [BsonIgnore]
        public string FullName
        {
            get { return $"{this.FirstName} {this.LastName}"; }

        }
    }
}
