﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class SchoolInquiry : TaggEntity
    {
        public TaggStatus Status { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string SchoolName { get; set; }
        public string DistrictName { get; set; }
        public string Title { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string PostalCode { get; set; }
    }
}
