﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class EmailTemplate : TaggEntity, IEmailTemplate
    {
        public EmailTemplate()
        {
            this.To = new List<string>();
            this.Cc = new List<string>();
            this.Replacements = new Dictionary<string, string>();
        }
        public string ExternalId { get; set; }
        public string Name { get; set; }
        public EmailTemplateType TemplateType { get; set; }
        public Dictionary<string,string> Replacements { get; set; }
        public string Subject { get; set; }
        public List<string> To { get; set; }
        public List<string> Cc { get; set; }
        public string From { get; set; }
    }
}
