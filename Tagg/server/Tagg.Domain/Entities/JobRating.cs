﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class JobRating : TaggEntity
    {
        public JobRating()
        {
            this.SubScores = new Dictionary<string, int>();
            this.SchoolScores = new Dictionary<string, int>();
        }

        [BsonRepresentation(BsonType.ObjectId)]
        public string SubstituteId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string TeacherId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string JobId { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string SchoolReviewerId { get; set; }

        public bool SubCompleted { get; set; }
        public bool SchoolCompleted { get; set; }
        public string SubComments { get; set; }

        public Dictionary<string,int> SubScores { get; set; }
        public Dictionary<string,int> SchoolScores { get; set; }

    }
}
