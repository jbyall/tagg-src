﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System;

namespace Tagg.Domain
{
    public class TaggEntity
    {
        [BsonRepresentation(BsonType.ObjectId)]
        public virtual string Id { get; set; }

        [JsonIgnore]
        public DateTime Created { get; internal set; } = DateTime.UtcNow;

        [JsonIgnore]
        public DateTime Modified { get; internal set; } = DateTime.UtcNow;
    }
}
