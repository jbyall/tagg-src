﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class Blackout : TaggEntity
    {
        public DateTime StartDate { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string SchoolId { get; set; }
    }
}
