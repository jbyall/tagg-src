﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using Tagg.Domain.ApiModels;

namespace Tagg.Domain
{
    public class CalendarEvent
    {
        public string Description { get; set; }
        public JobDetailsDto Job { get; set; }
        public UserAvailability Availibility { get; set; }
        public bool IsBlackout { get; set; } = false;
        public string SchoolId { get; set; }
    }

    public class UserAvailability
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
