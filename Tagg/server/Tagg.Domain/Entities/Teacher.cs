﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class Teacher : UserBase
    {
        public Teacher()
        {
            this.Subjects = new List<GradeSubject>();
            this.Favorites = new List<string>();
            this.Id = ObjectId.GenerateNewId().ToString();
        }
        // Populate with school pay rate by default. Can be overridden by school/district administrator.
        [BsonRepresentation(BsonType.ObjectId)]
        public string SchoolId { get; set; }
        public double PayRate { get; set; }
        public List<GradeSubject> Subjects { get; set; }
        public List<string> Favorites { get; set; }
    }

    public class TeacherLegacy : UserBase
    {
        public TeacherLegacy()
        {
            this.Subjects = new List<GradeSubject>();
            this.Favorites = new List<string>();
            this.Id = ObjectId.GenerateNewId().ToString();
        }
        public double PayRate { get; set; }
        public List<GradeSubject> Subjects { get; set; }
        public List<string> Favorites { get; set; }
    }


}
