﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver.GeoJsonObjectModel;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Domain
{
    public class Job : TaggEntity
    {
        public Job()
        {
            this.Offers = new List<JobOffer>();
        }

        [BsonRepresentation(BsonType.ObjectId)]
        public string SchoolId { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string SubstituteId { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string TeacherId { get; set; }
        [BsonRepresentation(BsonType.ObjectId)]
        public string CreatedBy { get; set; }
        public JobStatus Status { get; set; }
        public JobTransactionStatus TransactionStatus { get; set; }
        public double PayAmount { get; set; }
        public double ChargeAmount { get; set; }
        public double Rating { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime? AcceptedOn { get; set; }
        // Used to track teacher time off when no sub needed
        public bool SubNeeded { get; set; }
        public string LessonPlanId { get; set; }

        // School Location
        [JsonIgnore]
        public GeoJsonPoint<GeoJson2DGeographicCoordinates> Location { get; set; } = null;

        public GradeSubject Subject { get; set; }
        
        // Stores sub emails for all subs that have received an offer for the job.
        public List<JobOffer> Offers { get; set; }

    }

    public class JobOffer
    {
        public string SubId { get; set; }
        public DateTime SentOn { get; set; }
        public JobOfferStatus Status { get; set; }
        public string SMSId { get; set; }
    }

    public class JobOfferQueue : TaggEntity
    {
        public JobOfferQueue()
        {
            this.OfferQueue = new Queue<PendingJobOffer>();
        }
        public string JobId { get; set; }
        public Queue<PendingJobOffer> OfferQueue { get; set; }
        public DateTime StartTime { get; set; }
        public TaggStatus Status { get; set; }
    }

    public class PendingJobOffer
    {
        public string SendTo { get; set; }
        public DateTime SendOn { get; set; }
        public bool Sent { get; set; }

    }
}
