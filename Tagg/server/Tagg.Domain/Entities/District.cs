﻿using MongoDB.Bson;
using MongoDB.Driver;
using System.Collections.Generic;

namespace Tagg.Domain
{
    public class District : TaggEntity
    {
        public string Name { get; set; }
    }
}
