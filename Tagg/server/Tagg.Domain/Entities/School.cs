﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson.Serialization.IdGenerators;
using MongoDB.Driver;
using MongoDB.Driver.GeoJsonObjectModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tagg.Domain
{
    [BsonIgnoreExtraElements]
    public class School : TaggEntity
    {
        public School()
        {
            this.Contacts = new List<Contact>();
            this.Teachers = new List<TeacherLegacy>();
            this.Admins = new List<UserBase>();
        }
        public TaggStatus Status { get; set; }
        public string CustomerId { get; set; }
        public AccountStatus AccountStatus { get; set; }
        public string Name { get; set; }
        public string Url { get; set; }
        public string SchoolType { get; set; }
        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string Summary { get; set; }
        // Used as the default pay rate for teachers.
        public double PayRate { get; set; }
        public string Grades { get; set; }
        public Address Address { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public GeoJsonPoint<GeoJson2DGeographicCoordinates> Location { get; set; }

        [BsonRepresentation(BsonType.ObjectId)]
        public string DistrictId { get; set; }
        public List<Contact> Contacts { get; set; }
        [BsonElement("Teachers")]
        public List<TeacherLegacy> Teachers { get; set; }
        public List<UserBase> Admins { get; set; }
    }
}
