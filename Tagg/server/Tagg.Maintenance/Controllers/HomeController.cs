﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace Tagg.Maintenance.Controllers
{
    public class HomeController : Controller
    {
        private IConfiguration _config;

        public HomeController(IConfiguration config)
        {
            _config = config;
        }

        public IActionResult Index()
        {
            var now = DateTime.UtcNow;
            var tzInfo = TimeZoneInfo.Local;

            ViewData["message1"] = _config["Message1"];
            ViewData["message2"] = _config["Message2"];
            //ViewData["message2"] = _config["Message2"].Replace("[_TIME_]", $"{DateTime.Now.AddHours(1).ToString("g")} {tzInfo.Id}");
            return View();
        }

        private static DateTime ConvertToMountainTime(DateTime utc)
        {
            var mountainTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time");
            return TimeZoneInfo.ConvertTimeFromUtc(utc, mountainTimeZone);
        }
    }
}
