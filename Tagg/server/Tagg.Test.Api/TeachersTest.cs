using System;
using Xunit;
using RestSharp;
using RestSharp.Deserializers;
using System.Collections.Generic;
using Tagg.Domain.ApiModels;
using System.Linq;
using Tagg.Domain;

namespace Tagg.Test.Api
{
    public class TeachersTest
    {
        //private const string teachersUri = "https://tagg-api-staging.azurewebsites.net/api/teachers";
        //private const string schoolsUri = "https://tagg-api-staging.azurewebsites.net/api/schools";
        //private const string connectionString = "mongodb+srv://vitmongo:5ihBQnZFsxsvcq6A@cluster0-trxkp.mongodb.net/tagg-test";

        private const string teachersUri = "http://localhost:50706/api/teachers";
        private const string schoolsUri = "http://localhost:50706/api/schools";
        private const string connectionString = "mongodb://localhost/tagg-local";
        

        private RestClient _client;
        private JsonDeserializer _json;
        private TaggUnitOfWork _work;
        private string _schoolId;

        public TeachersTest()
        {
            _client = new RestClient(teachersUri);
            _json = new JsonDeserializer();
            //_work = new TaggUnitOfWork(connectionString);
            _schoolId = _work.Schools.GetQueryable().First().Id;
        }

        [Fact]
        public void GetAll_Valid_ShouldPass()
        {
            var request = new RestRequest("all", Method.GET);
            var response = _client.Execute(request);
            var result = _json.Deserialize<List<TeacherDto>>(response);

            Assert.True(result.Count > 0);
        }

        [Fact]
        public void Get_ValidEmail_ReturnsTeacher()
        {
            var request = new RestRequest(Method.GET);
            request.Resource = "5aaadb7eda13305938ed04c6";
            var response = _client.Execute(request);
            var result = _json.Deserialize<TeacherDto>(response);

            Assert.True(response.IsSuccessful);

        }

        [Fact]
        public void Get_InvalidId_ShouldFail()
        {
            var request = new RestRequest(Method.GET);
            request.Resource = "5aaadb7eda13305938ed04c9";
            var response = _client.Execute(request);

            Assert.True(response.StatusCode == System.Net.HttpStatusCode.NotFound);
        }

        [Fact]
        public void Create_Valid_ShouldPass()
        {
            // Arrange
            var newTeacher = getValidTeacher();
            var request = new RestRequest("create", Method.POST);
            request.AddJsonBody(newTeacher);

            // Act
            var response = _client.Execute(request);
            var result = _json.Deserialize<TeacherDto>(response);
            var insertedUser = _work.Users.GetByEmail(newTeacher.Email);
            var insertedTeacher = _work.Teachers.GetById(newTeacher.Id);

            // Assert
            Assert.True(response.IsSuccessful);
            Assert.NotNull(insertedTeacher);
            Assert.NotNull(insertedUser);
            Assert.True(insertedUser.Claims.Count > 0);
        }

        [Fact]
        public void Create_DuplicateEmail_ShouldFail()
        {
            var existingTeacher = getExistingTeacher();
            var request = new RestRequest("create", Method.POST);
            request.AddJsonBody(existingTeacher);

            var response = _client.Execute(request);

            //Assert
            Assert.False(response.IsSuccessful);
            Assert.True(_work.Users.Query(u => u.Email == existingTeacher.Email).Count() == 1);
            Assert.True(_work.Teachers.Query(t => t.FirstName == existingTeacher.FirstName).Count() == 0);
        }

        [Fact]
        public void Create_NoSchool_ShouldFail()
        {
            // Arrange
            var newTeacher = getValidTeacher();

            // Get any school
            var request = new RestRequest("create", Method.POST);
            request.AddJsonBody(newTeacher);

            // Act
            var response = _client.Execute(request);
            var result = _json.Deserialize<TeacherDto>(response);
            var insertedUser = _work.Users.GetByEmail(newTeacher.Email);
            var insertedTeacher = _work.Teachers.GetById(newTeacher.Id);

            // Assert
            Assert.False(response.IsSuccessful);
            Assert.Null(insertedTeacher);
            Assert.Null(insertedUser);
        }

        #region helpers
        private TeacherDto getValidTeacher()
        {
            var school = _work.Schools.GetQueryable().First();
            var now = DateTime.UtcNow.TimeOfDay;

            var result = new TeacherDto
            {
                FirstName = "UnitTest",
                LastName = "Teacher",
                Email = $"utest.h{now.Hours}.m{now.Minutes}.ms{now.Milliseconds}@mail.com",
                Address = new AddressDto
                {
                    Street1 = "101 Main Street",
                    City = "Test City",
                    State = "TS",
                    PostalCode = "99999",
                }
            };
            return result;
        }

        private TeacherDto getExistingTeacher()
        {
            var existingTeacher = _work.Teachers.GetQueryable().First();
            var result = getValidTeacher();
            result.FirstName = Guid.NewGuid().ToString();
            result.Email = existingTeacher.Email;
            return result;
        }
        #endregion
    }
}
