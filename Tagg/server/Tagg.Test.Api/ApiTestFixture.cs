﻿using System;
using Xunit;
using System.Collections.Generic;
using Tagg.Domain.ApiModels;
using System.Linq;
using Tagg.Domain;
using MongoDB.Driver;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Microsoft.AspNetCore.TestHost;
using Microsoft.AspNetCore.Hosting;
using TaggApi;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;

namespace Tagg.Test.Api
{
    // Used for 1-time setup before all tests of the test class are run
    public class ApiTestFixture : IDisposable
    {
        public const string connectionString = "mongodb://localhost/tagg-test";
        public TestServer _server;
        public MongoClient _mongoClient;
        public IMongoDatabase _db;

        public ApiTestFixture()
        {
            _server = new TestServer(new WebHostBuilder()
                .ConfigureAppConfiguration(SetupConfiguration)
                .UseStartup<Startup>());
            _mongoClient = new MongoClient(connectionString);
            _db = _mongoClient.GetDatabase("tagg-test");
        }

        public void Dispose()
        {
            _server.Dispose();
        }

        private void SetupConfiguration(WebHostBuilderContext ctx, IConfigurationBuilder builder)
        {
            builder.Sources.Clear();
            builder.AddJsonFile("appsettings.json", false, true);
        }
    }
}
