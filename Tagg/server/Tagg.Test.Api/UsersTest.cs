﻿using System;
using Xunit;
using System.Collections.Generic;
using Tagg.Domain.ApiModels;
using System.Linq;
using Tagg.Domain;
using MongoDB.Driver;
using Microsoft.Extensions.Configuration;
using System.Threading.Tasks;
using Microsoft.AspNetCore.TestHost;
using Microsoft.AspNetCore.Hosting;
using TaggApi;
using System.Net.Http;
using Newtonsoft.Json;
using System.Text;

namespace Tagg.Test.Api
{
    // Inherits IClassFixture for 1-time setup before all tests are run
    // Without this, automapper failes because it is initialized twice
    public class UsersTest : IClassFixture<ApiTestFixture>
    {   
        private const string usersUri = "api/users/";
        private IMongoCollection<MongoUser> _users;
        private IMongoCollection<School> _schools;
        private IMongoCollection<Substitute> _subs;
        private IMongoCollection<Teacher> _teachers;
        private HttpClient _client;
        private MongoClient _mongoClient;

        
        public UsersTest(ApiTestFixture baseFixture)
        {
            _client = baseFixture._server.CreateClient();
            _mongoClient = baseFixture._mongoClient;
            var db = baseFixture._db;
            _users = db.GetCollection<MongoUser>("users");
            _schools = db.GetCollection<School>("schools");
            _teachers = db.GetCollection<Teacher>("teachers");
            _subs = db.GetCollection<Substitute>("substitutes");
        }

        [Fact]
        public async Task CreateUser_NoName_Fails()
        {
            // Arrange
            var userType = UserType.SystemAdmin;
            var dto = new UserCreateDto
            {
                Email = $"fake{DateTime.Now.Millisecond}@test.com",
                UserType = userType,
            };
            var jsonContent = JsonConvert.SerializeObject(dto);
            var requestBody = new StringContent(jsonContent, Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(usersUri + "create", requestBody);

            // Assert
            var user = _users.AsQueryable().FirstOrDefault(u => u.Email == dto.Email);
            Assert.False(response.IsSuccessStatusCode);
            Assert.Null(user);
        }

        [Fact]
        public async Task CreateUser_ExistingEmail_Fails()
        {
            // Arrange
            var userType = UserType.SystemAdmin;
            var dto = new UserCreateDto
            {
                Email = $"fakeDupe{DateTime.Now.Millisecond}@test.com",
                UserType = userType,
                FirstName = "John",
                LastName = "Bond"
            };
            var jsonContent = JsonConvert.SerializeObject(dto);
            var requestBody = new StringContent(jsonContent, Encoding.UTF8, "application/json");

            // Act
            var firstUser = await _client.PostAsync(usersUri + "create", requestBody);
            var response = await _client.PostAsync(usersUri + "create", requestBody);

            // Assert
            Assert.False(response.IsSuccessStatusCode);
        }

        [Fact]
        public async Task CreateSysAdmin_Valid_CreatesUser()
        {
            // Arrange
            var userType = UserType.SystemAdmin;
            var dto = new UserCreateDto
            {
                Email = $"fakeSysAdmin{DateTime.Now.Millisecond}@test.com",
                UserType = userType,
                FirstName = "John",
                LastName = "Bond",
            };
            var jsonContent = JsonConvert.SerializeObject(dto);
            var requestBody = new StringContent(jsonContent, Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(usersUri + "create", requestBody);
            var result = JsonConvert.DeserializeObject<UserDto>(await response.Content.ReadAsStringAsync());

            // Assert
            var dbUser = _users.Find(u => u.Email == dto.Email).FirstOrDefault();

            Assert.True(response.IsSuccessStatusCode);
            Assert.NotNull(dbUser);
            Assert.True(hasExpectedClaims(dto, dbUser));
        }

        [Fact]
        public async Task CreateSchoolAdmin_Valid_CreatesUser()
        {
            // Arrange
            var userType = UserType.SchoolAdmin;
            var school = getTestSchool();
            var dto = new UserCreateDto
            {
                Email = $"fakeSchoolAdmin{DateTime.Now.Millisecond}@test.com",
                UserType = userType,
                FirstName = "John",
                LastName = "Bond",
                SchoolId = school.Id
            };
            var jsonContent = JsonConvert.SerializeObject(dto);
            var requestBody = new StringContent(jsonContent, Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(usersUri + "create", requestBody);
            var result = JsonConvert.DeserializeObject<UserDto>(await response.Content.ReadAsStringAsync());

            // Assert
            var dbUser = _users.Find(u => u.Email == dto.Email).FirstOrDefault();

            Assert.True(response.IsSuccessStatusCode);
            Assert.NotNull(dbUser);
            Assert.True(hasExpectedClaims(dto, dbUser));
        }

        [Fact]
        public async Task CreateSchoolAdmin_Valid_AddedToSchool()
        {
            // Arrange
            var userType = UserType.SchoolAdmin;
            var school = getTestSchool();
            var dto = new UserCreateDto
            {
                Email = $"fakeSchoolAdmin{DateTime.Now.Millisecond}@test.com",
                UserType = userType,
                FirstName = "John",
                LastName = "Bond",
                SchoolId = school.Id
            };
            var jsonContent = JsonConvert.SerializeObject(dto);
            var requestBody = new StringContent(jsonContent, Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(usersUri + "create", requestBody);
            var result = JsonConvert.DeserializeObject<UserDto>(await response.Content.ReadAsStringAsync());
            var idToCheck = result.Id;

            // Assert
            var dbUser = _users.Find(u => u.Email == dto.Email).FirstOrDefault();
            school = getSchoolById(school.Id);
            var admin = school.Admins.FirstOrDefault(a => a.Id == idToCheck);

            Assert.True(response.IsSuccessStatusCode);
            Assert.NotNull(admin);
            Assert.Equal(dbUser.Id, idToCheck);
            Assert.Equal(idToCheck, admin.Id);
        }

        [Fact]
        public async Task CreateTeacherUser_Valid_CreatesUser()
        {
            // Arrange
            var userType = UserType.Teacher;
            var school = getTestSchool();
            var dto = new UserCreateDto
            {
                Email = $"fakeTeacher{DateTime.Now.Millisecond}@test.com",
                UserType = userType,
                FirstName = "John",
                LastName = "Bond",
                SchoolId = school.Id
            };
            var jsonContent = JsonConvert.SerializeObject(dto);
            var requestBody = new StringContent(jsonContent, Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(usersUri + "create", requestBody);
            var result = JsonConvert.DeserializeObject<UserDto>(await response.Content.ReadAsStringAsync());

            // Assert
            var dbUser = _users.Find(u => u.Email == dto.Email).FirstOrDefault();

            Assert.True(response.IsSuccessStatusCode);
            Assert.NotNull(dbUser);
            Assert.True(hasExpectedClaims(dto, dbUser));
        }

        [Fact]
        public async Task CreateTeacherUser_Valid_CreatesTeacher()
        {
            // Arrange
            var userType = UserType.Teacher;
            var school = getTestSchool();
            var dto = new UserCreateDto
            {
                Email = $"fakeTeacher{DateTime.Now.Millisecond}@test.com",
                UserType = userType,
                FirstName = "John",
                LastName = "Bond",
                SchoolId = school.Id
            };
            var jsonContent = JsonConvert.SerializeObject(dto);
            var requestBody = new StringContent(jsonContent, Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(usersUri + "create", requestBody);
            var result = JsonConvert.DeserializeObject<UserDto>(await response.Content.ReadAsStringAsync());

            // Assert
            var dbUser = _users.Find(u => u.Email == dto.Email).FirstOrDefault();
            school = getSchoolById(school.Id);
            var dbTeacher = _teachers.AsQueryable().FirstOrDefault(t => t.Email == dto.Email);

            Assert.True(response.IsSuccessStatusCode);
            Assert.NotNull(dbTeacher);
            Assert.Equal(dbTeacher.FirstName, dto.FirstName);
            Assert.True(dbTeacher.Status == TaggStatus.New);
        }

        [Fact]
        public async Task CreateSubUser_Valid_CreatesUser()
        {
            // Arrange
            var userType = UserType.Substitute;
            var dto = new UserCreateDto
            {
                Email = $"fakeSub{DateTime.Now.Millisecond}@test.com",
                UserType = userType,
                FirstName = "John",
                LastName = "Bond",
            };
            var jsonContent = JsonConvert.SerializeObject(dto);
            var requestBody = new StringContent(jsonContent, Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(usersUri + "create", requestBody);

            // Assert
            Assert.True(response.IsSuccessStatusCode);
            var result = JsonConvert.DeserializeObject<UserDto>(await response.Content.ReadAsStringAsync());
            var dbUser = _users.Find(u => u.Email == dto.Email).FirstOrDefault();
            Assert.NotNull(dbUser);
            Assert.True(hasExpectedClaims(dto, dbUser));
        }

        [Fact]
        public async Task CreateSubUser_Valid_CreatesSub()
        {
            // Arrange
            var userType = UserType.Substitute;
            var dto = new UserCreateDto
            {
                Email = $"fakeSub{DateTime.Now.Millisecond}@test.com",
                UserType = userType,
                FirstName = "John",
                LastName = "Bond",
            };
            var jsonContent = JsonConvert.SerializeObject(dto);
            var requestBody = new StringContent(jsonContent, Encoding.UTF8, "application/json");

            // Act
            var response = await _client.PostAsync(usersUri + "create", requestBody);
            var result = JsonConvert.DeserializeObject<UserDto>(await response.Content.ReadAsStringAsync());

            // Assert
            var dbSub = _subs.Find(u => u.Email == dto.Email).FirstOrDefault();
            Assert.True(response.IsSuccessStatusCode);
            Assert.NotNull(dbSub);
            Assert.True(dbSub.Status == TaggStatus.New);
            Assert.False(dbSub.Enabled);
        }


        #region helpers
        private bool hasExpectedClaims(UserCreateDto dto, MongoUser user)
        {
            var accessClaim = user.Claims.FirstOrDefault(c => c.Type == TaggClaimTypes.ApiAccess);
            var firstNameClaim = user.Claims.FirstOrDefault(c => c.Type == TaggClaimTypes.FirstName);
            var lastNameClaim = user.Claims.FirstOrDefault(c => c.Type == TaggClaimTypes.LastName);
            var emailClaim = user.Claims.FirstOrDefault(c => c.Type == TaggClaimTypes.Email);
            var schoolClaim = user.Claims.FirstOrDefault(c => c.Type == TaggClaimTypes.School);

            bool hasExpectedAccess = false;
            switch (dto.UserType)
            {
                case UserType.Substitute:
                    hasExpectedAccess = accessClaim.Value == ApiAccessLevels.Substitute;
                    break;
                case UserType.Teacher:
                    hasExpectedAccess = accessClaim.Value == ApiAccessLevels.Teacher && schoolClaim.Value == dto.SchoolId;
                    break;
                case UserType.SchoolAdmin:
                    hasExpectedAccess = accessClaim.Value == ApiAccessLevels.SchoolAdmin && schoolClaim.Value == dto.SchoolId;
                    break;
                case UserType.DistrictAdmin:
                    hasExpectedAccess = accessClaim.Value == ApiAccessLevels.SchoolAdmin && schoolClaim != null;
                    break;
                case UserType.SystemAdmin:
                    hasExpectedAccess = accessClaim.Value == ApiAccessLevels.SystemAdmin;
                    break;
                default:
                    break;
            }


            return hasExpectedAccess &&
                firstNameClaim.Value == dto.FirstName &&
                lastNameClaim.Value == dto.LastName &&
                emailClaim.Value == dto.Email;
        }

        private School getTestSchool()
        {
            return _schools
                .AsQueryable()
                .FirstOrDefault();
        }

        private School getSchoolById(string id)
        {
            return _schools.AsQueryable().FirstOrDefault(s => s.Id == id);
        }
        #endregion
    }
}
