﻿using AutoMapper;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Tagg.Domain;

namespace DbConsole
{
    public class TransactionSummary
    {
        public string JobId { get; set; }
        public JobStatus JobStatus { get; set; }
        public DateTime JobDate { get; set; }
        public string TransactionId { get; set; }
        public string SubName { get; set; }
        public double PayAmount { get; set; }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var connectionString = "mongodb+srv://taggdbservice:kB22JQ4cVPCSMdyWJbZA@cluster0-trxkp.mongodb.net/tagg-staging";
            //var connectionString = "mongodb://localhost/tagg";
            var db = new DbWrapper(connectionString);

            try
            {
                var subjects = db.Teachers.AsQueryable().FirstOrDefault(t => t.Email == "teacher4@vitalize.team")?.Subjects;

                var builder = Builders<Substitute>.Filter.All(s => s.Subjects, subjects);
                var subs = db.Subs.Find(builder).ToList();

                var farley = db.Subs.AsQueryable()
                    .FirstOrDefault(x => x.LastName == "Farley");

                var farleysubjects = farley.Subjects;

                var farleyOffers = db.Jobs.AsQueryable()
                    .Where(j => j.Offers.Any(x => x.SubId == "5c19321a7e8d050e9c83b1e9"))
                    .ToList();

                foreach (var item in farleyOffers)
                {
                    var farleyHas = farleysubjects.Any(x => x == item.Subject);
                }
                var test = "";
            }
            catch (Exception ex)
            {

            }

            Console.WriteLine("Done");
            Console.Read();

            #region Old
            //var failedTransactions = db.Transactions.AsQueryable()
            //    .Where(t => t.Status == TransactionStatus.Failed)
            //    .OrderBy(t => t.ProcessOn)
            //    .ToList();

            //List<TransactionSummary> summaries = new List<TransactionSummary>();

            //foreach (var tr in failedTransactions)
            //{
            //    if (tr.Id != "5c103f9c52a3405c3c6b74e7")
            //    {
            //        tr.Status = TransactionStatus.Hold;
            //        db.Transactions.FindOneAndReplace(t => t.Id == tr.Id, tr);
            //        //var job = db.Jobs.AsQueryable().FirstOrDefault(j => j.Id == tr.JobId);
            //        //if (job != null && job.Status == JobStatus.Complete)
            //        //{
            //        //    var sub = db.Subs.AsQueryable().FirstOrDefault(s => s.Id == job.SubstituteId);
            //        //    summaries.Add(new TransactionSummary
            //        //    {
            //        //        JobId = job.Id,
            //        //        JobDate = job.StartDate,
            //        //        JobStatus = job.Status,
            //        //        PayAmount = job.PayAmount,
            //        //        SubName = sub.FullName,
            //        //        TransactionId = tr.Id
            //        //    });

            //        //    //Console.WriteLine($"{sub.FullName}-{job.StartDate.ToShortDateString()}-{job.PayAmount}");
            //        //}
            //        //else
            //        //{
            //        //    var test = job.Status;
            //        //}
            //    }

            //}

            ////foreach (var summary in summaries.OrderBy(s => s.JobDate).ToList())
            ////{
            ////    Console.WriteLine($"Job:{summary.JobId} | {summary.JobStatus.ToString()} | TRX:{summary.TransactionId} :: {summary.SubName}-{summary.JobDate.ToShortDateString()}-{summary.PayAmount}");
            ////}

            //var stop = "";

            ////var now = DateTime.UtcNow;
            ////var unclosed = db.Jobs.AsQueryable().Where(j => j.EndDate < now && j.Status == JobStatus.Open && j.SubNeeded).Count();

            ////var jeEnded = db.Jobs.AsQueryable()
            ////    .Where(j => j.EndDate < now)
            ////    .Where(j => j.Status == JobStatus.Accepted || j.Status == JobStatus.Open).Count();

            ////var stop = "";



            ////***************TAGG-453 CHECK TO SEE WHY TEACHER TIME OFF CALCULATIONS WERE WRONG****************************
            //var teacherIds = db.Teachers.AsQueryable().Select(t => t.Id).ToList();
            //var now = DateTime.UtcNow;

            //foreach (var teach in teacherIds)
            //{
            //    double hoursTaken = 0;
            //    double hoursCovered = 0;
            //    var jobs = db.Jobs
            //        .AsQueryable()
            //        .Where(j => j.TeacherId == teach && j.EndDate >= new DateTime(now.Year, 1, 1) && j.Status != JobStatus.Cancelled)
            //        .ToList();

            //    var jobsCovered = jobs.Where(j => j.SubNeeded == true).ToList();
            //    var jobsUncovered = jobs.Where(j => j.SubNeeded == false).ToList();

            //    jobs.ForEach(j => hoursTaken += (j.EndDate - j.StartDate).TotalHours);
            //    jobsCovered.ForEach(j => hoursCovered += (j.EndDate - j.StartDate).TotalHours);

            //    if (hoursTaken < 0 || hoursCovered < 0)
            //    {

            //    }
            //}

            //var jobsResult = work.CreateJobs("5b9d2c5272d4ef1acc3db0c2", 2, new DateTime(2018, 10, 10));
            //var jobsResult2 = work.CreateJobs("5bafd3eca0f1345b449e4d37", 2, new DateTime(2018, 10, 10));
            //var jobResult3 = work.CreateJobsWithOffers("5b9d2fdb72d4ef1acc3db0f0", 10, new DateTime(2018, 10, 30));

            //***********TEACHER MIGRATION AND TRANSACTION ID FIX*******************

            //var trxResult = transactionJobIdFix(db);

            //configureMapper();

            //var sw = new Stopwatch();
            //sw.Start();
            //if (teacherPreCheck(db))
            //{
            //    sw.Stop();
            //    Console.WriteLine($"teacherPreCheck time: {sw.ElapsedMilliseconds}");
            //    sw.Restart();

            //    migrateTeachers(db);
            //    sw.Stop();
            //    Console.WriteLine($"migrateTeachers time: {sw.ElapsedMilliseconds}");
            //    sw.Restart();
            //    var verify = teacherPostCheck(db);
            //    sw.Stop();
            //    Console.WriteLine($"verify time: {sw.ElapsedMilliseconds}");
            //    if (!verify)
            //    {
            //        Console.WriteLine("Post check failed");
            //    }
            //}
            #endregion
        }

        #region oldMethods
        //static bool transactionJobIdFix(DbWrapper db)
        //{
        //    var trx = db.Transactions.AsQueryable().ToList();
        //    foreach (var item in trx)
        //    {
        //        try
        //        {
        //            db.Transactions.FindOneAndReplace(t => t.Id == item.Id, item);
        //        }
        //        catch (Exception ex)
        //        {
        //            return false;
        //        }
        //    }
        //    return true;
        //}

        //static bool teacherPreCheck(DbWrapper db)
        //{
        //    Console.WriteLine("Running teacherPreCheck");
        //    var users = db.Users.AsQueryable().ToList();
        //    int userCount = users.Count();
        //    int subCount = db.Subs.AsQueryable().Count();
        //    int teacherCount = 0;
        //    int adminCount = 0;
        //    int zeroCount = 0;
        //    int oneCount = 0;
        //    int multipleCount = 0;

        //    foreach (var usr in users)
        //    {
        //        var teacherSchoolCount = db.Schools.AsQueryable()
        //            .Where(s => s.Teachers.Any(t => t.Id == usr.Id))
        //            .Count();

        //        switch (teacherSchoolCount)
        //        {
        //            case 0:
        //                zeroCount++;
        //                break;
        //            case 1:
        //                oneCount++;
        //                break;
        //            default:
        //                multipleCount++;
        //                break;
        //        }
        //    }

        //    var schools = db.Schools.AsQueryable().ToList();
        //    foreach (var sch in schools)
        //    {
        //        adminCount += sch.Admins.Count;
        //        teacherCount += sch.Teachers.Count;
        //    }

        //    if (oneCount != teacherCount || multipleCount > 0)
        //    {
        //        Console.WriteLine("teacherPreCheck failed.");
        //        return false;
        //    }
        //    Console.WriteLine("teacherPreCheckPassed.");
        //    return true;
        //}

        //static void migrateTeachers(DbWrapper db)
        //{
        //    Console.WriteLine("Migrating teachers to a new collection.");
        //    var schools = db.Schools.AsQueryable().ToList();
        //    foreach (var school in schools)
        //    {
        //        foreach (var teacherLegacy in school.Teachers)
        //        {
        //            var newTeacher = Mapper.Map<Teacher>(teacherLegacy);
        //            newTeacher.SchoolId = school.Id;
        //            db.Teachers.InsertOne(newTeacher);
        //        }
        //    }
        //}

        //static bool teacherPostCheck(DbWrapper db)
        //{
        //    Console.WriteLine("Running teacherPostCheck");
        //    var schools = db.Schools.AsQueryable().ToList();
        //    foreach (var sch in schools)
        //    {
        //        foreach (var legacyTeacher in sch.Teachers)
        //        {
        //            var newTeacher = db.Teachers.AsQueryable().FirstOrDefault(t => t.Id == legacyTeacher.Id);
        //            if (newTeacher == null || !teacherValidation(legacyTeacher, newTeacher))
        //            {
        //                Console.WriteLine($"teacherPostCheck failed on teacherId: {legacyTeacher.Id}");
        //                return false;
        //            }
        //        }
        //    }
        //    Console.WriteLine("teacherPostCheck passed");
        //    return true;
        //}

        //static bool teacherValidation(TeacherLegacy legacy, Teacher newTeach)
        //{
        //    return legacy.Address?.City == newTeach.Address?.City &&
        //        legacy.Address?.PostalCode == newTeach.Address?.PostalCode &&
        //        legacy.Address?.State == newTeach.Address?.State &&
        //        legacy.Address?.Street1 == newTeach.Address?.Street1 &&
        //        legacy.Address?.Street2 == newTeach.Address?.Street2 &&
        //        legacy.Bio == newTeach.Bio &&
        //        legacy.Deactivated == newTeach.Deactivated &&
        //        legacy.Email == newTeach.Email &&
        //        legacy.Enabled == newTeach.Enabled &&
        //        legacy.Favorites?.Count == newTeach.Favorites?.Count &&
        //        legacy.FirstName == newTeach.FirstName &&
        //        legacy.LastName == newTeach.LastName &&
        //        legacy.PayRate == newTeach.PayRate &&
        //        legacy.PhoneNumber == newTeach.PhoneNumber &&
        //        legacy.Picture == newTeach.Picture &&
        //        legacy.ReceiveEmail == newTeach.ReceiveEmail &&
        //        legacy.ReceivePhone == newTeach.ReceivePhone &&
        //        legacy.ReceiveSMS == newTeach.ReceiveSMS &&
        //        legacy.Status == newTeach.Status &&
        //        legacy.Subjects?.Count == newTeach.Subjects?.Count &&
        //        legacy.Title == newTeach.Title;
        //}
        #endregion
        static void configureMapper()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<TeacherLegacy, Teacher>();
            });
        }

        public static void ReferenceScript()
        {
            ////var jobCount = jobs.AsQueryable().Count();
            //var badJobs = jobs.AsQueryable()
            //    .ToList()
            //    .OrderByDescending(x => x.Created)
            //    .Where(j => j.StartDate < j.Created)
            //    .ToList();

            //Console.WriteLine("Created\t\t\tStart\t\t\tEnd\t\t\tCreatedBy\t\t\tTeacher");
            //foreach (var item in badJobs)
            //{
            //    var usr = users.AsQueryable().FirstOrDefault(x => x.Id == item.CreatedBy);
            //    var accessLevel = usr.Claims.FirstOrDefault(x => x.Type == "api_access")?.Value;

            //    if (!accessLevel.Contains("3") && !usr.Email.Contains("tagg"))
            //    {
            //        Console.WriteLine($"{item.Created.AddHours(-6).ToString("g")} | {item.StartDate.AddHours(-6).ToString("g")} | {item.EndDate.AddHours(-6).ToString("g")} | {usr.Email}");
            //    }

            //    //Console.WriteLine($"Start:");
            //    //Console.WriteLine($"Created:");
            //    //Console.WriteLine($"CreatedBy:");
            //    //Console.WriteLine($"Teacher:{item.TeacherId}");
            //}

            ////var cratedBys = badJobs.Select(j => j.CreatedBy).Distinct();
            ////Console.WriteLine("---------------------------------");
            ////foreach (var item in cratedBys)
            ////{

            ////    Console.WriteLine(usr.Email);
            ////}



            ////Console.WriteLine($"Distinct Created: {badJobs.Select(j => j.CreatedBy).Distinct().Count()}");
            ////Console.WriteLine($"Bad Job Count: {badJobs.Count()}");
        }
    }

    public class DbWrapper
    {
        public DbWrapper(string connectionString)
        {
            var mongoUrl = new MongoUrl(connectionString);
            var mongoSettings = MongoClientSettings.FromUrl(new MongoUrl(connectionString));
            mongoSettings.MaxConnectionIdleTime = TimeSpan.FromSeconds(10);
            mongoSettings.MaxConnectionLifeTime = TimeSpan.FromSeconds(30);


            MongoClient client = new MongoClient(mongoSettings);
            var dbName = mongoUrl.DatabaseName;

            this.db = client.GetDatabase(dbName);

            this.Blackouts = db.GetCollection<Blackout>(Collections.Blackouts);
            this.Districts = db.GetCollection<District>(Collections.Districts);
            this.Emails = db.GetCollection<EmailLog>(Collections.Emails);
            this.EmailTemplates = db.GetCollection<EmailTemplate>(Collections.EmailTemplates);
            this.Errors = db.GetCollection<ErrorLog>(Collections.Errors);
            this.Inquiries = db.GetCollection<SchoolInquiry>(Collections.Inquiries);
            this.Jobs = db.GetCollection<Job>(Collections.Jobs);
            this.JobRatings = db.GetCollection<JobRating>(Collections.JobRatings);
            this.Lookups = db.GetCollection<Lookups>(Collections.Lookups);
            this.Offers = db.GetCollection<JobOfferQueue>(Collections.OfferQueue);
            this.Schools = db.GetCollection<School>(Collections.Schools);
            this.Subs = db.GetCollection<Substitute>(Collections.Subs);
            this.Teachers = db.GetCollection<Teacher>(Collections.Teachers);
            this.Transactions = db.GetCollection<Transaction>(Collections.Transactions);
            this.Users = db.GetCollection<MongoUser>(Collections.Users);

        }

        public IMongoDatabase db { get; set; }
        public IMongoCollection<Blackout> Blackouts { get; set; }
        public IMongoCollection<District> Districts { get; set; }
        public IMongoCollection<EmailLog> Emails { get; set; }
        public IMongoCollection<EmailTemplate> EmailTemplates { get; set; }
        public IMongoCollection<ErrorLog> Errors { get; set; }
        public IMongoCollection<SchoolInquiry> Inquiries { get; set; }
        public IMongoCollection<Job> Jobs { get; set; }
        public IMongoCollection<JobRating> JobRatings { get; set; }
        public IMongoCollection<Lookups> Lookups { get; set; }
        public IMongoCollection<JobOfferQueue> Offers { get; set; }
        public IMongoCollection<School> Schools { get; set; }
        public IMongoCollection<Substitute> Subs { get; set; }
        public IMongoCollection<Teacher> Teachers { get; set; }
        public IMongoCollection<Transaction> Transactions { get; set; }
        public IMongoCollection<MongoUser> Users { get; set; }



        /// <summary>
        /// Creates job on a specific date
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="jobDate"></param>
        /// <returns></returns>
        public bool CreateJobOnDate(string schoolId, DateTime jobDate)
        {
            try
            {
                var referenceJob = this.Jobs.AsQueryable().FirstOrDefault();
                var newJob = referenceJob;
                newJob.Id = null;
                newJob.SchoolId = schoolId;
                newJob.StartDate = jobDate;
                newJob.EndDate = jobDate;
                this.Jobs.InsertOne(newJob);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        /// <summary>
        /// Creates a new job for each day starting on the minDate and ending when count of jobs has been created
        /// </summary>
        /// <param name="schoolId"></param>
        /// <param name="count"></param>
        /// <param name="minDate"></param>
        /// <returns></returns>
        public bool CreateJobs(string schoolId, int count, DateTime minDate)
        {
            try
            {
                var referenceJob = this.Jobs.AsQueryable().FirstOrDefault();
                var loopDate = new DateTime(minDate.Year, minDate.Month, minDate.Day, minDate.Hour, minDate.Minute, minDate.Second);
                for (int i = 0; i <= count; i++)
                {
                    var newJob = referenceJob;
                    newJob.Id = null;
                    newJob.StartDate = loopDate;
                    newJob.EndDate = loopDate;
                    this.Jobs.InsertOne(newJob);
                    loopDate = loopDate.AddDays(1);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CreateSchool(string newSchoolName)
        {
            try
            {
                var existingSchool = this.Schools.AsQueryable().FirstOrDefault();
                var newSchool = existingSchool;

                newSchool.Id = null;
                newSchool.Name = newSchoolName;
                this.Schools.InsertOne(newSchool);
                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CreateJobsWithOffers(string subId, int count, DateTime jobDate)
        {
            try
            {
                var referenceJob = this.Jobs.AsQueryable().FirstOrDefault();
                for (int i = 0; i <= count; i++)
                {
                    var newJob = referenceJob;
                    newJob.Id = null;
                    newJob.StartDate = jobDate;
                    newJob.EndDate = jobDate.AddHours(4);
                    newJob.Offers = new List<JobOffer> { new JobOffer { SubId = subId } };
                    this.Jobs.InsertOne(newJob);
                }

                return true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
