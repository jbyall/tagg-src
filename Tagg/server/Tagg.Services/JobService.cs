﻿using Sentry;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Tagg.Domain;
using Tagg.Domain.ApiModels;

namespace Tagg.Services
{
    public class JobService
    {
        private TaggUnitOfWork _work;
        private IEmailService _email;
        private ChatService _chat;

        public JobService(TaggUnitOfWork work, IEmailService emailService, PhoneService phone, ChatService chat)
        {
            _work = work;
            _email = emailService;
            _chat = chat;
        }

        public EntityResult Accept(string jobId, string subId)
        {
            try
            {
                SentrySdk.AddBreadcrumb($"Attempting to accept in JobService - Job:{jobId}, Sub:{subId}");
                var job = _work.Jobs.GetById(jobId);
                if (job != null && job.Status == JobStatus.Open)
                {
                    _work.Jobs.UpdateStatus(job.Id, JobStatus.Accepted, subId);
                    job.SubstituteId = subId;
                    if (acceptJob(job))
                    {
                        var userToAdd = !string.IsNullOrWhiteSpace(job.CreatedBy) && job.CreatedBy != job.TeacherId ? job.CreatedBy : job.TeacherId;
                        bool updateChatResult = addTeacherToChannel(job.Id, job.SubstituteId, userToAdd);
                        if (!updateChatResult)
                        {
                            SentrySdk.CaptureException(new Exception($"Failed to add school user to chat for JobId:{job.Id}"));
                        }
                        return new EntityResult();
                    }
                    // If something happened during the acceptJob method, reset
                    _work.Jobs.UpdateStatus(job.Id, JobStatus.Open, null);
                }

                return new EntityResult("Job not found.");
            }
            catch (Exception ex)
            {
                SentrySdk.AddBreadcrumb($"Exception from job service when accepting jobId:{jobId} ");
                SentrySdk.CaptureException(ex);
                return new EntityResult("Failed to accept job.");
            }
        }

        public EntityResult Decline(string jobId, string subId)
        {
            return _work.Jobs.Decline(jobId, subId);
        }

        #region Helpers
        private bool acceptJob(Job job)
        {
            job.Status = JobStatus.Accepted;
            job.AcceptedOn = DateTime.UtcNow;
            // Need to check expired, because multiple offers will exist for a sub if another sub accepts a job, then cancels.
            // We only want to accept the one that is not expired (should only be 1)
            var offerToAccept = job.Offers.FirstOrDefault(o => o.SubId == job.SubstituteId && o.Status != JobOfferStatus.Expired);
            if (offerToAccept != null)
            {
                offerToAccept.Status = JobOfferStatus.Accepted;
            }

            var jobUpdateResult = _work.Jobs.Update(job);
            if (jobUpdateResult.Succeeded)
            {
                _work.Offers.DisableQueue(job.Id);

            }

            try
            {
                var notifiySchoolSent = this.sendJobAcceptedEmail(job);
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
            }

            return jobUpdateResult.Succeeded;
        }

        private bool sendJobAcceptedEmail(Job model)
        {
            try
            {
                var job = _work.Jobs.GetDetailsSingle(model.Id);

                var teacher = _work.Teachers.GetById(job.TeacherId);
                var admins = _work.Schools.GetSchoolAdmins(job.SchoolId)
                    .Where(a => !string.IsNullOrWhiteSpace(a.Email));

                var emailTemplate = _work.EmailTemplates.GetByType(EmailTemplateType.JobAcceptance);
                if (teacher != null)
                {
                    emailTemplate.To.Add(teacher.Email);
                }
                foreach (var item in admins)
                {
                    emailTemplate.To.Add(item.Email);
                }
                emailTemplate.Replacements[EmailReplacements.AddressCityState] = $"{job.SchoolAddress.City}, {job.SchoolAddress.State} {job.SchoolAddress.PostalCode}";
                emailTemplate.Replacements[EmailReplacements.AddressStreet] = $"{job.SchoolAddress.Street1} {job.SchoolAddress.Street2}";
                emailTemplate.Replacements[EmailReplacements.Date] = ConvertToMountainTime(job.StartDate).ToLongDateString();
                emailTemplate.Replacements[EmailReplacements.Grade] = SubjectLookup.Get(job.Subject);
                emailTemplate.Replacements[EmailReplacements.School] = job.SchoolName;
                emailTemplate.Replacements[EmailReplacements.Sub] = job.SubstituteName;
                emailTemplate.Replacements[EmailReplacements.Teacher] = job.TeacherName;
                emailTemplate.Replacements[EmailReplacements.Time] = $"{JobService.ConvertToMountainTime(job.StartDate).ToShortTimeString()}-{JobService.ConvertToMountainTime(job.EndDate).ToShortTimeString()}";
                emailTemplate.Replacements[EmailReplacements.Pay] = job.PayAmount.ToString("C");

                var sendResult = _email.SendFromTemplate(emailTemplate);
                return sendResult.IsSuccessStatusCode;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return false;
            }
        }

        private bool addTeacherToChannel(string jobId, string subId, string teacherId)
        {
            try
            {
                //var school = _work.Schools.GetById(job.SchoolId);
                var existingChannel = _chat.GetChannelByInternalId($"{jobId}-{subId}");
                if (existingChannel != null)
                {
                    var subChatUser = _chat.AddUserToExistingChannel(teacherId, existingChannel.Sid);
                    return subChatUser != null;
                }

                return false;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return false;
            }
        }

        private static DateTime ConvertToMountainTime(DateTime utc)
        {
            var mountainTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time");
            return TimeZoneInfo.ConvertTimeFromUtc(utc, mountainTimeZone);
        }
        #endregion
    }
}
