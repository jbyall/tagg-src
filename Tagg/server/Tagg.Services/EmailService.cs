﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Tagg.Domain;
using Tagg.Services.Communication;

namespace Tagg.Services
{
    public class EmailService : IEmailService
    {
        private SendGridService service;
        private TaggUnitOfWork _work;
        private List<string> _whitelist;

        public EmailService(IConfiguration config, TaggUnitOfWork work)
        {
            this.service = new SendGridService(config["SendGrid:ApiKey"]);
            _work = work;
            this.Config = config;
            var whiteListConfig = config["WhiteList:Email"];
            _whitelist = string.IsNullOrWhiteSpace(whiteListConfig) ? null : whiteListConfig.Split(',').ToList();
        }

        //public ISendResult SendEmail(IEmailTemplate email)
        //{
        //    return service.SendSingleEmail(email);
        //}
        public IConfiguration Config { get; set; }

        public ISendResult Send(string to, string from, string subject, string content, string cc = null)
        {
            List<string> toList = new List<string> { to };
            List<string> ccList = new List<string>();

            if (!string.IsNullOrWhiteSpace(cc))
            {
                ccList.Add(cc);
            }

            return this.Send(toList, from, subject, content, ccList);
        }

        public ISendResult Send(List<string> to, string from, string subject, string content, List<string> cc = null)
        {
            var emailLog = new EmailLog
            {
                To = to,
                From = from,
                Subject = subject,
                Cc = cc ?? new List<string>(),
                Content = content
            };

            if (whiteListCheck(to) && whiteListCheck(cc))
            {
                _work.Emails.Add(emailLog);
                return service.SendSingleEmail(emailLog.To, emailLog.From, emailLog.Subject, emailLog.Content, emailLog.Cc);
            }
            return new SendResult(HttpStatusCode.Unauthorized);

        }

        public ISendResult SendFromTemplate(IEmailTemplate template)
        {
            if (whiteListCheck(template.To) && whiteListCheck(template.Cc))
            {
                return service.SendFromTemplate(template);
            }
            return new SendResult(HttpStatusCode.Unauthorized);
        }

        public void CancelEmail(string batchId)
        {
            service.CancelEmail(batchId);
        }

        private bool whiteListCheck(List<string> to)
        {

            if (to != null && _whitelist != null)
            {
                foreach (var item in to)
                {
                    if (_whitelist.Count(x => x.ToLower() == item.Split('@').Last()) == 0)
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}