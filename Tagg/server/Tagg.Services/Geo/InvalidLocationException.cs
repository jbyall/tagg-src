﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Services.Geo
{
    public class InvalidLocationException : Exception
    {
        public InvalidLocationException()
        {
        }

        public InvalidLocationException(string message)
            : base(message)
        {
        }

        public InvalidLocationException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
