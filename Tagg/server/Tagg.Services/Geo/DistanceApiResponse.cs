﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Services.Geo
{
    public class DistanceApiResponse
    {
        public List<string> destination_addresses { get; set; }
        public List<string> origin_addresses { get; set; }
        public List<Row> rows { get; set; }
        public string status { get; set; }

        public List<Distance> GetDistances()
        {
            var result = new List<Distance>();

            if (this.rows[0].elements[0].status == LocationService.zeroResults)
            {
                throw new InvalidLocationException("Could not determine distance");
            }

            foreach (var item in this.rows)
            {
                item.elements.ForEach(i => result.Add(i.distance));
            }
            return result;
        }
    }

    public class Distance
    {
        public string text { get; set; }
        public int value { get; set; }
    }

    public class Duration
    {
        public string text { get; set; }
        public int value { get; set; }
    }

    public class Element
    {
        public Distance distance { get; set; }
        public Duration duration { get; set; }
        public string status { get; set; }
    }

    public class Row
    {
        public List<Element> elements { get; set; }
    }
}
