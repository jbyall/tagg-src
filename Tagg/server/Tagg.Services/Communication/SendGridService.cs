﻿using Newtonsoft.Json;
using SendGrid;
using SendGrid.Helpers.Mail;
using SendGrid.Helpers.Reliability;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Tagg.Domain;

namespace Tagg.Services.Communication
{
    public class SendGridService
    {
        private string _apiKey;
        private SendGridClient _client;

        public SendGridService(string apiKey)
        {
            _apiKey = apiKey;
            _client = this.configureSendGridClient(_apiKey);
        }

        /// <summary>
        /// Basic email send.
        /// </summary>
        public ISendResult SendSingleEmail(List<string> to, string from, string subject, string content, List<string> cc)
        {
            try
            {
                if (to.Count > 0)
                {
                    //var message = MailHelper.CreateSingleEmail(new EmailAddress(from), null, subject, content, content);
                    var message = new SendGridMessage();
                    message.SetFrom(new EmailAddress(from));
                    message.SetSubject(subject);
                    to.ForEach(t => message.AddTo(new EmailAddress(t)));
                    message.HtmlContent = content;
                    //cc.ForEach(c => message.AddCc(new EmailAddress(c)));

                    var response = _client.SendEmailAsync(message).Result;

                    return new SendResult(response.StatusCode);
                }
                else
                {
                    return new SendResult(HttpStatusCode.ExpectationFailed);
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        /// <summary>
        /// Send using a pre-configured template the exists in SendGrid
        /// </summary>
        public ISendResult SendFromTemplate(IEmailTemplate template)
        {
            SendGridMessage message = new SendGridMessage();
            message.SetFrom(new EmailAddress(template.From));
            message.SetSubject(template.Subject);
            template.To.ForEach(t => message.AddTo(t));
            message.SetTemplateId(template.ExternalId);
            message.AddSubstitutions(template.Replacements);
            var response = _client.SendEmailAsync(message).Result;
            return new SendResult(response.StatusCode);
        }

        public void CancelEmail(string batchId)
        {
            var serializer = new JsonSerializer();
            var cancelBody = JsonConvert.SerializeObject(new CancelRequest { batch_id = batchId, status = "cancel" });
            var cancelRsult = _client.RequestAsync(SendGridClient.Method.POST, cancelBody).Result;

        }

        private SendGridClient configureSendGridClient(string apiKey)
        {
            var options = new SendGridClientOptions
            {
                ApiKey = apiKey,
                ReliabilitySettings = new ReliabilitySettings(3, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(10), TimeSpan.FromSeconds(3)),
            };

            return new SendGridClient(options);
        }

        private async Task<string> getBatchId()
        {
            var result = await _client.RequestAsync(SendGridClient.Method.POST, urlPath: "mail/batch");
            var test = JsonConvert.DeserializeObject<BatchRequest>(await result.Body.ReadAsStringAsync());
            return test.batch_id;
        }

    }

    public class CancelRequest
    {
        public string batch_id { get; set; }
        public string status { get; set; }
    }

    public class BatchRequest
    {
        public string batch_id { get; set; }
    }
}