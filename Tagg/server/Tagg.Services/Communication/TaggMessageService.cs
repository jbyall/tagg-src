﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tagg.Domain;
using Tagg.Domain.ApiModels;
using Tagg.Domain.Helpers;

namespace Tagg.Services.Communication
{
    public class TaggMessageService
    {
        private TaggUnitOfWork _work;
        private IEmailService _email;
        private ISmsService _sms;
        private ChatService _chat;
        private PhoneService _phone;
        private IConfiguration _config;

        public TaggMessageService(TaggUnitOfWork work, EmailService email, SmsService sms, ChatService chat, PhoneService phone, IConfiguration config)
        {
            _work = work;
            _email = email;
            _sms = sms;
            _chat = chat;
            _phone = phone;
            _config = config;
        }
        #region BaseSends
        public ISendResult SendEmail(IEmailTemplate email, MessageType messageType = MessageType.System)
        {
            return _email.SendFromTemplate(email);
        }

        public async Task<string> SendSms(ITaggMessage message, string number)
        {
            var messageId = await _sms.SendJobSmsAsync(number, message.Body);
            return messageId;
        }

        //public bool DeleteChatChannel(string uniqueId)
        //{
        //    return _chat.DeleteChannel(uniqueId);
        //}

        public bool DeleteChatChannel(string jobId, string subId)
        {
            try
            {
                var uniqueId = $"{jobId}-{subId}";
                var channel = _chat.GetChannelByInternalId(uniqueId);
                if (channel != null)
                {
                    return _chat.DeleteChannel(jobId, subId);
                }
                return true;
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        #endregion

        public BatchSendResult SendJobOffers(JobDetailsDto job, List<string> toSubIds)
        {
            var result = new BatchSendResult();
            foreach (var id in toSubIds)
            {
                var sub = _work.Subs.GetById(id);
                var offerResult = sendSubOffer(sub, job).Result;
                if (!offerResult.Succeeded)
                {
                    result.FailedIds.Add(id);
                }
                result.SMSId = offerResult.SmsID;
            }
            return result;
        }

        private async Task<SendSubOfferResult> sendSubOffer(Substitute sub, JobDetailsDto job)
        {
            var result = new SendSubOfferResult();
            AppMessage appMessage = new AppMessage
            {
                To = sub.Id,
                From = "Tagg",
                Subject = "New Job Offer!",
                Body = _config["SmsMessages:JobOffer"]
                    .Replace("_TEACHER_", job.TeacherName)
                    .Replace("_SCHOOL_", job.SchoolName)
                    .Replace("_GRADE_", SubjectLookup.Get(job.Subject))
                    .Replace("_DATE_", job.StartDate.ToShortDateString())
                    .Replace("_PAY_", $"${job.PayAmount}")
                    .Replace("-", " to ")
            };

            var schoolUserName = !string.IsNullOrWhiteSpace(job.CreatedBy) && job.CreatedBy != job.TeacherId ? await _work.Users.GetFullNameById(job.CreatedBy) : job.TeacherName;
            // Send in-app message
            var channelMetaData = new Dictionary<string, string>
            {
                {"isJob", "1" },
                {"teacherName", job.TeacherName },
                {"schoolUser", schoolUserName},
                {"subUser", $"{sub.FirstName} {sub.LastName}"},
                {"schoolName", job.SchoolName},
                {"grade", SubjectLookup.Get(job.Subject)},
                {"start", ConvertToMountainTime(job.StartDate).ToString("O") },
                {"end", ConvertToMountainTime(job.EndDate).ToString("O") },
                {"pay", job.PayAmount.ToString("C") },
                {"dateTitle", $"{ConvertToMountainTime(job.StartDate).ToString("MMMM d")} job" },
            };

            var channel = _chat.CreateChannel(
                        $"{ConvertToMountainTime(job.StartDate).ToString("MMM d")}, {job.SchoolName}",
                        $"{job.Id}-{sub.Id}",
                        channelMetaData,
                        $"Job has been accepted.");

            if (channel != null && channel.MembersCount == 0)
            {
                var member = _chat.AddUserToExistingChannel(sub.Id, channel.Sid);
            }

            // Send Email
            var user = _work.Users.GetByEmail(sub.Email);
            ISendResult externalResult = new SendResult(System.Net.HttpStatusCode.Accepted);
            if (sub.ReceiveEmail)
            {
                var template = _work.EmailTemplates.GetByType(EmailTemplateType.JobRequest);
                template.To.Add(sub.Email);
                template.Replacements = getOfferEmailData(job);
                externalResult = SendEmail(template);
            }

            // Send SMS
            if (sub.ReceiveSMS && user.PhoneNumberConfirmed)
            {
                Random generator = new Random();
                string smsUid = generator.Next(0, 999999).ToString("D6");
                appMessage.Body = appMessage.Body.Replace("_UID_", smsUid);
                var messageId = await SendSms(appMessage, user.PhoneNumber);
                result.SmsID = smsUid;
            }

            if (sub.ReceivePhone && !string.IsNullOrWhiteSpace(sub.PhoneNumber))
            {
                try
                {
                    _phone.InitiateJobCall(sub.PhoneNumber, job.Id, user.Id);
                }

                // If job call fails, swallow exception and move on
                catch (Exception ex)
                { }
            }

            if (externalResult.IsSuccessStatusCode && channel != null)
            {
                result.Succeeded = true;
            }
            return result;

        }

        private Dictionary<string, string> getOfferEmailData(JobDetailsDto job)
        {
            return new Dictionary<string, string>
            {
                {EmailReplacements.Date, ConvertToMountainTime(job.StartDate).ToLongDateString()},
                {EmailReplacements.School, job.SchoolName},
                {EmailReplacements.Teacher, job.TeacherName},
                {EmailReplacements.Grade, SubjectLookup.Get(job.Subject)},
                {EmailReplacements.Time, $"{TaggMessageService.ConvertToMountainTime(job.StartDate).ToShortTimeString()}-{TaggMessageService.ConvertToMountainTime(job.EndDate).ToShortTimeString()}"},
                {EmailReplacements.Pay, $"${job.PayAmount}"},
                {EmailReplacements.AddressStreet, $"{job.SchoolAddress.Street1} {job.SchoolAddress.Street2}" },
                {EmailReplacements.AddressCityState, $"{job.SchoolAddress.City}, {job.SchoolAddress.State} {job.SchoolAddress.PostalCode}"},
                {EmailReplacements.Href, $"{_email.Config["SubsClient:BaseUrl"]}"}
            };
        }

        private static DateTime ConvertToMountainTime(DateTime utc)
        {
            return TimeZoneInfo.ConvertTimeBySystemTimeZoneId(utc, "Mountain Standard Time");
        }

        //private List<Tuple<string,string>> getJobOfferHeaders(Job job)
        //{
        //    var schoolAddress = _work.Schools.GetQueryable()
        //        .Where(s => s.Id == job.SchoolId)
        //        .Select(s => s.Address)
        //        .FirstOrDefault();

        //    return new List<Tuple<string, string>>
        //    {
        //        Tuple.Create("Teacher", job.TeacherName),
        //        Tuple.Create("Grade", job.Subjects.FirstOrDefault()),
        //        Tuple.Create("Time", $"{job.StartDate.ToShortTimeString()} - {job.EndDate.ToShortTimeString()}"),
        //        Tuple.Create("Pay Rate", $"{job.PayAmount}"),
        //        Tuple.Create("Address", $"{schoolAddress?.Street1}\n{schoolAddress?.City},{schoolAddress?.State},{schoolAddress?.PostalCode}")
        //    };
        //}
    }
}
