﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tagg.Domain;
using Tagg.Domain.ApiModels;
using Twilio;
using Twilio.Base;
using Twilio.Jwt.AccessToken;
using Twilio.Rest.Chat.V2.Service;
using Twilio.Rest.Chat.V2.Service.Channel;

namespace Tagg.Services
{
    public class ChatService
    {
        private TaggUnitOfWork _work;
        private string _accountSid;
        private string _authToken;
        private string _apiKey;
        private string _apiSecret;
        private string _chatServiceId;

        public ChatService(IConfiguration config, TaggUnitOfWork work)
        {
            _work = work;
            _accountSid = config["Twilio:Sid"];
            _authToken = config["Twilio:Token"];
            _apiKey = config["Twilio:ApiKey"];
            _apiSecret = config["Twilio:ApiSecret"];
            _chatServiceId = config["Twilio:ChatServiceId"];
            TwilioClient.Init(_accountSid, _authToken);
        }

        public string GetChatToken(string userId)
        {
            var identity = userId;

            // Create a chat grant for the token
            var chatGrant = new ChatGrant()
            {
                ServiceSid = _chatServiceId
            };

            // Create a video grant for the token
            var grants = new HashSet<IGrant> { chatGrant };

            // Create an Access Token generator
            var token = new Token(_accountSid, _apiKey, _apiSecret, identity: identity, grants: grants);
            return token.ToJwt();
        }

        public UserResource CreateUser(string userId)
        {
            return UserResource.Create(pathServiceSid: _chatServiceId, identity: userId);
        }

        public UserResource GetUser(string userId)
        {
            return UserResource.Fetch(pathServiceSid: _chatServiceId, pathSid: userId);
        }

        public MemberResource AddUserToExistingChannel(string userId, string channelId)
        {
            var options = new CreateMemberOptions(_chatServiceId, channelId, userId);
            return MemberResource.Create(options);
        }

        public ChannelResource AddUserToNewChannel(string userId, string channelName)
        {
            var channel = CreateChannel(channelName);
            var member = AddUserToExistingChannel(userId, channel.Sid);
            channel = ChannelResource.Fetch(_chatServiceId, channel.Sid);
            return channel;
        }

        public ChannelResource CreateChannel(
            string channelName, 
            string internalId = "", 
            Dictionary<string, string> channelMetadata = null, 
            string defaultMessage = null, 
            string from = "system", 
            Dictionary<string, string> messageMetadata = null)
        {
            if (string.IsNullOrWhiteSpace(internalId))
            {
                internalId = Guid.NewGuid().ToString();
            }
            try
            {
                var existingChannel = this.GetChannelByInternalId(internalId);
                if (existingChannel != null)
                {
                    return existingChannel;
                }
                var options = new CreateChannelOptions(_chatServiceId)
                {
                    Type = "private",
                    FriendlyName = channelName,
                    UniqueName = internalId
                };
                if (channelMetadata != null)
                {
                    StringBuilder builder = new StringBuilder("{");
                    foreach (var item in channelMetadata)
                    {
                        builder.Append($"\"{item.Key}\":\"{item.Value}\",");
                    }
                    var metaResult = builder.ToString().TrimEnd(',');
                    metaResult += "}";
                    options.Attributes = metaResult;
                }

                var channelResult = ChannelResource.Create(options);

                
                if (channelResult != null && !string.IsNullOrWhiteSpace(defaultMessage))
                {
                    string metaResult = null;
                    if (messageMetadata != null)
                    {
                        StringBuilder builder = new StringBuilder("{");
                        foreach (var item in messageMetadata)
                        {
                            builder.Append($"\"{item.Key}\":\"{item.Value}\",");
                        }
                        metaResult = builder.ToString().TrimEnd(',');
                        metaResult += "}";
                    }

                    var defaultMessageResult = MessageResource.Create(
                        body: defaultMessage,
                        pathServiceSid: _chatServiceId,
                        pathChannelSid: channelResult.Sid,
                        from: from,
                        attributes: metaResult);
                }
                return channelResult;

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public MessageResource SendMessage(string content, string channel)
        {
            var channelResult = this.GetChannelByInternalId(channel);
            if (channelResult != null)
            {
                var result = MessageResource.Create(
                body: content,
                pathServiceSid: _chatServiceId,
                pathChannelSid: channel);

                return result;
            }
            throw new Exception("Failed to send message.");
        }

        public ChannelResource GetChannelBySid(string channelSid)
        {
            return ChannelResource.Fetch(_chatServiceId, channelSid);
        }

        public ResourceSet<ChannelResource> ListChannels()
        {
            var result = ChannelResource.Read(_chatServiceId);
            
            return result;
        }

        public ChannelResource GetChannelByInternalId(string internalId)
        {
            try
            {
                return ChannelResource.Fetch(_chatServiceId, internalId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public MessageResource SendSystemMessage(string channelId, string content)
        {
            var options = new CreateMessageOptions(_chatServiceId, channelId)
            {
                Body = content
            };
            var message = MessageResource.Create(options);
            return message;
        }

        public bool DeleteChannel(string channelId)
        {
            try
            {
                var options = new DeleteChannelOptions(_chatServiceId, channelId);
                return ChannelResource.Delete(options);
            }
            catch (Exception ex)
            {
                return true;
            }
        }

        public bool DeleteChannel(string jobId, string subId)
        {
            try
            {
                var uniqueId = $"{jobId}-{subId}";
                var options = new DeleteChannelOptions(_chatServiceId, uniqueId);
                return ChannelResource.Delete(options);
            }
            catch (Exception ex)
            {
                return true;
            }
        }

        public bool DeleteUser(string userId)
        {
            try
            {
                var options = new DeleteUserOptions(_chatServiceId, userId);
                return UserResource.Delete(options);
            }
            catch (Exception ex)
            {
                return true;
            }
        }

        public ResourceSet<UserResource> ListUsers()
        {
            var options = new ReadUserOptions(_chatServiceId);
            return UserResource.Read(options);
        }

        public async Task<bool> SendAdminMessage(AdminMessageDto dto)
        {
            try
            {
                var channelTo = "";
                var usersToAdd = new List<string>();
                var adminUser = _work.Users.GetById(dto.From);

                switch (dto.SendToOption)
                {
                    case SendToOption.User:
                        usersToAdd.Add(dto.SendTo);
                        channelTo = await _work.Users.GetFullNameById(dto.SendTo);
                        break;
                    case SendToOption.School:
                        usersToAdd = _work.Schools.GetUserIds(dto.SendTo);
                        channelTo = _work.Schools.Query(s => s.Id == dto.SendTo).FirstOrDefault()?.Name;
                        break;
                    case SendToOption.District:
                        usersToAdd = _work.Schools.GetUserIdsDistrict(dto.SendTo);
                        channelTo = _work.Schools.GetDistrict(dto.SendTo)?.Name;
                        break;
                    case SendToOption.AllUsers:
                        usersToAdd = _work.Users.GetQueryable().Select(x => x.Id).ToList();
                        channelTo = "All Tagg Users";
                        break;
                    default:
                        break;
                }

                var channelMetaData = new Dictionary<string, string>
                {
                    {"isJob", "0" },
                    {"teacherName", "" },
                    {"schoolUser", ""},
                    {"subUser", ""},
                    {"schoolName", ""},
                    {"grade", ""},
                    {"start", "" },
                    {"end", "" },
                    {"pay", "" },
                    {"dateTitle", $"Message from Tagg" },
                    {"adminUser",  $"{adminUser.FirstName} {adminUser.LastName}"},
                    {"to", channelTo}
                };

                var defaultMessageMeta = new Dictionary<string, string>
                {
                    {"fromName", $"{adminUser.FirstName} {adminUser.LastName}"}
                };

                var newChannel = this.CreateChannel(
                    channelName: dto.Subject, internalId: null, 
                    channelMetadata: channelMetaData, 
                    defaultMessage: dto.Body, 
                    from: adminUser.Id, 
                    messageMetadata: defaultMessageMeta);

                // Add system admin user(s) to channel
                var taggAddResult = this.AddUserToExistingChannel(dto.From, newChannel.Sid);

                foreach (var usr in usersToAdd)
                {
                    try { this.AddUserToExistingChannel(usr, newChannel.Sid); }
                    catch { }

                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
