﻿using System;
using System.Collections.Generic;
using System.Text;
using Tagg.Domain;

namespace Tagg.Services
{
    public class PaymentAccountData : IPaymentAccount
    {
        public string Id { get; set; }
        public string Email { get; set; }
    }
}
