﻿using System;
using System.Collections.Generic;
using System.Text;
using Tagg.Domain;
using Tagg.Services.Payments;
using AutoMapper;
using Stripe;
using Microsoft.Extensions.Configuration;

namespace Tagg.Services
{
    public class PaymentService : IPaymentService
    {
        public PaymentService(IConfiguration config)
        {
            Stripe.StripeConfiguration.SetApiKey(config["Stripe:SecretKey"]);
            var mapper = configureMapper();
            this.Customers = new StripeCustomerWrapper(mapper);
            this.Accounts = new StripeAccountWrapper(mapper);
            this.Webhooks = new StripeWebhooksWrapper(mapper);
        }
        public ICustomerService Customers { get; set; }
        public IAccountService Accounts { get; set; }
        public IWebhooksService Webhooks { get; set; }

        private IMapper configureMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<StripeCustomer, ICustomer>()
                    .ForMember(dest => dest.PaymentSourceId, src => src.MapFrom(x => x.DefaultSourceId));
            });
            return config.CreateMapper();
        }
    }
}
