﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using Tagg.Domain;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace Tagg.Services
{
    public class PhoneService
    {
        private TaggUnitOfWork _work;
        private string _accountSid;
        private string _authToken;
        private string _apiKey;
        private string _apiSecret;
        private string _from;
        private string _phoneWebhook;

        public PhoneService(IConfiguration config, TaggUnitOfWork work)
        {
            _work = work;
            _accountSid = config["Twilio:Sid"];
            _authToken = config["Twilio:Token"];
            _apiKey = config["Twilio:ApiKey"];
            _apiSecret = config["Twilio:ApiSecret"];
            _from = config["Twilio:From"];
            _phoneWebhook = config["Twilio:PhoneWebhook"];
            TwilioClient.Init(_accountSid, _authToken);
        }

        public void InitiateJobCall(string phoneNumber, string jobId, string subId)
        {
            try
            {
                var to = new PhoneNumber($"+1{phoneNumber}");
                var from = _from;
                var call = CallResource.Create(
                    to,
                    from,
                    url: new Uri(_phoneWebhook + jobId + "/" + subId));
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}
