﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Tagg.Domain;
using Tagg.Domain.Helpers;
using Twilio;
using Twilio.Rest.Api.V2010.Account;
using Twilio.Types;

namespace Tagg.Services
{
    public class SmsService : ISmsService
    {
        private string _sid;
        private string _token;
        private string _from;
        private IConfiguration _config;
        private List<string> _whitelist;

        public SmsService(IConfiguration config)
        {
            _sid = config["Twilio:Sid"];
            _token = config["Twilio:Token"];
            _from = config["Twilio:From"];
            _config = config;
            var whiteListConfig = config["WhiteList:SMS"];
            _whitelist = string.IsNullOrWhiteSpace(whiteListConfig) ? null : whiteListConfig.Split(',').ToList();
        }
        public async Task SendSmsAsync(string number, string message)
        {
            if (whiteListCheck(number))
            {
                TwilioClient.Init(_sid, _token);
                await MessageResource.CreateAsync(new PhoneNumber(number), from: new PhoneNumber(_from), body: message);
            }
        }

        public async Task<string> SendJobSmsAsync(string number, string message)
        {
            if (whiteListCheck(number))
            {
                TwilioClient.Init(_sid, _token);
                var response = await MessageResource.CreateAsync(new PhoneNumber(number), from: new PhoneNumber(_from), body: message);
                return response.Sid;
            }
            return string.Empty;
        }

        public async Task<List<SmsHistory>> GetMessagesByAccountId(string accountSid)
        {
            var result = new List<SmsHistory>();
            if (!string.IsNullOrWhiteSpace(accountSid))
            {
                TwilioClient.Init(_sid, _token);
                var query = new ReadMessageOptions
                {
                    PathAccountSid = accountSid,
                    Limit = 25,
                };

                var messages = await MessageResource.ReadAsync(query);
                result = messages.OrderByDescending(x => x.DateSent)
                    .Select(m => new SmsHistory {  DateSent = m.DateSent, Sid = m.Sid, To = m.To })
                    .ToList();
            }
            return result;
        }

        private bool whiteListCheck(string number)
        {
            if (!string.IsNullOrWhiteSpace(number) && _whitelist != null)
            {
                var numberClean = Regex.Replace(number, @"[\D]", "");
                return _whitelist.Count(x => x.ToLower() == numberClean) > 0;
            }
            return true;
        }
    }
}
