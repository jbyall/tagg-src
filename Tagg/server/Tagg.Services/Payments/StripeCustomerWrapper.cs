﻿using AutoMapper;
using Stripe;
using System;
using System.Collections.Generic;
using System.Text;
using Tagg.Domain;
using Tagg.Domain.ApiModels;

namespace Tagg.Services.Payments
{
    public class StripeCustomerWrapper : StripeServiceBase, ICustomerService
    {
        public StripeCustomerWrapper(IMapper mapper) : base(mapper) { }

        public ICustomer Create(CustomerCreateDto customer)
        {
            StripeCustomer customerResult = _customers.Create(new StripeCustomerCreateOptions
            {
                SourceToken = customer.SourceTokenId,
                Description = customer.Description,
            });

            var result = new Customer
            {
                Email = customerResult.Email,
                Id = customerResult.Id
            };
            return result;
        }

        public ICustomer GetById(string customerId)
        {
            StripeCustomer stripeResult = _customers.Get(customerId);
            return _mapper.Map<ICustomer>(stripeResult);
        }

        public bool VerifyBank(BankVerify dto)
        {
            try
            {
                var verifyBankOptions = new BankAccountVerifyOptions
                {
                    AmountOne = dto.AmountOne,
                    AmountTwo = dto.AmountTwo
                };
                CustomerBankAccount acct = _bank.Verify(dto.CustomerId, dto.BankAccountId, verifyBankOptions);
                return true;
            }
            catch (StripeException ex)
            {
                return false;
            }
            
        }

        public ChargeResponseDto CreateCharge(ConnectChargeDto dto)
        {

            try
            {
                var options = new StripeChargeCreateOptions
                {
                    Amount = dto.TotalAmountCents,
                    Currency = "usd",
                    Description = dto.Description,
                    CustomerId = dto.CustomerId,
                    Destination = dto.DestinationId,
                    DestinationAmount = dto.DestinationAmountCents
                };
                StripeCharge result = _charges.Create(options);
                return new ChargeResponseDto
                {
                    Succeeded = true,
                    Id = result.Id,
                    Status = result.Status,
                    TransactionId = result.BalanceTransactionId
                };
            }
            catch (Exception ex)
            {
                return new ChargeResponseDto
                {
                    Succeeded = false,
                    Status = ex.Message
                };
            }
        }

        //private int getTotalCharge(ConnectChargeDto dto)
        //{
        //    var payRate = dto.PayRateCents;
        //    var serviceFeeCheck = .25 * dto.PayRateCents;
        //    var serviceFee = serviceFeeCheck > 35 ? 35 : serviceFeeCheck;
        //    var transactionFee = .0124 * (payRate + serviceFee);
        //    var totalCharge = transactionFee + serviceFee + payRate;
        //    return usdDoubleToIntCents(totalCharge);
        //}

        
    }
}
