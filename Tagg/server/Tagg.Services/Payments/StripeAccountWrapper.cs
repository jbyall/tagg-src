﻿using AutoMapper;
using Stripe;
using System;
using System.Collections.Generic;
using System.Text;
using Tagg.Domain;

namespace Tagg.Services.Payments
{
    public class StripeAccountWrapper : StripeServiceBase, IAccountService
    {
        public StripeAccountWrapper(IMapper mapper):base(mapper) { }
        public IPaymentAccount GetAccountData(string code, string secret)
        {
            var token = _tokens.Create(new StripeOAuthTokenCreateOptions
            {
                ClientSecret = secret,
                Code = code,
                GrantType = "authorization_code",
                Scope = "express",
            });

            var account = _accounts.Get(token.StripeUserId);

            return new PaymentAccountData { Id = account.Id, Email = account.Email };
        }

        public string GetAccountLogin(string accountId)
        {
            var loginUrl = _login.Create(accountId);
            return loginUrl.Url;
        }
    }
}
