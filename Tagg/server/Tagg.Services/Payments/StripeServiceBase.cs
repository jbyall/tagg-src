﻿using AutoMapper;
using Stripe;
using System;
using System.Collections.Generic;
using System.Text;

namespace Tagg.Services.Payments
{
    public abstract class StripeServiceBase
    {
        internal StripeCustomerService _customers;
        internal StripeAccountService _accounts;
        internal StripeOAuthTokenService _tokens;
        internal StripeLoginLinkService _login;
        internal BankAccountService _bank;
        internal StripeChargeService _charges;
        internal IMapper _mapper;

        public StripeServiceBase(IMapper mapper)
        {
            _mapper = mapper;
            _customers = new StripeCustomerService();
            _accounts = new StripeAccountService();
            _tokens = new StripeOAuthTokenService();
            _login = new StripeLoginLinkService();
            _bank = new BankAccountService();
            _charges = new StripeChargeService();
        }
    }
}
