﻿using AutoMapper;
using Stripe;
using System;
using System.Collections.Generic;
using System.Text;
using Tagg.Domain;

namespace Tagg.Services.Payments
{
    public class StripeWebhooksWrapper : StripeServiceBase, IWebhooksService
    {
        public StripeWebhooksWrapper(IMapper mapper): base(mapper) { }

        public void ProcessCallback(string json)
        {
            StripeEvent sEvent = StripeEventUtility.ParseEvent(json);
            switch (sEvent.Type)
            {
                case StripeEvents.AccountUpdated:
                    // When connected (sub) accounts are changes we'll need to do something
                    break;
                case StripeEvents.ChargeFailed:
                case StripeEvents.ChargeSucceeded:
                case StripeEvents.ChargePending:
                    // When charges happen, we'll need to do something (update transaction)?
                    StripeEventData data = sEvent.Data;
                    break;
                default:
                    break;
            }

        }
    }
}
