﻿using Microsoft.Extensions.Configuration;
using MongoDB.Driver.GeoJsonObjectModel;
using RestSharp;
using RestSharp.Deserializers;
using System;
using System.Collections.Generic;
using System.Linq;
using Tagg.Domain;
using Tagg.Services.Geo;

namespace Tagg.Services
{
    public class DistanceResult
    {
        public int Value { get; set; }
        public string Text { get; set; }
        public Location ServiceLocation { get; set; }
        public bool InServiceArea { get; set; }
    }

    public class LocationService
    {
        public const string zeroResults = "ZERO_RESULTS";
        private string geocodeApiKey;
        private string distanceApiKey;
        private string geocodeBaseUri;
        private string distanceBaseUri;

        public LocationService(IConfiguration config)
        {
            geocodeApiKey = config["Google:GeocodingApiKey"];
            distanceApiKey = config["Google:DistanceApiKey"];
            geocodeBaseUri = config["Google:GeocodingBaseUrl"];
            distanceBaseUri = config["Google:DistanceBaseUrl"];
        }

        public List<DistanceResult> GetDistanceFromServiceAreas(string postalCode, int maxDistance = 100)
        {
            var results = new List<DistanceResult>();

            // SET ORIGIN LOCATION
            // TODO : Do this check from all schools (may need a location repository)
            var origin = new Location
            {
                Latitude = 38.886967,
                Longitude = -104.730710
            };
            // GET APPROX LOCATION OF POSTAL CODE
            var destination = getPostalCodeLocation(postalCode);
            var distances = GetDistancesBetweenLocations(origin, destination);
            foreach (var item in distances)
            {
                results.Add(new DistanceResult
                {
                    ServiceLocation = origin,
                    Value = item.value,
                    Text = item.text,
                    InServiceArea = (item.value / 1609) < maxDistance
                });
            }
            return results;
        }

        public ILocation GetLocationFromAddress(Address address)
        {
            try
            {
                var streetParts = address.Street1.Split(' ').ToList();
                var cityParts = address.City.Split(' ').ToList();
                var addressQueryBase = $"{geocodeBaseUri}address=";
                streetParts.ForEach(p => addressQueryBase += $"{p}+");
                addressQueryBase.TrimEnd('+');
                cityParts.ForEach(c => addressQueryBase += $"{c}+");
                addressQueryBase.TrimEnd('+');
                addressQueryBase += $"{address.State}&key={geocodeApiKey}";

                var client = new RestClient(addressQueryBase);
                var request = new RestRequest(Method.GET);

                var response = client.Execute(request);
                JsonDeserializer jsonDeserializer = new JsonDeserializer();
                var result = jsonDeserializer.Deserialize<GeocodeApiResponse>(response);
                return result.GetLocation();
            }
            catch (Exception ex)
            {
                return new Location();
            }

        }

        public GeoJsonPoint<GeoJson2DGeographicCoordinates> GetJsonLocationFromAddress(Address schoolAddress)
        {
            var location = this.GetLocationFromAddress(schoolAddress);
            return new GeoJsonPoint<GeoJson2DGeographicCoordinates>(
                new GeoJson2DGeographicCoordinates(location.Longitude, location.Latitude));
        }

        private Location getPostalCodeLocation(string postalCode)
        {
            var postalQueryUri = $"{geocodeBaseUri}components=postal_code:{postalCode}&key={geocodeApiKey}";

            var client = new RestClient(postalQueryUri);
            var request = new RestRequest(Method.GET);

            var response = client.Execute(request);
            JsonDeserializer jsonDeserializer = new JsonDeserializer();
            var result = jsonDeserializer.Deserialize<GeocodeApiResponse>(response);

            if (result.status.Equals(LocationService.zeroResults))
            {
                throw new InvalidLocationException(postalCode);
            }

            return result.GetLocation();
        }

        public List<Distance> GetDistancesBetweenLocations(Location origin, Location destination)
        {
            var distanceQueryUri = $"{distanceBaseUri}units=imperial&origins={origin.Latitude},{origin.Longitude}&destinations={destination.Latitude},{destination.Longitude}&key={distanceApiKey}";

            var client = new RestClient(distanceQueryUri);
            var request = new RestRequest(Method.GET);

            var response = client.Execute(request);
            JsonDeserializer jsonDeserializer = new JsonDeserializer();
            var result = jsonDeserializer.Deserialize<DistanceApiResponse>(response);
            return result.GetDistances();
        }
    }
}
