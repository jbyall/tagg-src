﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using AutoMapper;
using Tagg.Domain;
using Tagg.Services;

using Microsoft.Extensions.FileProviders;
using System.IO;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.IdentityModel.Protocols;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.AspNetCore.Authentication.OpenIdConnect;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Http;

namespace Tagg.Admin
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddSingleton<IConfiguration>(Configuration);
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            

            services.AddMvc();

            services.AddAuthentication(options =>
            {
                options.DefaultScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultSignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = OpenIdConnectDefaults.AuthenticationScheme;
                //options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
                .AddCookie(options =>
                {
                    options.ExpireTimeSpan = TimeSpan.FromMinutes(60);
                    options.Cookie.Name = Configuration["TaggIdentity:CookieName"];
                })
                .AddOpenIdConnect(OpenIdConnectDefaults.AuthenticationScheme, options =>
                {
                    options.SignInScheme = CookieAuthenticationDefaults.AuthenticationScheme;
                    options.RequireHttpsMetadata = false;
                    options.Authority = Configuration["TaggIdentity:Authority"];
                    options.ClientId = Configuration["TaggIdentity:ClientId"];
                    options.ResponseType = "code id_token";
                    options.SignInScheme = "Cookies";
                    options.Scope.Clear();
                    options.Scope.Add("openid");
                    options.Scope.Add("profile");
                    options.Scope.Add("api_access");
                    options.Scope.Add("taggapi");
                    options.Scope.Add("offline_access");
                    options.ClientSecret = Configuration["TaggIdentity:ClientSecret"]; ;

                    options.GetClaimsFromUserInfoEndpoint = true;
                    options.SaveTokens = true;

                    options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
                    {
                        NameClaimType = "name",
                        RoleClaimType = "role"
                    };
                });

            // Add configuration to be used in the app
            services.AddSingleton<IConfiguration>(Configuration);
            services.AddAutoMapper();
            services.AddScoped<TaggApiService>();

            // Add auth policies
            services.AddAuthorization(options =>
            {
                //options.AddPolicy(AuthPolicies.TaggUser, policy =>
                //    policy.RequireClaim(TaggClaimTypes.ApiAccess, ApiAccessLevels.Substitute, ApiAccessLevels.Teacher, ApiAccessLevels.SchoolAdmin, ApiAccessLevels.SystemAdmin));

                // TODO : Test policy for any user
                options.AddPolicy(AuthPolicies.TaggUser, policy =>
                    policy.RequireAssertion(context =>
                        context.User.HasClaim(c => c.Type == TaggClaimTypes.ApiAccess)
                    )
                );

                options.AddPolicy(AuthPolicies.Substitute, policy =>
                    policy.RequireClaim(TaggClaimTypes.ApiAccess, ApiAccessLevels.Substitute));

                options.AddPolicy(AuthPolicies.Teacher, policy =>
                    policy.RequireClaim(TaggClaimTypes.ApiAccess, ApiAccessLevels.Teacher));


                options.AddPolicy(AuthPolicies.SchoolAdmin, policy =>
                {
                    policy.RequireClaim(TaggClaimTypes.ApiAccess, ApiAccessLevels.SchoolAdmin);
                });

                // TODO : Test policy for any admin
                options.AddPolicy(AuthPolicies.AnyAdmin, policy =>
                    policy.RequireAssertion(context =>
                        context.User.HasClaim(c => c.Type == TaggClaimTypes.ApiAccess && (c.Value == ApiAccessLevels.SchoolAdmin || c.Value == ApiAccessLevels.SystemAdmin))
                    )
                );

                options.AddPolicy(AuthPolicies.SystemAdmin, policy =>
                    policy.RequireClaim(TaggClaimTypes.ApiAccess, ApiAccessLevels.SystemAdmin));

                //options.AddPolicy(AuthPolicies.SchoolAdminTemp, policy =>
                //    policy.RequireClaim(TaggClaimTypes.ApiAccess, ApiAccessLevels.SchoolAdminTemp));

                //options.AddPolicy(AuthPolicies.TeacherTemp, policy =>
                //    policy.RequireClaim(TaggClaimTypes.ApiAccess, ApiAccessLevels.TeacherTemp));
            });
            
            #region deleteHopefully
            //var jwtConfiguration = new Open

            //services.AddIdentity<MongoUser, MongoRole>()
            //    .RegisterMongoStores<MongoUser, MongoRole>(Configuration.GetConnectionString("MongoConnection"))
            //    .AddUserManager<UserManager<MongoUser>>()
            //    .AddSignInManager<SignInManager<MongoUser>>()
            //    .AddDefaultTokenProviders();




            //services.ConfigureApplicationCookie(options =>
            //{
            //    options.LoginPath = "/Home/Login";
            //    options.LogoutPath = "/Home/Logout";
            //    options.Cookie.Name = "TaggAdminCookie";
            //    options.Cookie.Expiration = TimeSpan.FromMinutes(60);
            //});

            //services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            //    .AddJwtBearer(options =>
            //    {
            //        options.Audience = "test";
            //        options.Authority = "http://localhost:50706";
            //        options.RequireHttpsMetadata = false;
            //        options.Events = new JwtBearerEvents
            //        {

            //        }
            //    });



            // Try this: https://github.com/aspnet/Security/blob/c5b566ed4abffac4cd7011e0465e012cf503c871/samples/JwtBearerSample/Startup.cs#L47-L53
            // Test this: Reference https://github.com/aspnet/Home/issues/2007
            //services.AddAuthorization(options =>
            //{
            //    options.DefaultPolicy = new AuthorizationPolicyBuilder(JwtBearerDefaults.AuthenticationScheme)
            //    .RequireAuthenticatedUser()
            //    .Build();
            //});
            //services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            //    .AddCookie(options =>
            //    {
            //        options.LoginPath = "/Home/login";
            //    });

            //var connectionString = Configuration.GetConnectionString("MongoConnection");

            //services.AddScoped<TaggDbContext>();
            //services.AddTransient<TaggSeed>(opt =>
            //{
            //    return new TaggSeed(connectionString);
            //});






            //// Add application services.
            //services.AddTransient<IEmailService, EmailService>((cfg) => {
            //    return new EmailService(Configuration);
            //});

            //services.AddTransient<ISmsService, SmsService>((cfg) =>
            //{
            //    return new SmsService(Configuration);
            //});

            //// Add tagg services
            //services.AddScoped<LocationService>((ctx) =>
            //{
            //    return new LocationService(Configuration);
            //});

            //services.AddScoped<TaggUnitOfWork>((ctx) =>
            //{
            //    return new TaggUnitOfWork(connectionString);
            //});
            #endregion

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();
            loggerFactory.AddDebug();

            // TODO : Disable this and uncomment below before production
            app.UseDeveloperExceptionPage();

            //if (env.IsDevelopment())
            //{
            //    //app.UseBrowserLink();
            //    app.UseDeveloperExceptionPage();
            //}
            //else
            //{
            //    app.UseExceptionHandler("/Home/Error");
            //}

            var fileProvider = new FileExtensionContentTypeProvider();
            fileProvider.Mappings[".csv"] = "text/csv";
            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")),
                ContentTypeProvider = fileProvider
            });
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseMvcWithDefaultRoute();
        }
    }
}
