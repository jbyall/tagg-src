﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authentication;
using RestSharp;
using RestSharp.Deserializers;
using Tagg.Domain.ApiModels;
using Microsoft.AspNetCore.Http.Authentication;
using Tagg.Domain;
using Newtonsoft.Json;
using IdentityModel.Client;
using System.Globalization;

namespace Tagg.Admin
{
    public class TaggApiService
    {
        private IConfiguration _config;
        private IMapper _mapper;
        private RestClient _client;
        private IHttpContextAccessor _httpContext;

        public TaggApiService(IConfiguration config, IMapper mapper, IHttpContextAccessor httpContext)
        {
            _config = config;
            _mapper = mapper;
            _client = new RestClient(_config["TaggApi:BaseUri"]);
            _httpContext = httpContext;

            this.Schools = new SchoolRequest(_client, _config, _httpContext);
            this.Lookups = new LookupRequest(_client, _config, _httpContext);
            this.Teachers = new TeacherRequest(_client, _config, _httpContext);
            this.Subs = new SubsRequest(_client, _config, _httpContext);
            this.Users = new UserRequest(_client, _config, _httpContext);
            this.Jobs = new JobRequest(_client, _config, _httpContext);
        }

        public SchoolRequest Schools { get; set; }
        public LookupRequest Lookups { get; set; }
        public TeacherRequest Teachers { get; set; }
        public SubsRequest Subs { get; set; }
        public UserRequest Users { get; set; }
        public JobRequest Jobs { get; set; }
    }

    public class LookupRequest : TaggApiRequest
    {
        public const string StatesUri = "lookup/states";
        public const string SchoolTypesUri = "lookup/schooltypes";
        public const string SubjectsUri = "lookup/subjects";

        public LookupRequest(RestClient client, IConfiguration config, IHttpContextAccessor http)
            : base(client, config, http) { }

        public async Task<List<Tagg.Domain.Lookup>> GetStates()
        {
            var request = new RestRequest(StatesUri, Method.GET);
            var resp = await this.Execute<List<Tagg.Domain.Lookup>>(request);
            return resp.Data;
        }

        public async Task<List<Tagg.Domain.Lookup>> GetSchoolTypes()
        {
            var request = new RestRequest(SchoolTypesUri, Method.GET);
            var resp = await this.Execute<List<Tagg.Domain.Lookup>>(request);
            return resp.Data;
        }

        public async Task<List<Tagg.Domain.Lookup>> GetSubjects()
        {
            var request = new RestRequest(SubjectsUri, Method.GET);
            var resp = await this.Execute<List<Tagg.Domain.Lookup>>(request);
            return resp.Data;
        }
    }

    public class JobRequest: TaggApiRequest
    {
        public const string Resource = "jobs/";
        public const string CreateUri = Resource + "create";
        public const string GetAllUri = Resource + "all";

        public JobRequest(RestClient client, IConfiguration config, IHttpContextAccessor http)
            : base(client, config, http) { }

        public async Task<List<JobDto>> GetAll()
        {
            var request = new RestRequest(GetAllUri, Method.GET);
            var response = await this.Execute(request);

            // TODO : Add this to the base class to handle dates properly
            var dateSettings = new JsonSerializerSettings { DateTimeZoneHandling = DateTimeZoneHandling.Local };
            return JsonConvert.DeserializeObject<List<JobDto>>(response.Content, dateSettings);
        }

        public async Task<JobCreateDto> Create(JobCreateDto job)
        {
            var request = new RestRequest(CreateUri, Method.POST);
            request.AddJsonBody(job);
            var response = await this.Execute<JobCreateDto>(request);
            return response.Data;
        }
    }

    public class TeacherRequest: TaggApiRequest
    {
        public const string Resource = "teachers/";
        public const string GetAllUri = Resource + "all";
        public const string GetSingleUri = Resource;
        public const string GetUserSchoolsUri = "schools/user/";
        public const string CreateUri = Resource + "create/";
        public const string GetByStatusUri = Resource + "status/";
        public const string UploadUri = Resource + "Upload/";
        

        public TeacherRequest(RestClient client, IConfiguration config, IHttpContextAccessor http)
            : base(client, config, http) { }

        public async Task<TeacherDto> GetById(string id)
        {
            var request = new RestRequest(Resource + id, Method.GET);
            var resp = await this.Execute<TeacherDto>(request);
            return resp.Data;
        }

        public async Task<List<TeacherDto>> GetAll()
        {
            var request = new RestRequest(GetAllUri, Method.GET);
            var resp = await this.Execute<List<TeacherDto>>(request);
            return resp.Data;
        }

        public async Task<TeacherDto> Create(TeacherDto teacher, string schoolId)
        {
            var request = new RestRequest(CreateUri + schoolId, Method.POST);
            request.AddJsonBody(teacher);
            var resp = await this.Execute<TeacherDto>(request);
            return resp.Data;
        }

        public async Task<List<SchoolDto>> GetTeacherSchools(string email)
        {
            var request = new RestRequest(GetUserSchoolsUri + email, Method.GET);
            var result = await this.Execute<List<SchoolDto>>(request);
            return result.Data;
        }

        public async Task<List<TeacherDto>> GetByStatus(string status)
        {
            var request = new RestRequest(GetByStatusUri + status, Method.GET);
            var result = await this.Execute<List<TeacherDto>>(request);
            return result.Data;
        }

        public async Task<bool> Upload(string schoolId, IFormFile file)
        {
            var request = new RestRequest(UploadUri + schoolId, Method.POST);
            request.AddObject(file);
            var result = await this.Execute(request);
            return result.IsSuccessful;
        }
    }

    public class SubsRequest : TaggApiRequest
    {
        public const string Resource = "Subs/";
        public const string GetAllUri = Resource + "all";
        public const string CreateUri = Resource + "create";
        public const string GetSingleUri = Resource;
        public const string ApproveUri = Resource + "approve/";
        public const string DisableUri = Resource + "disable/";
        public const string ExcludeUri = Resource + "exclude/";
        public const string RemoveExclusionUri = Resource + "exclude/remove/";

        public SubsRequest(RestClient client, IConfiguration config, IHttpContextAccessor http)
            : base(client, config, http) { }

        public async Task<bool> Exclude(string email, string schoolId)
        {
            var request = new RestRequest(ExcludeUri + $"{email}/{schoolId}", Method.GET);
            var response = await this.Execute(request);
            if (response.IsSuccessful)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> RemoveExclusion(string email, string schoolId)
        {
            var request = new RestRequest(RemoveExclusionUri + $"{email}/{schoolId}", Method.GET);
            var response = await this.Execute(request);
            if (response.IsSuccessful)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> Disable(string email)
        {
            var request = new RestRequest(DisableUri + email, Method.GET);
            var response = await this.Execute(request);
            if (response.IsSuccessful)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> Approve(string email)
        {
            var request = new RestRequest(ApproveUri + email, Method.GET);
            var response = await this.Execute(request);
            if (response.IsSuccessful)
            {
                return true;
            }
            return false;
        }

        public async Task<List<SubstituteDto>> GetAll()
        {
            var request = new RestRequest(GetAllUri, Method.GET);
            var response = await this.Execute<List<SubstituteDto>>(request);
            return response.Data;
        }

        public async Task<SubstituteDto> Create(SubstituteDto sub)
        {
            var request = new RestRequest(CreateUri, Method.POST);
            var response = await this.Execute<SubstituteDto>(request);
            return response.Data;
        }

        public async Task<SubstituteDto> GetSingle(string email)
        {
            var request = new RestRequest(GetSingleUri + email, Method.GET);
            var response = await this.Execute<SubstituteDto>(request);
            return response.Data;
        }
    }

    public class UserRequest : TaggApiRequest
    {
        public UserRequest(RestClient client, IConfiguration config, IHttpContextAccessor http)
            :base(client, config, http) { }

        public const string Resource = "users/";
        public const string GetSingleUri = Resource;
        public const string CreateAdminUri = Resource + "admin/school/";
        public const string GetAllUri = Resource + "all";
        public const string CreateUserUri = Resource + "Create";
        public const string SendInviteUri = Resource + "Invite/";
        public const string ConfirmEmailUri = "Register/Email/Confirm/";

        public async Task<UserDto> GetSingle(string email)
        {
            var request = new RestRequest(GetSingleUri + email, Method.GET);
            var response = await this.Execute<UserDto>(request);
            return response.Data;
        }

        public async Task<bool> CreateSchoolAdmin(string schoolId, UserCreateDto user)
        {
            var request = new RestRequest(CreateAdminUri + schoolId, Method.POST);
            request.AddJsonBody(user);
            var response = await this.Execute<object>(request);
            if (response.IsSuccessful)
            {
                return true;
            }
            return false;
        }

        public async Task<List<UserDto>> GetAll()
        {
            var request = new RestRequest(GetAllUri, Method.GET);
            var response = await this.Execute<List<UserDto>>(request);
            return response.Data;
        }

        public async Task<UserDto> CreateUser(UserCreateDto user)
        {
            var request = new RestRequest(CreateUserUri, Method.POST);
            request.AddJsonBody(user);
            var response = await this.Execute<UserDto>(request);
            return response.Data;
        }

        public async Task<bool> SendInvite(string email)
        {
            var request = new RestRequest(SendInviteUri + email, Method.GET);
            var result = await this.Execute<RestResponse>(request);
            if (result.IsSuccessful)
            {
                return true;
            }
            return false;
        }

        public async Task<bool> ConfirmEmail(string email)
        {
            var request = new RestRequest(ConfirmEmailUri + email, Method.GET);
            var result = await this.Execute<RestResponse>(request);
            if (result.IsSuccessful)
            {
                return true;
            }
            return false;
        }
    }

    public class SchoolRequest : TaggApiRequest
    {
        #region constants
        public const string Resource = "schools/";
        public const string GetAllUri = Resource + "all";
        public const string GetSingleUri = Resource;
        public const string CreateUri = Resource + "Create/";
        public const string UpdateUri = Resource + "update";

        public const string DistrictsCreateUri = Resource + "District";
        public const string DistrictsGetAllUri = Resource + "District/all";
        public const string DistrictsSingleUri = Resource + "District/single/";

        public const string TeachersGetUri = Resource + "teachers/";

        public const string InquiryAllUri = Resource + "Inquiry/All";
        public const string InquirySingleUri = Resource + "Inquiry/";
        public const string InquiriesByStatusUri = Resource + "Inquiry/status/";
        public const string InquiriesUpdateStatusUri = Resource + "Inquiry/status/";

        #endregion
        public SchoolRequest(RestClient client, IConfiguration config, IHttpContextAccessor http)
            : base(client, config, http) { }

        public async Task<List<SchoolDto>> GetAll()
        {
            var request = new RestRequest(GetAllUri, Method.GET);
            var response = await this.Execute<List<SchoolDto>>(request);
            return response.Data;
        }

        public async Task<List<TeacherDto>> GetTeachers(string schoolId)
        {
            var request = new RestRequest(TeachersGetUri + schoolId, Method.GET);
            var response = await this.Execute<List<TeacherDto>>(request);
            return response.Data;
        }

        public async Task<SchoolDto> Get(string schoolId)
        {
            var request = new RestRequest(GetSingleUri + schoolId, Method.GET);
            var response = await this.Execute<SchoolDto>(request);
            return response.Data;
        }

        public async Task<SchoolDto> Create(SchoolDto school)
        {
            var request = new RestRequest(CreateUri, Method.POST);
            request.AddJsonBody(school);
            var response = await this.Execute<SchoolDto>(request);
            return response.Data;
        }

        public async Task<List<SchoolInquiryDto>> InquiriesGetAll()
        {
            var request = new RestRequest(InquiryAllUri, Method.GET);
            var response = await this.Execute<List<SchoolInquiryDto>>(request);
            return response.Data;
        }

        public async Task<SchoolInquiryDto> InquiriesGetSingle(string inquiryId)
        {
            var request = new RestRequest(InquirySingleUri + inquiryId, Method.GET);
            var resp = await this.Execute<SchoolInquiryDto>(request);
            return resp.Data;
        }

        public async Task<List<SchoolInquiryDto>> InquiriesByStatus(TaggStatus status)
        {
            var request = new RestRequest(InquiriesByStatusUri + status.ToString(), Method.GET);
            var response = await this.Execute<List<SchoolInquiryDto>>(request);
            return response.Data;
        }

        public async Task<bool> InquiriesUpdateStatus(string inquiryId, TaggStatus status)
        {
            var request = new RestRequest(InquiriesUpdateStatusUri + inquiryId, Method.PUT);
            request.AddJsonBody(status.ToString());
            var response = await this.Execute<bool>(request);
            return response.Data;
        }

        public async Task<DistrictDto> DistrictsCreate(DistrictDto district)
        {
            var request = new RestRequest(DistrictsCreateUri, Method.POST);
            request.AddJsonBody(district);
            var resp = await this.Execute<DistrictDto>(request);
            return resp.Data;

        }

        public async Task<List<DistrictDto>> DistrictsGetAll()
        {
            var request = new RestRequest(DistrictsGetAllUri, Method.GET);
            var resp = await this.Execute<List<DistrictDto>>(request);
            return resp.Data;
        }

        public async Task<DistrictDto> DistrictsSingle(string id)
        {
            var request = new RestRequest(DistrictsSingleUri + id, Method.GET);
            var resp = await this.Execute<DistrictDto>(request);
            return resp.Data;
        }

        public async Task<SchoolDto> UpdateAsync(SchoolDto school)
        {
            var request = new RestRequest(UpdateUri, Method.POST);
            request.AddJsonBody(school);
            var resp = await this.Execute<SchoolDto>(request); _client.Execute<SchoolDto>(request);
            return resp.Data;
        }

    }

    public class TaggApiRequest
    {
        internal RestClient _client;
        internal IHttpContextAccessor _httpContext;
        private IConfiguration _config;
        internal JsonDeserializer _json;

        public TaggApiRequest(RestClient client, IConfiguration config, IHttpContextAccessor httpContext = null)
        {
            _client = client;
            _httpContext = httpContext;
            _config = config;
        }

        public async Task<IRestResponse<TResult>> Execute<TResult>(RestRequest request) where TResult : new()
        {
            HttpContext context = _httpContext.HttpContext;
            var accessToken = await context.GetTokenAsync("access_token");
            var expiresAt = await context.GetTokenAsync("expires_at");
            if (string.IsNullOrWhiteSpace(expiresAt) || (DateTime.Parse(expiresAt).AddSeconds(-60)).ToUniversalTime() < DateTime.UtcNow)
            {
                accessToken = await RenewTokens();
            }

            if (!string.IsNullOrWhiteSpace(accessToken))
            {
                _client.AddDefaultHeader("Authorization", $"Bearer {accessToken}");
            }
            return _client.Execute<TResult>(request);
        }

        public async Task<IRestResponse> Execute(RestRequest request)
        {
            HttpContext context = _httpContext.HttpContext;
            var accessToken = await context.GetTokenAsync("access_token");
            if (!string.IsNullOrWhiteSpace(accessToken))
            {
                _client.AddDefaultHeader("Authorization", $"Bearer {accessToken}");
            }

            return _client.Execute(request);
        }

        private async Task<string> RenewTokens()
        {
            var context = _httpContext.HttpContext;
            var discoveryClient = new DiscoveryClient(_config["TaggIdentity:Authority"]);
            var metaDataResponse = await discoveryClient.GetAsync();

            var tokenClient = new TokenClient(metaDataResponse.TokenEndpoint, "adminlocalclient", "test");

            var currentRefreshToken = await context.GetTokenAsync("refresh_token");
            var result = await tokenClient.RequestRefreshTokenAsync(currentRefreshToken);
            if (!result.IsError)
            {
                var oldIdToken = await context.GetTokenAsync("id_token");
                var newAccessToken = result.AccessToken;
                var newRefreshToken = result.RefreshToken;

                var tokens = new List<AuthenticationToken>();
                tokens.Add(new AuthenticationToken { Name = "id_token", Value = oldIdToken });
                tokens.Add(new AuthenticationToken { Name = "access_token", Value = newAccessToken });
                tokens.Add(new AuthenticationToken { Name = "refresh_token", Value = newRefreshToken });

                var expiresAt = DateTime.UtcNow + TimeSpan.FromSeconds(result.ExpiresIn);
                tokens.Add(new AuthenticationToken { Name = "expires_at", Value = expiresAt.ToString("o", CultureInfo.InvariantCulture) });

                var info = await context.AuthenticateAsync("Cookies");
                info.Properties.StoreTokens(tokens);
                await context.SignInAsync("Cookies", info.Principal, info.Properties);
                return newAccessToken;
            }
            return "";
        }

        //private async Task<string> renewAccessToken()
        //{
        //    var context = _httpContext.HttpContext;

        //    var discoveryClient = new DiscoveryClient
        //}
    }
}
