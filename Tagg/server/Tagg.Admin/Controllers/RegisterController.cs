﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;

namespace Tagg.Admin.Controllers
{
    public class RegisterController : Controller
    {
        private TaggApiService _api;
        private IMapper _mapper;

        public RegisterController(TaggApiService api,
            IMapper mapper)
        {
            _api = api;
            _mapper = mapper;
        }

        [HttpGet]
        public IActionResult Confirmation(string id)
        {
            var decodedToken = Base64UrlEncoder.Decode(id);
            var test = new JwtSecurityTokenHandler().ReadJwtToken(decodedToken);
            var emailClaim = test.Claims.FirstOrDefault(c => c.Type == "email");
            var model = new ConfirmationViewModel { EmailAddress = emailClaim.Value};


            return View(model);
        }

        [HttpPost]
        public async Task<IActionResult> Confirmation(ConfirmationViewModel model)
        {
            var test = await _api.Users.ConfirmEmail(model.EmailAddress);
            return View();
        }

        public IActionResult Admin()
        {
            return View();
        }
    }

    public class ConfirmationViewModel
    {
        public string EmailAddress { get; set; }
    }

}