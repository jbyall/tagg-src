﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using Tagg.Admin.Models;
using Tagg.Domain;
using Tagg.Services;
using Tagg.Domain.ApiModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using AutoMapper;

namespace Tagg.Admin.Controllers
{
    public class JobsController : Controller
    {
        private readonly IConfiguration _config;
        private TaggApiService _api;
        private IMapper _mapper;

        public JobsController(
            IConfiguration config,
            TaggApiService api,
            IMapper mapper)
        {
            _config = config;
            _api = api;
            _mapper = mapper;

        }

        public async Task<IActionResult> Index()
        {
            var jobs = await _api.Jobs.GetAll();
            return View(jobs);
        }

        public async Task<IActionResult> Teachers()
        {
            var teachers = await _api.Teachers.GetByStatus("Active");
            return View(new TeacherSelectModel(teachers));
        }

        [HttpPost]
        public IActionResult Teachers(TeacherSelectModel model)
        {
            return RedirectToAction("Create", new { id = model.TeacherId });
        }

        [HttpGet]
        public async Task<IActionResult> Create(string id)
        {
            throw new NotImplementedException();
            var job = await populateDefaultJob(id);
            var model = new JobCreateViewModel(job);
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(JobCreateViewModel job)
        {
            if (ModelState.IsValid)
            {
                var dto = _mapper.Map<JobCreateDto>(job);
                dto.Status = JobStatus.Open;
                var createResult = await _api.Jobs.Create(dto);
                return RedirectToAction("Index");
            }
            return View(job);
        }

        

        private async Task<JobDetailsDto> populateDefaultJob(string teacherId)
        {
            var teacher = await _api.Teachers.GetById(teacherId);
            var schools = await _api.Teachers.GetTeacherSchools(teacherId);
            var school = schools.FirstOrDefault();

            TimeSpan startTime;
            TimeSpan endTime;
            if (school != null && TimeSpan.TryParse(school.StartTime, out startTime) && TimeSpan.TryParse(school.EndTime, out endTime))
            {
                // TODO : FIX THIS LATER
                var result = new JobDetailsDto(new School(), new Teacher(), new Job());
                return result;
            }
            return null;
        }
    }
}