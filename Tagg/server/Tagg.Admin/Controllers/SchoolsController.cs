﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MongoDB.Driver.GeoJsonObjectModel;
using Tagg.Admin.Models;
using Tagg.Domain.ApiModels;
using Tagg.Domain;
using Tagg.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.AspNetCore.Identity;

namespace Tagg.Admin.Controllers
{
    [Authorize]
    public class SchoolsController : Controller
    {
        private readonly IMapper _mapper;
        private readonly TaggApiService _api;

        public SchoolsController(
            IMapper mapper,
            TaggApiService api)
        {
            _api = api;
            _mapper = mapper;
        }

        // GET: Schools
        public async Task<IActionResult> Index()
        {
            var schools = await _api.Schools.GetAll();
            return View(schools);
        }

        // GET: Schools/Details/5
        public async Task<IActionResult> Details(string id)
        {
            var school = await _api.Schools.Get(id);
            var teachers = await _api.Schools.GetTeachers(id);
            var model = new SchoolDetailViewModel();
            model.School = school;
            model.Teachers = teachers;

            return View(model);
        }

        // GET: Schools/Create
        public async Task<IActionResult> Create()
        {
            ViewData["states"] = new SelectList(await _api.Lookups.GetStates(), "Key", "Value");
            ViewData["schoolTypes"] = new SelectList(await _api.Lookups.GetSchoolTypes(), "Key", "Value");
            ViewData["districts"] = new SelectList(await _api.Schools.DistrictsGetAll(), "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SchoolViewModel model)
        {
            // TODO : Fix the timespan formatting (needs to be string)
            if (ModelState.IsValid)
            {
                var school = _mapper.Map<SchoolDto>(model);
                var result = _api.Schools.Create(school);
                return RedirectToAction("Index");
            }
            ViewData["states"] = new SelectList(await _api.Lookups.GetStates(), "Key", "Value");
            ViewData["schoolTypes"] = new SelectList(await _api.Lookups.GetSchoolTypes(), "Key", "Value");
            ViewData["districts"] = new SelectList(await _api.Schools.DistrictsGetAll(), "Id", "Name");
            return View(model);
        }

        public IActionResult CreateDistrict()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateDistrict(DistrictDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _api.Schools.DistrictsCreate(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: Schools/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return RedirectToAction("Index");
            }
            ViewData["states"] = new SelectList(await _api.Lookups.GetStates(), "Key", "Value");
            ViewData["schoolTypes"] = new SelectList(await _api.Lookups.GetSchoolTypes(), "Key", "Value");
            ViewData["districts"] = new SelectList(await _api.Schools.DistrictsGetAll(), "Id", "Name");
            var school = await _api.Schools.Get(id);
            //model = populateSchoolModel(model);
            return View(school);
        }

        // POST: Schools/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(SchoolDto school)
        {
            if (ModelState.IsValid)
            {
                var result = await _api.Schools.UpdateAsync(school);
                if (result != null)
                {
                    return RedirectToAction("Index");
                }
            }

            ViewData["states"] = new SelectList(await _api.Lookups.GetStates(), "Key", "Value");
            ViewData["schoolTypes"] = new SelectList(await _api.Lookups.GetSchoolTypes(), "Key", "Value");
            ViewData["districts"] = new SelectList(await _api.Schools.DistrictsGetAll(), "Id", "Name");
            return View(school);
        }

        #region Inquiries

        [HttpGet]
        public async Task<IActionResult> Inquiries()
        {
            var inquiries = await _api.Schools.InquiriesGetAll();
            return View(inquiries);
        }

        [HttpGet]
        public async Task<IActionResult> InquiryDetails(string id)
        {
            var inquiry = await _api.Schools.InquiriesGetSingle(id);
            return View(inquiry);
        }

        [HttpGet]
        public async Task<IActionResult> ConvertInquiry(string id)
        {
            var inquiry = await _api.Schools.InquiriesGetSingle(id);
            if (inquiry != null)
            {
                ViewData["districts"] = new SelectList(await _api.Schools.DistrictsGetAll(), "Id", "Name");
                var model = populateConvertModel(inquiry);
                model.InquiryId = id;
                return View(model);
            }
            return RedirectToAction("Inquiries");
            
        }

        [HttpPost]
        public async Task<IActionResult> ConvertInquiry(ConvertInquiryViewModel model)
        {
            if (ModelState.IsValid)
            {
                // District - Get existing or create new
                var district = string.IsNullOrWhiteSpace(model.District.Id) 
                    ? await _api.Schools.DistrictsCreate(model.District) 
                    : await _api.Schools.DistrictsSingle(model.District.Id);

                
                if (district != null)
                {
                    // School - Create new
                    model.School.DistrictId = district.Id;
                    var school = await _api.Schools.Create(model.School);
                    if (school == null)
                    {
                        ModelState.AddModelError("", "Could not create school. Confirm that the school does not already exist, and check the address.");
                    }
                    else
                    {
                        model.User.SchoolId = school.Id;
                        model.User.UserType = UserType.DistrictAdmin;
                        model.User.SendInvitEmail = true;
                        var adminResult = await _api.Users.CreateUser(model.User);
                        if (adminResult != null)
                        {
                            var updateInquiryResult = await _api.Schools.InquiriesUpdateStatus(model.InquiryId, TaggStatus.Complete);
                            return RedirectToAction("Index", "Schools");
                        }
                        else
                        {
                            ModelState.AddModelError("", $"Could not create school admin user account for {model.User.Email}");
                        }
                    }
                }
                else
                {
                    ModelState.AddModelError("", "District could not be created");
                }
            }
            ViewData["districts"] = new SelectList(await _api.Schools.DistrictsGetAll(), "Id", "Name");
            return View(model);

        }

        [HttpGet]
        public async Task<IActionResult> UpdateInquiryStatus(string inquiryId, string status)
        {
            TaggStatus statusResult;
            if (!string.IsNullOrWhiteSpace(inquiryId) && Enum.TryParse(status, true, out statusResult))
            {
                var result = await _api.Schools.InquiriesUpdateStatus(inquiryId, statusResult);
            }
            return RedirectToAction("Inquiries");
        }
        #endregion

        #region helpers
        private ConvertInquiryViewModel populateConvertModel(SchoolInquiryDto inquiry)
        {
            var result = new ConvertInquiryViewModel();
            result.User = new UserCreateDto { FirstName = inquiry.FirstName, LastName = inquiry.LastName, Email = inquiry.Email, PhoneNumber = inquiry.PhoneNumber };
            result.District = new DistrictDto { Name = inquiry.DistrictName };
            result.School = _mapper.Map<SchoolDto>(inquiry);
            return result;
        }
        #endregion
    }
}
