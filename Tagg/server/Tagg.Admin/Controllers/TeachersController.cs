﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Tagg.Admin.Models;
using Tagg.Domain;
using Tagg.Domain.ApiModels;

namespace Tagg.Admin.Controllers
{
    public class TeachersController : Controller
    {
        private IMapper _mapper;
        //private IEmailService _emailService;
        //private IConfiguration _config;
        private TaggApiService _api;

        public TeachersController(TaggApiService api, IMapper mapper)
        {
            _mapper = mapper;
            //_emailService = emailService;
            //_config = config;
            _api = api;
        }

        // GET: Teachers
        public async Task<IActionResult> Index()
        {
            var teachers = await _api.Teachers.GetAll();
            return View(teachers);
        }

        // GET: Teachers/Details/5
        public async Task<IActionResult> Details(string id)
        {
            var teacher = await _api.Teachers.GetById(id);
            return View(teacher);
        }

        // GET: Teachers/Create
        public async Task<IActionResult> Create()
        {
            var model = new TeacherDto();
            var schools = await _api.Schools.GetAll();
            ViewData["schools"] = new SelectList(schools, "Id", "Name");
            return View(model);
        }

        public async Task<IActionResult> Invite(string id)
        {
            var result = await _api.Users.SendInvite(id);
            if (result)
            {
                ViewData["Message"] = $"Invite email sent to {id}.";
                return View();
            }
            ViewData["Message"] = $"Failed to send invite email to {id}.";
            return View();

        }

        public async Task<IActionResult> Delete(string id)
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TeacherDto model)
        {
            if (ModelState.IsValid)
            {
                model.Status = TaggStatus.Pending;
                var response = await _api.Teachers.Create(model, model.SchoolId);
                return RedirectToAction("Details", new { id = response.Email });
                //var result = await createTeacherAsync(teacher, model.SchoolId);
                //if (result.Succeeded)
                //{
                //    // On succeeded, return to index
                //    return RedirectToAction(nameof(Index));
                //}
                //result.Errors.ForEach(e => ModelState.AddModelError("", e));
            }

            // If we get here, there was an error and return the view
            var schools = await _api.Schools.GetAll();
            ViewData["schools"] = new SelectList(schools, "Id", "Name");
            return View(model);
        }

        public IActionResult Upload()
        {
            var model = populateTeacherUploadModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Upload(TeacherUploadModel model)
        {
            if (model.Upload == null)
            {
                ModelState.AddModelError("Upload", "Please upload a file.");
            }
            if (ModelState.IsValid)
            {
                var uploadResult = await _api.Teachers.Upload(model.SchoolId, model.Upload);
                if (uploadResult)
                {
                    // On succeeded, return to index
                    return RedirectToAction(nameof(Index));
                }
                ModelState.AddModelError("", "An error occurred while uploading teachers. Please check ");
            }

            // If errors, return view to user
            model = await populateTeacherUploadModel(model);
            return View(model);
        }


        public IActionResult Download()
        {
            var pathToSample = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "TaggTeacherUpload.csv");
            return PhysicalFile(pathToSample, "application/octet-stream", "TaggTeacherUpload.csv");

        }

        private async Task<TeacherViewModel> populateTeacherModel(TeacherViewModel model = null)
        {
            if (model == null)
            {
                model = new TeacherViewModel();
            }

            var subjects = await _api.Lookups.GetSubjects();
            var schools = await _api.Schools.GetAll();

            var defaultSchool = !string.IsNullOrWhiteSpace(model.SchoolId) ? model.SchoolId : schools.First().Id.ToString();

            //model.Subjects = subjects.Select(s => new MultiSelectModel { Key = s.Key, Value = s.Value, Selected = true }).ToList();
            model.SchoolList = new SelectList(schools, "Id", "Name", defaultSchool);

            return model;
        }

        private async Task<TeacherUploadModel> populateTeacherUploadModel(TeacherUploadModel model = null)
        {
            if (model == null)
            {
                model = new TeacherUploadModel();
            }

            model.Upload = null;
            var schools = await _api.Schools.GetAll();
            var defaultSchool = !string.IsNullOrWhiteSpace(model.SchoolId) ? model.SchoolId : schools.First().Id.ToString();
            model.SchoolList = new SelectList(schools, "Id", "Name", defaultSchool);
            return model;
        }

        //private async Task<EntityResult> processBulkUpload(TeacherUploadModel model)
        //{
        //    var formFile = model.Upload;
        //    var fileSize = formFile.Length;
        //    var fileName = WebUtility.HtmlEncode(Path.GetFileName(formFile.FileName));
        //    var contentType = formFile.ContentType;
        //    var fileExt = Path.GetExtension(fileName);

        //    if (fileSize > 1048576)
        //    {
        //        return new EntityResult("File size must be less than than 1MB.");
        //    }

        //    if (fileExt != ".csv")
        //    {
        //        return new EntityResult("File must be a CSV (.csv) file.");
        //    }

        //    var errors = new List<string>();
        //    var teachers = new Dictionary<int,Teacher>();

        //    using (var reader = new StreamReader(formFile.OpenReadStream(), 
        //        new UTF8Encoding(encoderShouldEmitUTF8Identifier: false, throwOnInvalidBytes: true),
        //        detectEncodingFromByteOrderMarks: true))
        //    {
        //        var csv = new CsvHelper.CsvReader(reader);
        //        csv.Read();
        //        csv.ReadHeader();
        //        int i = 1;
        //        while (csv.Read())
        //        {
        //            string firstName = csv[0];
        //            string lastName = csv[1];
        //            string email = csv[2];
        //            string phoneNumber = csv[3];
        //            var namePattern = @"([^\w\s]|\d)";      //Any non-word chars and numbers (except space)
        //            var emailPattern = @"[^a-zA-Z0-9@.]";   //Anything that is not a letter, number, @ or period
        //            var phonePattern = @"\D";               //Any non-digit char

        //            var teacherModel = new TeacherViewModel
        //            {
        //                FirstName = Regex.Replace(firstName, namePattern, string.Empty),
        //                LastName = Regex.Replace(lastName, namePattern, string.Empty),    
        //                Email = Regex.Replace(email, emailPattern , string.Empty),
        //                PhoneNumber = Regex.Replace(phoneNumber, phonePattern, string.Empty),
        //                SchoolId = model.SchoolId
        //            };
        //            teachers.Add(i, _mapper.Map<TeacherViewModel, Teacher>(teacherModel));
        //            i++;
        //        }
        //    }
            
        //    foreach (var item in teachers)
        //    {
        //        var creationResult = _api.Teachers.Create(_mapper.Map<TeacherDto>(item.Value), model.SchoolId);
        //        //if (!creationResult.Succeeded)
        //        //{
        //        //    errors.Add($"Error in row {item.Key}: {creationResult.Errors.First()}");
        //        //}
        //    }

        //    var result = new EntityResult();
        //    if (errors.Count > 0)
        //    {
        //        errors.ForEach(e => result.AddError(e));
        //        result.AddError("Teachers have been created for all other rows. Please fix the errors and submit the file again with only the rows listed above.");
        //    }
        //    return result;
        //}

        //// POST: Teachers/Create
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Create([Bind("Id,UserProfileId,SchoolId")] Teacher teacher)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        _work.Teachers.Upsert(teacher);
        //        await _context.SaveChangesAsync();
        //        return RedirectToAction(nameof(Index));
        //    }
        //    ViewData["SchoolId"] = new SelectList(_context.Schools, "Id", "Id", teacher.SchoolId);
        //    ViewData["UserProfileId"] = new SelectList(_context.UserProfiles, "Id", "Id", teacher.UserProfileId);
        //    return View(teacher);
        //}

        //// GET: Teachers/Edit/5
        //public async Task<IActionResult> Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var teacher = await _context.Teachers.SingleOrDefaultAsync(m => m.Id == id);
        //    if (teacher == null)
        //    {
        //        return NotFound();
        //    }
        //    ViewData["SchoolId"] = new SelectList(_context.Schools, "Id", "Id", teacher.SchoolId);
        //    ViewData["UserProfileId"] = new SelectList(_context.UserProfiles, "Id", "Id", teacher.UserProfileId);
        //    return View(teacher);
        //}

        //// POST: Teachers/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> Edit(int id, [Bind("Id,UserProfileId,SchoolId")] Teacher teacher)
        //{
        //    if (id != teacher.Id)
        //    {
        //        return NotFound();
        //    }

        //    if (ModelState.IsValid)
        //    {
        //        try
        //        {
        //            _context.Update(teacher);
        //            await _context.SaveChangesAsync();
        //        }
        //        catch (DbUpdateConcurrencyException)
        //        {
        //            if (!TeacherExists(teacher.Id))
        //            {
        //                return NotFound();
        //            }
        //            else
        //            {
        //                throw;
        //            }
        //        }
        //        return RedirectToAction(nameof(Index));
        //    }
        //    ViewData["SchoolId"] = new SelectList(_context.Schools, "Id", "Id", teacher.SchoolId);
        //    ViewData["UserProfileId"] = new SelectList(_context.UserProfiles, "Id", "Id", teacher.UserProfileId);
        //    return View(teacher);
        //}

        //// GET: Teachers/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var teacher = await _context.Teachers
        //        .Include(t => t.School)
        //        .Include(t => t.UserProfile)
        //        .SingleOrDefaultAsync(m => m.Id == id);
        //    if (teacher == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(teacher);
        //}

        //// POST: Teachers/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public async Task<IActionResult> DeleteConfirmed(int id)
        //{
        //    var teacher = await _context.Teachers.SingleOrDefaultAsync(m => m.Id == id);
        //    _context.Teachers.Remove(teacher);
        //    await _context.SaveChangesAsync();
        //    return RedirectToAction(nameof(Index));
        //}
    }
}
