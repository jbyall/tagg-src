﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;
using Tagg.Admin.Models;
using Tagg.Domain;
using Tagg.Domain.ApiModels;

namespace Tagg.Admin.Controllers
{
    public class SubstitutesController : Controller
    {
        private TaggApiService _api;

        public SubstitutesController(TaggApiService api)
        {
            _api = api;
        }

        // GET: Substitutes
        public async Task<IActionResult> Index(string id)
        {
            var allSubs = await _api.Subs.GetAll();
            ViewData["Title"] = "Subs - All";
            return View(allSubs);

            //if (string.IsNullOrWhiteSpace(id))
            //{
            //var allSubs = await _api.Subs.GetAll();
            //ViewData["Title"] = "Subs - All";
            //return View(allSubs);
            //}
            //var statusResult = Enum.Parse<TaggStatus>(id, true);
            //var results = await getSubsByStatus(statusResult);
            //var pageTitle = "";
            //switch (statusResult)
            //{
            //    case TaggStatus.New:
            //    case TaggStatus.Pending:
            //        pageTitle = "Applicants";
            //        break;
            //    case TaggStatus.Disabled:
            //        pageTitle = "Subs - Disabled";
            //        break;
            //    default:
            //        pageTitle = "Subs - Active";
            //        break;
            //}
            //ViewData["Title"] = pageTitle;
            //return View(results);
        }

        [HttpGet]
        public async Task<IActionResult> Pending()
        {
            var result = await getPendingSubs();
            ViewData["Title"] = "Applicants";
            return View("Index", result);
        }

        public async Task<IActionResult> Approve(string id)
        {
            var updateResult = await _api.Subs.Approve(id);
            return RedirectToAction("Details", new { id = id });
        }

        public async Task<IActionResult> Disable(string id)
        {
            var updateResult = await _api.Subs.Disable(id);
            return RedirectToAction("Details", new { id = id });
        }

        [HttpGet]
        public async Task<IActionResult> Exclude(string id)
        {
            var model = new SubExcludeViewModel { SubEmail = id };
            ViewData["schools"] = new SelectList(await _api.Schools.GetAll(), "Id", "Name");
            return View(model);
        }

        
        public async Task<IActionResult> Exclude(SubExcludeViewModel model)
        {
            var result = await _api.Subs.Exclude(model.SubEmail, model.SchoolId);
            if (result)
            {
                return RedirectToAction("Details", new { id = model.SubEmail});
            }
            ViewData["schools"] = new SelectList(await _api.Schools.GetAll(), "Id", "Name");
            ModelState.AddModelError("", "Failed to exclude sub from school");
            return View(model);
        }

        
        public async Task<IActionResult> RemoveExclusion(string id, string school)
        {
            await _api.Subs.RemoveExclusion(id, school);
            return RedirectToAction("Details", new { id = id });
        }

        #region scaffold
        // GET: Substitutes/Details/5
        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var substitute = await _api.Subs.GetSingle(id);

            if (substitute == null)
            {
                return NotFound();
            }

            return View(substitute);
        }

        // GET: Substitutes/Create
        public IActionResult Create()
        {
            //ViewData["UserProfileId"] = new SelectList(_context.UserProfiles, "Id", "Id");
            return View();
        }

        // POST: Substitutes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SubstituteDto substitute)
        {
            if (ModelState.IsValid)
            {
                var result = _api.Subs.Create(substitute);
                return RedirectToAction("Index");
            }
            return View(substitute);
        }

        // GET: Substitutes/Edit/5
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var substitute = await _api.Subs.GetSingle(id);
            //if (substitute == null)
            //{
            //    return NotFound();
            //}
            //ViewData["UserProfileId"] = new SelectList(_context.UserProfiles, "Id", "Id", substitute.UserProfileId);
            return View(substitute);
        }

        // POST: Substitutes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,UserProfileId,Photo,EducationLevel,HasTeachingLicense,HasTranscripts,Resume,ApplicationComplete,Enabled,ApprovalDate,Status,Bio,Hobbies,LinkedInUrl,MinPayAmount,MaxDistance")] Substitute substitute)
        {
            //if (id != substitute.Id)
            //{
            //    return NotFound();
            //}

            //if (ModelState.IsValid)
            //{
            //    try
            //    {
            //        _context.Update(substitute);
            //        await _context.SaveChangesAsync();
            //    }
            //    catch (DbUpdateConcurrencyException)
            //    {
            //        if (!SubstituteExists(substitute.Id))
            //        {
            //            return NotFound();
            //        }
            //        else
            //        {
            //            throw;
            //        }
            //    }
            //    return RedirectToAction(nameof(Index));
            //}
            //ViewData["UserProfileId"] = new SelectList(_context.UserProfiles, "Id", "Id", substitute.UserProfileId);
            return View(substitute);
        }

        //// GET: Substitutes/Delete/5
        //public async Task<IActionResult> Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return NotFound();
        //    }

        //    var substitute = await _work.Subs.GetQueryable()
        //        .SingleOrDefaultAsync(m => m.Id == id);
        //    if (substitute == null)
        //    {
        //        return NotFound();
        //    }

        //    return View(substitute);
        //}

        // POST: Substitutes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            //var substitute = await _work.Subs.GetQueryable().SingleOrDefaultAsync(m => m.Id == id);
            //_work.Subs.GetQueryable().Remove(substitute);
            //await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }
        #endregion

        #region helpers
        private async Task<bool> SubstituteExists(string id)
        {
            var sub = await _api.Subs.GetSingle(id);
            return sub != null;
        }

        private async Task<List<SubstituteDto>> getSubsByStatus(TaggStatus status)
        {
            var result = await _api.Subs.GetAll();
            switch (status)
            {
                case TaggStatus.New:
                case TaggStatus.Pending:
                    return result.Where(s => s.Status == TaggStatus.New || s.Status == TaggStatus.Pending).ToList();
                default:
                    return result.Where(s => s.Status == status).ToList();
            }
        }

        // TODO : git rid of this and figure out why it's not working with getSubsByStatus
        private async Task<List<SubstituteDto>> getPendingSubs()
        {
            var result = await _api.Subs.GetAll();
            result = result.Where(s => s.Status == TaggStatus.Pending).ToList();
            return result;
        }
        #endregion
    }
}
