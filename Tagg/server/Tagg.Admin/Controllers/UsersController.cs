﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Tagg.Admin.Models;
using Tagg.Domain;
using Tagg.Domain.ApiModels;

namespace Tagg.Admin.Controllers
{
    public class UsersController : Controller
    {
        private TaggApiService _api;
        private IMapper _mapper;

        public UsersController(TaggApiService api, 
            IMapper mapper)
        {
            _api = api;
            _mapper = mapper;
        }
        public async Task<IActionResult> Index(string status = null)
        {
            var users = await _api.Users.GetAll();
            return View(users);
        }

        public async Task<IActionResult> Create()
        {
            var schools = await _api.Schools.GetAll();
            ViewData["schools"] = new SelectList(schools, "Id", "Name");
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(UserCreateDto model)
        {
            if (ModelState.IsValid)
            {
                var createResult = await _api.Users.CreateUser(model);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public async Task<IActionResult> Details(string email)
        {
            var user = await _api.Users.GetSingle(email);
            return View(user);
        }

        //private IQueryable<ApplicationUser> getUsersByStatus(string status)
        //{
        //    var result = _work
        //    return string.IsNullOrWhiteSpace(status) ? result : result.Where(p => p.Status == status);
        //}
    }
}