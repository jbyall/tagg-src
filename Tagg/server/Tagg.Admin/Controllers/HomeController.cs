﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using RestSharp;
using Tagg.Admin.Models;
using Tagg.Domain;

namespace Tagg.Admin.Controllers
{
    //[Authorize]
    public class HomeController : Controller
    {
        private IConfiguration _config;
        private TaggApiService _api;

        public HomeController(
            IConfiguration config,
            TaggApiService api)
        {
            _config = config;
            _api = api;
        }

        [Authorize(Policy = AuthPolicies.SystemAdmin)]
        public async Task<IActionResult> Index()
        {
            //var model = new AdminDashModel();
            //model.ActiveSubs = _work.Subs.CountQuery(s => s.Status == UserStatus.Active);
            //model.DisabledSubs = _work.Subs.CountQuery(s => s.Status == UserStatus.Disabled);
            //model.AllSchools = _work.Schools.Count();
            //model.AllTeachers = _work.Teachers.Count();
            //model.PendingApplicants = _work.Subs.CountQuery(s => s.Status == SubstituteStatus.Pending);
            //model.NewApplicants = _work.Subs.CountQuery(s => s.Status == SubstituteStatus.Applicant);
            //model.IncompleteApplicants = 0;
            //model.AllUsers = _work.Users.Count();
            //return View(model);
            var model = await populateDashModel();
            return View(model);
        }

        public IActionResult Error(string errorCode = null)
        {
            ViewData["ErrorMessage"] = errorCode ?? "500 - Internal Server Error";
            return View();
        }

        public IActionResult Logout()
        {
            return new SignOutResult(new[] { "Cookies" });//, "oidc" });
        }

        #region Helpers
        private async Task<AdminDashModel> populateDashModel()
        {
            var result = new AdminDashModel();
            var pendingInquiries = await _api.Schools.InquiriesByStatus(TaggStatus.New);
            var allSubs = await _api.Subs.GetAll();
            if (pendingInquiries != null)
            {
                result.InquiryCount = pendingInquiries.Count;
                
            }
            var subsCount = allSubs?.Count(s => s.Status == TaggStatus.Pending);
            result.PendingApplicants = subsCount ?? 0;
            return result;
        }
        #endregion
    }


}
