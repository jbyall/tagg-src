﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using RestSharp;
using RestSharp.Deserializers;
using Tagg.Domain;
using Tagg.Domain.ApiModels;
using Tagg.Services;

namespace Tagg.Admin.Controllers
{
    [Produces("application/json")]
    [Route("api/Districts")]
    public class DistrictsController : Controller
    {
        private TaggApiService _api;
        private IMapper _mapper;

        public DistrictsController(IMapper mapper, TaggApiService api)
        {
            _api = api;
            _mapper = mapper;
        }

        [HttpGet("{id}", Name = "GetDistrict")]
        public async Task<IActionResult> Get(string id)
        {
            //var result = _work.Schools.GetDistrict(id);
            //if (result == null)
            //{
            //    return NotFound();
            //}

            //return Ok(_mapper.Map<DistrictDto>(result));
            return Ok();
        }

        [HttpGet("all")]
        public IActionResult GetAll()
        {
            //var districts = _work.Schools.GetAllDistricts();
            //var result = _mapper.Map<List<DistrictDto>>(districts);
            //return Ok(result);
            return Ok();
        }

        [HttpPost("Create")]
        public IActionResult CreateDistrict([FromBody] DistrictDto model)
        {
            //if (ModelState.IsValid)
            //{
            //    var district = _mapper.Map<District>(model);
            //    var createResult = _work.Schools.AddDistrict(district);
            //    if (createResult.Succeeded)
            //    {
            //        var result = _mapper.Map<SchoolDto>(createResult.Entity);
            //        return CreatedAtRoute("GetDistrict", new { id = result.Id }, result);
            //    }
            //}
            //return BadRequest();
            return Ok();
        }
    }
}