﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Tagg.Domain.ApiModels;

namespace Tagg.Admin.Models
{
    public class JobCreateViewModel
    {
        public JobCreateViewModel()
        {

        }
        public JobCreateViewModel(JobDetailsDto details)
        {
            this.StartDate = details.StartDate;
            this.EndDate = details.EndDate;
            this.StartTime = details.StartDate.TimeOfDay;
            this.EndTime = details.EndDate.TimeOfDay;
            this.SchoolName = details.SchoolName;
            this.TeacherName = details.TeacherName;
            this.TeacherId = details.TeacherId;
            this.PayAmount = details.PayAmount;
        }

        [Display(Name = "Start Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public DateTime StartDate { get; set; }

        [Display(Name = "End Date")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:d}")]
        public DateTime EndDate { get; set; }

        [Display(Name = "Start Time")]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true)]//, DataFormatString = "{0:g}")]
        public TimeSpan StartTime { get; set; }

        [Display(Name = "End Time")]
        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:g}")]
        public TimeSpan EndTime { get; set; }

        [Display(Name ="School")]
        public string SchoolName { get; set; }

        [Display(Name = "Teacher")]
        public string TeacherName { get; set; }

        [Display(Name = "Teacher Email")]
        public string TeacherEmail { get; set; }

        public string TeacherId { get; set; }

        public double PayAmount { get; set; }
    }
}
