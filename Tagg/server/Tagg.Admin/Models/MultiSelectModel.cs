﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tagg.Admin.Models
{
    public class MultiSelectModel
    {
        public string Key { get; set; }
        public string Value { get; set; }
        public bool Selected { get; set; }
    }
}
