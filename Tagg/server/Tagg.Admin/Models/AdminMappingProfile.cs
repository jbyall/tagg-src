﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tagg.Domain;
using Tagg.Domain.ApiModels;

namespace Tagg.Admin.Models
{
    public class AdminMappingProfile : Profile
    {
        public AdminMappingProfile()
        {
            CreateMap<Teacher, TeacherViewModel>().ReverseMap();
            CreateMap<AddressDto, AddressViewModel>().ReverseMap();
            CreateMap<JobCreateViewModel, JobCreateDto>()
                .ForMember(x => x.EndDate, ex => ex.ResolveUsing(vm =>{ return vm.EndDate + vm.EndTime; }))
                .ForMember(x => x.StartDate, ex => ex.ResolveUsing(vm => { return vm.StartDate + vm.StartTime; }))
                .ReverseMap();
            CreateMap<SchoolDto, SchoolViewModel>()
                .ForMember(s => s.StartTime, ex => ex.ResolveUsing(vm => { return vm.StartTime.ToString(); }))
                .ForMember(s => s.EndTime, ex => ex.ResolveUsing(vm => { return vm.EndTime.ToString(); }))
                .ReverseMap();

            CreateMap<UserViewModel, MongoUser>()
                .ConvertUsing(vm =>
                {
                    return new MongoUser { Email = vm.Email, UserName = vm.Email, FirstName = vm.FirstName, LastName = vm.LastName };
                });

            CreateMap<SchoolInquiryDto, SchoolDto>()
                .ConvertUsing(dto =>
                {
                    return new SchoolDto
                    {
                        Name = dto.SchoolName,
                        Address = new AddressDto
                        {
                            City = dto.City,
                            State = dto.State,
                            PostalCode = dto.PostalCode
                        },
                        ContactEmail = dto.Email,
                        ContactPhone = dto.PhoneNumber
                    };
                });



            #region OldMaps
            //CreateMap<SchoolViewModel, School>()
            //    .ForMember(m => m.Address, ex => ex.ResolveUsing(vm =>
            //    {
            //        return new Address
            //        {
            //            Street1 = vm.Street,
            //            City = vm.City,
            //            PostalCode = vm.PostalCode,
            //            State = vm.State,

            //        };
            //    }))
            //    .ReverseMap();

            //CreateMap<School, SchoolViewModel>()
            //    .ForMember(s => s.City, ex => ex.MapFrom(s => s.Address.City))
            //    .ForMember(s => s.Street, ex => ex.MapFrom(s => s.Address.Street1))
            //    .ForMember(s => s.State, ex => ex.MapFrom(s => s.Address.State))
            //    .ForMember(s => s.PostalCode, ex => ex.MapFrom(s => s.Address.PostalCode))
            //    .ReverseMap();


            //.ForMember(t => t.FirstName, ex => ex.MapFrom(t => t.FirstName))
            //.ForMember(t => t.LastName, ex => ex.MapFrom(t => t.LastName))
            //.ForMember(t => t.Email, ex => ex.MapFrom(t => t.Email))
            //.ForMember(t => t.PhoneNumber, ex => ex.MapFrom(t => t.PhoneNumber))
            //.ForMember(t => t.Bio, ex => ex.MapFrom(t => t.Bio))
            //.ForMember(t => t.SchoolName, ex => ex.MapFrom(t => t.School.Name));

            //CreateMap<TeacherViewModel, Teacher>()
            //    .ConvertUsing(vm =>
            //    {
            //        var user = new ApplicationUser
            //        {
            //            UserName = vm.Email,
            //            Email = vm.Email,
            //            PhoneNumber = vm.PhoneNumber
            //        };
            //        var result = new Teacher
            //        {
            //            UserProfile = new UserProfile
            //            {
            //                FirstName = vm.FirstName,
            //                LastName = vm.LastName,
            //                Bio = vm.Bio,
            //                User = user,
            //                UserId = user.Id
            //            },
            //            SchoolId = vm.SchoolId,
            //            Id = vm.Id
            //        };
            //        return result;
            //    });
            //.ForMember(t => t.UserProfile.FirstName, ex => ex.MapFrom(t => t.FirstName))
            //.ForMember(t => t.UserProfile.LastName, ex => ex.MapFrom(t => t.LastName))
            //.ForMember(t => t.UserProfile.User.UserName, ex => ex.MapFrom(t => t.Email))
            //.ForMember(t => t.UserProfile.User.Email, ex => ex.MapFrom(t => t.Email))
            //.ForMember(t => t.UserProfile.User.PhoneNumber, ex => ex.MapFrom(t => t.PhoneNumber))
            //.ForMember(t => t.UserProfile.Bio, ex => ex.MapFrom(t => t.Bio));
            #endregion
        }
    }
}
