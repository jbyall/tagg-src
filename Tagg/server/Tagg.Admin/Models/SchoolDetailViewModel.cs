﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tagg.Domain.ApiModels;

namespace Tagg.Admin.Models
{
    public class SchoolDetailViewModel
    {
        public SchoolDetailViewModel()
        {
            this.Teachers = new List<TeacherDto>();
        }
        public SchoolDto School { get; set; }
        public List<TeacherDto> Teachers { get; set; }
    }
}
