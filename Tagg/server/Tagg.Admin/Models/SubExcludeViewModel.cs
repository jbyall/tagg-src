﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tagg.Admin.Models
{
    public class SubExcludeViewModel
    {
        public string SubEmail { get; set; }
        public string SubId { get; set; }
        public string SchoolId { get; set; }
    }
}
