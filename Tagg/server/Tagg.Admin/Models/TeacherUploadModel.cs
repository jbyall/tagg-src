﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Tagg.Admin.Models
{
    public class TeacherUploadModel
    {
        public string Title { get; set; }
        public IFormFile Upload { get; set; }
        [Display(Name = "School Name")]
        public string SchoolId { get; set; }
        public SelectList SchoolList { get; set; }
    }
}
