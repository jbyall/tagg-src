﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Tagg.Admin.Models
{
    public class SchoolViewModel
    {
        public string Id { get; set; }
        [Display(Name = "District")]
        public string DistrictId { get; set; }

        [Display(Name="School Name")]
        [Required]
        public string Name { get; set; }

        [Display(Name = "SchoolType")]
        public string SchoolType { get; set; }

        public AddressViewModel Address { get; set; }

        [Display(Name = "Primary Phone")]
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = false, DataFormatString ="{0:###-###-####}")]
        public string ContactPhone { get; set; }

        [Display(Name = "Primary Email")]
        [Required, EmailAddress]
        public string ContactEmail { get; set; }

        [Display(Name = "Website")]
        public string Url { get; set; }

        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:g}")]
        public TimeSpan StartTime { get; set; }

        [DataType(DataType.Time)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:g}")]
        public TimeSpan EndTime { get; set; }
    }

    public class AddressViewModel
    {
        [Required]
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        [Required]
        public string City { get; set; }
        public string State { get; set; }

        [Display(Name = "Zip Code")]
        [Required]
        public string PostalCode { get; set; }
    }
}
