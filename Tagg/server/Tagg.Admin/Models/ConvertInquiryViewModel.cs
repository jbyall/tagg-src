﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tagg.Domain.ApiModels;

namespace Tagg.Admin.Models
{
    public class ConvertInquiryViewModel
    {
        public string InquiryId { get; set; }
        public SchoolDto School { get; set; }
        public UserCreateDto User { get; set; }
        public DistrictDto District { get; set; }
    }
}
