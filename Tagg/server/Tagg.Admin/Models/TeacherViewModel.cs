﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Tagg.Admin.Models
{
    public class TeacherViewModel
    {
        public string Id { get; set; }
        [Display(Name = "First Name")]

        public string FirstName { get; set; }
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        public string Email { get; set; }
        [Display(Name = "Phone Number")]
        public string PhoneNumber { get; set; }
        public string Bio { get; set; }

        [Display(Name = "School")]
        public string SchoolId { get; set; }
        public SelectList SchoolList { get; set; }

        [Display(Name = "School")]
        public string SchoolName { get; set; }
    }
}
