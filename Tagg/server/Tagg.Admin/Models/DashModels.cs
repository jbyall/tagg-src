﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Tagg.Admin
{
    public class AdminDashModel
    {
        public int InquiryCount { get; set; }
        public int ActiveSubs { get; set; }
        public int DisabledSubs { get; set; }
        public int AllSchools { get; set; }
        public int AllTeachers { get; set; }
        public int PendingApplicants { get; set; }
        public int NewApplicants { get; set; }
        public int IncompleteApplicants { get; set; }
        public int AllUsers { get; set; }
    }
}
