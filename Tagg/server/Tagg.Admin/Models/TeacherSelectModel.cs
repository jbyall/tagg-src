﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tagg.Domain;
using Tagg.Domain.ApiModels;

namespace Tagg.Admin.Models
{
    public class TeacherSelectModel
    {
        public TeacherSelectModel()
        {

        }
        public TeacherSelectModel(List<TeacherDto> teachers)
        {
            this.TeacherList = this.TeacherList = new SelectList(teachers, "Email", "FullName");
        }
        public TeacherSelectModel(List<TeacherDto> teachers, List<SchoolDto> schools)
        {
            this.TeacherList = new SelectList(teachers, "Id", "FullName");
            this.SchoolList = new SelectList(schools, "Id", "Name");
        }
        public string TeacherId { get; set; }
        public string SchoolId { get; set; }
        public SelectList SchoolList { get; set; }
        public SelectList TeacherList { get; set; }
    }
}
