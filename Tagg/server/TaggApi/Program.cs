﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
//using Serilog;
//using Serilog.Events;
//using Serilog.Sinks.SystemConsole.Themes;
using Tagg.Domain;

namespace TaggApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args)
        {
            return WebHost.CreateDefaultBuilder(args)
            .UseStartup<Startup>()
            .UseSentry(o =>
            {
                o.MaxBreadcrumbs = 200;
                o.DecompressionMethods = DecompressionMethods.None;
                o.MaxQueueItems = 100;
                o.ShutdownTimeout = TimeSpan.FromSeconds(5);
                o.MinimumEventLevel = LogLevel.Error;
                o.SendDefaultPii = true;
            })
            .Build();
        }

        // Add this
        public static void SetupConfiguration(WebHostBuilderContext ctx, IConfigurationBuilder builder)
        {
            // Removing the default configuration options
            builder.Sources.Clear();

            builder.AddJsonFile("appsettings.json", false, true)
                .AddJsonFile($"appsettings.{ctx.HostingEnvironment.EnvironmentName}.json", true, true)
                .AddEnvironmentVariables();
        }
    }
}
