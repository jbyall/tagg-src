﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Tagg.Domain;
using Tagg.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.Extensions.FileProviders;
using System.IO;
using Swashbuckle.AspNetCore.Swagger;
using IdentityServer4.AccessTokenValidation;
using MongoDB.Driver;
using Newtonsoft.Json.Converters;
using System;
using System.Net.Sockets;

namespace TaggApi
{
    public class Startup
    {

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            
            // Add configuration to be used in the app
            services.AddSingleton<IConfiguration>(Configuration);
            

            var connectionString = Configuration.GetConnectionString("MongoConnection");

            services.AddSingleton<MongoClient>(opt =>
            {
                var mongoSettings = MongoClientSettings.FromUrl(new MongoUrl(Configuration.GetConnectionString("MongoConnection")));
                mongoSettings.MaxConnectionIdleTime = TimeSpan.FromSeconds(60);
                Action<Socket> socketConfigurator = s => ConfigureSocket(s);
                mongoSettings.ClusterConfigurator = cb => cb.ConfigureTcp(tcp => tcp.With(socketConfigurator: socketConfigurator));

                return new MongoClient(mongoSettings);
            });

            IServiceProvider provider = services.BuildServiceProvider();

            // Set password policy
            services.AddIdentity<MongoUser, MongoRole>(options =>
            {
                options.Password.RequireDigit = true;
                options.Password.RequiredLength = 8;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = true;
                options.Password.RequireLowercase = true;
                options.Tokens.ChangePhoneNumberTokenProvider = "Phone";

                options.User.RequireUniqueEmail = true;

                options.Lockout.MaxFailedAccessAttempts = 20;
            })
                .RegisterMongoStores<MongoUser, MongoRole>(connectionString, provider.GetService<MongoClient>())
                .AddUserManager<UserManager<MongoUser>>()
                .AddSignInManager<SignInManager<MongoUser>>()
                .AddDefaultTokenProviders();

            services.Configure<DataProtectionTokenProviderOptions>(options =>
            {
                options.TokenLifespan = TimeSpan.FromHours(24);
            });


            services.AddAutoMapper();

            // Add application services.
            

            services.AddTransient<IEmailService, EmailService>();

            services.AddTransient<ISmsService, SmsService>((cfg) =>
            {
                return new SmsService(Configuration);
            });

            services.AddTransient<PhoneService>();

            // Add tagg services
            services.AddScoped<LocationService>((ctx) =>
            {
                return new LocationService(Configuration);
            });
            services.AddScoped<TaggUnitOfWork>();
            services.AddScoped<IPaymentService, PaymentService>();
            services.AddScoped<ChatService>();
            services.AddScoped<JobService>();

            // API Configuration
            services.AddCors();
            services.AddMvc()
                .AddJsonOptions(opt => {
                    opt.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
                });

            services.AddIdentityServer()
                .AddDeveloperSigningCredential()
                .AddInMemoryIdentityResources(IdentityServerConfig.GetIdentityResources())
                .AddInMemoryClients(IdentityServerConfig.GetClients())
                .AddInMemoryApiResources(IdentityServerConfig.GetApiResource())
                .AddAspNetIdentity<MongoUser>();

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "Tagg API", Version = "v1" });
            });

            // Add auth and jwt settings
            services.AddAuthentication(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .AddIdentityServerAuthentication(options =>
                {
                    options.Authority = Configuration["TaggIdentity:Authority"];
                    options.RequireHttpsMetadata = false;
                    options.ApiName = Configuration["TaggIdentity:ApiName"];
                    //options.ApiSecret = "test";
                });

            // Add auth policies
            services.AddAuthorization(options =>
            {
                
                options.AddPolicy(AuthPolicies.TaggUser, policy =>
                    policy.AddAuthenticationSchemes(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                    .RequireAssertion(context =>
                        context.User.HasClaim(c => c.Type == TaggClaimTypes.ApiAccess)
                    )
                );

                options.AddPolicy(AuthPolicies.Substitute, policy =>
                    policy.AddAuthenticationSchemes(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                    .RequireClaim(TaggClaimTypes.ApiAccess, ApiAccessLevels.Substitute));

                options.AddPolicy(AuthPolicies.Teacher, policy =>
                    policy.AddAuthenticationSchemes(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                    .RequireClaim(TaggClaimTypes.ApiAccess, ApiAccessLevels.Teacher));


                options.AddPolicy(AuthPolicies.SchoolAdmin, policy =>
                {
                    policy.AddAuthenticationSchemes(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                    .RequireClaim(TaggClaimTypes.ApiAccess, ApiAccessLevels.SchoolAdmin);
                });

            options.AddPolicy(AuthPolicies.AnyAdmin, policy =>
                policy.AddAuthenticationSchemes(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                .RequireAssertion(context =>
                        context.User.HasClaim(c => c.Type == TaggClaimTypes.ApiAccess && (c.Value == ApiAccessLevels.SchoolAdmin || c.Value == ApiAccessLevels.DistrictAdmin || c.Value == ApiAccessLevels.SystemAdmin))
                    )
                );

                options.AddPolicy(AuthPolicies.SystemAdmin, policy =>
                    policy.AddAuthenticationSchemes(IdentityServerAuthenticationDefaults.AuthenticationScheme)
                    .RequireClaim(TaggClaimTypes.ApiAccess, ApiAccessLevels.SystemAdmin));

            });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
                app.UseDatabaseErrorPage();
            }
            else
            {
                // Global Exception handler
                //app.UseExceptionHandler("/Home/Error");
                app.UseExceptionHandler(appBuilder =>
                {
                    appBuilder.Run(async context =>
                    {
                        context.Response.StatusCode = 500;
                        await context.Response.WriteAsync("An unexpected error happened. Try again later.");
                    });
                });
            }

            var fileProvider = new FileExtensionContentTypeProvider();
            fileProvider.Mappings[".csv"] = "application/octet-stream";
            fileProvider.Mappings[".docx"] = MimeTypes.MSDocX;
            //app.UseStaticFiles(new StaticFileOptions
            //{
            //    FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")),
            //    ContentTypeProvider = fileProvider
            //});

            app.UseIdentityServer();
            app.UseStaticFiles();



            app.UseCors(builder =>
                builder.AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());
                //.WithExposedHeaders("WWW-Authenticate"));

            app.UseAuthentication();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });

            //app.UseSwagger();
            //app.UseSwaggerUI(c =>
            //{
            //    c.SwaggerEndpoint("/swagger/v1/swagger.json", "Tagg API V1");
            //});
        }

        public static void ConfigureSocket(Socket s)
        {
            var keepAliveValues = new KeepAliveValues()
            {
                OnOff = 1,
                KeepAliveTime = 120 * 1000,   // 120 seconds in milliseconds
                KeepAliveInterval = 10 * 1000 // 10 seconds in milliseconds
            };

            s.IOControl(IOControlCode.KeepAliveValues, keepAliveValues.ToBytes(), null);
        }

    }

    internal struct KeepAliveValues
    {
        public ulong OnOff { get; set; }
        public ulong KeepAliveTime { get; set; }
        public ulong KeepAliveInterval { get; set; }

        public byte[] ToBytes()
        {
            var bytes = new byte[24];
            Array.Copy(BitConverter.GetBytes(OnOff), 0, bytes, 0, 8);
            Array.Copy(BitConverter.GetBytes(KeepAliveTime), 0, bytes, 8, 8);
            Array.Copy(BitConverter.GetBytes(KeepAliveInterval), 0, bytes, 16, 8);
            return bytes;
        }
    }

}
