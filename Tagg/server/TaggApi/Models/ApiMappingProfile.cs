﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Tagg.Domain;
using Tagg.Domain.ApiModels;
using Tagg.Domain.Helpers;

namespace TaggApi.Models
{
    public class ApiMappingProfile : Profile
    {
        public ApiMappingProfile()
        {
            CreateMap<AddressDto, Address>().ReverseMap();
            CreateMap<ContactDto, Contact>().ReverseMap();
            CreateMap<DistrictDto, District>().ReverseMap();
            CreateMap<JobDto, Job>().ReverseMap();
            CreateMap<JobCreateDto, Job>().ReverseMap();
            CreateMap<SchoolDto, School>()
                .ForMember(d => d.Teachers, opt => opt.Ignore());
            CreateMap<School, SchoolDto>()
                .ForMember(d => d.Teachers, opt => opt.Ignore());

            CreateMap<SubstituteDto, Substitute>();
            CreateMap<Transaction, TransactionDto>();
            CreateMap<AppMessageDto, AppMessage>().ReverseMap();
            CreateMap<MessageThreadDto, MessageThread>().ReverseMap();
            CreateMap<Substitute, SubstituteDto>().ReverseMap();
                //.ForMember(d => d.HasPaymentAccount, conf => conf.ResolveUsing(s => { return !string.IsNullOrWhiteSpace(s.PaymentId); }));
            CreateMap<SubDisplayDto, Substitute>().ReverseMap();
            CreateMap<TeacherDto, Teacher>().ReverseMap();
            CreateMap<TeacherDetailDto, Teacher>().ReverseMap();
            CreateMap<SchoolInquiryDto, SchoolInquiry>().ReverseMap();
            CreateMap<Job, JobDetailsDto>().ReverseMap();
            CreateMap<JobRating, JobRatingDto>().ReverseMap();
            CreateMap<SchoolTeachersResult, SchoolDto>()
                .ConvertUsing(d => 
                {
                    var result = Mapper.Map<SchoolDto>(d.School);
                    result.Teachers = Mapper.Map<List<TeacherDto>>(d.Teachers);
                    return result;
                });

            //CreateMap<SchoolAdminDto, Teacher>().ReverseMap();
            //CreateMap<SchoolAdminDto, MongoUser>()
            //    .ForMember(u => u.UserName, ex => ex.MapFrom(a => a.Email));

            CreateMap<MongoUser, Teacher>()
                .ReverseMap();

            CreateMap<UserDto, MongoUser>()
                .ConvertUsing(d =>
                {
                    var user = new MongoUser
                    {
                        UserName = d.Email,
                        Email = d.Email,
                        PhoneNumber = d.PhoneNumber
                    };
                    user.AddClaim(new Claim(TaggClaimTypes.FirstName, d.FirstName));
                    user.AddClaim(new Claim(TaggClaimTypes.LastName, d.LastName));
                    user.AddClaim(new Claim(TaggClaimTypes.Email, d.Email));
                    user.AddClaim(new Claim(TaggClaimTypes.PhoneNumber, d.PhoneNumber));

                    return user;
                });
            CreateMap<MongoUser, UserDto>()
                .ConvertUsing(u =>
                {
                    var firstNameClaim = u.Claims.FirstOrDefault(c => c.Type == TaggClaimTypes.FirstName);
                    var lastNameClaim = u.Claims.FirstOrDefault(c => c.Type == TaggClaimTypes.LastName);
                    var emailClaim = u.Claims.FirstOrDefault(c => c.Type == TaggClaimTypes.Email);
                    var phoneClaim = u.Claims.FirstOrDefault(c => c.Type == TaggClaimTypes.PhoneNumber);

                    return new UserDto
                    {
                        Id = u.Id,
                        FirstName = firstNameClaim?.Value ?? "",
                        LastName = lastNameClaim?.Value ?? "",
                        Email = emailClaim?.Value ?? "",
                        PhoneNumber = u.PhoneNumber,
                        UserType = UserHelper.GetUserTypeFromClaim(u.Claims),
                        IsLockedOut = u.LockoutEndDateUtc > DateTime.Now
                    };
                });
            CreateMap<UserRegisterDto, Substitute>()
                .ForMember(s => s.Address, ex => ex.ResolveUsing(vm =>
                {
                    return new Address
                    {
                        PostalCode = vm.PostalCode
                    };
                }))
                .ReverseMap();

            CreateMap<UserRegisterDto, Teacher>()
                .ForMember(s => s.Address, ex => ex.ResolveUsing(vm =>
                {
                    return new Address
                    {
                        PostalCode = vm.PostalCode
                    };
                }))
                .ReverseMap();
            CreateMap<UserBase, Substitute>()
                .ConvertUsing(u =>
                {
                    return new Substitute
                    {
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        Email = u.Email,
                        Status = u.Status
                    };
                });
            CreateMap<UserBase, Teacher>()
                .ConvertUsing(u =>
                {
                    return new Teacher
                    {
                        FirstName = u.FirstName,
                        LastName = u.LastName,
                        Email = u.Email,
                        Status = u.Status
                    };
                });
        }
    }
}
