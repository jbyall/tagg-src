﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaggApi.Models
{
    public class TestLoginViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
