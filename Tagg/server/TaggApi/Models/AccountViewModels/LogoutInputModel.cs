﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TaggApi.Models
{
    public class LogoutInputModel
    {
        public string LogoutId { get; set; }
    }
}
