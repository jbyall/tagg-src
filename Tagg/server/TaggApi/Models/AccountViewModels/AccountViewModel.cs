﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tagg.Domain;

namespace TaggApi.Models
{
    public class AccountViewModel
    {
        public AccountViewModel() { }
        //public AccountViewModel(UserProfile profile)
        //{
        //    FirstName = profile.FirstName;
        //    LastName = profile.LastName;
        //    Email = profile.User.Email;
        //    PostalCode = profile.Address.PostalCode;
        //    Street1 = profile.Address.Street1;
        //    Street2 = profile.Address.Street2;
        //    City = profile.Address.City;
        //    State = profile.Address.State;
        //    PhoneNumber = profile.User.PhoneNumber;
        //    ProfileId = profile.Id;
        //    IsComplete = !string.IsNullOrWhiteSpace(profile.Address?.Street1);
        //}
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string PostalCode { get; set; }
        public string Street1 { get; set; }
        public string Street2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PhoneNumber { get; set; }
        public int ProfileId { get; set; }
        public bool IsComplete { get; set; }
    }
}
