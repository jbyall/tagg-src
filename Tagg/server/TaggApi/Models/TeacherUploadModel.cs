﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace TaggApi.Controllers
{
	public class TeacherUploadModel
	{
		public string SchoolId { get; set; }
		public IFormFile File { get; set; }
	}
}