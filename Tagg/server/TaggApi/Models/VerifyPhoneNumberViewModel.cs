﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TaggApi.Models
{
    public class VerifyPhoneNumberViewModel
    {
        [Required]
        public string Code { get; set; }

        [Required]
        [Phone]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }
    }

    public class TempPhoneNumberViewModel
    {
        public string UserId { get; set; }
        public string PhoneNumber { get; set; }
    }

    public class VerifyTempPhoneViewModel
    {
        public string Code { get; set; }
        public string PhoneNumber { get; set; }
        public string UserId { get; set; }
    }
}
