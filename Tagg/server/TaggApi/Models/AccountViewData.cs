﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Tagg.Domain;

namespace TaggApi.Models
{
    public class AccountViewData
    {
        public AccountViewData()
        {
            this.EducationLevels = new List<Lookup>();
            this.Subjects = new List<Lookup>();
            this.States = new List<Lookup>();
        }

        public AccountViewData(List<Lookup> educationLevels, List<Lookup> subjects, List<Lookup> states)
        {
            this.EducationLevels = educationLevels;
            this.Subjects = subjects.OrderBy(s => s.Order).ToList();
            this.States = states.OrderBy(s => s.Key).ToList();
        }
        public List<Lookup> EducationLevels { get; set; }
        public List<Lookup> States { get; set; }
        public List<Lookup> Subjects { get; set; }
    }
}
