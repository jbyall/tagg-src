﻿using IdentityServer4;
using IdentityServer4.Models;
using IdentityServer4.Test;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Tagg.Domain;

namespace TaggApi
{
    public static class IdentityServerConfig
    {
        public static IEnumerable<IdentityResource> GetIdentityResources()
        {
            return new List<IdentityResource>
            {
                new IdentityResources.OpenId(),
                new IdentityResources.Profile
                {
                    UserClaims = new List<string>
                    {
                        TaggClaimTypes.Email,
                        TaggClaimTypes.FirstName,
                        TaggClaimTypes.LastName,
                        TaggClaimTypes.School
                    }
                },
                new IdentityResource("api_access", new List<string>{"api_access"})
            };
        }

        public static IEnumerable<ApiResource> GetApiResource()
        {
            return new List<ApiResource>
            {
                new ApiResource("taggapi", "Tagg Api", new List<string>(){ "api_access", "email", TaggClaimTypes.School }),
            };
        }

        public static IEnumerable<Client> GetClients()
        {
            var result = new List<Client>();

            // LOCAL CLIENTS
            var localClients = new List<Client>
            {
                //new Client
                //{
                //     ClientName = "Tagg Admin",
                //     ClientId = "adminlocalclientold",
                //     AllowedGrantTypes = GrantTypes.Hybrid,
                //     RedirectUris = new List<string>
                //     {
                //         "http://localhost:50703/signin-oidc"
                //     },
                //     AllowedScopes =
                //    {
                //        IdentityServerConstants.StandardScopes.OpenId,
                //        IdentityServerConstants.StandardScopes.Profile,
                //        "api_access",
                //        "taggapi"
                //    },
                //     RequireConsent = false,
                //     ClientSecrets = new List<Secret>{new Secret("test".Sha256())},

                //     // TODO : Consider adding a scope to explicitly request the claims
                //     AlwaysIncludeUserClaimsInIdToken = true,
                //     PostLogoutRedirectUris =
                //    {
                //        "http://localhost:50703/signout-callback-oidc"
                //    },
                //     //AccessTokenLifetime = 60,
                //     //IdentityTokenLifetime = 60,
                //     //AuthorizationCodeLifetime = 60,
                //     //AccessTokenLifetime = 60,
                //     AllowOfflineAccess = true
                //},

                new Client
                {
                    ClientName = "Tagg Admin",
                    ClientId = "adminlocalclient",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api_access",
                        "taggapi"
                    },
                    RedirectUris = new List<string>
                    {
                        "http://localhost:50703/auth-callback",
                        "http://localhost:50703/assets/silent-refresh.html"
                    },
                    PostLogoutRedirectUris = new List<string>
                    {
                        "http://localhost:50703"
                    },
                    RequireConsent = false,
                    AllowAccessTokensViaBrowser = true,
                    //AllowOfflineAccess = true,
                    AllowedCorsOrigins = new List<string> { "http://localhost:50703/" },
                },

                new Client
                {
                    ClientId = "schoolslocalclient",
                    ClientName = "Tagg Schools",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api_access",
                        "taggapi"
                    },
                    RedirectUris = new List<string>
                    {
                        "http://localhost:56329/auth-callback",
                        "http://localhost:56329/silent-refresh.html"
                    },
                    PostLogoutRedirectUris = new List<string>
                    {
                        "http://localhost:56329"
                    },
                    RequireConsent = false,
                    AllowAccessTokensViaBrowser = true,
                    //AllowOfflineAccess = true,
                    AllowedCorsOrigins = new List<string> { "http://localhost:56329/" },
                },

                new Client
                {
                    ClientId = "subslocalclient",
                    ClientName = "Tagg Subs",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api_access",
                        "taggapi"
                    },
                    PostLogoutRedirectUris = new List<string>
                    {
                        "http://localhost:62790"
                    },
                    RedirectUris = new List<string>
                    {
                        "http://localhost:62790/auth-callback",
                        "http://localhost:62790/silent-refresh.html"
                    },
                    RequireConsent = false,
                    AllowAccessTokensViaBrowser = true,
                    //AllowOfflineAccess = true,
                    AllowedCorsOrigins = new List<string> { "http://localhost:62790/" },
                },
            };

            // STAGING CLIENTS
            var stagingClients = new List<Client>
            {

                new Client
                {
                     ClientName = "Tagg Admin",
                     ClientId = "adminstageclient",
                     AllowedGrantTypes = GrantTypes.Implicit,
                     RedirectUris = new List<string>
                     {
                         "https://taggadmin-staging.azurewebsites.net/auth-callback",
                         "https://taggadmin-staging.azurewebsites.net/assets/silent-refresh.html"
                     },
                     AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api_access",
                        "taggapi"
                    },
                     RequireConsent = false,
                     AllowAccessTokensViaBrowser = true,
                     AllowedCorsOrigins = new List<string> { "https://taggadmin-staging.azurewebsites.net/" },

                     // TODO : Consider adding a scope to explicitly request the claims
                     AlwaysIncludeUserClaimsInIdToken = true,
                     PostLogoutRedirectUris =
                    {
                        "https://taggadmin-staging.azurewebsites.net"
                    },
                },

                new Client
                {
                    ClientId = "schoolsstageclient",
                    ClientName = "Tagg Schools",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api_access",
                        "taggapi"
                    },
                    RedirectUris = new List<string>
                    {
                        "https://tagg-schools-staging.azurewebsites.net/auth-callback",
                        "https://tagg-schools-staging.azurewebsites.net/silent-refresh.html"
                    },
                    PostLogoutRedirectUris =
                    {
                        "https://tagg-schools-staging.azurewebsites.net"
                    },
                    RequireConsent = false,
                    AllowAccessTokensViaBrowser = true,
                    AllowedCorsOrigins = new List<string> { "https://tagg-schools-staging.azurewebsites.net/" },
                },

                new Client
                {
                    ClientId = "subsstageclient",
                    ClientName = "Tagg Schools",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api_access",
                        "taggapi"
                    },
                    RedirectUris = new List<string>
                    {
                        "https://tagg-subs-staging.azurewebsites.net/auth-callback",
                        "https://tagg-subs-staging.azurewebsites.net/silent-refresh.html"
                    },
                    PostLogoutRedirectUris =
                    {
                        "https://tagg-subs-staging.azurewebsites.net/"
                    },
                    RequireConsent = false,
                    AllowAccessTokensViaBrowser = true,
                    AllowedCorsOrigins = new List<string> { "https://tagg-subs-staging.azurewebsites.net/" },
                },
            };

            // PROD CLIENTS
            var prodClients = new List<Client>
            {
                new Client
                {
                     ClientName = "Tagg Admin",
                     ClientId = "adminclient",
                     AllowedGrantTypes = GrantTypes.Implicit,
                     RedirectUris = new List<string>
                     {
                         "https://system.taggeducation.com/auth-callback",
                         "https://system.taggeducation.com/assets/silent-refresh.html"
                     },
                     AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api_access",
                        "taggapi"
                    },
                     RequireConsent = false,
                     AllowAccessTokensViaBrowser = true,
                     AllowedCorsOrigins = new List<string> { "https://system.taggeducation.com/" },

                     // TODO : Consider adding a scope to explicitly request the claims
                     AlwaysIncludeUserClaimsInIdToken = true,
                     PostLogoutRedirectUris =
                    {
                        "https://system.taggeducation.com"
                    },
                     //AccessTokenLifetime = 60,
                     //RefreshTokenExpiration = TokenExpiration.Sliding,
                     //AllowOfflineAccess = true
                },

                new Client
                {
                    ClientId = "schoolsclient",
                    ClientName = "Tagg Schools",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api_access",
                        "taggapi"
                    },
                    RedirectUris = new List<string>
                    {
                        "https://schools.taggeducation.com/auth-callback",
                        "https://schools.taggeducation.com/silent-refresh.html"
                    },
                    PostLogoutRedirectUris = new List<string>
                    {
                        "https://schools.taggeducation.com"
                    },
                    RequireConsent = false,
                    AllowAccessTokensViaBrowser = true,
                    AllowOfflineAccess = true,
                    AllowedCorsOrigins = new List<string> { "https://schools.taggeducation.com/" }//, "https://tagg-schools.azurewebsites.net/" },
                },

                new Client
                {
                    ClientId = "subsclient",
                    ClientName = "Tagg Schools",
                    AllowedGrantTypes = GrantTypes.Implicit,
                    AllowedScopes =
                    {
                        IdentityServerConstants.StandardScopes.OpenId,
                        IdentityServerConstants.StandardScopes.Profile,
                        "api_access",
                        "taggapi"
                    },
                    RedirectUris = new List<string>
                    {
                        "https://subs.taggeducation.com/auth-callback",
                        "https://subs.taggeducation.com/silent-refresh.html"
                    },
                    PostLogoutRedirectUris = new List<string>
                    {
                        "https://subs.taggeducation.com"
                    },
                    RequireConsent = false,
                    AllowAccessTokensViaBrowser = true,
                    AllowOfflineAccess = true,
                    AllowedCorsOrigins = new List<string> { "https://subs.taggeducation.com/" }//,"https://tagg-subs.azurewebsites.net/" },
                },
            };

            result.AddRange(localClients);
            result.AddRange(stagingClients);
            result.AddRange(prodClients);
            return result;
        }
    }
}
