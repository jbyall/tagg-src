﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Sentry;
using Tagg.Domain;
using Tagg.Domain.ApiModels;
using Tagg.Services;
using TaggApi.Models;
using Twilio;
using Twilio.Jwt.AccessToken;
using Twilio.Rest.Chat.V2.Service;
using Twilio.Rest.Chat.V2.Service.Channel;


namespace TaggApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Messages")]
    [Authorize(Policy = AuthPolicies.TaggUser)]
    public class MessagesController : Controller
    {
        private readonly IEmailService _emailService;
        private readonly IConfiguration _config;
        private UserManager<MongoUser> _userManager;
        private ChatService _chat;
        private string _accountSid;
        private string _authToken;
        private string _apiKey;
        private string _apiSecret;
        private string _chatServiceId;
        private TaggUnitOfWork _work;
        private IMapper _mapper;

        public MessagesController(IEmailService emailService,
            IConfiguration config,
            TaggUnitOfWork work,
            IMapper mapper,
            UserManager<MongoUser> userManager,
            ChatService chat)
        {
            _emailService = emailService;
            _config = config;
            _work = work;
            _mapper = mapper;
            _userManager = userManager;
            _chat = chat;

            //TwilioClient.Init(_accountSid, _authToken);
        }

        //[HttpGet("{id}", Name = "GetMessage")]
        //public async Task<ActionResult> Get(string id)
        //{
        //    var result = _work.Messages.GetById(id);
        //    if (result == null)
        //    {
        //        return NotFound();
        //    }
        //    return Ok(_mapper.Map<AppMessageDto>(result));
        //}

        [HttpGet("token")]
        public IActionResult GetToken()
        {
            var identity = getAuthorizedUserId();
            var jwt = _chat.GetChatToken(identity);
            if (!string.IsNullOrWhiteSpace(jwt))
            {
                return Ok(jwt);
            }
            return BadRequest();

        }

        [HttpGet("channels")]
        [AllowAnonymous]
        public IActionResult GetChannels()
        {
            var allChannels = _chat.ListChannels();
            return Ok(allChannels);
        }

        [HttpPost("channel/{channelName}")]
        public IActionResult CreateChannel(string channelName)
        {
            var channel = _chat.CreateChannel(channelName);
            if (channel != null)
            {
                return Ok(channel);
            }
            return BadRequest("Could not create channel.");

        }

        [HttpPost("channel/job/{jobId}")]
        public IActionResult JobChannelCreate(string jobId)
        {
            var job = _work.Jobs.GetById(jobId);
            if (job != null)
            {
                // Check for existing channel
                var existingChannel = _chat.GetChannelByInternalId(jobId);
                if (existingChannel != null)
                {
                    return Ok(existingChannel.UniqueName);
                }

                // Create it if it doesn't exist
                var channel = _chat.CreateChannel("Job Message", jobId);
                if (channel != null)
                {
                    var subChatUser = _chat.AddUserToExistingChannel(job.SubstituteId, channel.Sid);
                    var teacherChatUser = _chat.AddUserToExistingChannel(job.TeacherId, channel.Sid);
                    if (teacherChatUser != null && subChatUser != null)
                    {
                        return Ok(channel.UniqueName);
                    }
                }
                return StatusCode(500, "Could not create message channel.");
            }
            return NotFound();
        }

        [HttpPost("channel/user")]
        [AllowAnonymous]
        public IActionResult AddMember([FromBody] AddMemberDto model)
        {
            if (ModelState.IsValid)
            {
                var chatUser = _chat.AddUserToExistingChannel(model.UserIdentity, model.ChannelId);
                if (chatUser != null)
                {
                    return Ok(chatUser);
                }
                return BadRequest("Couldn't add user to channel");
            }
            return BadRequest("Invalid model");

        }

        [HttpGet("user/{userId}")]
        public IActionResult GetUser(string userId)
        {
            if (!string.IsNullOrWhiteSpace(userId))
            {

                var user = _chat.GetUser(userId);
                if (user != null)
                {
                    return Ok(user);
                }
                return NotFound();
            }
            return BadRequest();
        }

        [HttpPost("user")]
        [AllowAnonymous]
        public IActionResult CreateUser([FromBody] ChatUserCreateDto model)
        {
            if (ModelState.IsValid)
            {
                var chatUser = _chat.CreateUser(model.Identity);
                if (chatUser != null)
                {
                    return Ok(chatUser);
                }
                return BadRequest("Could not create chat user");
            }
            return BadRequest("Invalid model");
        }

        [HttpDelete("channel/{channelId}")]
        [AllowAnonymous]
        public IActionResult DeleteChannel(string channelId)
        {
            if (_chat.DeleteChannel(channelId))
            {
                return NoContent();
            }
            return BadRequest();

        }

        [HttpDelete("channels")]
        [AllowAnonymous]
        public IActionResult DeleteAllChannels()
        {
            string env = _config["Server:Env"];
            if (env == "Development")
            {
                try
                {
                    var allChannels = _chat.ListChannels();
                    foreach (var item in allChannels)
                    {
                        _chat.DeleteChannel(item.Sid);
                    }
                    return NoContent();
                }
                catch (Exception ex)
                {
                    SentrySdk.CaptureException(ex);
                    return StatusCode(500);
                }
            }
            return BadRequest();
        }

        [HttpDelete("users")]
        [AllowAnonymous]
        public IActionResult DeleteAllUsers()
        {
            string env = _config["Server:Env"];
            if (env == "Development")
            {
                try
                {
                    var allUsers = _chat.ListUsers();
                    foreach (var item in allUsers)
                    {
                        _chat.DeleteUser(item.Sid);
                    }
                    return NoContent();
                }
                catch (Exception ex)
                {
                    SentrySdk.CaptureException(ex);
                    return StatusCode(500);
                }
            }
            return BadRequest();

        }

        [HttpGet("testdup/{channelUid}")]
        [AllowAnonymous]
        public IActionResult TestDup(string channelUid)
        {
            var result = _chat.CreateChannel("TestChannel", channelUid);
            return Ok(result);
        }

        private string getAuthorizedUserId()
        {
            try
            {
                return this.User.Claims.FirstOrDefault(c => c.Type == "sub").Value;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return "";
            }
        }

        // Old code

        //[HttpPost("Add/{threadId}")]
        //[AllowAnonymous]
        //public IActionResult Add(string threadId, [FromBody] AppMessageDto messageDto)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        AppMessage message = _mapper.Map<AppMessage>(messageDto);

        //        var addResult = _work.Messages.AddMessage(message, threadId);
        //        if (addResult.Succeeded)
        //        {
        //            var result = _mapper.Map<MessageThreadDto>(addResult.Entity);
        //            return Ok(result);
        //        }
        //    }
        //    return BadRequest(ModelState);
        //}

        //[HttpPost("Threads/Add")]
        //[AllowAnonymous]
        //public IActionResult AddThread([FromBody] MessageThreadDto dto)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var thread = _mapper.Map<MessageThread>(dto);
        //        var addResult = _work.Messages.Add(thread);
        //        if (addResult.Succeeded)
        //        {
        //            var result = _mapper.Map<MessageThreadDto>(addResult.Entity);
        //            return Ok(result);
        //        }
        //    }
        //    return BadRequest(ModelState);
        //}

        //[HttpPut("{messageId}", Name = "EditMessage")]
        //public IActionResult MarkAsRead(string messageId)
        //{
        //    var result = _work.Messages.MarkAsRead(messageId);
        //    return Ok(result.Entity);
        //    //message.ReadOn = DateTime.Now;

        //    //var editResult = _work.Messages.Update(message);

        //    //if (editResult.Succeeded)
        //    //{
        //    //    var result = _mapper.Map<AppMessageDto>(editResult.Entity);
        //    //    return CreatedAtRoute("GetMessage", new { id = result.Id }, result);
        //    //}

        //    //return BadRequest(ModelState);
        //}

        //[HttpGet("Threads/{email}/{skip:int?}/{take:int?}", Name = "GetThreads")]
        //[AllowAnonymous]
        //public async Task<ActionResult> GetThreads(string email, int skip = 0, int take = 10)
        //{
        //    //string email = getAuthorizedUserEmail();

        //    var threads = _work.Messages.GetThreadsByUserId(email, skip, take);

        //    return Ok(_mapper.Map<List<MessageThreadDto>>(threads));
        //}

        //[HttpGet("Thread/{id}", Name = "GetThread")]
        //public async Task<ActionResult> GetThread(string id)
        //{
        //    //string email = getAuthorizedUserEmail();

        //    MessageThread thread = _work.Messages.GetById(id);

        //    var result = _mapper.Map<MessageThreadDto>(thread);

        //    return Ok(result);
        //}

        //[HttpDelete("Thread/{threadId}")]
        //public async Task<ActionResult> DeleteThread(string threadId)
        //{
        //    var result = _work.Messages.DeleteThread(threadId);
        //    if (result.Succeeded)
        //    {
        //        return NoContent();
        //    }
        //    return BadRequest();

        //}
    }

    public class AddMemberDto
    {
        [Required]
        public string UserIdentity { get; set; }
        [Required]
        public string ChannelId { get; set; }
    }

    public class ChatUserCreateDto
    {
        [Required]
        public string Identity { get; set; }
    }
}