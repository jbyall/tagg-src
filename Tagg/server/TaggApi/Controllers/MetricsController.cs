﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Sentry;
using Tagg.Domain;
using Tagg.Domain.ApiModels;

namespace TaggApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Metrics")]
    public class MetricsController : Controller
    {
        private IMapper _mapper;
        private TaggUnitOfWork _work;
        private UserManager<MongoUser> _userManager;

        public MetricsController(
            TaggUnitOfWork work,
            IMapper mapper,
            UserManager<MongoUser> userManager)
        {
            _mapper = mapper;
            _work = work;
            _userManager = userManager;
        }

        [HttpGet("Teacher/{id}")]
        public async Task<IActionResult> GetForTeacher(string id)
        {
            var teacher = _work.Teachers.GetById(id);
            if (teacher != null)
            {
                var result = getTeacherMetrics(id);
                return Ok(result);
            }
            return NotFound();

        }

        [HttpGet("Sub/{id}")]
        public async Task<IActionResult> GetForSub(string id)
        {
            var sub = _work.Subs.GetById(id);
            if (sub != null)
            {
                var result = getSubMetrics(id);
                result.Sub = _mapper.Map<SubstituteDto>(sub);
                return Ok(result);
            }
            return NotFound();

        }

        [HttpGet("School/{id}")]
        public async Task<IActionResult> GetForSchool(string id)
        {
            var school = _work.Schools.GetById(id);
            if (school != null)
            {
                var result = getSchoolMetrics(id);
                return Ok(result);
            }
            return NotFound();
        }

        [HttpGet("District/{schoolId}")]
        public async Task<IActionResult> GetForDistrict(string schoolId)
        {
            var districtId = _work.Schools.GetDistrictIdBySchoolId(schoolId);
            if (!string.IsNullOrWhiteSpace(districtId))
            {
                var result = getDistrictMetrics(districtId);
                return Ok(result);
            }
            return NotFound();
        }



        #region helpers
        private TeacherMetricsDto getTeacherMetrics(string id)
        {
            var result = new TeacherMetricsDto();
            try
            {
                var now = DateTime.UtcNow;
                var monthStart = new DateTime(now.Year, now.Month, 1);

                var allTeacherJobs = _work.Jobs.Query(j => j.TeacherId == id);
                var sytdStart = now.Month <= 7 ? new DateTime(now.Year - 1, 8, 1) : new DateTime(now.Year, 8, 1);
                //var schoolJobsSytd = allSchoolJobs.Where(j => j.StartDate >= sytdStart && j.StartDate <= now);
                var teacherJobsSytd = allTeacherJobs.Where(j => j.StartDate >= sytdStart && j.StartDate <= now);

                var jobsCovered = teacherJobsSytd.Where(j => j.SubNeeded && j.SubstituteId != null).ToList();
                var jobsUncovered = teacherJobsSytd.Where(j => !j.SubNeeded).ToList();


                // calculate all time off
                teacherJobsSytd.ToList().ForEach(j => result.TimeOff += (j.EndDate - j.StartDate).Hours);

                // calculate time off covered
                jobsCovered.ForEach(j => result.TimeCovered += (j.EndDate - j.StartDate).Hours);

                // calculate subs hired
                result.SubsHired = jobsCovered.Count;

                // Get most recent job
                result.RecentJob = _work.Jobs.GetByTeacherId(id, 1)
                    .Where(j => j.SubNeeded && j.SubstituteId != null)
                    .OrderByDescending(j => j.StartDate)
                    .FirstOrDefault();

                // Get favorite sub
                var mostUsedSubs = (from subJob in allTeacherJobs.Where(j => j.SubNeeded && j.Status == JobStatus.Complete)
                                    group subJob by subJob.SubstituteId into grp
                                    select new { SubId = grp.Key, Count = grp.Count() })
                                   .OrderByDescending(sj => sj.Count)
                                   .Take(1)
                                   .FirstOrDefault();
                if (mostUsedSubs != null)
                {
                    var sub = _work.Subs.GetById(mostUsedSubs.SubId);
                    result.FavoriteSub = $"{sub?.FirstName} {sub?.LastName}";
                }

                return result;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return result;
            }
        }

        private SubMetricsDto getSubMetrics(string id)
        {
            var result = new SubMetricsDto();
            try
            {
                var now = DateTime.UtcNow;

                var allJobs = _work.Jobs.Query(j => j.SubstituteId == id && j.Status == JobStatus.Complete);
                var jobsYtd = allJobs.Where(j => j.EndDate >= new DateTime(now.Year, 1, 1)).ToList();

                // calculate earning in last month
                result.EarningsLastMonth = jobsYtd
                    .Where(j => j.StartDate >= now.AddDays(-30))
                    .Sum(j => j.PayAmount);

                // calculate average earnings per month
                var earnAvgMonthQuery = (from j in jobsYtd
                                         group j by j.StartDate.Month into grp
                                         select grp.Average(x => x.PayAmount));

                result.EarningsAvgPerMonth = earnAvgMonthQuery.Count() > 0 ? earnAvgMonthQuery.Average() : 0;

                // calculate rating
                result.Rating = allJobs.Count() > 3 ? allJobs.Average(j => j.Rating) : 3;

                // calculate number of days subbing
                result.DaysSubbing = jobsYtd.Count;

                // calculate number of schools worked for
                result.TotalSchools = allJobs.Select(j => j.SchoolId).Distinct().Count();

                // calculate favorite school
                if (allJobs.Count() > 0)
                {
                    var favoriteSchoolId = allJobs.Max(j => j.SchoolId);
                    result.FavoriteSchool = _work.Schools
                        .Query(s => s.Id == favoriteSchoolId)
                        .Select(x => x.Name)
                        .FirstOrDefault();
                    // calculate favorite grade
                    result.FavoriteGrade = allJobs.Max(j => j.Subject);
                }

                return result;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return result;
            }

        }

        private SchoolMetricsDto getSchoolMetrics(string id)
        {
            var result = new SchoolMetricsDto();
            try
            {
                var now = DateTime.UtcNow;
                var monthStart = new DateTime(now.Year, now.Month, 1);
                var monthEnd = monthStart.AddMonths(1).AddDays(-1);

                var allSchoolJobs = _work.Jobs.Query(j => j.SchoolId == id && j.Status == JobStatus.Complete);

                var sytdStart = now.Month < 7 ? new DateTime(now.Year - 1, 8, 1) : new DateTime(now.Year, 8, 1);
                //var schoolJobsSytd = allSchoolJobs.Where(j => j.StartDate >= sytdStart && j.StartDate <= now);
                var schoolJobsSytd = allSchoolJobs.Where(j => j.StartDate >= sytdStart && j.StartDate <= now);
                var schoolJobsSytdCount = schoolJobsSytd.Count();
                var subRequestSytdCount = schoolJobsSytd.Where(j => j.SubNeeded).Count();

                // calculate number of sub jobs this month
                result.SubDaysMonth = schoolJobsSytd.Count(j => j.SubNeeded && j.StartDate >= monthStart && j.StartDate <= now);

                var daysCoveredSytd = schoolJobsSytd.Count(j => j.SubNeeded);

                // calculate number % of jobs filled for this school year

                result.FilledSytdPercent = subRequestSytdCount > 0 ? (daysCoveredSytd * 100) / subRequestSytdCount : 0;

                // calculate total days taken off by teachers this month
                result.TeacherDaysMonth = schoolJobsSytd
                    .Count(j => j.StartDate >= monthStart && j.StartDate <= now);

                // calculate total days taken off by teachers this school year
                result.TeacherDaysSytd = schoolJobsSytd.Count();

                // calculate total days covered by sub this school year
                result.SubDaysSytd = daysCoveredSytd;

                // calculate average sub rating from all teachers
                var avgPredicate = allSchoolJobs.Where(j => j.Rating > 0);
                result.AvgRating = avgPredicate.Count() > 0 ? avgPredicate.Average(j => j.Rating) : 0;

                // retrieve top 3 most used subs
                var mostUsedSubs = (from subJob in allSchoolJobs.Where(j => j.SubNeeded && j.StartDate <= now)
                                    group subJob by subJob.SubstituteId into grp
                                    select new { SubId = grp.Key, Count = grp.Count() })
                                   .OrderByDescending(sj => sj.Count)
                                   .Take(3)
                                   .ToList();

                foreach (var item in mostUsedSubs)
                {
                    if (item.SubId != null)
                    {
                        var sub = _work.Subs.GetById(item.SubId);
                        result.PopularSubs += $"{sub.FirstName} {sub.LastName}, ";
                    }
                }
                result.PopularSubs = result.PopularSubs.Trim(' ', ',');

                return result;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return result;
            }
        }

        private DistrictMetricsDto getDistrictMetrics(string id)
        {
            var result = new DistrictMetricsDto();
            try
            {
                var now = DateTime.UtcNow;
                var monthStart = new DateTime(now.Year, now.Month, 1);

                var schoolIds = _work.Schools
                    .Query(s => s.DistrictId == id)
                    .Select(s => s.Id);

                //var allDistrictJobsComplete = _work.Jobs.Query(j => schoolIds.Any(x => x == j.Id) && j.Status == JobStatus.Complete);
                var allDistrictJobs = from sids in schoolIds
                                      join adj in _work.Jobs.GetQueryable() on sids equals adj.SchoolId
                                      select adj;
                var allDistrictJobsComplete = allDistrictJobs.Where(j => j.Status == JobStatus.Complete);

                var sytdStart = now.Month < 7 ? new DateTime(now.Year - 1, 8, 1) : new DateTime(now.Year, 8, 1);
                var districtJobsYtdComplete = allDistrictJobsComplete.Where(j => j.StartDate >= sytdStart && j.StartDate <= now);
                var districtJobsYtdAll = allDistrictJobs.Where(j => j.StartDate >= sytdStart && j.StartDate <= now);
                var districtJobsYtdCount = districtJobsYtdComplete.Count();

                var daysCoveredSytd = districtJobsYtdComplete.Count(j => j.SubNeeded);

                // calculate % of jobs filled for this school year to date
                result.JobsFilledSytdPercent = districtJobsYtdCount > 0 ? (daysCoveredSytd * 100) / districtJobsYtdCount : 0;

                // calculate total days taken off by teachers this month
                result.TeacherDaysMonth = districtJobsYtdComplete
                    .Count(j => j.StartDate >= monthStart && j.StartDate <= now);

                // calculate today days taken off by teachers this school year
                result.TeacherDaysSytd = districtJobsYtdComplete.Count();

                result.SubDaysSytd = daysCoveredSytd;

                result.SubDaysMonth = districtJobsYtdComplete.Count(j => j.SubNeeded && j.StartDate >= monthStart && j.StartDate <= now);

                // calculate average sub rating from all teachers
                var avgPredicate = allDistrictJobsComplete.Where(j => j.Rating > 0);
                result.AvgRating = avgPredicate.Count() > 0 ? avgPredicate.Average(j => j.Rating) : 0;

                var today = new DateTime(now.Year, now.Month, now.Day);
                var tonight = today.AddDays(1).AddMinutes(-1);
                result.JobsFilledToday = districtJobsYtdAll.Count(j => j.StartDate >= today && j.StartDate <= tonight && j.SubNeeded && j.Status == JobStatus.Accepted);
                result.JobsUnfilledToday = districtJobsYtdAll.Count(j => j.StartDate >= today && j.StartDate <= tonight && j.SubNeeded && j.Status == JobStatus.Open);

                return result;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return result;
            }
        }
        #endregion
    }
}