﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Sentry;
using Tagg.Domain;
using Tagg.Domain.ApiModels;
using Tagg.Services;
using TaggApi.Models;

namespace TaggApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Teachers")]
    [Authorize(Policy = AuthPolicies.TaggUser)]
    public class TeachersController : Controller
    {
        private readonly IEmailService _emailService;
        private readonly ILogger _logger;
        private readonly IConfiguration _config;
        private UserManager<MongoUser> _userManager;
        private TaggUnitOfWork _work;
        private LocationService _location;
        private IMapper _mapper;

        public TeachersController(IEmailService emailService,
            IConfiguration config,
            TaggUnitOfWork work,
            LocationService location,
            IMapper mapper,
            UserManager<MongoUser> userManager)
        {
            _emailService = emailService;
            _config = config;
            _work = work;
            _location = location;
            _mapper = mapper;
            _userManager = userManager;
        }

        [HttpGet("{id}", Name = "GetTeacher")]
        public IActionResult Get(string id)
        {
            var teacher = _work.Teachers.GetById(id);
            if (teacher == null)
            {
                return NotFound();
            }
            var result = _mapper.Map<TeacherDto>(teacher);
            result.PhoneVerified = _work.Users.GetById(id)?.PhoneNumberConfirmed;

            return Ok(result);
        }

        [HttpGet("details/{id}")]
        public IActionResult GetDetails(string id)
        {
            //var result = getTeacherDetails(id);
            var result = getTeacherDetails(id);
            return Ok(result);
        }

        [HttpGet("all")]
        public IActionResult GetAll()
        {
            var teachers = _work.Teachers.GetAll();
            return Ok(_mapper.Map<List<TeacherDto>>(teachers));
        }

        [HttpGet("status/{statusId}")]
        public IActionResult GetByStatus(string statusId)
        {
            var result = new List<Teacher>();
            TaggStatus status;
            if (Enum.TryParse(statusId, true, out status))
            {
                result = _work.Teachers.Query(t => t.Status == status).ToList();
                return Ok(_mapper.Map<List<TeacherDto>>(result));
            }
            return NoContent();
        }

        [HttpGet("school/{schoolId}")]
        public IActionResult GetBySchool(string schoolId)
        {
            return Ok(_mapper.Map<List<TeacherDto>>(_work.Teachers.GetAllBySchoolId(schoolId)));
        }

        [HttpPost("Create/{schoolId}")]
        public async Task<IActionResult> Create(string schoolId, [FromBody] TeacherDto model)
        {
            if (ModelState.IsValid)
            {
                var createResult = await createTeacherAsync(model, schoolId);
                if (createResult.Succeeded)
                {
                    var result = _mapper.Map<TeacherDto>(createResult.Entity);
                    return CreatedAtRoute("GetTeacher", new { id = result.Id }, result);
                }
                createResult.Errors.ForEach(e => ModelState.AddModelError("", e));
                return StatusCode(409, ModelState);
            }
            return BadRequest(ModelState);
        }

        [HttpPost("Update")]
        [Authorize(Policy = AuthPolicies.Teacher)]
        public async Task<IActionResult> Update([FromBody] TeacherDto model)
        {
            if (ModelState.IsValid)
            {
                var teacher = _mapper.Map<Teacher>(model);
                teacher.Status = getTeacherStatus(teacher);
                var updateResult = _work.Teachers.Update(teacher);
                if (updateResult.Succeeded)
                {
                    var result = await getUpdateResult(updateResult.Entity);
                    return Ok(result);
                }
            }
            return BadRequest();
        }

        [HttpPost("Upload/{schoolId}")]
        public async Task<IActionResult> Upload(string schoolId, IFormFile file)
        {

            if (file == null || string.IsNullOrWhiteSpace(schoolId) || !_work.Schools.Exists(schoolId))
            {
                return BadRequest();
            }
            var uploadResult = await processBulkUpload(file, schoolId);
            if (uploadResult.Succeeded)
            {
                return NoContent();
            }

            return BadRequest();
        }

        [HttpGet("Favorite/{teacherId}/{subId}")]
        public IActionResult ToggleFavorite(string teacherId, string subId)
        {
            var teacher = _work.Teachers.GetById(teacherId);
            var sub = _work.Subs.GetById(subId);
            if (teacher != null && sub != null)
            {
                var favResult = addFavorite(teacher, sub);
                if (!favResult)
                {
                    return StatusCode(500);
                }
                return Ok();
            }
            return NotFound();
        }

        [HttpPut("pay/{teacherId}/{payRate:double?}")]
        public IActionResult UpdatePayRate(string teacherId, double? payRate = null)
        {
            var teacher = _work.Teachers.GetById(teacherId);
            if (teacher != null && payRate.HasValue)
            {
                teacher.PayRate = payRate.Value;
                var updateResult = _work.Teachers.Update(teacher);
                if (updateResult.Succeeded)
                {
                    return NoContent();
                }
            }
            return BadRequest();
        }


        #region Helpers
        private async Task<EntityResult<Teacher>> createTeacherAsync(TeacherDto model, string schoolId)
        {
            var result = new EntityResult<Teacher>();
            if (!_work.Schools.Exists(schoolId))
            {
                result.AddError("School not found");
                return result;
            }
            var teacher = _mapper.Map<Teacher>(model);
            var userCreateModel = new UserCreateDto
            {
                Email = teacher.Email,
                FirstName = teacher.FirstName,
                LastName = teacher.LastName,
                PhoneNumber = teacher.PhoneNumber,
                UserType = UserType.Teacher,
                SendInvitEmail = true,
                SchoolId = schoolId
            };
            var createUserResult = await _work.Users.CreateAsync(userCreateModel);
            var inviteResult = await sendInviteEmail(createUserResult.Entity, UserType.Teacher);


            // insert teacher and userprofile
            teacher.Status = TaggStatus.New;
            teacher.Id = createUserResult.Entity.Id;
            teacher.SchoolId = schoolId;
            var insertResult = _work.Teachers.Add(teacher);
            if (!insertResult.Succeeded)
            {
                // return erros if teacher creation fails
                insertResult.Errors.ForEach(e => result.AddError(e));
            }

            return insertResult;

        }

        private async Task<EntityResult<Teacher>> processBulkUpload(IFormFile file, string schoolId)
        {
            var fileSize = file.Length;
            var fileName = WebUtility.HtmlEncode(Path.GetFileName(file.FileName));
            var contentType = file.ContentType;
            var fileExt = Path.GetExtension(fileName);

            if (fileSize > 1048576)
            {
                return new EntityResult<Teacher>("File size must be less than than 1MB.");
            }

            if (fileExt != ".csv")
            {
                return new EntityResult<Teacher>("File must be a CSV (.csv) file.");
            }

            var errors = new List<string>();
            var teachers = new Dictionary<int, TeacherDto>();

            using (var reader = new StreamReader(file.OpenReadStream(),
                new UTF8Encoding(encoderShouldEmitUTF8Identifier: false, throwOnInvalidBytes: true),
                detectEncodingFromByteOrderMarks: true))
            {
                var csv = new CsvHelper.CsvReader(reader);
                csv.Read();
                csv.ReadHeader();
                int i = 1;
                while (csv.Read())
                {
                    string firstName = csv[0];
                    string lastName = csv[1];
                    string email = csv[2];
                    string phoneNumber = csv[3];
                    var namePattern = @"([^\w\s]|\d)";      //Any non-word chars and numbers (except space)
                    var emailPattern = @"[^a-zA-Z0-9@._]";   //Anything that is not a letter, number, @ or period
                    var phonePattern = @"\D";               //Any non-digit char

                    var teacherModel = new TeacherDto
                    {
                        FirstName = Regex.Replace(firstName, namePattern, string.Empty),
                        LastName = Regex.Replace(lastName, namePattern, string.Empty),
                        Email = Regex.Replace(email, emailPattern, string.Empty),
                        PhoneNumber = Regex.Replace(phoneNumber, phonePattern, string.Empty),
                    };
                    teachers.Add(i, teacherModel);
                    i++;
                }
            }

            foreach (var item in teachers)
            {
                var creationResult = await createTeacherAsync(item.Value, schoolId);
                if (!creationResult.Succeeded)
                {
                    errors.Add($"Error in row {item.Key}: {creationResult.Errors.First()}");
                }
            }

            var result = new EntityResult<Teacher>();
            if (errors.Count > 0)
            {
                errors.ForEach(e => result.AddError(e));
                result.AddError("Teachers have been created for all other rows. Please fix the errors and submit the file again with only the rows listed above.");
            }
            return result;
        }

        private TeacherDetailDto getTeacherDetails(string id)
        {
            var teacher = _work.Teachers.GetById(id);
            var result = _mapper.Map<TeacherDetailDto>(teacher);
            try
            {
                var now = DateTime.UtcNow;
                var jobsYtd = _work.Jobs.Query(j => j.TeacherId == id && j.EndDate >= new DateTime(now.Year, 1, 1) && j.Status != JobStatus.Cancelled).ToList();
                var jobsCovered = jobsYtd.Where(j => j.SubNeeded == true).ToList();
                var jobsUncovered = jobsYtd.Where(j => j.SubNeeded == false).ToList();

                // calculate all time off
                jobsYtd.ForEach(j => result.HoursTaken += (j.EndDate - j.StartDate).TotalHours);

                // calculate time off covered
                jobsCovered.ForEach(j => result.HoursCovered += (j.EndDate - j.StartDate).TotalHours);

                return result;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return result;
            }
        }

        private TaggStatus getTeacherStatus(Teacher teacher)
        {
            if ((teacher.ReceiveEmail || teacher.ReceivePhone || teacher.ReceiveSMS) && teacher.Subjects.Count > 0)
            {
                return TaggStatus.Active;
            }
            return teacher.Status;
        }

        public async Task<TeacherUpdateResultDto> getUpdateResult(Teacher teacher)
        {
            var user = await _userManager.FindByIdAsync(teacher.Id);
            if (user.PhoneNumber != teacher.PhoneNumber)
            {
                var updateUserResult = await _userManager.SetPhoneNumberAsync(user, teacher.PhoneNumber);
            }

            var dto = _mapper.Map<TeacherDto>(teacher);
            bool requireVerification = dto.ReceiveSMS && !user.PhoneNumberConfirmed;
            var result = new TeacherUpdateResultDto
            {
                Teacher = dto,
                PhoneNumber = teacher.PhoneNumber,
                VerificationRequired = requireVerification
            };
            return result;
        }

        private bool addFavorite(Teacher teacher, Substitute sub)
        {
            var favoritedRemoved = teacher.Favorites.Remove(sub.Id);
            if (!favoritedRemoved)
            {
                teacher.Favorites.Add(sub.Id);
            }
            var updateResult = _work.Teachers.Update(teacher, true);
            return updateResult.Succeeded;
        }

        public async Task<bool> sendInviteEmail(MongoUser user, UserType userType)
        {
            var token = await getInviteToken(user);
            var urlBase = "";
            switch (userType)
            {
                // TODO : add register verify endpoint in subs app
                case UserType.Substitute:
                    urlBase = _config["SubsClient:BaseUrl"] + _config["SubsClient:EmailVerifyEndpoint"];
                    break;
                case UserType.Teacher:
                case UserType.SchoolAdmin:
                case UserType.DistrictAdmin:
                    urlBase = _config["SchoolsClient:BaseUrl"] + _config["SchoolsClient:EmailVerifyEndpoint"];
                    break;
                // TODO : add register verify endpoint in admin client
                case UserType.SystemAdmin:
                    urlBase = _config["AdminClient:BaseUrl"] + _config["AdminClient:EmailVerifyEndpoint"];
                    break;
                default:
                    break;
            }

            var url = $"{urlBase}{HttpUtility.UrlEncode(token)}";
            var emailTemplate = _work.EmailTemplates.GetByType(EmailTemplateType.TeacherWelcome);
            emailTemplate.Replacements[EmailReplacements.Href] = url;
            emailTemplate.To.Add(user.Email);
            var sendResult = _emailService.SendFromTemplate(emailTemplate);

            //var sendResult = _emailService.SendEmail(user.Email, "support@taggeducation.com", "Tagg Education Invitation", "Please click the following link to get started: <a href=\"" + url + "\">here</a>");
            return sendResult.IsSuccessStatusCode;
        }

        private async Task<string> getInviteToken(MongoUser user, int expireMinutes = 0)
        {
            var userClaims = await _userManager.GetClaimsAsync(user);
            var tokenClaims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            // TODO : What happens if there is no email claim? Exception?
            tokenClaims.Add(userClaims.FirstOrDefault(c => c.Type == TaggClaimTypes.Email));

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Tokens:key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                    issuer: _config["Tokens:Issuer"],
                    audience: _config["Tokens:Audience"],
                    claims: tokenClaims,
                    expires: DateTime.UtcNow.AddMinutes(expireMinutes),
                    signingCredentials: creds
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }
        #endregion
    }

}