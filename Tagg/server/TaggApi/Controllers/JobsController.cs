﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Sentry;
using Tagg.Domain;
using Tagg.Domain.ApiModels;
using Tagg.Services;
using TaggApi.Models;

namespace TaggApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Jobs")]
    [Authorize(Policy = AuthPolicies.TaggUser)]
    public class JobsController : Controller
    {
        private readonly IEmailService _emailService;
        private readonly IConfiguration _config;
        private TaggUnitOfWork _work;
        private LocationService _location;
        private IMapper _mapper;
        private ChatService _chat;
        private ISmsService _sms;
        private JobService _jobService;

        public JobsController(IEmailService emailService,
            IConfiguration config,
            TaggUnitOfWork work,
            LocationService location,
            IMapper mapper,
            ChatService chat,
            ISmsService sms,
            JobService jobService)
        {
            _emailService = emailService;
            _config = config;
            _work = work;
            _location = location;
            _mapper = mapper;
            _chat = chat;
            _sms = sms;
            _jobService = jobService;
        }

        [HttpGet("{id}", Name = "GetJob")]
        public async Task<ActionResult> Get(string id)
        {
            var result = _work.Jobs.GetById(id);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<JobDto>(result));
        }

        [HttpGet("Details/{id}")]
        public IActionResult GetDetails(string id)
        {
            var userId = this.getAuthorizedUserId();
            var userType = UserHelper.GetUserTypeFromClaim(this.User.Claims);

            var job = _work.Jobs.GetById(id);
            if (job != null)
            {

                if (userType == UserType.Substitute && job.Status == JobStatus.Accepted && job.SubstituteId != userId)
                {
                    return StatusCode(409);
                }

                var school = _work.Schools.GetById(job.SchoolId);
                var teacher = _work.Teachers.GetById(job.TeacherId);
                Substitute sub = null;
                if (!string.IsNullOrWhiteSpace(job.SubstituteId))
                {
                    sub = _work.Subs.GetDetails(job.SubstituteId, school.Id);
                }
                return Ok(new JobDetailsDto(school, teacher, job, sub));

            }
            return NotFound();
        }

        [HttpGet("all")]
        public IActionResult GetAll()
        {
            var jobs = _work.Jobs.GetAll();
            return Ok(_mapper.Map<List<JobDto>>(jobs));
        }

        [HttpGet("details/{limit:int?}/{skip:int?}/{sort?}/{direction?}")]
        public IActionResult Details(int limit = 100, int skip = 0, string sort = null, string direction = null)
        {
            var result = _work.Jobs.GetDetailsAll(limit, skip, sort, direction);
            return Ok(result);
        }

        [HttpGet("Subs/{limit:int?}")]
        [Authorize(Policy = AuthPolicies.Substitute)]
        public IActionResult GetSubJobsAll(int limit = 100)
        {

            var subId = getAuthorizedUserId();
            var allSubJobs = _work.Jobs.GetBySubId(subId, DateTime.UtcNow.AddYears(-2), DateTime.UtcNow.AddYears(2), limit);
            var result = new AllJobsDto();

            result.Recent = allSubJobs
                .Where(j => j.Status == JobStatus.Complete)
                .OrderBy(j => j.StartDate)
                .ToList();

            result.Accepted = allSubJobs
                .Where(j => j.Status == JobStatus.Accepted)
                .OrderBy(j => j.StartDate)
                .ToList();

            result.Offered = _work.Jobs.GetOffersBySubId(subId, DateTime.UtcNow)
                .AsQueryable()
                .OrderBy(j => j.StartDate)
                .ToList();

            return Ok(result);
        }

        [HttpGet("Subs/Summary/{limit:int?}")]
        [Authorize(Policy = AuthPolicies.Substitute)]
        public IActionResult GetSubJobsSummary(int limit = 9)
        {
            var subId = getAuthorizedUserId();
            var allSubJobs = _work.Jobs.GetBySubId(subId, DateTime.UtcNow.AddMonths(-1), DateTime.UtcNow.AddMonths(2), limit);
            var result = new AllJobsDto();

            result.Accepted = allSubJobs
                .Where(j => j.Status == JobStatus.Accepted)
                .Where(j => j.StartDate > DateTime.UtcNow.Date && j.StartDate <= DateTime.UtcNow.AddDays(7).Date)
                .OrderBy(j => j.StartDate)
                .Take(5)
                .ToList();

            if (result.Accepted.Count < 5)
            {
                result.Offered = _work.Jobs.GetOffersBySubId(subId, DateTime.UtcNow)
                    .AsQueryable()
                    .Where(j => j.StartDate > DateTime.UtcNow.Date && j.StartDate <= DateTime.UtcNow.AddDays(7).Date)
                    .OrderBy(j => j.StartDate)
                    .Take(5 - result.Accepted.Count)
                    .ToList();
            }

            result.Recent = allSubJobs
                    .Where(j => j.Status == JobStatus.Complete)
                    .OrderBy(j => j.StartDate)
                    .Take(3)
                    .ToList();

            return Ok(result);
        }

        [HttpGet("School/{schoolId}/{date}/{limit:int?}")]
        [Authorize(Policy = AuthPolicies.AnyAdmin)]
        public IActionResult GetSchoolJobs(string schoolId, string date, int limit = 100)
        {
            if (!string.IsNullOrWhiteSpace(schoolId))
            {
                DateTime currentDate;
                if (DateTime.TryParse(date, out currentDate))
                {
                    List<JobDetailsDto> allSchoolJobs = _work.Jobs.GetJobDetails(schoolId, limit);
                    SchoolJobsDto result = new SchoolJobsDto(allSchoolJobs, currentDate);

                    return Ok(result);
                }
            }
            return BadRequest();
        }

        [HttpGet("Teacher/{teacherId}/{limit:int?}")]
        public async Task<IActionResult> GetTeacherJobs(string teacherId, int limit = 100)
        {
            if (!string.IsNullOrWhiteSpace(teacherId))
            {
                var result = new List<JobDetailsDto>();
                var jobs = _work.Jobs
                    .Query(j => j.TeacherId == teacherId && j.SubNeeded)
                    .OrderBy(j => j.StartDate)
                    .Take(limit)
                    .ToList();

                foreach (var j in jobs)
                {
                    var sub = _work.Subs.GetById(j.SubstituteId);
                    result.Add(new JobDetailsDto(j, sub?.FullName));
                }

                return Ok(result);

            }
            return BadRequest();
        }

        [HttpGet("Admin/{month:int}/{year:int}")]
        public async Task<IActionResult> GetAdminJobs(int month, int year)
        {
            // Return jobs for all schools that admin user belongs to FOR THE CURRENT SCHOOL YEAR
            List<JobDetailsDto> adminJobs = new List<JobDetailsDto>();
            var adminId = this.getAuthorizedUserId();
            var schools = _work.Schools.GetByAdminId(adminId);
            if (schools != null && schools.Count > 0)
            {
                DateTime minDate = DateTime.UtcNow.AddMonths(-2);
                DateTime maxDate = DateTime.UtcNow.AddMonths(2);
                var now = DateTime.UtcNow;

                // If current month is after july or later
                // Return jobs for July of this year until july of next year
                if (now.Month > 6)
                {
                    minDate = new DateTime(now.Year, 7, 1);     // July 1 of current year
                    maxDate = new DateTime(now.Year + 1, 7, 1); // July 1 of next year
                }
                // else
                // Return jobs for July of last yer until july of this year
                else
                {
                    minDate = new DateTime(now.Year - 1, 7, 1); //July 1 of last year
                    maxDate = new DateTime(now.Year, 7, 1);     //July 1 of this year
                }
                foreach (var school in schools)
                {
                    var schoolJobs = _work.Jobs.GetBySchoolId(school.School.Id, minDate, maxDate);
                    if (schoolJobs.Count > 0)
                    {
                        adminJobs.AddRange(schoolJobs);
                    }
                }
                SchoolJobsDto result = new SchoolJobsDto(adminJobs, DateTime.UtcNow);
                return Ok(result);
            }
            return NoContent();
        }

        [HttpGet("Teacher/unrated/{teacherId}")]
        public IActionResult GetTeacherUnrated(string teacherId)
        {
            var unrated = _work.JobRatings.GetIncompleteByTeacherId(teacherId).Select(r => r.JobId);
            return Ok(unrated.ToList());
        }

        [HttpGet("sub/unrated/{subid}")]
        public IActionResult GetSubUnrated(string subId)
        {
            var unrated = _work.JobRatings.GetIncompleteBySubId(subId).Select(r => r.JobId);
            return Ok(unrated.ToList());
        }


        [HttpGet("accept/{jobId}/{subId}")]
        public async Task<IActionResult> Accept(string jobId, string subId)
        {
            try
            {
                var jobAcceptResult = _jobService.Accept(jobId, subId);
                if (jobAcceptResult.Succeeded)
                {
                    return Ok();
                }
                return StatusCode(409, "Job has been closed or no longer exists.");
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return BadRequest();
            }

        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] JobCreateDto model)
        {
            if (ModelState.IsValid)
            {
                var schoolTeacher = _work.Teachers.GetWithSchool(model.TeacherId);
                if (schoolTeacher.School == null || schoolTeacher.Teacher == null)
                {
                    return NotFound(0);
                }


                var job = populateJob(model, schoolTeacher.School, schoolTeacher.Teacher);
                var jobResult = _work.Jobs.Add(job);
                if (jobResult.Succeeded)
                {
                    if (!job.SubNeeded)
                    {
                        return NoContent();
                    }
                    else
                    {
                        var search = new SubSearchDto
                        {
                            Date = model.StartDate,
                            SchoolId = model.SchoolId,
                            TeacherId = model.TeacherId
                        };
                        var subsForJob = executeSubSearch(search, schoolTeacher.School, schoolTeacher.Teacher);
                        if (subsForJob.Total == 0)
                        {
                            _work.Jobs.UpdateStatus(jobResult.Entity.Id, JobStatus.Cancelled, null);
                            return StatusCode(409, "No subs available.");
                        }
                        var offerDetails = new JobOfferDetailsDto
                        {
                            Job = model,
                            Favorites = subsForJob.Favorites.Select(s => s.Id).ToList(),
                            Others = subsForJob.Others.Select(s => s.Id).ToList(),
                        };
                        try
                        {
                            var queueResult = _work.Offers.CreateOfferQueue(jobResult.Entity, offerDetails);
                            if (queueResult.Succeeded)
                            {
                                return NoContent();
                            }
                        }
                        catch (Exception ex)
                        {
                            SentrySdk.CaptureException(ex);
                        }

                        return StatusCode(500, "Failed to request sub.");
                    }
                }
            }
            return BadRequest(ModelState);
        }

        [HttpPost("offer")]
        public IActionResult CreateOffers([FromBody] JobOfferDetailsDto model)
        {
            if (ModelState.IsValid)
            {
                var schoolTeacher = _work.Teachers.GetWithSchool(model.Job.TeacherId);
                if (schoolTeacher.School == null || schoolTeacher.Teacher == null)
                {
                    return NotFound();
                }

                var job = populateJob(model.Job, schoolTeacher.School, schoolTeacher.Teacher);
                var createResult = _work.Jobs.Add(job);
                if (createResult.Succeeded)
                {
                    var queueResult = _work.Offers.CreateOfferQueue(createResult.Entity, model);
                    if (queueResult.Succeeded)
                    {
                        return NoContent();
                    }
                }
            }
            return BadRequest(ModelState);
        }

        // Cancel by teacher/school
        [HttpPut("cancel/{jobId}")]
        public async Task<IActionResult> Cancel(string jobId)
        {
            if (!string.IsNullOrWhiteSpace(jobId))
            {
                var cancelResult = _work.Jobs.Cancel(jobId);
                if (cancelResult.Succeeded)
                {
                    var queueDisableResult = _work.Offers.DisableQueue(jobId);
                    if (!string.IsNullOrWhiteSpace(cancelResult.Entity.SubstituteId))
                    {
                        try { _chat.SendMessage("This job has been cancelled.", $"{cancelResult.Entity.Id}-{cancelResult.Entity.SubstituteId}"); }
                        catch { }
                        var notificationResult = await this.sendSchoolCancelledNotification(jobId);
                    }
                    return NoContent();
                }
            }
            return BadRequest();
        }

        // Cancel by sub
        [HttpPut("cancel/sub/{jobId}")]
        public async Task<IActionResult> CancelSub(string jobId)
        {
            var job = _work.Jobs.GetById(jobId);
            var substituteId = this.getAuthorizedUserId();

            // check to make sure job exists and the authorized user is the sub on the job
            if (job != null && job.SubstituteId == substituteId)
            {
                var restartOfferResult = _work.Offers.ReopenQueue(job);
                if (restartOfferResult.Succeeded)
                {
                    try { _chat.SendMessage("This job has been cancelled.", $"{job.Id}-{substituteId}"); }
                    catch { }
                    try { var notificationResult = await this.sendSubCancelledNotification(jobId); }
                    catch { }
                    
                    return NoContent();
                }
            }
            SentrySdk.AddBreadcrumb($"SubOnJob: {job.SubstituteId}, SubCancelling: {substituteId}");
            SentrySdk.CaptureMessage($"Sub cancelling not sub on job");
            return NotFound();
        }

        [HttpPost("lessonplan/{jobId}")]
        public IActionResult SaveLessonPlan(string jobId, IFormFile file)
        {
            var job = _work.Jobs.GetById(jobId);
            if (job != null)
            {
                if (file != null && this.isValidLessonPlanFile(file))
                {
                    var lessonPlanFileId = _work.Jobs.UpdateLessonPlan(jobId, file.OpenReadStream(), file.FileName, file.ContentType);
                    return Ok(lessonPlanFileId);
                }
            }
            return NotFound();
        }

        [HttpGet("lessonplan/{id}")]
        [AllowAnonymous]
        public IActionResult GetLessonPlan(string id)
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                var result = _work.Jobs.GetLessonPlan(id);
                if (result.Succeeded)
                {
                    return File(result.Stream, result.ContentType);
                }
                return NotFound();
            }
            return BadRequest();
        }

        [HttpPost("rate/sub")]
        public IActionResult AddSubRating([FromBody] JobRatingDto dto)
        {
            var job = _work.Jobs.GetById(dto.JobId);
            if (job != null)
            {
                var ratingEntity = _mapper.Map<JobRating>(dto);
                dto.Ratings.ForEach(r => ratingEntity.SubScores.Add(r.Category, r.Rating));
                var addResult = _work.JobRatings.TeacherSubmission(ratingEntity);
                var updateResult = _work.Jobs.AddRating(job.Id, dto.Ratings.Average(x => x.Rating));

                if (updateResult.Succeeded && addResult.Succeeded)
                {
                    return Ok(true);
                }
                return BadRequest();
            }
            return NotFound();
        }

        [HttpPost("rate/school")]
        public IActionResult AddSchoolRating([FromBody] JobRatingDto dto)
        {
            var job = _work.Jobs.GetById(dto.JobId);
            if (job != null)
            {
                var ratingEntity = _mapper.Map<JobRating>(dto);
                dto.Ratings.ForEach(r => ratingEntity.SchoolScores.Add(r.Category, r.Rating));
                var addResult = _work.JobRatings.SubSubmission(ratingEntity);

                if (addResult.Succeeded)
                {
                    return Ok(true);
                }
                return BadRequest();
            }
            return NotFound();
        }

        [HttpGet("decline/{jobId}/{subId}")]
        public IActionResult Decline(string jobId, string subId)
        {
            var result = _work.Jobs.Decline(jobId, subId);
            var removeChat = _chat.DeleteChannel($"{jobId}-{subId}");
            if (result.Succeeded)
            {
                return NoContent();
            }

            return NotFound();
        }

        #region helpers
        private Job populateJob(JobCreateDto dto, School school, Teacher teacher)
        {
            var result = _mapper.Map<Job>(dto);
            result.Location = school.Location;
            result.SchoolId = school.Id;
            result.TeacherId = teacher.Id;
            result.Subject = teacher.Subjects.FirstOrDefault();

            var jobTimeSpan = dto.EndDate - dto.StartDate;

            var jobPayRate = teacher.PayRate > 0 ? teacher.PayRate : school.PayRate;
            if (dto.SubNeeded)
            {
                result.PayAmount = jobTimeSpan.TotalHours > 4 ? jobPayRate : (jobPayRate / 2);
                result.ChargeAmount = result.PayAmount > 0 ? TransactionHelper.CalculateTotalAmountUsd(result.PayAmount) : 0;
            }
            else
            {
                result.PayAmount = 0;
                result.ChargeAmount = 0;
            }


            if (dto.StartDate.TimeOfDay == TimeSpan.Zero)
            {
                result.StartDate = dto.StartDate + school.StartTime;
            }
            if (dto.EndDate.TimeOfDay == TimeSpan.Zero)
            {
                result.EndDate = dto.EndDate + school.EndTime;
            }

            result.CreatedBy = this.getAuthorizedUserId();
            //result.EndDate = result.EndDate.ToLocalTime();
            //result.StartDate = result.StartDate.ToLocalTime();
            return result;
        }

        private bool isValidLessonPlanFile(IFormFile file)
        {
            bool isValidMimeType = file.ContentType == MimeTypes.MSDoc
                || file.ContentType == MimeTypes.MSDocX
                || file.ContentType == MimeTypes.PDF;
            bool isValidFileSize = file.Length > 0 && file.Length < 50000000;

            return isValidMimeType && isValidFileSize;
        }

        private string getAuthorizedUserId()
        {
            try
            {
                return this.User.Claims.FirstOrDefault(c => c.Type == "sub").Value;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return "";
            }
        }

        private SubSearchResultDto executeSubSearch(SubSearchDto search, School school, Teacher teacher)
        {
            var result = new SubSearchResultDto();

            if (school != null && teacher != null)
            {
                var criteria = new SubSearchCriteria
                {
                    DateAvaliable = search.Date,
                    SchoolId = search.SchoolId,
                    Location = school.Location,
                    ActiveOnly = true,
                    EnabledOnly = true
                };
                var availableSubs = _work.Subs.Search(criteria);
                result.Total = availableSubs.Count;
                var favorites = availableSubs.Where(s => teacher.Favorites.Contains(s.Id)).ToList();
                var others = availableSubs.Where(s => !teacher.Favorites.Contains(s.Id)).ToList();

                result.Favorites = _mapper.Map<List<SubDisplayDto>>(favorites);
                result.Others = _mapper.Map<List<SubDisplayDto>>(others);
                result.Favorites.ForEach(s => s.IsFavorite = true);
            }
            return result;

        }

        /// <summary>
        /// For sending job cancelled notification to teacher if sub cancels a job
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>
        private async Task<bool> sendSubCancelledNotification(string jobId)
        {
            bool result = true;
            try
            {
                var job = _work.Jobs.GetDetailsSingle(jobId);
                var teacher = _work.Teachers.GetById(job.TeacherId);
                var user = _work.Users.GetById(job.TeacherId);
                var admins = _work.Schools.GetSchoolAdmins(job.SchoolId);

                if (teacher != null && teacher.ReceiveEmail)
                {
                    var emailTemplate = _work.EmailTemplates.GetByType(EmailTemplateType.JobCancelledBySub);
                    emailTemplate.To.Add(teacher.Email);
                    foreach (var admin in admins)
                    {
                        if (!string.IsNullOrWhiteSpace(admin.Email))
                        {
                            emailTemplate.To.Add(admin.Email);
                        }
                    }
                    emailTemplate.Replacements[EmailReplacements.Date] = ConvertToMountainTime(job.StartDate).ToLongDateString();
                    emailTemplate.Replacements[EmailReplacements.School] = job.SchoolName;
                    emailTemplate.Replacements[EmailReplacements.Teacher] = job.TeacherName;
                    emailTemplate.Replacements[EmailReplacements.Grade] = SubjectLookup.Get(job.Subject);
                    emailTemplate.Replacements[EmailReplacements.Time] = $"{JobsController.ConvertToMountainTime(job.StartDate).ToShortTimeString()}-{JobsController.ConvertToMountainTime(job.EndDate).ToShortTimeString()}";
                    emailTemplate.Replacements[EmailReplacements.AddressCityState] = $"{job.SchoolAddress.City}, {job.SchoolAddress.State} {job.SchoolAddress.PostalCode}";
                    emailTemplate.Replacements[EmailReplacements.AddressStreet] = $"{job.SchoolAddress.Street1} {job.SchoolAddress.Street2}";
                    emailTemplate.Replacements[EmailReplacements.Sub] = job.SubstituteName;

                    var sendResult = _emailService.SendFromTemplate(emailTemplate);
                    if (!sendResult.IsSuccessStatusCode)
                    {
                        result = false;
                    }
                }

                if (teacher != null && teacher.ReceiveSMS && user.PhoneNumberConfirmed)
                {
                    await _sms.SendSmsAsync(user.PhoneNumber, $"Your job on {ConvertToMountainTime(job.StartDate).ToLongDateString()} was cancelled by {job.SubstituteName}. We have restarted the job search. Login to https://schools.taggeducation.com to view details.");
                }

                return result;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return false;
            }
        }

        /// <summary>
        /// For sending job cancelled notification to sub if school cancels a job
        /// </summary>
        /// <param name="jobId"></param>
        /// <returns></returns>
        private async Task<bool> sendSchoolCancelledNotification(string jobId)
        {
            bool result = true;
            try
            {
                var job = _work.Jobs.GetDetailsSingle(jobId);
                var sub = _work.Subs.GetById(job.SubstituteId);
                var user = _work.Users.GetById(job.SubstituteId);

                if (sub != null && sub.ReceiveEmail)
                {
                    var emailTemplate = _work.EmailTemplates.GetByType(EmailTemplateType.JobCancelledByTeacher);
                    emailTemplate.To.Add(sub.Email);
                    emailTemplate.Replacements[EmailReplacements.Date] = ConvertToMountainTime(job.StartDate).ToLongDateString();
                    emailTemplate.Replacements[EmailReplacements.School] = job.SchoolName;
                    emailTemplate.Replacements[EmailReplacements.Teacher] = job.TeacherName;
                    emailTemplate.Replacements[EmailReplacements.Grade] = SubjectLookup.Get(job.Subject);
                    emailTemplate.Replacements[EmailReplacements.Time] = $"{JobsController.ConvertToMountainTime(job.StartDate).ToShortTimeString()}-{JobsController.ConvertToMountainTime(job.EndDate).ToShortTimeString()}";
                    emailTemplate.Replacements[EmailReplacements.AddressCityState] = $"{job.SchoolAddress.City}, {job.SchoolAddress.State} {job.SchoolAddress.PostalCode}";
                    emailTemplate.Replacements[EmailReplacements.AddressStreet] = $"{job.SchoolAddress.Street1} {job.SchoolAddress.Street2}";
                    emailTemplate.Replacements[EmailReplacements.Pay] = $"${job.PayAmount}";
                    emailTemplate.Replacements[EmailReplacements.Href] = $"{_config["SubsClient:BaseUrl"]}";


                    var sendResult = _emailService.SendFromTemplate(emailTemplate);
                    if (!sendResult.IsSuccessStatusCode)
                    {
                        result = false;
                    }
                }

                if (sub != null && sub.ReceiveSMS && user.PhoneNumberConfirmed)
                {
                    await _sms.SendSmsAsync(user.PhoneNumber, $"Your job on {ConvertToMountainTime(job.StartDate).ToLongDateString()} was cancelled. Login to https://subs.taggeducation.com to view details.");
                }

                return result;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return false;
            }
        }

        private static DateTime ConvertToMountainTime(DateTime utc)
        {
            var mountainTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time");
            return TimeZoneInfo.ConvertTimeFromUtc(utc, mountainTimeZone);
        }
        #endregion
    }
}