﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tagg.Services;

namespace TaggApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Location")]
    public class LocationController : Controller
    {
        private LocationService _locationService;

        public LocationController(LocationService locationService)
        {
            _locationService = locationService;
        }

        [HttpGet("Check/{id}")]
        public IActionResult Check(string id)
        {
            if (string.IsNullOrWhiteSpace(id))
            {
                return BadRequest("No location provided");
            }
            List<DistanceResult> result = _locationService.GetDistanceFromServiceAreas(id);
            return Json(result);
        }
    }
}