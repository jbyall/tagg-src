﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using MongoDB.Driver;
using Sentry;
using Tagg.Domain;
using Tagg.Domain.ApiModels;
using Tagg.Services;
using Twilio;
using Twilio.AspNet.Core;
using Twilio.Rest.Api.V2010.Account;
using Twilio.TwiML;
using Twilio.TwiML.Voice;
using Twilio.Types;
using static Twilio.TwiML.Voice.Say;

namespace TaggApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Phone")]
    public class PhoneController : TwilioController
    {
        private IConfiguration _config;
        private TaggUnitOfWork _work;
        private PhoneService _phone;
        private string _phoneWebhook;
        private JobService _jobService;

        public PhoneController(
            IConfiguration config, 
            TaggUnitOfWork work, 
            IMapper mapper, 
            IEmailService emailService, 
            UserManager<MongoUser> userManager, 
            PhoneService phone,
            JobService jobService)
        {
            _config = config;
            _work = work;
            _phone = phone;
            _phoneWebhook = config["Twilio:PhoneWebhook"];
            _jobService = jobService;
        }

        [HttpPost("job/{jobId?}/{subId?}/{digits?}")]
        public IActionResult Index(string jobId = "", string subId = "", string digits = "")
        {
            var response = new VoiceResponse();
            try
            {
                var job = _work.Jobs.GetDetailsSingle(jobId);

                if (job == null)
                {
                    renderJobNotFound(response);
                }
                else if (!string.IsNullOrWhiteSpace(digits))
                {
                    switch (digits)
                    {
                        case "1":
                            var acceptResult = _jobService.Accept(job.Id, subId);
                            if (acceptResult.Succeeded)
                            {
                                response.Say(_config["PhoneMessages:JobAccepted"].Replace("_TEACHER_",job.TeacherName), VoiceEnum.Man);
                            }
                            else
                            {
                                response.Say(_config["PhoneMessages:JobTaken"]);
                            }

                            break;
                        case "2":
                            var declineResult = _jobService.Decline(job.Id, subId);
                            response.Say(_config["PhoneMessages:JobDecline"], VoiceEnum.Man);
                            break;
                        default:
                            response.Say(_config["PhoneMessages:Invalid"]).Pause(1);
                            renderMainMenu(response, job, subId);
                            break;
                    }
                }
                else
                {
                    this.renderMainMenu(response, job, subId);
                }

                return TwiML(response);
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                response = new VoiceResponse();
                this.renderJobNotFound(response);
                return TwiML(response);
            }

        }

        private VoiceResponse renderMainMenu(VoiceResponse response, JobDetailsDto job, string subId)
        {
            var message = _config["PhoneMessages:JobOffer"]
                           .Replace("_TEACHER_", job.TeacherName)
                           .Replace("_SCHOOL_", job.SchoolName)
                           .Replace("_GRADE_", SubjectLookup.Get(job.Subject))
                           .Replace("_DATE_", job.StartDate.ToShortDateString())
                           .Replace("_PAY_", $"${job.PayAmount}")
                           .Replace("-", " to ");

            response.Gather(numDigits: 1)
                    .Say(message, VoiceEnum.Man)
                    .Redirect(new Uri(_phoneWebhook + job.Id + "/" + subId));
            return response;
        }

        private VoiceResponse renderJobNotFound(VoiceResponse response)
        {
            var message = _config["PhoneMessages:Exception"];

            response.Say(message, VoiceEnum.Man).Pause(1);
            return response;
        }

        //[HttpGet("test")]
        //public IActionResult Test()
        //{
        //    //return Ok("this worked.");
        //    _phone.InitiateJobCall("7202611925");
        //    return NoContent();
        //}
    }
}