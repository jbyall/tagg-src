﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Sentry;
using Tagg.Domain;
using Tagg.Services;
using Twilio.AspNet.Common;
using Twilio.AspNet.Core;
using Twilio.TwiML;

namespace TaggApi.Controllers
{
    public class SmsController : TwilioController
    {
        private TaggUnitOfWork _work;
        private JobService _jobs;
        private IConfiguration _config;

        public SmsController(
            TaggUnitOfWork work,
            JobService jobs,
            IConfiguration config
            ) : base()
        {
            _work = work;
            _jobs = jobs;
            _config = config;
        }

        [HttpPost]
        public TwiMLResult Index(SmsRequest request)
        {
            var response = new MessagingResponse();
            try
            {
                if (!string.IsNullOrWhiteSpace(request.Body))
                {
                    var job = _work.Jobs
                        .Query(j => j.Offers.Any(o => o.SMSId == request.Body) && j.StartDate > DateTime.UtcNow)
                            .FirstOrDefault();

                    if (job != null && job.Status == JobStatus.Open)
                    {
                        var offer = job.Offers.FirstOrDefault(o => o.SMSId == request.Body);
                        var acceptResult = _jobs.Accept(job.Id, offer.SubId);
                        string responseMessage = acceptResult.Succeeded ? _config["SmsMessages:JobAccepted"] : _config["SmsMessages:JobTaken"];
                        response.Message(responseMessage);
                    }
                    else
                    {
                        response.Message(_config["SmsMessages:JobTaken"]);
                    }

                }
                else
                {
                    response.Message(_config["SmsMessages:JobTaken"]);
                }
                
                return TwiML(response);
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                response.Message(_config["SmsMessages:JobTaken"]);
                return TwiML(response);
            }
        }
    }
}