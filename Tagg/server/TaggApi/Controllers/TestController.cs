﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Tagg.Domain;
using Tagg.Services;
using Twilio.AspNet.Core;

namespace TaggApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Test")]
    //[Authorize(Policy = AuthPolicies.SystemAdmin)]
    public class TestController : TwilioController
    {
        private TaggUnitOfWork _work;
        private IMapper _mapper;
        private IEmailService _emailService;
        private IConfiguration _config;
        private UserManager<MongoUser> _userManager;
        private PhoneService _phone;
        private SmsService _sms;

        public TestController(IConfiguration config, TaggUnitOfWork work, IMapper mapper, IEmailService emailService, UserManager<MongoUser> userManager, PhoneService phone, ISmsService smsService, ChatService chat)
        {
            _work = work;
            _mapper = mapper;
            _emailService = emailService;
            _config = config;
            _userManager = userManager;
            _phone = phone;
            var email = new EmailService(config, work);
            _sms = new SmsService(config);
        }
        [HttpGet("time")]
        public IActionResult GetSystemTime()
        {
            var result = ConvertToMountainTime(DateTime.UtcNow);
            return Ok(result);
        }

        [HttpGet("ma/report")]
        public IActionResult MonumentJobsReport()
        {
            string env = _config["Server:Env"];
            if (env == "Development")
            {
                var jobs = _work.Jobs.GetBySchoolId("5b68902dd761b21ba8c2ecf2", 1000).OrderBy(x => x.StartDate);
                using (StreamWriter sw = System.IO.File.CreateText($"C:\\Users\\jb\\Desktop\\compare\\report-{DateTime.Now.Month}-{DateTime.Now.Day}-{DateTime.Now.Year}.csv"))
                {
                    foreach (var item in jobs)
                    {
                        item.StartDate = item.StartDate.ToLocalTime();
                        item.EndDate = item.EndDate.ToLocalTime();
                    }
                    var csvWriter = new CsvHelper.CsvWriter(sw);
                    csvWriter.WriteRecords(jobs);
                }
                return Ok();
            }
            return NotFound();


        }

        [HttpGet("sentry")]
        public IActionResult SentryTest()
        {
            string env = _config["Server:Env"];
            if (env == "Development")
            {
                throw new NotImplementedException();
            }
            return NotFound();
        }


        private static DateTime ConvertToMountainTime(DateTime utc)
        {
            var mountainTimeZone = TimeZoneInfo.FindSystemTimeZoneById("Mountain Standard Time");
            return TimeZoneInfo.ConvertTimeFromUtc(utc, mountainTimeZone);
        }

        //[HttpPost("seed")]
        //public async Task<IActionResult> Seed([FromBody]SeedModel model)
        //{
        //    string env = _config["Server:Env"];
        //    if (env == "Development")
        //    {
        //        var seed = new TaggSeed(_config, _userManager);
        //        bool refresh = false || model.Refresh;
        //        int schoolCount = model.SchoolCount > 0 ? model.SchoolCount : 1;
        //        int teacherCount = model.TeacherCount > 0 ? model.TeacherCount : 1;

        //        var result = await seed.Seed("john.bond@vitalize.team", refresh, schoolCount, teacherCount);
        //        return Ok();
        //    }
        //    return NotFound();

        //}

        [HttpGet("seed")]
        public async Task<IActionResult> Seed()
        {
            string env = _config["Server:Env"];
            if (env == "Development")
            {
                var seed = new TaggSeed(_config, _userManager);
                var result = await seed.Seed("john.bond@vitalize.team", true);
                return Ok();
            }
            return BadRequest();
        }

        ////[HttpGet("seed/jobs")]
        ////public async Task<IActionResult> SeedFakeJobs()
        ////{
        ////    string env = _config["Server:Env"];
        ////    if (env == "Development")
        ////    {
        ////        var templateJob = _work.Jobs.GetQueryable().FirstOrDefault();
        ////        var startDate = new DateTime(2018, 1, 1, 7, 30, 0);
        ////        var endDate = new DateTime(2018, 1, 1, 15, 30, 0);

        ////        for (int i = 0; i < 110; i++)
        ////        {
        ////            var fakeJob = templateJob;
        ////            fakeJob.Id = null;
        ////            fakeJob.StartDate = startDate;
        ////            fakeJob.EndDate = endDate;
        ////            _work.Jobs.Add(fakeJob);
        ////            startDate = startDate.AddDays(1);
        ////            endDate = endDate.AddDays(1);
        ////        }
        ////        return Ok();
        ////    }
        ////    return BadRequest();

        ////}

        //[HttpGet("refresh/emails")]
        //public IActionResult RefreshEmails()
        //{
        //    string env = _config["Server:Env"];
        //    if (env == "Development")
        //    {
        //        var seed = new TaggSeed(_config, _userManager);
        //        seed.RefreshEmails();
        //        return Ok();
        //    }
        //    return BadRequest();
        //}

        //[HttpGet("Email/{templateType:int?}")]
        //public IActionResult Email(int templateType)
        //{
        //    string env = _config["Server:Env"];
        //    if (env == "Development")
        //    {
        //        EmailTemplateType emailType = (EmailTemplateType)templateType;
        //        var template = _work.EmailTemplates.GetByType(emailType);
        //        switch (emailType)
        //        {
        //            case EmailTemplateType.EmailConfirmation:
        //                template.Replacements[EmailReplacements.Href] = "www.vitalize.team";
        //                break;
        //            case EmailTemplateType.TeacherWelcome:
        //                template.Replacements[EmailReplacements.Href] = "www.vitalize.team";
        //                break;
        //            case EmailTemplateType.SubWelcome:
        //                template.Replacements[EmailReplacements.Href] = "www.vitalize.team";
        //                break;
        //            case EmailTemplateType.SchoolSetup:
        //                template.Replacements[EmailReplacements.Href] = "www.vitalize.team";
        //                break;
        //            case EmailTemplateType.JobRequest:
        //                template.Replacements[EmailReplacements.Href] = "www.vitalize.team";
        //                template.Replacements[EmailReplacements.Date] = DateTime.Now.ToShortDateString();
        //                template.Replacements[EmailReplacements.AddressCityState] = "Littleton, CO 80123";
        //                template.Replacements[EmailReplacements.AddressStreet] = "4801 S Wadsworth Blvd";
        //                template.Replacements[EmailReplacements.Grade] = "1st Grade";
        //                template.Replacements[EmailReplacements.Pay] = "$100";
        //                template.Replacements[EmailReplacements.School] = "John School";
        //                template.Replacements[EmailReplacements.Teacher] = "John Bond";
        //                template.Replacements[EmailReplacements.Time] = "7:30 AM - 2:15 PM";
        //                break;
        //            case EmailTemplateType.SchoolInquiry:
        //            default:
        //                break;
        //        }
        //        template.To.Add("john.bond@vitalize.team");

        //        var sendResult = _emailService.SendFromTemplate(template);
        //        return Ok(sendResult);
        //    }
        //    return BadRequest();
        //}

        ////[HttpGet("Cancel/{batchId}")]
        ////public IActionResult Cancel(string batchId)
        ////{
        ////    _emailService.CancelEmail(batchId);
        ////    return Ok();
        ////}

        //[HttpGet("Index")]
        //public IActionResult Index()
        //{
        //    var result = new DiagnosticResult("Index test");
        //    sw.Start();
        //    var states = _work.Lookup.GetStates();
        //    sw.Stop();
        //    sw.Reset();
        //    result.Times.Add("states", sw.ElapsedMilliseconds);
        //    result.Message = $"states count: {states.Count}";

        //    return Ok(result);
        //    //sw.Start();
        //    //var client = new MongoClient(connectionString);
        //    //sw.Stop();
        //    //sw.Reset();
        //    //result.Times.Add("client", sw.ElapsedMilliseconds);

        //    //sw.Start();
        //    //var db = client.GetDatabase(dbName);
        //    //sw.Stop();
        //    //sw.Reset();
        //    //result.Times.Add("db", sw.ElapsedMilliseconds);

        //    //sw.Start();
        //    //var lookups = new LookupRepo(connectionString);
        //    //sw.Stop();
        //    //sw.Reset();
        //    //result.Times.Add("repo", sw.ElapsedMilliseconds);

        //    //sw.Start();
        //    //var states = lookups.GetStates();
        //    //sw.Stop();
        //    //sw.Reset();
        //    //result.Times.Add("states", sw.ElapsedMilliseconds);
        //    //result.Message = $"states count: {states.Count}";


        //}

        //[HttpGet("sms/{jobId}")]
        //public async Task<IActionResult> Sms(string jobId)
        //{
        //    string env = _config["Server:Env"];
        //    if (env == "Development")
        //    {
        //        try
        //        {
        //            var job = _work.Jobs.GetDetailsSingle(jobId);
        //            await _sms.SendSmsAsync("7204993365", $"Your job on {job.StartDate.ToLongDateString()} was cancelled by {job.SubstituteName}. Login to https://schools.taggeducation.com to view details.");
        //            //AppMessage appMessage = new AppMessage
        //            //{
        //            //    From = "Tagg",
        //            //    Subject = "New Job Offer!",
        //            //    Body = $"You just received a new job offer to sub for. Login to https://subs.taggeducation.com to view.",
        //            //};

        //            //await _messages.SendSms(appMessage, "7202611925");
        //            //return Ok();
        //        }
        //        catch (Exception ex)
        //        {
        //            return BadRequest();
        //        }
        //    }
        //    return BadRequest();

        //}

        ////[HttpPost("Voice")]
        ////public IActionResult Voice()
        ////{
        ////    var replaced = file.Replace("_TEACHER_", "John Bond");
        ////    replaced = replaced.Replace("_SCHOOL_", "John School");
        ////    replaced = replaced.Replace("_GRADE_", "6TH grade english");
        ////    replaced = replaced.Replace("_DATE_", DateTime.Now.ToShortDateString());
        ////    replaced = replaced.Replace("_PAY_", "$100");

        ////    return TwiML(response);
        ////}

        //[HttpGet("voice")]
        //public IActionResult Voice()
        //{
        //    string env = _config["Server:Env"];
        //    if (env == "Development")
        //    {
        //        _phone.InitiateJobCall("7202611925", "5b497440c6541a0950b5b7c3", "5b496b8fc6541a0950b5b6f0");
        //        return NoContent();
        //    }

        //    return BadRequest();

        //}

        //[HttpGet("seed/templates")]
        //public IActionResult SeedEmailTemplates()
        //{
        //    try
        //    {
        //        var seed = new TaggSeed(_config, _userManager);
        //        seed.RefreshEmailTemplates();
        //        return NoContent();
        //    }
        //    catch (Exception ex)
        //    {
        //        return StatusCode(500, ex.Message);
        //    }
        //}



        //[HttpGet("config")]
        //public IActionResult Config()
        //{
        //    var result = new
        //    {
        //        Server = _config["Server:Env"],
        //        Subs = _config["SubsClient:BaseUrl"],
        //        Schools = _config["SchoolsClient:BaseUrl"],
        //        Admin = _config["AdminClient:BaseUrl"],
        //        IDP = $"Authority - {_config["TaggIdentity:Authority"]} | HostName - {_config["TaggIdentity:HostName"]}",
        //    };
        //    return Ok(result);
        //}
    }


}