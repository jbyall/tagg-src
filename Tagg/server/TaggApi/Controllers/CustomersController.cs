﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Tagg.Domain;
using Tagg.Domain.ApiModels;
using Tagg.Services;

namespace TaggApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Customers")]
    public class CustomersController : Controller
    {
        private IPaymentService _payments;
        private TaggUnitOfWork _work;
        private IConfiguration _config;
        private IMapper _mapper;

        public CustomersController(TaggUnitOfWork work, IConfiguration config, IMapper mapper)
        {
            _payments = new PaymentService(config);
            _work = work;
            _config = config;
            _mapper = mapper;
        }

        [HttpPost]
        public IActionResult Create([FromBody] CustomerCreateDto model)
        {
            var result = _payments.Customers.Create(model);
            if (result != null)
            {
                var school = _work.Schools.GetById(model.SchoolId);
                if (school != null)
                {
                    var updateResult = _work.Schools.AddCustomerId(school.Id, result.Id);
                    return NoContent();
                }
            }
            return BadRequest();
        }

        [HttpPost("Verify")]
        [Authorize(Policy = AuthPolicies.AnyAdmin)]
        public IActionResult VerifyBank([FromBody] BankVerifyDto dto)
        {
            var school = _work.Schools.GetById(dto.SchoolId);
            if (school != null)
            {
                BankVerify verifyModel = populateVerifyModel(dto, school.CustomerId);
                var verifyResult = _payments.Customers.VerifyBank(verifyModel);
                if (verifyResult)
                {
                    var statusUpdateResult = _work.Schools.UpdateAccountStatus(school.Id, AccountStatus.Verified);
                    if (statusUpdateResult.Succeeded)
                    {
                        return NoContent();
                    }
                }
            }

            return BadRequest();
        }

        [HttpGet("{schoolId}")]
        [Authorize(Policy = AuthPolicies.AnyAdmin)]
        public IActionResult GetBySchoolId(string schoolId)
        {
            var school = _work.Schools.GetById(schoolId);
            if (school != null && !string.IsNullOrWhiteSpace(school.CustomerId))
            {
                var customer = _payments.Customers.GetById(school.CustomerId);
                return Ok(customer);
            }
            return NotFound();
        }

        [HttpGet("Charge/{transactionId}")]
        [Authorize(Policy = AuthPolicies.AnyAdmin)]
        public IActionResult CreateCharge(string transactionId)
        {
            var trx = _work.Transactions.GetById(transactionId);
            if (trx != null)
            {
                var charge = new ConnectChargeDto
                {
                    TotalAmountCents = TransactionHelper.UsdDoubleToIntCents(trx.TotalAmountUsd),
                    Currency = "usd",
                    CustomerId = trx.CustomerId,
                    Description = trx.Description,
                    DestinationId = trx.DestinationId,
                    DestinationAmountCents = TransactionHelper.UsdDoubleToIntCents(trx.PayRateUsd)
                };
                try
                {
                    var result = _payments.Customers.CreateCharge(charge);
                    if (result.Succeeded)
                    {
                        var trxUpdate = _work.Transactions.UpdateStatus(trx.Id, TransactionStatus.Pending, result.Status);
                    }
                    else
                    {
                        var trxUpdate = _work.Transactions.UpdateStatus(trx.Id, TransactionStatus.Failed, result.Status);
                    }
                    return NoContent();
                    
                }
                catch (Exception ex)
                {
                    return StatusCode(500);
                }
            }
            return NotFound();
            
        }

        [HttpGet("Transactions/{schoolId}")]
        [Authorize(Policy = AuthPolicies.AnyAdmin)]
        public IActionResult GetTransactions(string schoolId)
        {
            var customerId = _work.Schools.GetCustomerId(schoolId);
            var transactions = _work.Transactions.GetByCustomerId(customerId, 500);
            return Ok(transactions);
        }

		[HttpGet("Transactions/{limit:int?}/{skip:int?}/{sort?}/{direction?}")]
		[Authorize(Policy = AuthPolicies.AnyAdmin)]
		public IActionResult GetTransactions(int limit = 100, int skip = 0, string sort = null, string direction = null)
		{
			var results = _work.Transactions.GetDetailList(limit, skip, sort, direction);

			return Ok(results);
		}

		[HttpPut("Transactions/cancel/{id}")]
        [Authorize(Policy = AuthPolicies.AnyAdmin)]
        public IActionResult CancelTransaction(string id)
        {
            var trx = _work.Transactions.GetById(id);
            if(trx != null)
            {
                var updateResult = _work.Transactions.UpdateStatus(id, TransactionStatus.Cancelled);
                if (updateResult.Succeeded)
                {
                    return NoContent();
                }
            }
            return NotFound();
        }

        #region helpers
        private BankVerify populateVerifyModel(BankVerifyDto dto, string customerId)
        {
            ICustomer customer = _payments.Customers.GetById(customerId);
            return new BankVerify
            {
                AmountOne = dto.AmountOne,
                AmountTwo = dto.AmountTwo,
                CustomerId = customer.Id,
                BankAccountId = customer.PaymentSourceId
            };
        }

		private List<TransactionDto> mapTransactions(List<Transaction> transactions)
		{
			var result = new List<TransactionDto>();
			foreach (var trx in transactions)
			{
				var job = _work.Jobs.GetById(trx.JobId);
				var teacher = _work.Teachers.GetById(job.TeacherId);
				var sub = _work.Subs.GetById(job.SubstituteId);
				var dto = _mapper.Map<TransactionDto>(trx);
				dto.TeacherName = teacher?.FullName;
				dto.SubName = sub?.FullName;
				dto.JobNumber = job.Id.Substring(18).ToUpper();
				dto.JobDate = job.StartDate;
				result.Add(dto);
			}
			return result;
		}
		#endregion
	}
}