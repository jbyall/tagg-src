﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Sentry;
using Tagg.Domain;
using Tagg.Domain.ApiModels;

namespace TaggApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Calendar")]
    [Authorize(Policy = AuthPolicies.TaggUser)]
    public class CalendarController : Controller
    {
        private TaggUnitOfWork _work;
        private UserManager<MongoUser> _userManager;
        private IMapper _mapper;

        public CalendarController(TaggUnitOfWork work, UserManager<MongoUser> userManager, IMapper mapper)
        {
            _work = work;
            _userManager = userManager;
            _mapper = mapper;
        }

        #region Subs
        [HttpGet("Subs/{month:int}/{year:int}")]
        [Authorize(Policy = AuthPolicies.Substitute)]
        public IActionResult GetSubMonth(int month, int year)
        {
            var userId = this.getAuthorizedUserId();
            var sub = _work.Subs.GetById(userId);
            if (sub != null)
            {
                List<CalendarDay> days = CalendarHelper.GetCalendarMonth(month, year);
                var events = populateSubCalendar(days, sub);
                return Ok(events);
            }

            return NotFound();
        }

        [HttpGet("admin/{month:int}/{year:int}")]
        [Authorize(Policy = AuthPolicies.AnyAdmin)]
        public IActionResult GetAdminMonth(int month, int year)
        {
            var adminId = this.getAuthorizedUserId();
            var schoolWithTeachers = _work.Schools.GetByAdminId(adminId);
            List<CalendarDay> days = CalendarHelper.GetCalendarMonth(month, year);
            foreach (var school in schoolWithTeachers)
            {
                days = populateSchoolCalendar(days, school.School.Id);
            }

            return Ok(days);
        }

        [HttpGet("Subs/{date}")]
        [Authorize(Policy = AuthPolicies.Substitute)]
        public IActionResult UpdateSubAvailability(string date)
        {
            var subId = this.getAuthorizedUserId();
            DateTime availDate;
            if (!string.IsNullOrWhiteSpace(subId) && DateTime.TryParse(date, out availDate))
            {
                var updateResult = _work.Subs.UpdateAvailability(subId, availDate);
                if (updateResult.Succeeded)
                {
                    return NoContent();
                }
                return BadRequest();
            }
            return NotFound();
        }


        #endregion

        #region Teachers
        [HttpGet("Teachers/{month:int}/{year:int}")]
        [Authorize(Policy = AuthPolicies.Teacher)]
        public IActionResult GetTeacherMonth(int month, int year)
        {
            var teacherId = this.getAuthorizedUserId();
            var teacher = _work.Teachers.GetById(teacherId);
            if (teacher != null)
            {
                // TODO : Get sub availability and jobs
                List<CalendarDay> days = CalendarHelper.GetCalendarMonth(month, year);
                var events = populateTeacherCalendar(days, teacher);
                return Ok(events);
            }

            return NotFound();
        }
        #endregion

        #region Schools
        [HttpPost("blackout/add")]
        public IActionResult AddBlackout([FromBody] BlackoutAddDto dto)
        {
            if (dto != null && dto.Day > DateTime.UtcNow)
            {
                var addSucceeded = this.addBlackouts(dto);
                if (addSucceeded)
                {
                    return NoContent();
                }
            }
            return BadRequest();
        }

        [HttpPost("blackout/remove")]
        public IActionResult RemoveBlackout([FromBody] BlackoutAddDto dto)
        {
            if (dto != null && dto.Day > DateTime.UtcNow)
            {
                var removeSucceeded = this.removeBlackouts(dto);
                if (removeSucceeded)
                {
                    return NoContent();
                }
            }
            return BadRequest();
        }

        #endregion

        #region helpers
        private string getAuthorizedUserId()
        {
            try
            {
                return this.User.Claims.FirstOrDefault(c => c.Type == "sub").Value;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return "";
            }
        }

        private string getTeacherSchoolClaim()
        {
            try
            {
                return this.User.Claims.FirstOrDefault(c => c.Type == TaggClaimTypes.School).Value;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return null;
            }
        }

        public List<CalendarDay> populateSubCalendar(List<CalendarDay> calendar, Substitute sub)
        {
            var maxDate = calendar.Max(d => d.Date).AddDays(1).Date;

            // NOTE: If first week of month doesn't start on monday,
            //      calendar will contain placeholder dates for year 0001
            //      This was causing performance issues, so where clause
            //      was added to set the min date to the first week day of the actual
            //      month being requested
            var minDate = calendar.Where(d => d.Date.Year > 2001).Min(d => d.Date);
            var result = new List<CalendarDay>();

            // Get jobs for sub that are within the calendar range (min/max dates)
            var jobs = _work.Jobs
                .GetBySubId(sub.Id, minDate, maxDate);

            if (maxDate > DateTime.UtcNow)
            {
                jobs.AddRange(_mapper.Map<List<JobDetailsDto>>(_work.Jobs.GetSubOffers(sub, DateTime.UtcNow.Date, maxDate)));
            }

            // Get sub's availability entries that are within the calendar range (min/max dates)
            var availability = sub.Availability
                .Where(a => a.StartDate.Date >= minDate && a.StartDate.Date <= maxDate)
                .ToList();

            foreach (var j in jobs)
            {
                var today = calendar.FirstOrDefault(c => c.Date.Date == j.StartDate.Date);
                try { today.Events.Add(new CalendarEvent { Job = j }); }
                catch{ } // If for some reason today is null, swallow and continue
            }

            foreach (var av in availability)
            {
                var today = calendar.FirstOrDefault(c => c.Date.Date == av.StartDate.Date);
                try { today.Events.Add(new CalendarEvent { Availibility = av }); }
                catch { } // If for some reason today is null, swallow and continue

            }
            return calendar;
        }

        public List<CalendarDay> populateTeacherCalendar(List<CalendarDay> calendar, Teacher teacher)
        {
            var maxDate = calendar.Max(d => d.Date);

            // NOTE: If first week of month doesn't start on monday,
            //      calendar will contain placeholder dates for year 0001
            //      This was causing performance issues, so where clause
            //      was added to set the min date to the first week day of the actual
            //      month being requested
            var minDate = calendar.Where(d => d.Date.Year > 2001).Min(d => d.Date);
            var result = new List<CalendarDay>();

            // Get jobs for teacher that are within the calendar range (min/max dates)
            var jobs = _work.Jobs
                .GetByTeacherId(teacher.Id)
                .Where(j => j.StartDate > minDate.Date && j.StartDate < maxDate.AddDays(1).Date && j.Status != JobStatus.Cancelled)
                .ToList();

            var teacherSchool = this.getTeacherSchoolClaim();
            var blackouts = new List<Blackout>();
            if (!string.IsNullOrWhiteSpace(teacherSchool))
            {
                blackouts = _work.Schools.GetBlackouts(teacherSchool, minDate, maxDate);
            }

            foreach (var j in jobs)
            {
                var today = calendar.FirstOrDefault(c => c.Date.Date == j.StartDate.Date);
                try { today.Events.Add(new CalendarEvent { Job = j }); }
                catch { } // If for some reason today is null, swallow and continue
            }

            foreach(var b in blackouts)
            {
                var today = calendar.FirstOrDefault(c => c.Date.Date == b.StartDate.Date);
                try { today.Events.Add(new CalendarEvent { IsBlackout = true }); }
                catch { }
            }

            return calendar;
        }

        public List<CalendarDay> populateSchoolCalendar(List<CalendarDay> calendar, string schoolId)
        {
            var maxDate = calendar.Max(d => d.Date);

            // NOTE: If first week of month doesn't start on monday,
            //      calendar will contain placeholder dates for year 0001
            //      This was causing performance issues, so where clause
            //      was added to set the min date to the first week day of the actual
            //      month being requested
            var minDate = calendar.Where(d => d.Date.Year > 2001).Min(d => d.Date);
            var result = new List<CalendarDay>();

            var schoolJobs = _work.Jobs.GetBySchoolId(schoolId, minDate, maxDate.AddDays(1).Date)
            //var schoolJobs = _work.Jobs.GetBySchoolId(schoolId)
                .Where(j => j.StartDate > minDate.Date && j.StartDate < maxDate.AddDays(1).Date && j.Status != JobStatus.Cancelled)
                .ToList();

            var blackouts = _work.Schools.GetBlackouts(schoolId, minDate, maxDate);

            foreach (var j in schoolJobs)
            {
                var today = calendar.FirstOrDefault(c => c.Date.Date == j.StartDate.Date);
                try { today.Events.Add(new CalendarEvent { Job = j }); }
                catch { } // If for some reason today is null, swallow and continue
            }

            foreach (var b in blackouts)
            {
                var today = calendar.FirstOrDefault(c => c.Date.Date == b.StartDate.Date);
                try { today.Events.Add(new CalendarEvent { IsBlackout = true, SchoolId = b.SchoolId }); }
                catch { }
            }

            return calendar;
        }

        private bool addBlackouts(BlackoutAddDto dto)
        {
            bool success = true;
            foreach (var s in dto.Schools)
            {
                var insertResult = _work.Schools.AddBlackout(s, dto.Day);
                success = insertResult.Succeeded ? success : false;
            }

            return success;
        }

        private bool removeBlackouts(BlackoutAddDto dto)
        {
            bool success = true;
            foreach (var s in dto.Schools)
            {
                var deleteResult = _work.Schools.RemoveBlackout(s, dto.Day);
                success = deleteResult.Succeeded ? success : false;
            }
            return success;
        }
        #endregion
    }
}