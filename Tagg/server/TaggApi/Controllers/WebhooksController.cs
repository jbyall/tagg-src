﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tagg.Domain;
using Stripe;
using MongoDB.Driver;
using Newtonsoft.Json;
using Sentry;

namespace TaggApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Webhooks")]
    public class WebhooksController : Controller
    {
        private IPaymentService _payments;
        private TaggUnitOfWork _work;
        private IMongoCollection<TestWebhookPost> _hooks;
        private IEmailService _email;

        public WebhooksController(IPaymentService payments, TaggUnitOfWork work, IEmailService email)
        {
            _payments = payments;
            _work = work;
            _hooks = _work.DB.GetCollection<TestWebhookPost>("hooks");
            _email = email;
        }

        [HttpPost]
        public IActionResult Index()
        {
            var json = new StreamReader(HttpContext.Request.Body).ReadToEnd();
            var result = new StripeEvent();
            var log = new TestWebhookPost();
            try
            {
                result = StripeEventUtility.ParseEvent(json);
                log.EventType = result.Type;
                _hooks.InsertOne(log);
                //switch (result.Type)
                //{
                //    case StripeEvents.ChargeFailed:
                //        var charge = StripeEventUtility.ParseEventDataItem<StripeCharge>(result.Data.Object);
                //        _hooks.InsertOne(charge);
                //        break;
                //    case StripeEvents.AccountUpdated:
                //        var account = StripeEventUtility.ParseEventDataItem<StripeAccount>(result.Data.Object);
                //        break;
                //    default:
                //        break;
                //}
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return BadRequest();
            }
            
            return NoContent();
        }
    }
}