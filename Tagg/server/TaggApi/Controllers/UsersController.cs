﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using MongoDB.Bson;
using MongoDB.Driver.GridFS;
using Sentry;
using Tagg.Domain;
using Tagg.Domain.ApiModels;
using Tagg.Services;
using TaggApi.Models;

namespace TaggApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Users")]
    [Authorize(Policy = AuthPolicies.TaggUser)]
    public class UsersController : Controller
    {
        private readonly IEmailService _emailService;
        private readonly ILogger _logger;
        private readonly IConfiguration _config;
        private TaggUnitOfWork _work;
        private LocationService _location;
        private IMapper _mapper;
        private UserManager<MongoUser> _userManager;
        private ISmsService _smsService;

        public UsersController(IEmailService emailService,
            IConfiguration config,
            TaggUnitOfWork work,
            LocationService location,
            IMapper mapper,
            UserManager<MongoUser> userManager,
            ISmsService smsService)
        {
            _emailService = emailService;
            _config = config;
            _work = work;
            _location = location;
            _mapper = mapper;
            _userManager = userManager;
            _smsService = smsService;
        }

        [HttpGet("{id}", Name = "GetUser")]
        public async Task<IActionResult> Get(string id)
        {
            var user = _work.Users.GetById(id);
            if (user != null)
            {
                var result = _mapper.Map<UserDto>(user);
                return Ok(_mapper.Map<UserDto>(user));
            }
            return BadRequest();
        }

        // Currently only used for admins
        [HttpPost("{id}")]
        [Authorize(Policy = AuthPolicies.SystemAdmin)]
        public IActionResult Update([FromBody] UserDto model)
        {
            var existingUser = _work.Users.GetById(model.Id);
            if (existingUser != null)
            {
                // TODO: Finish this
            }
            return BadRequest();
        }

        [HttpGet("all")]
        [Authorize(Policy = AuthPolicies.SystemAdmin)]
        public IActionResult GetAll()
        {
            // TODO : Fix error with mapper here
            var results = _work.Users.GetAll().ToList();
            return Ok(_mapper.Map<List<UserDto>>(results));
        }

        [HttpGet("list/{type}")]
        [Authorize(Policy = AuthPolicies.SystemAdmin)]
        public IActionResult List(string userType = null)
        {
            UserType typeCast;
            if (!string.IsNullOrWhiteSpace(userType) && Enum.TryParse(userType, out typeCast))
            {

            }
            var result = _work.Users.GetAll();
            return Ok(_mapper.Map<List<UserDto>>(result));
        }

        //[HttpPost("Admin/School/{schoolId}")]
        //[Authorize(Policy = AuthPolicies.AnyAdmin)]
        //public async Task<IActionResult> CreateSchoolAdmin(string schoolId, [FromBody] UserCreateDto model)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var user = new MongoUser();
        //        user = _mapper.Map<MongoUser>(model);
        //        var userCreateResult = await createSchoolUser(model, schoolId);
        //        if (userCreateResult.Succeeded)
        //        {
        //            //send email verify 
        //            var adminTeacher = _mapper.Map<Teacher>(userCreateResult.Entity);
        //            adminTeacher.Status = TaggStatus.New;
        //            var adminCreateResult = _work.Teachers.AddToSchool(adminTeacher, schoolId);

        //            // Get the correct login redirect
        //            var sendResult = await sendInviteEmail(user, UserType.SchoolAdmin);
        //            if (sendResult)
        //            {
        //                return Ok();
        //            }
        //            return StatusCode(503, "Unable to send email.");
        //        }

        //    }
        //    return BadRequest();
        //}

        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] UserCreateDto model)
        {
            if (ModelState.IsValid)
            {
                var result = await _work.Users.CreateAsync(model);
                if (result.Succeeded)
                {
                    if (model.SendInvitEmail)
                    {
                        await sendInviteEmail(result.Entity, model.UserType);
                    }
                    var entityResult = createUserEntity(result.Entity.Id, model);
                    return Ok(_mapper.Map<UserDto>(result.Entity));
                }
                result.Errors.ForEach(e => ModelState.AddModelError("", e));
                return StatusCode(500, ModelState);
            }
            return BadRequest(ModelState);

        }

        [HttpGet("Invite/{emailAddress}")]
        public async Task<IActionResult> SendInviteEmail(string emailAddress)
        {
            var user = await _userManager.FindByEmailAsync(emailAddress);
            if (user != null)
            {
                var sendResult = await sendInviteEmail(user, UserHelper.GetUserTypeFromClaim(user.Claims));
                if (sendResult)
                {
                    return NoContent();
                }
            }
            return NotFound();
        }

        [HttpPost("Photo/{userId}")]
        public async Task<IActionResult> UpdatePhoto(string userId, IFormFile file)
        {
            var user = _work.Users.GetById(userId);
            if (file != null && user != null && this.isValidImageFile(file))
            {
                var photoId = _work.Users.UpdatePhoto(userId, file.OpenReadStream(), file.FileName, file.ContentType);
                return Ok(photoId);
            }
            return BadRequest();
        }

        [HttpGet("Photo/{id}")]
        [AllowAnonymous]
        public IActionResult GetPhoto(string id)
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                var result = _work.Users.GetPhoto(id);
                if (result.Succeeded)
                {
                    return File(result.Stream, result.ContentType);
                }
                return NotFound();
            }
            return BadRequest();
        }

        [HttpPut("email/{currentEmail}/{newEmail}")]
        public async Task<IActionResult> ChangeEmailRequest(string currentEmail, string newEmail)
        {
            var authorizedUserEmail = this.getAuthorizedUserEmail();
            // check to make sure the authorized user is the one making the change
            if (currentEmail.ToLower() == authorizedUserEmail.ToLower())
            {
                var user = await _userManager.FindByEmailAsync(authorizedUserEmail);

                // Validate the email change to make sure it's not already taken
                user.Email = newEmail;
                var validator = _userManager.UserValidators.FirstOrDefault();
                var validationResult = await validator.ValidateAsync(_userManager, user);
                if (validationResult.Succeeded)
                {
                    var emailChangeToken = await _userManager.GenerateChangeEmailTokenAsync(user, newEmail);
                    var callback = Url.Action(
                        "ChangeEmail",
                        "Users",
                        new { userId = user.Id, newEmail = newEmail, code = HttpUtility.UrlEncode(emailChangeToken) }, protocol: Request.Scheme);

                    var emailTemplate = _work.EmailTemplates.GetByType(EmailTemplateType.ChangeEmail);
                    emailTemplate.Replacements[EmailReplacements.Href] = callback;
                    emailTemplate.To.Add(newEmail);

                    var sendResult = _emailService.SendFromTemplate(emailTemplate);
                    if (sendResult.IsSuccessStatusCode)
                    {
                        return Ok();
                    }
                    else
                    {
                        return StatusCode(503, "Unable to change email.");
                    }
                }

            }
            return BadRequest();
        }

        [HttpGet("email/change", Name = "ChangeEmail")]
        [AllowAnonymous]
        public async Task<IActionResult> ChangeEmail(string userId, string newEmail, string code)
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                // TODO : handle no code
            }

            var decodedToken = HttpUtility.UrlDecode(code);

            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return BadRequest("Unable to find user");
            }

            var redirectUrl = getEmailChangeRedirect(UserHelper.GetUserTypeFromClaim(user.Claims));

            var result = await _userManager.ChangeEmailAsync(user, newEmail, decodedToken);
            if (result.Succeeded)
            {
                var updateResult = await this.updateEmailByUserType(user, newEmail);
                if (!string.IsNullOrWhiteSpace(redirectUrl))
                {
                    return Redirect(redirectUrl);
                }
            }

            return BadRequest();
        }

        [HttpGet("Lockout/{userId}")]
        [Authorize(Policy = AuthPolicies.SystemAdmin)]
        public IActionResult Lockout(string userId)
        {
            var user = _work.Users.GetById(userId);
            var uType = UserHelper.GetUserTypeFromClaim(user.Claims);

            if (user != null)
            {
                user.LockoutEndDateUtc = new DateTime(9999, 12, 30);
                var updateResult = _work.Users.Update(user);

                switch (uType)
                {
                    case UserType.Substitute:
                        var subUpdate = _work.Subs.Disable(user.Id);
                        if (!subUpdate.Succeeded)
                        {
                            return StatusCode(500, subUpdate.Errors.FirstOrDefault());
                        }
                        break;
                    default:
                        break;
                }

                if (updateResult.Succeeded)
                {
                    return NoContent();
                }
            }
            return NotFound();
        }

        [HttpGet("Unlock/{userId}")]
        [Authorize(Policy = AuthPolicies.SystemAdmin)]
        public IActionResult Unlock(string userId)
        {
            var user = _work.Users.GetById(userId);
            var uType = UserHelper.GetUserTypeFromClaim(user.Claims);

            if (user != null)
            {
                user.LockoutEndDateUtc = null;
                var updateResult = _work.Users.Update(user);

                if (updateResult.Succeeded)
                {
                    return NoContent();
                }
            }
            return NotFound();
        }

        [HttpDelete("{userId}")]
        [Authorize(Policy = AuthPolicies.SystemAdmin)]
        public IActionResult Delete(string userId)
        {
            // TODO : Delete User logic needs to be confirmed & tested thoroughly before uncommenting.
            //string env = _config["Server:Env"];
            //if (env == "Development")
            //{
            //    var deleteResult = _work.Users.Delete(userId);
            //    if (deleteResult.Succeeded)
            //    {
            //        return NoContent();
            //    }
            //}

            return BadRequest();
        }

        [HttpGet("invite/resend/{userId}")]
        [Authorize(Policy = AuthPolicies.AnyAdmin)]
        public async Task<IActionResult> ResendInvite(string userId)
        {
            var user = _work.Users.GetById(userId);
            if (user != null)
            {
                var userType = UserHelper.GetUserTypeFromClaim(user.Claims);
                var sendResult = await sendInviteEmail(user, userType);
                if (sendResult)
                {
                    return NoContent();
                }
            }
            return NotFound();
        }

        [HttpGet("invite/link/{userId}")]
        [Authorize(Policy = AuthPolicies.SystemAdmin)]
        public async Task<IActionResult> GetEmailConfirmationLink(string userId)
        {
            var user = await _userManager.FindByIdAsync(userId);
            if (user != null && !user.EmailConfirmed)
            {
                try
                {
                    var emailConfLink = await this.getEmailConfirmLink(user);
                    return Ok(emailConfLink);
                }
                catch (Exception ex)
                {
                    SentrySdk.CaptureException(ex);
                }
            }
            return BadRequest();
        }
        #region helpers

        private string getAuthorizedUserId()
        {
            try
            {
                return this.User.Claims.FirstOrDefault(c => c.Type == "sub").Value;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return "";
            }
        }

        private string getAuthorizedUserEmail()
        {
            try
            {
                return this.User.Claims.FirstOrDefault(c => c.Type == TaggClaimTypes.Email).Value;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return "";
            }
        }

        private async Task<bool> updateEmailByUserType(MongoUser user, string newEmail)
        {
            // Update user claim
            var claimUpdate = await _work.Users.UpdateClaim(user, TaggClaimTypes.Email, newEmail);
            EntityResult entityUpdate = null;
            if (claimUpdate.Succeeded)
            {
                // Update other locations that contain the user's email
                var uType = UserHelper.GetUserTypeFromClaim(user.Claims);
                switch (uType)
                {
                    case UserType.Substitute:
                        entityUpdate = _work.Subs.UpdateEmail(user.Id, newEmail);
                        break;
                    case UserType.Teacher:
                        entityUpdate = _work.Teachers.UpdateEmail(user.Id, newEmail);
                        break;
                    // TODO : Not sure if the email on the admin user base is actually used.
                    case UserType.SchoolAdmin:
                    case UserType.DistrictAdmin:
                    case UserType.SystemAdmin:
                    default:
                        entityUpdate = new EntityResult();
                        break;
                }
            }

            return entityUpdate.Succeeded;
        }

        public async Task<bool> sendInviteEmail(MongoUser user, UserType userType)
        {
            var token = await getInviteToken(user);
            var urlBase = "";
            var encodedToken = "";
            IEmailTemplate emailTemplate = null;
            switch (userType)
            {
                // TODO : add register verify endpoint in subs app
                case UserType.Substitute:
                    urlBase = _config["SubsClient:BaseUrl"] + _config["SubsClient:EmailVerifyEndpoint"];
                    emailTemplate = _work.EmailTemplates.GetByType(EmailTemplateType.SubWelcome);
                    encodedToken = HttpUtility.UrlEncode(token);
                    break;
                case UserType.Teacher:
                case UserType.SchoolAdmin:
                    urlBase = _config["SchoolsClient:BaseUrl"] + _config["SchoolsClient:EmailVerifyEndpoint"];
                    emailTemplate = _work.EmailTemplates.GetByType(EmailTemplateType.TeacherWelcome);
                    encodedToken = HttpUtility.UrlEncode(token);
                    break;
                case UserType.DistrictAdmin:
                    urlBase = _config["SchoolsClient:BaseUrl"] + _config["SchoolsClient:EmailVerifyEndpoint"];
                    emailTemplate = _work.EmailTemplates.GetByType(EmailTemplateType.SchoolSetup);
                    encodedToken = HttpUtility.UrlEncode(token);
                    break;
                // TODO : add register verify endpoint in admin client
                case UserType.SystemAdmin:
                    urlBase = _config["AdminClient:BaseUrl"] + _config["AdminClient:EmailVerifyEndpoint"];
                    emailTemplate = _work.EmailTemplates.GetByType(EmailTemplateType.TeacherWelcome);
                    encodedToken = HttpUtility.UrlEncode(token);
                    break;
                default:
                    break;
            }

            var url = $"{urlBase}{encodedToken}";
            emailTemplate.Replacements[EmailReplacements.Href] = url;
            emailTemplate.To.Add(user.Email);
            var sendResult = _emailService.SendFromTemplate(emailTemplate);

            //var sendResult = _emailService.SendEmail(user.Email, "support@taggeducation.com", "Tagg Education Invitation", "Please click the following link to get started: <a href=\"" + url + "\">here</a>");
            return sendResult.IsSuccessStatusCode;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public async Task<EntityResult<MongoUser>> createSchoolUser(UserCreateDto userModel, string schoolId)
        {
            var result = new EntityResult<MongoUser>();
            if (!_work.Schools.Exists(schoolId))
            {
                result.AddError("School not found");
                return result;
            }
            var userResult = await _work.Users.CreateAsync(userModel);
            if (userResult.Succeeded)
            {
                var addClaimResult = _work.Users.AddClaimAsync(userModel.Email, new Claim(TaggClaimTypes.School, schoolId));
            }
            return userResult;
        }

        private async Task<string> getInviteToken(MongoUser user, int expireMinutes = 0)
        {
            var userClaims = await _userManager.GetClaimsAsync(user);
            var tokenClaims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            // TODO : What happens if there is no email claim? Exception?
            tokenClaims.Add(userClaims.FirstOrDefault(c => c.Type == TaggClaimTypes.Email));

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Tokens:key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                    issuer: _config["Tokens:Issuer"],
                    audience: _config["Tokens:Audience"],
                    claims: tokenClaims,
                    expires: DateTime.UtcNow.AddMinutes(expireMinutes),
                    signingCredentials: creds
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private async Task<string> getAdminInviteToken(MongoUser user, int expireMinutes = 0)
        {
            var userClaims = await _userManager.GetClaimsAsync(user);
            var tokenClaims = new List<Claim>();

            // TODO : What happens if there is no email claim? Exception?
            tokenClaims.Add(userClaims.FirstOrDefault(c => c.Type == TaggClaimTypes.Email));

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Tokens:key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                    issuer: _config["Tokens:Issuer"],
                    audience: _config["Tokens:Audience"],
                    claims: tokenClaims,
                    expires: DateTime.UtcNow.AddMinutes(expireMinutes),
                    signingCredentials: creds
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private async Task<string> getEmailConfirmLink(MongoUser user)
        {
            var token = await _userManager.GenerateEmailConfirmationTokenAsync(user);
            return Url.Action(
                "EmailVerify",
                "Register",
                new { email = user.Email, code = HttpUtility.UrlEncode(token)},
                Request.Scheme,
                _config["TaggIdentity:HostName"]);
        }

        private Task<MongoUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }

        private EntityResult createUserEntity(string userId, UserCreateDto dto)
        {
            switch (dto.UserType)
            {
                case UserType.Substitute:
                    var sub = _mapper.Map<Substitute>(getUserBase(dto));
                    var subResult = _work.Subs.Add(sub);
                    return subResult.ToGenericResult();
                case UserType.Teacher:
                    var teacher = _mapper.Map<Teacher>(getUserBase(dto));
                    teacher.Id = userId;
                    teacher.PhoneNumber = dto.PhoneNumber;
                    teacher.SchoolId = dto.SchoolId;
                    var teacherResult = _work.Teachers.Add(teacher);
                    return teacherResult.ToGenericResult();
                case UserType.SchoolAdmin:
                    return _work.Schools.AddSchoolAdmin(dto.SchoolId,
                        new UserBase
                        {
                            Id = userId,
                            FirstName = dto.FirstName,
                            LastName = dto.LastName,
                            Email = dto.Email,
                            PhoneNumber = dto.PhoneNumber,
                            Enabled = true
                        });
                case UserType.DistrictAdmin:
                    return _work.Schools.AddDistrictAdmin(dto.SchoolId,
                        new UserBase
                        {
                            Id = userId,
                            FirstName = dto.FirstName,
                            LastName = dto.LastName,
                            Email = dto.Email,
                            PhoneNumber = dto.PhoneNumber,
                            Enabled = true
                        });
                case UserType.SystemAdmin:
                default:
                    return new EntityResult();
            }
        }

        private UserBase getUserBase(UserCreateDto dto)
        {
            return new UserBase
            {
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                Email = dto.Email,
                Status = TaggStatus.New,
            };
        }

        private bool isValidImageFile(IFormFile file)
        {
            var hasValidContentType = file.ContentType == "image/gif" || file.ContentType == "image/jpeg" || file.ContentType == "image/png";
            var isValidFileSize = file.Length > 0 && file.Length < 30000000;
            return hasValidContentType && isValidFileSize;
        }

        private string getEmailChangeRedirect(UserType userType)
        {
            switch (userType)
            {
                case UserType.Substitute:
                    return _config["SubsClient:BaseUrl"] + "settings";
                case UserType.Teacher:
                    return _config["SchoolsClient:BaseUrl"] + "teachers/settings";
                case UserType.SchoolAdmin:
                case UserType.DistrictAdmin:
                    return _config["SchoolsClient:BaseUrl"] + "admin/settings";
                case UserType.SystemAdmin:
                    return _config["AdminClient:BaseUrl"];
                default:
                    return "";
            }

        }
        #endregion

    }
}