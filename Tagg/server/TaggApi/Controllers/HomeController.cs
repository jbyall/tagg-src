﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Tagg.Domain;
using TaggApi.Models;

namespace TaggApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Home")]
    public class HomeController : Controller
    {
        private IConfiguration _config;
        private UserManager<MongoUser> _userManager;

        public HomeController(IConfiguration config, UserManager<MongoUser> userManager)
        {
            _config = config;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            ViewData["Subs"] = _config["SubsClient:BaseUrl"];
            ViewData["Teachers"] = _config["SchoolsClient:BaseUrl"];
            return View();
        }
    }
}

public class SeedModel
{
    public bool Refresh { get; set; }
    public int SchoolCount { get; set; }
    public int TeacherCount { get; set; }
}
