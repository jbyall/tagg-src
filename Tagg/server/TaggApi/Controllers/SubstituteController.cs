﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using TaggApi.Models;

using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.Extensions.Configuration;
using Tagg.Domain;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Tagg.Services;
using System.Net.Http;
using System.IO;
using Tagg.Domain.ApiModels;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Sentry;

namespace TaggApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Subs")]
    [Authorize(Policy = AuthPolicies.TaggUser)]
    public class SubstituteController : Controller
    {
        private readonly UserManager<MongoUser> _userManager;
        private readonly IEmailService _emailService;
        private readonly ILogger _logger;
        private readonly IConfiguration _config;
        private TaggUnitOfWork _work;
        private LocationService _location;
        private IMapper _mapper;

        public SubstituteController(UserManager<MongoUser> userManager,
            IEmailService emailService,
            IConfiguration config,
            TaggUnitOfWork work,
            LocationService location,
            IMapper mapper)
        {
            _userManager = userManager;
            _emailService = emailService;
            _config = config;
            _work = work;
            _location = location;
            _mapper = mapper;
        }

        [HttpGet("{subId}", Name = "GetSub")]
        public IActionResult Get(string subId)
        {
            var result = _work.Subs.GetById(subId);
            if (result == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<SubstituteDto>(result));
        }

        [HttpGet("status/{status}")]
        public IActionResult GetByStatus(string status)
        {
            TaggStatus statusResult;
            if (Enum.TryParse(status, true, out statusResult))
            {
                var result = _work.Subs.GetByStatus(statusResult);
                return Ok(_mapper.Map<List<SubstituteDto>>(result));
            }
            return BadRequest("status");
        }

        [HttpGet("all")]
        public IActionResult GetAll()
        {
            return Ok(_work.Subs.GetAll().ToList());
        }

        [HttpGet("all/{range}")]
        public IActionResult GetAll(string range)
        {
            if (range == "active")
            {
                return Ok(_work.Subs.Query(s => s.Enabled == true).ToList());
            }

            if(range == "disabled")
            {
                return Ok(_work.Subs.Query(s => s.Enabled == false).ToList());
            }
            return BadRequest();
            
        }

        [HttpGet("Approve/{subId}")]
        [Authorize(Policy = AuthPolicies.SystemAdmin)]
        public IActionResult Approve(string subId)
        {
            var existingSub = _work.Subs.GetById(subId);
            if (existingSub != null)
            {
                existingSub.Enabled = true;
                existingSub.Status = TaggStatus.Active;
                existingSub.ApprovalDate = DateTime.UtcNow;
                existingSub.ApprovedBy = getAuthorizedUserId();
                var updateResult = _work.Subs.Update(existingSub);
                if (updateResult.Succeeded)
                {
                    return NoContent();
                }
            }
            return NotFound();
        }

        [HttpGet("Disable/{subId}")]
        public IActionResult Disable(string subId)
        {
            var existingSub = _work.Subs.GetById(subId);
            if (existingSub != null)
            {
                existingSub.Enabled = false;
                existingSub.Status = TaggStatus.Disabled;
                var updateResult = _work.Subs.Update(existingSub);
                if (updateResult.Succeeded)
                {
                    return NoContent();
                }
            }
            return NotFound();
        }

        [HttpGet("account")]
        public async Task<IActionResult> GetAccount()
        {
            try
            {
                var subId = getAuthorizedUserId();
                var sub = _work.Subs.GetByIdWithRating(subId);
                if (sub != null)
                {
                    return Ok(_mapper.Map<SubstituteDto>(sub));
                }

                return NotFound();
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return BadRequest("Could not retrieve account.");
            }
        }

        [HttpPost("create")]
        public async Task<IActionResult> Create([FromBody]SubstituteDto model)
        {
            if (ModelState.IsValid)
            {
                var sub = _mapper.Map<Substitute>(model);
                sub.Location = _location.GetJsonLocationFromAddress(sub.Address);

                var createResult = _work.Subs.Add(sub);
                if (createResult.Succeeded)
                {
                    var result = _mapper.Map<SubstituteDto>(createResult.Entity);
                    return CreatedAtRoute("GetSub", new { subId = result.Id}, result);
                }
            }
            return BadRequest(ModelState);
        }

        [HttpPost("account/save")]
        public async Task<IActionResult> SaveAccount([FromBody] Substitute model)
        {
            if (ModelState.IsValid)
            {
                if (model.Address != null)
                {
                    model.Location = _location.GetJsonLocationFromAddress(model.Address);
                }
                model.Status = getSubStatus(model);
                var accountUpdateResult = _work.Subs.Update(model);
                if (accountUpdateResult.Succeeded)
                {
                    return Ok(model);
                }
                accountUpdateResult.Errors.ForEach(e => ModelState.AddModelError("", e));
            }
            return BadRequest(ModelState);
        }

        [HttpPost("account/picture")]
        public async Task<IActionResult> SaveAccountPicture([FromBody] Substitute model)
        {
            var subId = getAuthorizedUserId();
            var sub = _work.Subs.GetById(subId);
            if (sub != null)
            {
                sub.Picture = model.Picture;
                var updateResult = _work.Subs.Update(sub);
                if (updateResult.Succeeded)
                {
                    return NoContent();
                }
            }
            return BadRequest();

        }

        [HttpPost("account/resume/{userId}")]
        public IActionResult SaveResume(string userId, IFormFile file)
        {
            var user = _work.Users.GetById(userId);
            if (file != null && user != null && this.isValidResumeFile(file))
            {
                var resumeFileId = _work.Subs.UpdateResume(userId, file.OpenReadStream(), file.FileName, file.ContentType);
                return Ok(resumeFileId);
            }
            return BadRequest();
        }

        [HttpGet("resume/{id}")]
        [AllowAnonymous]
        public IActionResult GetResume(string id)
        {
            if (!string.IsNullOrWhiteSpace(id))
            {
                var result = _work.Subs.GetResume(id);
                if (result.Succeeded)
                {
                    return File(result.Stream, result.ContentType);
                }
                return NotFound();
            }
            return BadRequest();
        }

        [HttpGet("exclude/{subId}/{schoolId}")]
        public ActionResult ExcludeFromSchool(string subId, string schoolId)
        {
            var sub = _work.Subs.GetById(subId);
            if (sub != null)
            {
                var result = _work.Subs.ExcludeFromSchool(subId, schoolId);
                if (result.Succeeded)
                {
                    return NoContent();
                }
            }
            return NotFound();
        }

        [HttpGet("exclude/remove/{subId}/{schoolId}")]
        public ActionResult RemoveExclusion(string subId, string schoolId)
        {
            var sub = _work.Subs.GetById(subId);
            if (sub != null)
            {
                var result = _work.Subs.RemoveExclusion(subId, schoolId);
                if (result.Succeeded)
                {
                    return NoContent();
                }
            }
            return NotFound();
        }

        [HttpPost("Search")]
        public ActionResult Search([FromBody] SubSearchDto search)
        {
            if (ModelState.IsValid)
            {
                var teacher = _work.Teachers.GetById(search.TeacherId);
                if (teacher != null)
                {
                    SubSearchResultDto result = executeSubSearch(search);
                    return Ok(result);
                }
                return NotFound();
            }
            return BadRequest(ModelState);

        }

        [HttpGet("Search/school/{schoolId}")]
        public IActionResult SearchForSchool(string schoolId)
        {
            var result = executeSearchForSchool(schoolId);
            return Ok(result);
        }

        [HttpGet("Search/teacher/{teacherId}/{schoolId}")]
        public IActionResult SearchForTeacher(string teacherId, string schoolId)
        {
            var result = executeSearchForSchool(schoolId).Where(s => !s.Exclusions.Any(x => x == schoolId));
            return Ok(result);
        }

        [HttpPut("deactivate/{userId}")]
        [Authorize(Policy = AuthPolicies.Substitute)]
        public IActionResult Deactivate(string userId)
        {
            var authorizedUser = getAuthorizedUserId();
            if (userId == authorizedUser)
            {
                var result = _work.Subs.Deactivate(userId);
                if (result.Succeeded)
                {
                    return NoContent();
                }
            }
            return BadRequest();
        }

        [HttpPut("activate/{userId}")]
        [Authorize(Policy = AuthPolicies.Substitute)]
        public IActionResult Activate(string userId)
        {
            var authorizedUser = getAuthorizedUserId();
            if (userId == authorizedUser)
            {
                var result = _work.Subs.Reactivate(userId);
                if (result.Succeeded)
                {
                    return NoContent();
                }
            }
            return BadRequest();
        }

        //[HttpGet("search/school/{schoolId}")]
        //public IActionResult SearchForSchool(string schoolId)
        //{
        //    if (!string.IsNullOrWhiteSpace(schoolId))
        //    {
        //        var school = _work.Schools.GetById(schoolId);
        //        if (school != null)
        //        {
        //            var result = _work.Subs.GetForSchool(schoolId, school.Location);
        //            return Ok(_mapper.Map<List<SubDisplayDto>>(result));
        //        }
        //        return NotFound();
        //    }
        //    return BadRequest();
        //}

        #region helpers
        private string getAuthorizedUserId()
        {
            try
            {
                return this.User.Claims.FirstOrDefault(c => c.Type == "sub").Value;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return "";
            }
        }

        private bool isValidResumeFile(IFormFile file)
        {
            bool isValidMimeType = file.ContentType == MimeTypes.MSDoc
                || file.ContentType == MimeTypes.MSDocX
                || file.ContentType == MimeTypes.PDF;
            bool isValidFileSize = file.Length > 0 && file.Length < 50000000;

            return isValidMimeType && isValidFileSize;
        }

        private TaggStatus getSubStatus(Substitute model)
        {
            var existingSub = _work.Subs.GetById(model.Id);
            if (existingSub != null)
            {
                if (existingSub.Status == TaggStatus.New && model.Subjects.Count > 0)
                {
                    return TaggStatus.Pending;
                }
            }
            return existingSub.Status;
        }

        private SubSearchResultDto executeSubSearch(SubSearchDto search)
        {
            var result = new SubSearchResultDto();
            var teacherSchool = _work.Teachers.GetWithSchool(search.TeacherId);

            if (teacherSchool.School != null && teacherSchool.Teacher != null)
            {
                var teacher = teacherSchool.Teacher;
                var school = teacherSchool.School;
                var criteria = new SubSearchCriteria
                {
                    DateAvaliable = search.Date,
                    PayRate = teacher.PayRate > 0 ? teacher.PayRate : school.PayRate,
                    SchoolId = search.SchoolId,
                    Location = school.Location,
                    ActiveOnly = true,
                    EnabledOnly = true,
                    Subjects = teacher.Subjects,
                    TeacherId = teacher.Id
                };
                var availableSubs = _work.Subs.Search(criteria, 100, school.Id);
                result.Total = availableSubs.Count;
                var favorites = availableSubs
                    .Where(s => teacher.Favorites.Contains(s.Id))
                    .OrderByDescending(s => s.Rating)
                    .ToList();

                var others = availableSubs
                    .Where(s => !teacher.Favorites.Contains(s.Id))
                    .OrderByDescending(s => s.Rating)
                    .ToList();

                result.Favorites = _mapper.Map<List<SubDisplayDto>>(favorites);
                result.Others = _mapper.Map<List<SubDisplayDto>>(others);
                result.Favorites.ForEach(s => s.IsFavorite = true);
            }
            return result;

        }

        private List<SubDisplayDto> executeSearchForSchool(string schoolId)
        {
            var result = new List<SubDisplayDto>();
            var school = _work.Schools.GetById(schoolId);

            if (school != null)
            {
                var criteria = new SubSearchCriteria
                {
                    DateAvaliable = null,
                    PayRate = school.PayRate,
                    SchoolId = null,
                    Location = school.Location,
                    ActiveOnly = true,
                    EnabledOnly = true
                };
                var availableSubs = _work.Subs.Search(criteria, 100, school.Id);
                availableSubs = availableSubs.OrderBy(x => x.LastName).ToList();
                result = _mapper.Map<List<SubDisplayDto>>(availableSubs);
            }
            return result;

        }
        #endregion
    }
}