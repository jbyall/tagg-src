﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Sentry;
using Tagg.Domain;
using Tagg.Domain.ApiModels;
using Tagg.Services;

namespace TaggApi.Controllers
{
    [Produces("application/json")]
    [Route("api/System")]
    [Authorize(Policy = AuthPolicies.SystemAdmin)]
    public class SystemController : Controller
    {
        private TaggUnitOfWork _work;
        private IMapper _mapper;
        private ChatService _chat;

        public SystemController(TaggUnitOfWork work, IMapper mapper, ChatService chat)
        {
            _work = work;
            _mapper = mapper;
            _chat = chat;
        }

        [HttpGet("dash")]
        public IActionResult GetDashData()
        {
            try
            {
                var result = new SystemDashDto();
                result.InquiryCount = _work.Inquiries.Query(i => i.Status == TaggStatus.New).Count();
                result.ApplicationCount = _work.Subs.Query(s => s.Status == TaggStatus.Pending).Count();

                return Ok(result);
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return BadRequest();
            }
        }

        [HttpGet("schools")]
        public IActionResult GetSchoolSelect()
        {
            var result = _work.Schools.GetQueryable()
                .Select(s => new SchoolSelectDto { Name = s.Name, Id = s.Id });

            return Ok(result.ToList());
        }

        [HttpGet("autocomplete/user/{term}")]
        public IActionResult AutocompleteUser(string term = "")
        {
            if (term != "x")
            {
                var users = _work.Users.Autocomplete(term);
                return Ok(users);
            }
            return Ok(new List<AutocompleteUser>());
        }

        [HttpGet("autocomplete/school/{term}")]
        public IActionResult AutocompleteSchool(string term = "")
        {
            if (term != "x")
            {
                var users = _work.Schools.Autocomplete(term);
                return Ok(users);
            }
            return Ok(new List<AutocompleteSchool>());
        }

        [HttpGet("autocomplete/district/{term}")]
        public IActionResult AutocompleteDistrict(string term = "")
        {
            if (term != "x")
            {
                var users = _work.Schools.AutocompleteDistrict(term);
                return Ok(users);
            }
            return Ok(new List<AutocompleteSchool>());
        }

        [HttpPost("send")]
        public async Task<IActionResult> SendSystemMessage([FromBody] AdminMessageDto dto)
        {
            if (ModelState.IsValid && await _chat.SendAdminMessage(dto))
            {
                return NoContent();
            }
            return BadRequest();
        }
    }
}