﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Tagg.Domain.ApiModels;
using Tagg.Services;
using RestSharp;
using Tagg.Domain;
using Microsoft.Extensions.Configuration;
using Microsoft.AspNetCore.Authorization;
using Sentry;

namespace TaggApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Payments")]
    public class PaymentsController : Controller
    {
        private IPaymentService _payments;
        private TaggUnitOfWork _work;
        private IConfiguration _config;

        public PaymentsController(TaggUnitOfWork work, IConfiguration config, IPaymentService payments)
        {
            _payments = payments;
            _work = work;
            _config = config;
        }

        /// <summary>
        /// This endpoint is used for creating and connecting the sub's Stripe account
        /// 1. Sub client app redirects user to stripe connect onboarding process
        /// 2. Stripe hits this callback to provide an OAuth authorization code
        /// 3. The payment service posts back to Stripe to get the sub's account details
        /// </summary>
        [HttpGet("callback")]
        [AllowAnonymous]
        public IActionResult Callback(string code)
        {
            // Using the auth code param provided by Stripe, payment service retrieves the account data
            var account = _payments.Accounts.GetAccountData(code, _config["Stripe:SecretKey"]);

            // After receiving account data from Stripe, save account number to database
            var sub = _work.Subs.GetByEmail(account.Email);
            sub.PaymentId = account.Id;
            var updateResult = _work.Subs.Update(sub);

            // Redirect user back to the Sub's client application
            return RedirectPermanent($"{_config["SubsClient:BaseUrl"]}profile");
        }

        /// <summary>
        /// This endpoint is used to generate a 1-time login link for the connected account user (sub)
        /// to view their Stripe account dashboard.
        /// </summary>
        [HttpGet("login")]
        [Authorize(Policy = AuthPolicies.TaggUser)]
        public IActionResult GetLoginLink()
        {
            var subId = getAuthorizedUserId();
            if (!string.IsNullOrWhiteSpace(subId))
            {
                var sub = _work.Subs.GetById(subId);
                var loginUrl = _payments.Accounts.GetAccountLogin(sub.PaymentId);
                if (!string.IsNullOrWhiteSpace(loginUrl))
                {
                    return Ok(loginUrl);
                }
                // Let this fall through to Unauthorized, because if there are any errors we don't want anyone to know details.
            }
            return Unauthorized();
        }

        #region helpers
        private string getAuthorizedUserId()
        {
            try
            {
                return this.User.Claims.FirstOrDefault(c => c.Type == "sub").Value;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return "";
            }
        }
        #endregion
    }
}