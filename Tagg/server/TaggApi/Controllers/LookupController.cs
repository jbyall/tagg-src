﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Tagg.Domain;
using MongoDB.Driver;
using AutoMapper;
using TaggApi.Models;

namespace TaggApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Lookup")]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class LookupController : Controller
    {
        private TaggUnitOfWork _work;
        private IMapper _mapper;

        public LookupController(TaggUnitOfWork work, IMapper mapper)
        {
            _work = work;
            _mapper = mapper;
        }

        [HttpGet("States")]
        public IActionResult GetStates()
        {
            return Ok(_work.Lookup.GetStates());
        }

        [HttpGet("SchoolTypes")]
        public IActionResult GetSchoolTypes()
        {
            return Ok(_work.Lookup.GetSchoolTypes());
        }

        [HttpGet("Subjects")]
        public IActionResult GetSubjects()
        {
            return Ok(_work.Lookup.GetSubjects());
        }

        [HttpGet("AccountViewData")]
        public IActionResult GetAccountViewData()
        {
            var states = _work.Lookup.GetStates();
            var edLevels = _work.Lookup.GetEducationLevels();
            var subjects = _work.Lookup.GetSubjects();

            var model = new AccountViewData(edLevels, subjects, states);
            return Ok(model);
        }


    }
}