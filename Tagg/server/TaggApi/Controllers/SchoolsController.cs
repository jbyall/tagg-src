﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;
using Sentry;
using Tagg.Domain;
using Tagg.Domain.ApiModels;
using Tagg.Services;

namespace TaggApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Schools")]
    [Authorize(Policy = AuthPolicies.TaggUser)]
    public class SchoolsController : Controller
    {
        private readonly IEmailService _emailService;
        private readonly IConfiguration _config;
        private TaggUnitOfWork _work;
        private LocationService _location;
        private IMapper _mapper;
        private UserManager<MongoUser> _userManager;

        public SchoolsController(IEmailService emailService,
            IConfiguration config,
            TaggUnitOfWork work,
            LocationService location,
            IMapper mapper,
            UserManager<MongoUser> userManager)
        {
            _emailService = emailService;
            _config = config;
            _work = work;
            _location = location;
            _mapper = mapper;
            _userManager = userManager;
        }

        [HttpGet("{id}", Name = "GetSchool")]
        public IActionResult Get(string id)
        {
            var result = _work.Schools.GetWithTeachers(id);
            result.Teachers = result.Teachers.OrderBy(x => x.LastName).ToList();
            if (result == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<SchoolDto>(result));
        }

        [HttpGet("details/{id}")]
        public IActionResult GetDetails(string id)
        {
            var school = _work.Schools.GetById(id);
            if (school == null)
            {
                return NotFound();
            }
            var dto = _mapper.Map<SchoolDto>(school);
            var result = new SchoolDetailDto(dto);
            result.availableSubCount = _work.Subs.GetCountForSchool(school.Id, school.Location);
            return Ok(result);
        }

        [HttpGet("all")]
        [Authorize(Policy = AuthPolicies.SystemAdmin)]
        public IActionResult GetAll()
        {
            var schools = _work.Schools.GetAll();
            var result = _mapper.Map<List<SchoolDto>>(schools);
            return Ok(result);
        }

        [HttpGet("User")]
        public async Task<IActionResult> GetUserSchools()
        {
            var currentUserId = getAuthorizedUserId();
            if (!string.IsNullOrWhiteSpace(currentUserId))
            {
                var schoolWithTeachers = _work.Schools.GetByAdminId(currentUserId);
                var result = new List<SchoolDetailDto>();
                foreach (var item in schoolWithTeachers)
                {
                    var schoolDetail = new SchoolDetailDto(_mapper.Map<SchoolDto>(item));
                    schoolDetail.availableSubCount = _work.Subs.GetCountForSchool(item.School.Id, item.School.Location);
                    result.Add(schoolDetail);
                }
                return Ok(result);
            }
            return Unauthorized();
        }

        [HttpGet("admin/summary/{userId}")]
        public IActionResult AdminSummaryGet(string userId)
        {
            try
            {
                var query = _work.Schools.GetByAdminIdQueryable(userId)
                        .Select(s => new AutocompleteSchool { Id = s.Id, Name = s.Name })
                        .ToList();

                return Ok(query);
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return BadRequest();
            }
        }

        [HttpPost("Create")]
        public async Task<IActionResult> Create([FromBody] SchoolDto model)
        {
            if (ModelState.IsValid)
            {
                var school = _mapper.Map<School>(model);
                var currentUserId = getAuthorizedUserId();
                UserBase schoolAdmin = _work.Users.GetUserBaseFromPrinciple(currentUserId);
                if (currentUserId != null)
                {

                    //school.Admins.Add();
                    school.Status = TaggStatus.New;
                    school.Location = _location.GetJsonLocationFromAddress(school.Address);

                    var userId = getAuthorizedUserId();
                    var schoolByAdmin = _work.Schools.Query(s => s.Admins.Any(x => x.Id == userId)).FirstOrDefault();
                    if (schoolByAdmin != null)
                    {
                        school.DistrictId = schoolByAdmin.DistrictId;
                        school.Admins.Add(schoolAdmin);
                    }

                    var createResult = _work.Schools.Add(school);
                    if (createResult.Succeeded)
                    {
                        var result = _mapper.Map<SchoolDto>(createResult.Entity);
                        return CreatedAtRoute("GetSchool", new { id = result.Id }, result);
                    }
                }


            }
            return BadRequest(ModelState);
        }

        [HttpPost("Update/{id}")]
        public IActionResult Update(string id, [FromBody] SchoolDto model)
        {
            if (ModelState.IsValid)
            {
                var school = _mapper.Map<School>(model);
                school.Location = _location.GetJsonLocationFromAddress(school.Address);
                school.Status = getSchoolStatus(school);
                var updateResult = _work.Schools.Update(school);
                if (updateResult.Succeeded)
                {
                    return NoContent();
                }
            }
            return BadRequest(ModelState);
        }


        [HttpGet("Admin/{schoolId}", Name = "GetSchoolAdmins")]
        public async Task<IActionResult> GetAdmins(string schoolId)
        {
            var school = _work.Schools.GetById(schoolId);
            if (school != null)
            {
                var admins = _work.Schools.GetSchoolAdmins(schoolId);
                return Ok(_mapper.Map<List<SchoolAdminDto>>(admins));
            }
            return NotFound("School not found.");
        }

        [HttpPut("Admin/disable/{schoolId}/{adminId}")]
        [Authorize(Policy = AuthPolicies.AnyAdmin)]
        public IActionResult AdminDisable(string schoolId, string adminId)
        {
            var userLockout = _work.Users.Lockout(adminId, new DateTime(3000, 12, 31));
            var result = _work.Schools.DisableSchoolAdmin(schoolId, adminId);
            if (result.Succeeded)
            {
                return NoContent();
            }
            return BadRequest();
        }

        [HttpPut("Admin/enable/{schoolId}/{adminId}")]
        [Authorize(Policy = AuthPolicies.AnyAdmin)]
        public IActionResult AdminEnable(string schoolId, string adminId)
        {
            var userUnlock = _work.Users.Unlock(adminId);
            var result = _work.Schools.EnableSchoolAdmin(schoolId, adminId);
            if (result.Succeeded)
            {
                return NoContent();
            }
            return BadRequest();
        }

        //[HttpPost("Admin/{schoolId}")]
        //public IActionResult AddAdmin(string schoolId, [FromBody] SchoolAdminCreateDto model)
        //{
        //    var school = _work.Schools.GetById(schoolId);
        //    if (school != null)
        //    {
        //        var result = _work.Schools.AddSchoolAdmin(schoolId, model.AdminUserId);
        //        return NoContent();
        //    }
        //    return NotFound();
        //}

        #region districts
        [HttpGet("District/all")]
        public IActionResult GetAllDistricts()
        {
            var result = _work.Schools.GetAllDistricts();
            return Ok(_mapper.Map<List<DistrictDto>>(result));
        }

        [HttpGet("District/single/{districtId}")]
        public IActionResult GetSingleDistrict(string districtId)
        {
            var result = _work.Schools.GetDistrict(districtId);
            if (result != null)
            {
                return Ok(_mapper.Map<DistrictDto>(result));
            }
            return NotFound();
        }

        [HttpGet("District/{districtId}", Name = "GetDistrict")]
        public IActionResult GetByDistrict(string districtId)
        {
            var schools = _work.Schools.GetByDistrictId(districtId);
            var result = _mapper.Map<List<SchoolDto>>(schools);
            return Ok(result);
        }

        [HttpPost("District")]
        public IActionResult CreateDistrict([FromBody] DistrictDto model)
        {
            if (ModelState.IsValid)
            {
                var createResult = _work.Schools.AddDistrict(_mapper.Map<District>(model));
                if (createResult.Succeeded)
                {
                    var result = _mapper.Map<DistrictDto>(createResult.Entity);
                    return CreatedAtRoute("GetDistrict", new { districtId = result.Id }, result);
                }
            }
            return BadRequest(ModelState);
        }
        #endregion

        #region teachers
        [HttpGet("teachers/{schoolId}")]
        public IActionResult GetTeachers(string schoolId)
        {
            return Ok(_mapper.Map<List<TeacherDto>>(_work.Teachers.GetAllBySchoolId(schoolId)));
        }
        #endregion

        #region inquiries
        [HttpPost("Inquiry")]
        [AllowAnonymous]
        public IActionResult CreateInquriy([FromBody] SchoolInquiryDto model)
        {
            if (ModelState.IsValid)
            {
                var inquiry = _mapper.Map<SchoolInquiry>(model);
                inquiry.Status = TaggStatus.New;
                var createResult = _work.Inquiries.Add(inquiry);
                if (createResult.Succeeded)
                {
                    var emailTemplate = _work.EmailTemplates.GetByType(EmailTemplateType.SchoolInquiry);
                    emailTemplate.To.Add(model.Email);
                    var sendResult = _emailService.SendFromTemplate(emailTemplate);

                    var result = _mapper.Map<SchoolInquiryDto>(createResult.Entity);
                    return CreatedAtRoute("GetInquiry", new { id = result.Id }, result);
                }
            }
            return BadRequest(ModelState);
        }

        [HttpGet("Inquiry/{id}", Name = "GetInquiry")]
        public IActionResult GetInqury(string id)
        {
            var result = _work.Inquiries.GetById(id);
            if (result == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<SchoolInquiryDto>(result));
        }

        [HttpGet("Inquiry/All")]
        public IActionResult GetAllInquries()
        {
            var result = _work.Inquiries.GetAll();
            if (result == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<List<SchoolInquiryDto>>(result));
        }

        [HttpGet("Inquiry/status/{statusId}")]
        public IActionResult GetInquiriesByStatus(string statusId)
        {
            var result = new List<SchoolInquiry>();
            TaggStatus status = TaggStatus.Active;
            if (Enum.TryParse(statusId, out status))
            {
                result = _work.Inquiries.Query(i => i.Status == status).ToList();
            }
            else
            {
                result = _work.Inquiries.GetAll();
            }


            return Ok(_mapper.Map<List<SchoolInquiryDto>>(result));
        }

        [HttpPut("Inquiry/status/{inquiryId}")]
        public IActionResult UpdateInquiryStatus(string inquiryId, [FromBody]string status)
        {
            TaggStatus parsedStatus = TaggStatus.Pending;
            if (Enum.TryParse(status, out parsedStatus))
            {
                var inquiry = _work.Inquiries.GetById(inquiryId);
                if (inquiry != null)
                {
                    inquiry.Status = parsedStatus;
                    var updateResult = _work.Inquiries.Update(inquiry);
                    return Ok();
                }
                return NotFound();
            }
            return BadRequest("Invalid status");

        }

        [HttpPost("inquiry/convert/{inquiryId}")]
        public async Task<IActionResult> ConvertInquiry(string inquiryId, [FromBody] SchoolInquiryDto dto)
        {
            var inquiry = _work.Inquiries.GetById(inquiryId);
            if (inquiry != null && ModelState.IsValid && inquiry.Status != TaggStatus.Complete)
            {
                var inquiryConvertResult = await this.convertInquiry(dto, inquiry);
                if (inquiryConvertResult)
                {
                    return NoContent();
                }
            }
            return BadRequest();
        }
        #endregion

        #region helpers
        //private bool addTeacherToSchool(Teacher teacher, string schoolId)
        //{
        //    var result = _work.Teachers.AddToSchool(teacher, schoolId);
        //    return result.Succeeded;
        //}


        private string getAuthorizedUserId()
        {
            try
            {
                return this.User.Claims.FirstOrDefault(c => c.Type == "sub").Value;
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return "";
            }
        }

        private TaggStatus getSchoolStatus(School school)
        {
            TaggStatus result = TaggStatus.New;
            if (!string.IsNullOrWhiteSpace(school.Address.Street1) && school.Status == TaggStatus.New)
            {
                result = TaggStatus.Pending;
            }

            return result;
        }

        private async Task<bool> convertInquiry(SchoolInquiryDto dto, SchoolInquiry inquiry)
        {
            var districtResult = _work.Schools.AddDistrict(new District { Name = dto.DistrictName });

            if (districtResult.Succeeded)
            {
                // Create new School
                var schoolAddress = new Address { City = dto.City, State = dto.State, PostalCode = dto.PostalCode };
                var school = new School { Name = dto.SchoolName, Address = schoolAddress, DistrictId = districtResult.Entity.Id };
                var schoolCreateResult = _work.Schools.Add(school);
                if (schoolCreateResult.Succeeded)
                {
                    var newUser = new UserCreateDto
                    {
                        FirstName = dto.FirstName,
                        LastName = dto.LastName,
                        Email = dto.Email,
                        SchoolId = schoolCreateResult.Entity.Id,
                        UserType = UserType.DistrictAdmin
                    };

                    var userCreateResult = await _work.Users.CreateAsync(newUser);
                    if (userCreateResult.Succeeded)
                    {
                        var entityResult = createUserEntity(userCreateResult.Entity.Id, newUser);
                        if (entityResult.Succeeded)
                        {
                            var sendResult = await this.sendInviteEmail(userCreateResult.Entity);
                            if (sendResult)
                            {
                                inquiry.Status = TaggStatus.Complete;
                                var inquiryUpdateResult = _work.Inquiries.Update(inquiry);
                                return true;
                            }
                        }
                    }
                }
            }

            return false;
        }

        public async Task<bool> sendInviteEmail(MongoUser user)
        {
            var token = await getInviteToken(user);
            var urlBase = _config["SchoolsClient:BaseUrl"] + _config["SchoolsClient:EmailVerifyEndpoint"];
            IEmailTemplate emailTemplate = _work.EmailTemplates.GetByType(EmailTemplateType.SchoolSetup);
            var encodedToken = HttpUtility.UrlEncode(token);
            var url = $"{urlBase}{encodedToken}";
            emailTemplate.Replacements[EmailReplacements.Href] = url;
            emailTemplate.To.Add(user.Email);
            var sendResult = _emailService.SendFromTemplate(emailTemplate);
            return sendResult.IsSuccessStatusCode;
        }

        private async Task<string> getInviteToken(MongoUser user, int expireMinutes = 0)
        {
            var userClaims = await _userManager.GetClaimsAsync(user);
            var tokenClaims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            // TODO : What happens if there is no email claim? Exception?
            tokenClaims.Add(userClaims.FirstOrDefault(c => c.Type == TaggClaimTypes.Email));

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Tokens:key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                    issuer: _config["Tokens:Issuer"],
                    audience: _config["Tokens:Audience"],
                    claims: tokenClaims,
                    expires: DateTime.UtcNow.AddMinutes(expireMinutes),
                    signingCredentials: creds
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private EntityResult createUserEntity(string userId, UserCreateDto dto)
        {
            switch (dto.UserType)
            {
                case UserType.Substitute:
                    var sub = _mapper.Map<Substitute>(getUserBase(dto));
                    var subResult = _work.Subs.Add(sub);
                    return subResult.ToGenericResult();
                case UserType.Teacher:
                    var teacher = _mapper.Map<Teacher>(getUserBase(dto));
                    teacher.Id = userId;
                    teacher.PhoneNumber = dto.PhoneNumber;
                    teacher.SchoolId = dto.SchoolId;
                    var teacherResult = _work.Teachers.Add(teacher);
                    return teacherResult.ToGenericResult();
                case UserType.SchoolAdmin:
                case UserType.DistrictAdmin:
                    return _work.Schools.AddSchoolAdmin(dto.SchoolId,
                        new UserBase
                        {
                            Id = userId,
                            FirstName = dto.FirstName,
                            LastName = dto.LastName,
                            Email = dto.Email,
                            PhoneNumber = dto.PhoneNumber,
                            Enabled = true
                        });
                case UserType.SystemAdmin:
                default:
                    return new EntityResult();
            }
        }

        private UserBase getUserBase(UserCreateDto dto)
        {
            return new UserBase
            {
                FirstName = dto.FirstName,
                LastName = dto.LastName,
                Email = dto.Email,
                Status = TaggStatus.New,
            };
        }
        #endregion

    }
}