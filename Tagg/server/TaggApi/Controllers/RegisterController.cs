﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TaggApi.Models;
using Tagg.Domain;
using Microsoft.AspNetCore.Identity;
using System.Security.Claims;
using System.Web;
using Microsoft.Extensions.Configuration;
using AutoMapper;
using Tagg.Domain.ApiModels;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Sentry;

namespace TaggApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Register")]
    [Authorize(Policy = AuthPolicies.TaggUser)]
    public class RegisterController : Controller
    {
        private UserManager<MongoUser> _userManager;
        private IEmailService _emailService;
        private IConfiguration _config;
        private TaggUnitOfWork _work;
        private ISmsService _smsService;
        private IMapper _mapper;

        public RegisterController(UserManager<MongoUser> userManager,
            IEmailService emailService, IConfiguration configuration,
            TaggUnitOfWork work, ISmsService smsService,
            IMapper mapper)
        {
            _userManager = userManager;
            _emailService = emailService;
            _config = configuration;
            _work = work;
            _smsService = smsService;
            _mapper = mapper;
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<IActionResult> Post([FromBody] UserRegisterDto model)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var userModel = new UserCreateDto
                    {

                    };
                    var user = new MongoUser { UserName = model.Email, Email = model.Email, FirstName = model.FirstName, LastName = model.LastName };
                    var result = await _userManager.CreateAsync(user, model.Password);
                    if (result.Succeeded)
                    {
                        var claims = new List<Claim>
                        {
                            new Claim(TaggClaimTypes.Email, model.Email),
                            new Claim(TaggClaimTypes.FirstName, model.FirstName),
                            new Claim(TaggClaimTypes.LastName, model.LastName),
                            new Claim(TaggClaimTypes.ApiAccess, model.ApiAccessLevel)
                        };

                        var claimsResult = await _userManager.AddClaimsAsync(user, claims);
                        if (claimsResult.Succeeded)
                        {
                            var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                            //var callback = $"{_config["TaggIdentity:SubConfirmEmailUri"]}userId={user.Id}&code={HttpUtility.UrlEncode(code)}";
                            var callback = Url.Action(
                                "ConfirmEmail",
                                "Register",
                                new { userId = user.Id, code = HttpUtility.UrlEncode(code) },
                                Request.Scheme, 
                                _config["TaggIdentity:HostName"]);
                            //// don't use host for localhost debugging
                            //callback = Url.Action(
                            //    "ConfirmEmail",
                            //    "Register",
                            //    new { userId = user.Id, code = HttpUtility.UrlEncode(code) },
                            //    protocol: Request.Scheme);
                        

                            var emailTemplate = _work.EmailTemplates.GetByType(EmailTemplateType.SubWelcome);
                            emailTemplate.Replacements[EmailReplacements.Href] = callback;
                            emailTemplate.To.Add(model.Email);
                            var sendResult = _emailService.SendFromTemplate(emailTemplate);
                            //var sendResult = _emailService.SendEmail(model.Email, "support@taggeducation.com", "Confirm your account", "Please confirm your account by clicking <a href=\"" + callback + "\">here</a>");

                            switch (model.ApiAccessLevel)
                            {
                                case ApiAccessLevels.Substitute:
                                    var sub = _mapper.Map<UserRegisterDto, Substitute>(model);
                                    sub.Status = TaggStatus.New;
                                    sub.Id = user.Id;
                                    _work.Subs.Add(sub);
                                    break;
                                case ApiAccessLevels.Teacher:
                                    //_work.Teachers.Add(_mapper.Map<RegisterDto, Teacher>(model));
                                    break;
                                default:
                                    break;
                            }
                            return Ok();
                        }

                        // Fall-through
                        claimsResult.Errors.ToList().ForEach(e => ModelState.AddModelError("", e.Description));

                    }
                    if (result.Errors.Any(e => e.Code == "DuplicateUserName"))
                    {
                        return StatusCode(409);
                    }
                    result.Errors.ToList().ForEach(e => ModelState.AddModelError("", e.Description));

                }
                return BadRequest(ModelState);
            }
            catch (Exception ex)
            {
                SentrySdk.CaptureException(ex);
                return BadRequest(ex.Message);
            }
        }

        [HttpPost("partial")]
        [AllowAnonymous]
        public async Task<IActionResult> PartialRegister([FromBody] UserRegisterDto model)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(model.Email);
                if (user != null)
                {
                    var pwResetToken = await _userManager.GeneratePasswordResetTokenAsync(user);
                    var passwordSetResult = await _userManager.ResetPasswordAsync(user, pwResetToken, model.Password);
                    if (passwordSetResult.Succeeded)
                    {
                        return NoContent();
                    }
                }
            }
            return BadRequest();
        }

        [HttpPost("update")]
        [AllowAnonymous]
        public async Task<IActionResult> Update([FromBody] UserRegisterDto model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);
            if (ModelState.IsValid && user != null)
            {
                // TODO : Add password validator in client app to make sure it meets requiremetns
                // or return detailed error message from here. see https://stackoverflow.com/questions/48350506/how-to-validate-password-strength-with-angular-5-validator-pattern
                IdentityResult passwordUpdate = new IdentityResult();
                if (!user.HasPassword())
                {
                    passwordUpdate = await _userManager.AddPasswordAsync(user, model.Password);
                    if (passwordUpdate.Succeeded)
                    {
                        return NoContent();
                    }
                }
                // if user already has password for some unknown reason, return error
                // TODO : Determine if it's safe/possible to change password if it already exists
                return StatusCode(500, "Unable to update password.");
            }
            return BadRequest();
        }

        [HttpGet("Email/Confirm/{email}")]
        [AllowAnonymous]
        public async Task<IActionResult> EmailConfirm(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user != null)
            {
                if (!user.EmailConfirmed)
                {
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callback = Url.RouteUrl("EmailVerify", new { email = email, code = HttpUtility.UrlEncode(code) }, protocol: Request.Scheme);

                    var emailTemplate = _work.EmailTemplates.GetByType(EmailTemplateType.EmailConfirmation);
                    emailTemplate.Replacements[EmailReplacements.Href] = callback;
                    emailTemplate.To.Add(email);
                    var sendResult = _emailService.SendFromTemplate(emailTemplate);

                    //var sendResult = _emailService.SendEmail(email, "support@taggeducation.com", "Confirm your account", "Please confirm your account by clicking <a href=\"" + callback + "\">here</a>");
                    if (sendResult.IsSuccessStatusCode)
                    {
                        return Ok();
                    }
                    else
                    {
                        return StatusCode(503, "Unable to send email.");
                    }
                }
                
            }
            return NotFound();
        }

        [HttpGet("Email/Confirm/Manual/{email}")]
        [Authorize(Policy = AuthPolicies.SystemAdmin)]
        public async Task<IActionResult> EmailConfirmManual(string email)
        {
            var user = await _userManager.FindByEmailAsync(email);
            if (user != null)
            {
                if (!user.EmailConfirmed)
                {
                    var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);
                    var callback = Url.RouteUrl("EmailVerify", new { email = email, code = HttpUtility.UrlEncode(code) }, protocol: Request.Scheme);
                    return Ok(callback);
                }

            }
            return NotFound();
        }

        [HttpGet("Email/Verify", Name = "EmailVerify")]
        [AllowAnonymous]
        public async Task<IActionResult> EmailVerify(string email, string code)
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                // TODO : Handle no code
            }

            var decodedToken = HttpUtility.UrlDecode(code);
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                return BadRequest($"Unable to load user.");
            }

            var redirectUrl = getEmailVerifyRedirect(UserHelper.GetUserTypeFromClaim(user.Claims));

            
            var result = await _userManager.ConfirmEmailAsync(user, decodedToken);
            if (result.Succeeded)
            {
                if (!string.IsNullOrWhiteSpace(redirectUrl))
                {
                    var token = await getRegisterToken(user);
                    return Redirect($"{redirectUrl}{token}");
                }

            }
            return Redirect(redirectUrl);
        }

        [HttpGet("ConfirmEmail")]
        [AllowAnonymous]
        public async Task<IActionResult> ConfirmEmail(string userId, string code)
        {
            if (string.IsNullOrWhiteSpace(code))
            {
                // TODO : Handle no code
            }

            var decodedToken = HttpUtility.UrlDecode(code);
            if (userId == null || code == null)
            {
                return RedirectToAction("", "Home");
            }
            var user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                return BadRequest($"Unable to load user.");
            }

            var subsClientUrl = _config["SubsClient:BaseUrl"];
            var hooks = _work.DB.GetCollection<TestWebhookPost>("hooks");
            hooks.InsertOne(new TestWebhookPost
            {
                EventType = subsClientUrl,
                ReceivedOn = DateTime.Now
            });
            
            var result = await _userManager.ConfirmEmailAsync(user, decodedToken);
            if (result.Succeeded)
            {
                if (!string.IsNullOrWhiteSpace(subsClientUrl))
                {
                    return RedirectPermanent($"{subsClientUrl}register/confirm");
                }

            }
            return RedirectPermanent(subsClientUrl);
        }

        [HttpGet("phone/verify/{phoneNumber}")]
        public async Task<IActionResult> VerifyPhoneNumber(string phoneNumber)
        {
            if (!string.IsNullOrWhiteSpace(phoneNumber))
            {
                var user = await GetCurrentUserAsync();
                var code = await _userManager.GenerateChangePhoneNumberTokenAsync(user, phoneNumber);
                await _smsService.SendSmsAsync(phoneNumber, $"Your security code is: {code}");
                return Ok();

            }
            return BadRequest("Phone number required.");
        }

        [HttpPost("phone/verify")]
        // Confirms the token and verifies phone number
        public async Task<IActionResult> VerifyPhoneNumber([FromBody] VerifyPhoneNumberViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await GetCurrentUserAsync();
                var result = await _userManager.ChangePhoneNumberAsync(user, model.PhoneNumber, model.Code);
                if (result.Succeeded)
                {
                    return Ok();
                }
            }
            return BadRequest("Could not update phone number.");
        }

#region helpers
        private async Task<string> getRegisterToken(MongoUser user, int expireMinutes = 0)
        {
            
            var tokenClaims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
            };

            var userClaims = await _userManager.GetClaimsAsync(user);
            foreach (var item in userClaims)
            {
                tokenClaims.Add(new Claim(item.Type, item.Value));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Tokens:key"]));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                    issuer: _config["Tokens:Issuer"],
                    audience: _config["Tokens:Audience"],
                    claims: tokenClaims,
                    expires: DateTime.UtcNow.AddMinutes(expireMinutes),
                    signingCredentials: creds
                );
            return new JwtSecurityTokenHandler().WriteToken(token);
        }

        private Task<MongoUser> GetCurrentUserAsync()
        {
            return _userManager.GetUserAsync(HttpContext.User);
        }

        private string getEmailVerifyRedirect(UserType userType)
        {
            switch (userType)
            {
                case UserType.Substitute:
                    return _config["SubsClient:BaseUrl"] + _config["SubsClient:RegisterEndpoint"];
                case UserType.Teacher:
                case UserType.SchoolAdmin:
                case UserType.DistrictAdmin:
                    return _config["SchoolsClient:BaseUrl"] + _config["SchoolsClient:RegisterEndpoint"];
                case UserType.SystemAdmin:
                    return _config["AdminClient:BaseUrl"] + _config["AdminClient:RegisterEndpoint"];
                default:
                    return "";
            }
            
        }
#endregion
    }
}