export const environment = {
  // DEPLOYMENT-CHECK
  //*********** PROD ***********************************************
  production: true,
  apiUrl: 'https://connect.taggeducation.com/api/',
  userPhotoEndpoint: 'https://connect.taggeducation.com/api/users/photo/',
  stsAuthority: 'https://connect.taggeducation.com/',
  passwordResetUrl: 'https://connect.taggeducation.com/account/ForgotPassword',
  stsClientId: 'adminclient',
  stsResponseType: 'id_token token',
  stsScope: 'openid profile api_access taggapi',
  logoutRedirect: 'https://system.taggeducation.com',
  silentRefreshUrl: 'https://system.taggeducation.com/assets/silent-refresh.html',
  stsRedirectUri: 'https://system.taggeducation.com/auth-callback'

  //*********** STAGING ***********************************************
  // production: true,
  // //Api Settings
  // apiUrl: 'https://tagg-api-staging.azurewebsites.net/api/',
  // userPhotoEndpoint: 'https://tagg-api-staging.azurewebsites.netapi/users/photo/',
  // stsAuthority: 'https://tagg-api-staging.azurewebsites.net/',
  // passwordResetUrl: 'https://tagg-api-staging.azurewebsites.net/account/ForgotPassword',
  // stsClientId:'adminstageclient',
  // stsResponseType:'id_token token',
  // stsScope: 'openid profile api_access taggapi',
  // // Admin App Settings
  // logoutRedirect: 'https://taggadmin-staging.azurewebsites.net',
  // silentRefreshUrl: 'https://taggadmin-staging.azurewebsites.net/assets/silent-refresh.html',
  // stsRedirectUri:'https://taggadmin-staging.azurewebsites.net/auth-callback'

};