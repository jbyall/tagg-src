import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from "@angular/common";
//import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminGuard } from '../auth/admin.guard';
import { TaggCommonModule } from '../tagg-common.module';
import { RegisterConfirmComponent } from './register-confirm.component';
import { RegisterVerifyComponent } from './register-verify.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            { path: 'confirmation/:id', component: RegisterVerifyComponent },
            { path: 'admin/:id', component: RegisterConfirmComponent },
        ]),
        TaggCommonModule
    ],
    exports: [

    ],
    providers: [
    ],
    declarations: [
        RegisterConfirmComponent,
        RegisterVerifyComponent
    ]
})
export class RegisterModule { }