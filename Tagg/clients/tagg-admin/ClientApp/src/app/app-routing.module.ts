// Modules
import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";
// Services

// Components
import { HomeComponent } from './layout/home.component';
import { PageNotFoundComponent } from "./layout/page-not-found.component";
import { AuthCallbackComponent } from './auth/auth-callback.component';
import { AdminGuard } from './auth/admin.guard';
import { TransactionListComponent } from './layout/transaction-list.component';
import { MessagesComponent } from './messages/messages.component';



@NgModule({
    imports: [
        RouterModule.forRoot([
            { path: 'home', component: HomeComponent, canActivate: [AdminGuard]},
            { 
                path: 'districts',
                loadChildren: './districts/district.module#DistrictModule',
            },
            { 
                path: 'jobs',
                loadChildren: './jobs/job.module#JobModule',
            },
            { 
                path: 'subs',
                loadChildren: './subs/sub.module#SubModule',
            },
            { 
                path: 'schools',
                loadChildren: './schools/school.module#SchoolModule',
            },
            { 
                path: 'users',
                loadChildren: './users/user.module#UserModule',
            },
            { 
                path: 'register',
                loadChildren: './register/register.module#RegisterModule',
            },
            { path: 'messages', component: MessagesComponent, canActivate:[AdminGuard]},
            { path: 'transactions', component: TransactionListComponent, canActivate:[AdminGuard] },
            { path: 'auth-callback', component: AuthCallbackComponent },
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: '**', component: PageNotFoundComponent }
        ])
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
