import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-district-create',
  templateUrl: './district-create.component.html',
  styleUrls: ['./district-create.component.css']
})
export class DistrictCreateComponent implements OnInit {
  public showUpdateError: boolean;
  public showForm: boolean;
  public districtName: string;

  constructor(private dataService: DataService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.dataService.setHeaders();
    this.districtName = '';
  }

  onSubmit(){
    console.log(this.districtName);
    this.dataService.districtCreate(this.districtName)
      .subscribe(resp => {
        this.router.navigate(["/districts"]);
      })
  }

}
