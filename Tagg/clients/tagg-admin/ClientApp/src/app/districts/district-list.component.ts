import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { SchoolStatus, AccountStatus, GradesSelect } from '../models/constants';
import { School } from '../models/school';
import { SelectDto } from '../models/dto';

@Component({
  selector: 'app-district-list',
  templateUrl: './district-list.component.html',
  styleUrls: ['./district-list.component.css']
})
export class DistrictListComponent implements OnInit {

  public districts: SelectDto[];
  public dataSource = new MatTableDataSource();
  public columns = ['name'];
  public selectedDistrict: SelectDto;
  public districtSchools: School[];
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private dataService: DataService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.dataService.setHeaders();
    this.getDistricts();
  }

  getDistricts() {
    this.dataService.districtSelectGet()
      .subscribe(resp => {
        this.districts = resp;
        this.dataSource = new MatTableDataSource(this.districts);
        this.dataSource.paginator = this.paginator;
        if(this.districts.length > 0){
          this.onSelect(this.districts[0].id);
        }
      })
  }

  onSelect(id: string){
    this.dataService.schoolsByDistrictGet(id)
      .subscribe(resp => {
        this.districtSchools = resp;
        this.selectedDistrict = this.districts.find(d => d.id === id);
      })
  }

  applyFilter(filterValue: string){
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }

}
