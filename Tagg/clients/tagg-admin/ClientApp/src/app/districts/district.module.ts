import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminGuard } from '../auth/admin.guard';
import { TaggCommonModule } from '../tagg-common.module';
import { DistrictCreateComponent } from './district-create.component';
import { DistrictListComponent } from './district-list.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            { path: '', component: DistrictListComponent, canActivate: [AdminGuard] },
            { path: 'create', component: DistrictCreateComponent, canActivate: [AdminGuard] },
        ]),
        TaggCommonModule
    ],
    declarations: [
        DistrictCreateComponent,
        DistrictListComponent
    ],
    entryComponents: [],
    providers: [
    ],
})
export class DistrictModule { }