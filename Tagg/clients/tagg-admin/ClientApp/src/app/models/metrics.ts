import { Job } from "./job";

export class TeacherMetrics{
    public timeOff: number;
    public timeCovered: number;
    public subsHired: number;
    public recentJob: Job;
    public favoriteSub: string; 
}

export class SchoolMetrics{
    public subDaysMonth: number;
    public filledSytdPercent: number;
    public teacherDaysMonth: number;
    public teacherDaysSytd: number;
    public subDaysSytd: number;
    public avgRating: number;
    public popularSubs: number;
}

export class DistrictMetrics{
    public teacherDaysMonth: number;
    public teacherDaysSytd: number;
    public subDaysMonth: number;
    public subDaysSytd: number;
    public jobsFilledToday: number;
    public jobsUnfilledToday: number;
    public jobsFilledSytdPercent: number;
    public avgRating: number;
}