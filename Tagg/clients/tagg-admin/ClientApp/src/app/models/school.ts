import { Address } from "./location";
import { Teacher, SchoolAdmin, UserDto } from "./user";
import { SelectDto } from "./dto";

export class SchoolInqury {
    public id: string;
    public firstName: string;
    public lastName: string;
    public email: string;
    public phoneNumber: string;
    public schoolName: string;
    public districtName: string;
    public title: string;
    public city: string;
    public state: string;
    public postalCode: string;
    public status: string;
}

export class School {
    constructor(
        public name?: string,
        public address?: Address,
        public contactPhone?: string,
        public contactEmail?: string,
    ){ }
    public id: string;
    public url: string;
    public schoolType: string;
    public startTime: string;
    public endTime: string;
    public summary: string;
    public payRate: number;
    public status: string;
    public teachers: Teacher[];
    public grades: string;
    public customerId: string;
    public accountStatus: number;
    public admins: SchoolAdmin[];
    public districtId: string;
}

export class SchoolDetail {
    public schoolBase: School;
    public availableSubCount: number;
}

export class InquiryConvert{
    public inquiryId: string;
    public school: School;
    public user: UserDto;
    public district: SelectDto;
}

export class District{
    public id: string;
    public name: string;
}