export class Address {
    constructor(
        public city?: string,
        public state?: string,
        public postalCode?: string,
    ){ }
    public street1: string;
    public street2: string;
}

// Result from distance calculation
export class DistanceResult {
    constructor(
        public text: string,
        public value: number,
        public serviceLocation: Location,
        public inServiceArea: boolean
    ) { }
}

//
export class Location {
    public latitude: number;
    public longitude: number;
}