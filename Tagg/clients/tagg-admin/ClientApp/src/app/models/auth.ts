export class LoginModel {
    constructor(
        public email: string,
        public password: string
    ) { }
}

export class TokenResponse {
    constructor(
        public token: string = "",
        public expiration: Date = new Date()
    ) { }
}