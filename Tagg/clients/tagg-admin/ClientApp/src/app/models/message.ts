export class MessageDto{
    constructor(
        public from: string,
        public sendToOption: number,
        public body: string,
        public subject: string,
        public sendTo?: string,
    ){}
    
}