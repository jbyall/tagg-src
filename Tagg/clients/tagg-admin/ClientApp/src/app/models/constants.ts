

export const GradesSelect: any[] = [
    { key: "Pre-K", value: "Pre-K" },
    { key: "K", value: "Kindergarten" },
    { key: "1", value: "1st Grade" },
    { key: "2", value: "2nd Grade" },
    { key: "3", value: "3rd Grade" },
    { key: "4", value: "4th Grade" },
    { key: "5", value: "5th Grade" },
    { key: "6", value: "6th Grade" },
    { key: "7", value: "7th Grade" },
    { key: "8", value: "8th Grade" },
    { key: "9", value: "9th Grade" },
    { key: "10", value: "10th Grade" },
    { key: "11", value: "11th Grade" },
    { key: "12", value: "12th Grade" }
]

export const Hours: any[] = [
    { key: 1, value: "1" },
    { key: 2, value: "2" },
    { key: 3, value: "3" },
    { key: 4, value: "4" },
    { key: 5, value: "5" },
    { key: 6, value: "6" },
    { key: 7, value: "7" },
    { key: 8, value: "8" },
    { key: 9, value: "9" },
    { key: 10, value: "10" },
    { key: 11, value: "11" },
    { key: 12, value: "12" },
]

export const Meridiems: any[] = [
    { key:0, value:"AM"},
    { key:1, value:"PM"}
]

export const SubjectSelect: any[] = [
    { key: 0, value: "Pre-K" },
    { key: 1, value: "Kindergarten" },
    { key: 2, value: "1st Grade" },
    { key: 3, value: "2nd Grade" },
    { key: 4, value: "3rd Grade" },
    { key: 5, value: "4th Grade" },
    { key: 6, value: "5th/6th Grade" },
    { key: 7, value: "Grades 6-8 Math" },
    { key: 8, value: "Grades 6-8 English" },
    { key: 9, value: "Grades 6-8 Social Studies" },
    { key: 10, value: "Grades 6-8 Science" },
    { key: 11, value: "Grades 6-8 Electives" },
    { key: 12, value: "Grades 9-12 Math" },
    { key: 13, value: "Grades 9-12 English" },
    { key: 14, value: "Grades 9-12 Social Studies" },
    { key: 15, value: "Grades 9-12 Science" },
    { key: 16, value: "Grades 9-12 Electives" },
]

export const ContactMethodSelect: any[] = [
    { key: 0, value: "Email" },
    { key: 1, value: "Text" },
    { key: 2, value: "Phone" }
]

export const UserTypesSelect: any[] = [
    { key: 0, value: "Substitute" },
    { key: 1, value: "Teacher" },
    { key: 2, value: "School Admin" },
    { key: 3, value: "District Admin" },
    { key: 4, value: "System Admin" },
];

export const EducationDisplay: any[]=[
    "None",
    "Associates Degree",
    "Bachelors Degree",
    "Masters Degree",
    "Doctoral Degree"
]



export const UserTypesDisplay: any[]=[
    "Substitute",
    "Teacher",
    "School Admin",
    "District Admin",
    "System Admin"
]

export const SubjectDisplay: any[]=[
    "Pre-K",
    "Kindergarten",
    "1st Grade",
    "2nd Grade",
    "3rd Grade",
    "4th Grade",
    "5th/6th Grade",
    "Grades 6-8 Math",
    "Grades 6-8 English",
    "Grades 6-8 Social Studies",
    "Grades 6-8 Science",
    "Grades 6-8 Electives",
    "Grades 9-12 Math",
    "Grades 9-12 English",
    "Grades 9-12 Social Studies",
    "Grades 9-12 Science",
    "Grades 9-12 Electives",
];

export const InquiryStatus: any[]=[
    "Complete",
    "Complete",
    "Complete",
    "Complete",
    "New",
];

export const SchoolStatus: any[]=[
    "Disabled",
    "Active",
    "Pending",
    "Active",
    "New",
];

export const TeacherStatus: any[]=[
    "Disabled",
    "Active",
    "Pending",
    "Active",
    "New",
];

export const AccountStatus: any[]=[
    "Not Created",
    "Verified",
    "Hold"
];

export const AdminJobStatus: any[]=[
    "Pending",
    "Covered",
    "Cancelled",
    "Closed"
];

export const TransactionStatus: any[]=[
    "Hold",
    "Pending",
    "Succeeded",
    "Failed"
];

export const ApplicationStatus: any[]=[
    "Disabled",
    "Active",
    "Pending",
    "Complete",
    "New",
]

export const MonthMap: any[] = [
    'Jan',
    'Feb',
    'March',
    'April',
    'May',
    'June',
    'July',
    'Aug',
    'Sept',
    'Oct',
    'Nov',
    'Dec',
];

export const States: any[] = [
    {
        "key": "AL",
        "value": "Alabama",
        "order": 0
    },
    {
        "key": "AK",
        "value": "Alaska",
        "order": 0
    },
    {
        "key": "AZ",
        "value": "Arizona",
        "order": 0
    },
    {
        "key": "AR",
        "value": "Arkansas",
        "order": 0
    },
    {
        "key": "CA",
        "value": "California",
        "order": 0
    },
    {
        "key": "CO",
        "value": "Colorado",
        "order": 0
    },
    {
        "key": "CT",
        "value": "Connecticut",
        "order": 0
    },
    {
        "key": "DE",
        "value": "Delaware",
        "order": 0
    },
    {
        "key": "DC",
        "value": "District of Columbia",
        "order": 0
    },
    {
        "key": "FL",
        "value": "Florida",
        "order": 0
    },
    {
        "key": "GA",
        "value": "Georgia",
        "order": 0
    },
    {
        "key": "HI",
        "value": "Hawaii",
        "order": 0
    },
    {
        "key": "ID",
        "value": "Idaho",
        "order": 0
    },
    {
        "key": "IL",
        "value": "Illinois",
        "order": 0
    },
    {
        "key": "IN",
        "value": "Indiana",
        "order": 0
    },
    {
        "key": "IA",
        "value": "Iowa",
        "order": 0
    },
    {
        "key": "KS",
        "value": "Kansas",
        "order": 0
    },
    {
        "key": "KY",
        "value": "Kentucky",
        "order": 0
    },
    {
        "key": "LA",
        "value": "Louisiana",
        "order": 0
    },
    {
        "key": "ME",
        "value": "Maine",
        "order": 0
    },
    {
        "key": "MT",
        "value": "Montana",
        "order": 0
    },
    {
        "key": "NE",
        "value": "Nebraska",
        "order": 0
    },
    {
        "key": "NV",
        "value": "Nevada",
        "order": 0
    },
    {
        "key": "NH",
        "value": "New Hampshire",
        "order": 0
    },
    {
        "key": "NJ",
        "value": "New Jersey",
        "order": 0
    },
    {
        "key": "NM",
        "value": "New Mexico",
        "order": 0
    },
    {
        "key": "NY",
        "value": "New York",
        "order": 0
    },
    {
        "key": "NC",
        "value": "North Carolina",
        "order": 0
    },
    {
        "key": "ND",
        "value": "North Dakota",
        "order": 0
    },
    {
        "key": "OH",
        "value": "Ohio",
        "order": 0
    },
    {
        "key": "OK",
        "value": "Oklahoma",
        "order": 0
    },
    {
        "key": "OR",
        "value": "Oregon",
        "order": 0
    },
    {
        "key": "MD",
        "value": "Maryland",
        "order": 0
    },
    {
        "key": "MA",
        "value": "Massachusetts",
        "order": 0
    },
    {
        "key": "MI",
        "value": "Michigan",
        "order": 0
    },
    {
        "key": "MN",
        "value": "Minnesota",
        "order": 0
    },
    {
        "key": "MS",
        "value": "Mississippi",
        "order": 0
    },
    {
        "key": "MO",
        "value": "Missouri",
        "order": 0
    },
    {
        "key": "PA",
        "value": "Pennsylvania",
        "order": 0
    },
    {
        "key": "RI",
        "value": "Rhode Island",
        "order": 0
    },
    {
        "key": "SC",
        "value": "South Carolina",
        "order": 0
    },
    {
        "key": "SD",
        "value": "South Dakota",
        "order": 0
    },
    {
        "key": "TN",
        "value": "Tennessee",
        "order": 0
    },
    {
        "key": "TX",
        "value": "Texas",
        "order": 0
    },
    {
        "key": "UT",
        "value": "Utah",
        "order": 0
    },
    {
        "key": "VT",
        "value": "Vermont",
        "order": 0
    },
    {
        "key": "VA",
        "value": "Virginia",
        "order": 0
    },
    {
        "key": "WA",
        "value": "Washington",
        "order": 0
    },
    {
        "key": "WV",
        "value": "West Virginia",
        "order": 0
    },
    {
        "key": "WI",
        "value": "Wisconsin",
        "order": 0
    },
    {
        "key": "WY",
        "value": "Wyoming",
        "order": 0
    }
];