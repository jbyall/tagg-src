export class AdminRegisterModel {
    constructor(
        public firstName: string = '',
        public lastName: string = '',
        public password: string = '',
        public confirmPassword: string = '',
        public role: string = '',
    ) { }
}



// Model for register form
export class RegisterModel {
    constructor(
        public firstName: string = '',
        public lastName: string = '',
        public email: string = '',
        public password: string = '',
        public confirmPassword: string = '',
        public postalCode: string = '',
        public apiAccessLevel: string = ''
    ) { }
}