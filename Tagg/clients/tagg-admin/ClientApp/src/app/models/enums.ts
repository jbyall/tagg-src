export enum JobStatus{
    Open = 0,
    Accepted = 1,
    Cancelled = 2,
    Complete = 3
}

export enum JobModalType{
    Admin = 0,
    AdminRecent = 1,
    Pending = 1,
    Accepted = 2,
    Recent = 3
}

export enum UserType{
    Substitute = 0,
    Teacher = 1,
    SchoolAdmin = 2,
    DistrictAdmin = 3,
    SystemAdmin = 4
}

export enum SendToOption{
    User = 0,
    School = 1,
    District = 2,
    All = 3
}