import { Address } from "./location";

export class IUser {
    public email: string;
    public id: string;
    public firstName: string;
    public lastName: string;
    public phoneNumber: string;
    public school: string;
    public type: string;
    public apiAccess: string;
}

export class UserDto {
    constructor(
        public userType?: number,
        public id?: string,
        public firstName?: string,
        public lastName?: string,
        public email?: string,
        public phoneNumber?: string,
        public schoolId?: string,
        public isLockedOut?: boolean,
    ) { }

}

export class UserCreateModel {
    public firstName: string;
    public lastName: string;
    public email: string;
    public phoneNumber: string;
    public sendInvitEmail: boolean = true;
    public schoolId: string;
    // Default to teacher
    public userType: number;
}

export class SchoolAdmin {
    public id: string;
    public firstName: string;
    public lastName: string;
    public fullName: string;
    public email: string;
    public enabled: boolean;
    public status: number;

    // Not currently used
    public phoneNumber: string;
    public picture: string;
    public bio: string;
    public title: string;
    public receivePhone: boolean;
    public receiveSMS: boolean;
    public receiveEmail: boolean;

}

export class Substitute {
    constructor(
        public educationLevel: string = '',
        public hasTeachingLicense: boolean = false,
        public hasTranscripts: boolean = false,
        public resumeFile: string = '',
        public applicationComplete: boolean = false,
        //public approvalDate: Date = new Date(),
        public linkedInUrl: string = '',
        public minPayAmount: 0,
        public maxDistance: 0,
        public location: null,
        public subjects: string[] = [],
        public id: string = '',
        public userType: 0,
        public firstName: string = '',
        public lastName: string = '',
        public email: string = '',
        public phoneNumber: string = '',
        public picture: null,
        public bio: string = '',
        public title: string = '',
        public receivePhone: boolean = true,
        public receiveSMS: boolean = true,
        public receiveEmail: boolean = true,
        public enabled: boolean = false,
        public status: number = 0,
        public interests: string = '',
        public address: Address = new Address(),
        public isFavorite: boolean = false,
        public sendRequest: boolean = true,
        public rating: number = 0,
        public subbedAtSchool: boolean = false,
        public ratingCount: number = 0
    ) { }
}

export class Teacher {
    public id: string;
    public userType: number;
    public schoolId: string;
    public firstName: string;
    public lastName: string;
    public email: string;
    public phoneNumber: string;
    public picture: string;
    public bio: string;
    public title: string;
    public receivePhone: boolean;
    public receiveSMS: boolean;
    public receiveEmail: boolean;
    public enabled: boolean;
    public status: number;
    public address: Address;
    public fullName: string;
    public subjects: number[];
    public jobs: string[];
    public favorites: string[];
    public payRate: number;
}

export class TeacherDetails {
    public id: string;
    public userType: number;
    public schoolId: string;
    public firstName: string;
    public lastName: string;
    public email: string;
    public phoneNumber: string;
    public picture: string;
    public bio: string;
    public title: string;
    public receivePhone: boolean;
    public receiveSMS: boolean;
    public receiveEmail: boolean;
    public enabled: boolean;
    public status: number;
    public address: Address;
    public fullName: string;
    public subjects: number[];
    public jobs: string[];
    public favorites: string[];
    public hoursTaken: number;
    public hoursCovered: number;
    public payRate: number;
}

export class TeacherUpdateResult {
    public phoneNumber: string;
    public verificationRequired: boolean;
    public teacher: Teacher;
}