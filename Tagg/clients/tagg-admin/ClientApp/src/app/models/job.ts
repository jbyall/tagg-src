export class JobContainer {
    public jobs: Job[];
    public totalCount: number;
}

export class Job {
    public id: string;
    public teacherId: string;
    public subject: string;
    public payAmount: number;
    public chargeAmount: number;
    public startDate: Date;
    public endDate: Date;
    public teacherName: string;
    public teacherEmail: string;
    public schoolName: string;
    public status: number;
    public subNeeded: boolean;
    public substituteId: string;
    public substituteName: string;
    public lessonPlanId: string;
    public rating: number;
    public createdBy: string;
    public subRating: number;
    public ratingCount: number;
    public subbedAtSchool: boolean;
}

export class JobCreateModel{
    public schoolId: string;
    public teacherId: string;
    public startDate: Date;
    public endDate: Date;
    public subNeeded: boolean;
}

export class SchoolJobsResult{
    public recent: Job[];
    public upcoming: Job[];
    public today: Job[];
}