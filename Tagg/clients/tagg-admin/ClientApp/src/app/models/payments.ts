export class CustomerAccountInfo{
    public country: string = 'US';
    public currency: string = 'usd';
    public routingNumber: string;
    public accountNumber: string;
    public accountHolderName: string;
    public accountHolderType: string = 'individual';
}

export class CustomerCreateModel{
    public sourceTokenId: string;
    public description: string;
    public schoolId: string;
}

export class BankVerifyModel{
    public amountOne: number;
    public amountTwo: number;
    public schoolId: string;
}

export class TransactionContainer {
    public transactions: Transaction[];
    public totalCount: number;
}

export class Transaction{
    public id: string;
    public jobId: string;
    public jobNumber: string;
    public status: number;
    public customerId: string;
    public processOn: Date;
    public payRateUsd: number;
    public totalAmountUsd: number;
    public teacherName: string;
    public subName: string;
    public jobDate: Date;
}