import { Job } from "./job";

export class TimeSpan {
    public hour: number;
    public minute: number;
}

export class CalendarDay {
    public date: Date;
    public year: number;
    public monthIndex: number;
    public dayOfMonthIndex: number;
    public dayOfWeekIndex: number;
    public dayOfWeek: string;
    public events: CalendarEvent[];
}

export class CalendarEvent {
    public description: string;
    public job: Job;
    public availability: UserAvailability;
}

export class UserAvailability {
    public startDate: Date;
    public endDate: Date;
}

export class Lookup {
    constructor(
        public key: string,
        public value: string,
        public id: number,
        public type: string
    ) { }
}

export class MultiSelect {
    constructor(
        public key: string = '',
        public value: string = '',
        public selected: boolean = false
    ) { }
}

export interface SendToOption{
    value: number;
    display: string;
  }

export class AutocompleteUser{
    public firstName: string;
    public lastName: string;
    public id: string;
}

export class AutocompleteSchool{
    public name: string;
    public id: string;
}

// export class Message {
//     public to: string;
//     public from: string;
//     public subject: string;
//     public body: string;
//     public readOn?: Date;
//     public id: string;
//     public created: Date;
// }

// export class MessageThread{
//     public id: string;
//     public subject: string;
//     public messages: Message[];
//     public audience: string[];
//     public jobId: string;
// }




