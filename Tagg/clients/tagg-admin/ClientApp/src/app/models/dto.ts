export class DashModel {
    public inquiryCount: number;
    public applicationCount: number;
}

export class SelectDto {
    constructor(
        public id?: string,
        public name?: string
    ) { }

}