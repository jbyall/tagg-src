// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from "./app-routing.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Services

// Components
import { AppComponent } from './app.component';
import { HomeComponent } from './layout/home.component';
import { PageNotFoundComponent } from "./layout/page-not-found.component";
//import { AccountComponent } from './shared/account/account.component';
import { CommonModule } from '@angular/common';
import { DataService } from './services/data.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthCallbackComponent } from './auth/auth-callback.component';
import { AdminGuard } from './auth/admin.guard';
import { AuthService } from './auth/auth.service';

import { MessageService } from './services/message.service';
import { RatingGuard } from './auth/rating.guard';
import { TaggCommonModule } from './tagg-common.module';
import { ViewJobDialog } from './dialogs/view-job.dialog';
import { JobDetailsDialog } from './dialogs/job-details.dialog';
import { TransactionListComponent } from './layout/transaction-list.component';
import { NewMessageDialog } from './dialogs/new-message.dialog';
import { UserDeleteDialog } from './dialogs/user-delete.dialog';
import { JobEditDialog } from './dialogs/job-edit.dialog';
import { EmailLinkDialog } from './dialogs/email-link.dialog';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PageNotFoundComponent,
    AuthCallbackComponent,
    TransactionListComponent,
    ViewJobDialog,
    JobEditDialog,
    JobDetailsDialog,
    NewMessageDialog,
    UserDeleteDialog,
    EmailLinkDialog
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule.forRoot(),
    TaggCommonModule
  ],
  providers: [
    AdminGuard,
    RatingGuard,
    AuthService,
    DataService,
    MessageService
  ],
  entryComponents: [
    ViewJobDialog,
    JobDetailsDialog,
    JobEditDialog,
    NewMessageDialog,
    UserDeleteDialog,
    EmailLinkDialog
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
