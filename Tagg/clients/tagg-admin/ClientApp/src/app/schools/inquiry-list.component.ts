import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { InquiryStatus } from '../models/constants';
import { SchoolInqury } from '../models/school';

@Component({
  selector: 'app-inquiry-list',
  templateUrl: './inquiry-list.component.html',
  styleUrls: ['./inquiry-list.component.css']
})
export class InquiryListComponent implements OnInit {
  public inquiries: SchoolInqury[];
  public dataSource = new MatTableDataSource();// new UserDataSource(this.dataService);
  public columns = ['school', 'district', 'name', 'email', 'state', 'status'];
  public inquiryStatus = InquiryStatus;
  public selectedInquiry: SchoolInqury;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private dataService: DataService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.dataService.setHeaders();
    this.getInquiries();
  }

  getInquiries(){
    this.dataService.schoolInquiriesGet()
      .subscribe(resp => {
        this.inquiries = resp;
        this.dataSource = new MatTableDataSource(this.inquiries);
        this.dataSource.paginator = this.paginator;
        this.selectedInquiry = this.inquiries[0];
      });
  }

  applyFilter(filterValue: string){
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }

  onSelect(inquiryId: string){
    let newSelected = this.inquiries.find(u => u.id === inquiryId);
    if(newSelected){
      this.selectedInquiry = newSelected;
    }
  }

  onConvert(inquiryId: string){
    this.router.navigate(['/schools', 'inquiry', this.selectedInquiry.id]);
  }

}
