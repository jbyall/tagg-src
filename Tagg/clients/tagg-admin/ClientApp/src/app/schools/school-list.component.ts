import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { SchoolStatus, AccountStatus, GradesSelect } from '../models/constants';
import { School } from '../models/school';

@Component({
  selector: 'app-school-list',
  templateUrl: './school-list.component.html',
  styleUrls: ['./school-list.component.css']
})
export class SchoolListComponent implements OnInit {
  public schools: School[];
  public dataSource = new MatTableDataSource();
  public columns = ['name', 'status', 'account', 'grades'];
  public schoolStatus = SchoolStatus;
  public accountStatus = AccountStatus;
  public grades = GradesSelect;
  public selectedSchool: School;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private dataService: DataService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.dataService.setHeaders();
    this.getSchools();
  }

  getSchools(){
    this.dataService.schoolsListGet()
      .subscribe(resp => {
        this.schools = resp;
        this.dataSource = new MatTableDataSource(this.schools);
        this.dataSource.paginator = this.paginator;
        this.selectedSchool = this.schools[0];
      })
  }

  applyFilter(filterValue: string){
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }

  onSelect(id: string){
    let newSelected = this.schools.find(u => u.id === id);
    if(newSelected){
      this.selectedSchool = newSelected;
    }
  }

  onViewTeachers(){
    this.router.navigate(['/schools', this.selectedSchool.id, 'teachers']);
  }

}
