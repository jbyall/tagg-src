import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { District, School } from '../models/school';
import { States, Meridiems, Hours, GradesSelect } from '../models/constants';
import { FormBuilder, AbstractControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import { SelectDto } from '../models/dto';
import { Address } from '../models/location';

@Component({
  selector: 'app-school-create',
  templateUrl: './school-create.component.html',
  styleUrls: ['./school-create.component.css']
})
export class SchoolCreateComponent implements OnInit {

  public schoolForm: FormGroup;
  public school: School;
  public districts: SelectDto[];
  public selectedDistrictId: string;
  public schoolType: string;
  public states = States;

  public hours = Hours;
  public startHour: number;
  public startMinute: number;
  public startMeridiem = Meridiems.find(m => m.key == 0);
  public endHour: number;
  public endMeridiem = Meridiems.find(m => m.key == 1);
  public endMinute: number;
  public grades = GradesSelect;
  public startGrade: any;
  public endGrade: any;

  public minutes: any[] = [];

  public emailError: string;

  public showForm: boolean;

  constructor(public fb: FormBuilder, public sb: MatSnackBar ,private dataService: DataService, private route: ActivatedRoute, private router: Router) {
    for (let i = 0; i < 60; i++) {
      // Populate array of minutes for school time drop-downs
      this.minutes.push({ key: i, value: `${i}`.padStart(2, '0') });
    };

    this.school = new School();
  }

  get name(): AbstractControl { return this.schoolForm.get('name') };
  get contactPhone(): AbstractControl { return this.schoolForm.get('contactPhone') };
  get contactEmail(): AbstractControl { return this.schoolForm.get('contactEmail') };
  get url(): AbstractControl { return this.schoolForm.get('url') };
  get payRate(): AbstractControl { return this.schoolForm.get('payRate') };
  get street(): AbstractControl { return this.schoolForm.get('address').get('street1') };
  get city(): AbstractControl { return this.schoolForm.get('address').get('city') };
  get state(): AbstractControl { return this.schoolForm.get('address').get('state') };
  get postalCode(): AbstractControl { return this.schoolForm.get('address').get('postalCode') };

  ngOnInit() {
    this.dataService.setHeaders();
    this.dataService.districtSelectGet()
      .subscribe(resp => {
        this.districts = resp;
        console.log(this.districts);
      });
      this.school = new School();
      this.school.address = new Address();
  }

  onDistrictSelected() {
    this.populateForm();
  }

  populateForm() {
    this.schoolForm = this.fb.group({
      name: [this.school.name, Validators.required],
      url: [this.school.url, Validators.required],
      contactEmail: [this.school.contactEmail, [Validators.required, Validators.email]],
      contactPhone: [this.school.contactPhone, Validators.required],
      summary: this.school.summary,
      payRate: [this.school.payRate || '', Validators.required],
      address: this.fb.group({
        street1: [this.school.address.street1, Validators.required],
        street2: this.school.address.street2,
        city: [this.school.address.city, Validators.required],
        state: [this.school.address.state, Validators.required],
        postalCode: [this.school.address.postalCode, Validators.required]
      }),
    });
    this.showForm = true;
  }

  toggleStartMeridiem() {
    this.startMeridiem = this.startMeridiem.key == 0 ?
      Meridiems.find(m => m.key == 1) : Meridiems.find(m => m.key == 0);
  }

  toggleEndMeridiem() {
    this.endMeridiem = this.endMeridiem.key == 0 ?
      Meridiems.find(m => m.key == 1) : Meridiems.find(m => m.key == 0);
  }

  onSubmit() {
    
    let s = Object.assign({}, this.schoolForm.value);
    this.school.schoolType = this.schoolType;
    this.school.grades = `${this.startGrade}-${this.endGrade}`;

    let startHour = this.startMeridiem.key > 0 ? this.startHour + 12 : this.startHour;
    let endHour = this.endMeridiem.key > 0 ? this.endHour + 12 : this.endHour;

    this.school.startTime = `${startHour}:${this.startMinute}:00`;
    this.school.endTime = `${endHour}:${this.endMinute}:00`;

    this.school.name = s.name;
    this.school.url = s.url;
    this.school.contactEmail = s.contactEmail;
    this.school.contactPhone = s.contactPhone;
    this.school.address = s.address;
    this.school.payRate = s.payRate;
    this.school.summary = s.summary;
    this.school.districtId = this.selectedDistrictId;


    this.dataService.schoolCreate(this.school)
      .subscribe(resp => {
        this.showSnackbar('School created successfully!');
        this.router.navigate(['/schools']);
      });

    this.schoolForm.markAsPristine();
  }

  showSnackbar(message: string) {
    this.sb.open(message, 'Dismiss', {
      duration: 5000
    });
  }

  markDirty(){

  }

  propsValid(): boolean{
    return this.schoolType != undefined &&
    this.startGrade != undefined && 
    this.endGrade != undefined && 
    this.startHour != undefined && 
    this.startMinute != undefined && 
    this.endHour != undefined && 
    this.endMinute != undefined;
  }

}
