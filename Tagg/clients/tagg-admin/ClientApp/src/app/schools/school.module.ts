import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminGuard } from '../auth/admin.guard';
import { TaggCommonModule } from '../tagg-common.module';
import { SchoolListComponent } from './school-list.component';
import { SchoolEditComponent } from './school-edit.component';
import { SchoolCreateComponent } from './school-create.component';
import { InquiryListComponent } from './inquiry-list.component';
import { InquiryConvertComponent } from './inquiry-convert.component';
import { TeacherListComponent } from './teacher-list.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            { path: '', component: SchoolListComponent, canActivate: [AdminGuard] },
            { path: 'create', component: SchoolCreateComponent, canActivate: [AdminGuard] },
            { path: 'inquiries', component: InquiryListComponent, canActivate: [AdminGuard] },
            { path: 'inquiry/:id', component: InquiryConvertComponent, canActivate: [AdminGuard] },
            { path: ':id', component: SchoolEditComponent, canActivate: [AdminGuard] },
            { path: ':id/teachers', component: TeacherListComponent, canActivate: [AdminGuard] },
            
        ]),
        TaggCommonModule
    ],
    declarations: [
    SchoolListComponent,
    SchoolEditComponent,
    SchoolCreateComponent,
    InquiryListComponent,
    InquiryConvertComponent,
    TeacherListComponent],
    entryComponents: [],
    providers: [
    ],
})
export class SchoolModule { }