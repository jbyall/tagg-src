import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { SchoolInqury, InquiryConvert, School } from '../models/school';
import { UserDto } from '../models/user';
import { SelectDto } from '../models/dto';
import { Address } from '../models/location';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-inquiry-convert',
  templateUrl: './inquiry-convert.component.html',
  styleUrls: ['./inquiry-convert.component.css']
})
export class InquiryConvertComponent implements OnInit {
  public inquiryId: string;
  public inquiry: SchoolInqury;
  public convertModel: InquiryConvert;
  public existingDistricts: SelectDto[];
  public districtId: string;

  public convertForm: FormGroup;
  public showForm: boolean;
  public showUpdateError: boolean;

  constructor(private dataService: DataService, private route: ActivatedRoute, private router: Router, public fb: FormBuilder, private sb: MatSnackBar) { 

  }

  get school(): AbstractControl { return this.convertForm.get('school') };
  get city(): AbstractControl { return this.convertForm.get('city') };
  get state(): AbstractControl { return this.convertForm.get('state') };
  get postalCode(): AbstractControl { return this.convertForm.get('postalCode') };
  get firstName(): AbstractControl { return this.convertForm.get('firstName') };
  get lastName(): AbstractControl { return this.convertForm.get('lastName') };
  get email(): AbstractControl { return this.convertForm.get('email') };

  ngOnInit() {
    this.dataService.setHeaders();
    this.inquiryId = this.route.snapshot.params['id'];
    this.dataService.schoolInquirySingle(this.inquiryId)
      .subscribe(resp => {
        this.inquiry = resp;
        this.populateConvertModel();
      })
  }

  populateConvertModel(){
    let conversion = new InquiryConvert();
    conversion.user = new UserDto(null, null, this.inquiry.firstName, this.inquiry.lastName, this.inquiry.email, this.inquiry.phoneNumber);
    conversion.district = new SelectDto(null, this.inquiry.districtName);
    let address = new Address(this.inquiry.city, this.inquiry.state, this.inquiry.postalCode);
    conversion.school = new School(this.inquiry.schoolName, address, this.inquiry.phoneNumber, this.inquiry.email);
    this.convertModel = conversion;
    this.getExistingDistricts();
    this.populateForm();
    console.log(this.convertModel);
  }

  onSubmit(){
    this.showUpdateError = false;
    this.inquiry.schoolName = this.school.value;
    this.inquiry.city = this.city.value;
    this.inquiry.state = this.state.value;
    this.inquiry.postalCode = this.postalCode.value;
    this.inquiry.firstName = this.firstName.value;
    this.inquiry.lastName = this.lastName.value;
    this.inquiry.email = this.email.value;

    console.log('submitted', this.inquiry);

    this.dataService.inquiryConvertPost(this.inquiryId, this.inquiry)
      .subscribe(resp => {
        this.openSnackBar("Inquiry converted successfully");
        this.router.navigate(['/schools', 'inquiries']);
      }, err => {
        this.showUpdateError = true;
      })
  }

  openSnackBar(message: string){
    this.sb.open(message, 'Dismiss');
  }

  getExistingDistricts(){
    this.dataService.districtSelectGet()
      .subscribe(resp => {
        this.existingDistricts = resp;
      })
  }

  onSelectDistrict(){
    let selectedDistrict = this.existingDistricts.find(d => d.id == this.districtId);
    this.inquiry.districtName = selectedDistrict.name;
  }

  populateForm(){
    this.convertForm = this.fb.group({
      school: [this.inquiry.schoolName, Validators.required],
      city: [this.inquiry.city, Validators.required],
      state: [this.inquiry.state, Validators.required],
      postalCode: [this.inquiry.postalCode, Validators.required],
      email: [this.inquiry.email, [Validators.required, Validators.email]],
      firstName: [this.inquiry.firstName, Validators.required],
      lastName: [this.inquiry.lastName, Validators.required]
    });
    this.showForm = true;
  }

 

}
