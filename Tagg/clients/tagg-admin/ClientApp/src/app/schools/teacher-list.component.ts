import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormBuilder } from '@angular/forms';
import { MatSnackBar, MatTableDataSource, MatPaginator } from '@angular/material';
import { Teacher } from '../models/user';
import { TeacherStatus } from '../models/constants';

@Component({
  selector: 'app-teacher-list',
  templateUrl: './teacher-list.component.html',
  styleUrls: ['./teacher-list.component.css']
})
export class TeacherListComponent implements OnInit {
  public teachers: Teacher[];
  public dataSource = new MatTableDataSource();// new UserDataSource(this.dataService);
  public columns = ['firstName', 'lastName', 'phone', 'email', 'status'];
  public selectedTeacher: Teacher;
  public statusDisplay = TeacherStatus;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private dataService: DataService, private route: ActivatedRoute, private router: Router, public fb: FormBuilder, private sb: MatSnackBar) { }

  ngOnInit() {
    this.dataService.setHeaders();
    let schoolId = this.route.snapshot.paramMap.get('id');
    this.dataService.teacherGetBySchoolId(schoolId)
      .subscribe(resp =>{
        this.teachers = resp;
        this.dataSource = new MatTableDataSource(this.teachers);
        this.dataSource.paginator = this.paginator;
        this.selectedTeacher = this.teachers[0];
      })

  }

  applyFilter(filterValue: string){
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }

  onSelectTeacher(userId: string){
    let newSelected = this.teachers.find(u => u.id === userId);
    if(newSelected){
      this.selectedTeacher = newSelected;
    }
  }

}
