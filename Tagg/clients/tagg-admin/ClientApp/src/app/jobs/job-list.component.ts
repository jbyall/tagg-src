import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatPaginator, MatSnackBar, MatSort, MatDialog } from '@angular/material';
import { Job } from '../models/job';
import { SubjectDisplay, AdminJobStatus } from '../models/constants';
import { JobEditDialog } from '../dialogs/job-edit.dialog';

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.css']
})
export class JobListComponent implements OnInit {
  public jobs: Job[];
  public resultsLength = 0;
  public dataSource = new MatTableDataSource();
  public columns = ['startDate', 'schoolName', 'substituteName', 'teacherName', 'subject', 'status'];
  public jobStatus = AdminJobStatus;
  public subjects = SubjectDisplay;
  public selectedJob: Job;
  public isLoadingResults = true;

  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private dataService: DataService, private route: ActivatedRoute, private router: Router, private sb: MatSnackBar, private dialog: MatDialog) { }

  ngOnInit() {
    this.sort.sortChange.subscribe(() => {
      this.paginator.pageIndex = 0;
      this.isLoadingResults = true;
      this.getJobs(this.paginator.pageSize, 0, this.sort.active, this.sort.direction);
    });
    this.paginator.page.subscribe(() => {
      this.isLoadingResults = true;
      this.getJobs(this.paginator.pageSize, this.paginator.pageSize * this.paginator.pageIndex, this.sort.active, this.sort.direction);
    });

    this.dataService.setHeaders();
    this.getJobs(10, 0, "startDate", "desc");
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  getJobs(fetch: number, skip: number, sort: string, direction: string) {

    // console.log("fetch = " + fetch);
    // console.log("skip = " + skip);
    // console.log("sort = " + sort);
    // console.log("direction = " + direction);

    this.dataService.jobsList(fetch, skip, sort, direction)
      .subscribe(resp => {
        this.jobs = resp.jobs;
        this.dataSource = new MatTableDataSource(this.jobs);
        this.selectedJob = this.jobs[0];
        this.resultsLength = resp.totalCount;
        this.isLoadingResults = false;
      });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }

  onSelectJob(jobId: string) {
    let newSelected = this.jobs.find(u => u.id === jobId);
    if (newSelected) {
      this.selectedJob = newSelected;
      console.log(this.selectedJob);
    }
  }

  onCancelJob(jobId: string) {
    this.dataService.jobCancel(jobId)
      .subscribe(resp => {
        this.openSnackbar('Job cancelled successfully')
      }, err => { this.sb.open('Failed to cancel job.') })
  }

  onEditJob(jobId: string) {
    let dialogRef = this.dialog.open(JobEditDialog, {
      width: '600px',
      data: { job: this.selectedJob }
    });

    dialogRef.afterClosed().subscribe(result => {
      //this.populateCalendar();
    });
  }

  openSnackbar(message: string) {
    this.sb.open(message, 'Dismiss');
  }

}
