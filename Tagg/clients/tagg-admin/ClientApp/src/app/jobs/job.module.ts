import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminGuard } from '../auth/admin.guard';
import { TaggCommonModule } from '../tagg-common.module';
import { JobListComponent } from './job-list.component';
import { JobCreateComponent } from './job-create.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            { path: '', component: JobListComponent, data:{ range:'all' }, canActivate: [AdminGuard] },
            { path: 'upcoming', component: JobListComponent, data:{ range: 'week'}, canActivate: [AdminGuard] },
            { path: 'create', component: JobCreateComponent, canActivate: [AdminGuard] },
        ]),
        TaggCommonModule
    ],
    declarations: [
        JobListComponent,
        JobCreateComponent
    ],
    entryComponents: [],
    providers: [
    ],
})
export class JobModule { }