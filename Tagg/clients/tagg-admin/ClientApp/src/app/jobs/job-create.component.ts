import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserCreateModel, Teacher } from '../models/user';
import { FormGroup, AbstractControl, FormBuilder, Validators, FormControl } from '@angular/forms';
import { UserTypesSelect, Hours, Meridiems } from '../models/constants';
import { SelectDto } from '../models/dto';
import { MatSnackBar } from '@angular/material';
import { Job, JobCreateModel } from '../models/job';
import { School } from '../models/school';

@Component({
  selector: 'app-job-create',
  templateUrl: './job-create.component.html',
  styleUrls: ['./job-create.component.css']
})
export class JobCreateComponent implements OnInit {
  public job: JobCreateModel;
  public schools: School[];
  public teachers: Teacher[];

  public selectedSchool: School;
  public jobForm: FormGroup;

  public selectedSchoolId: string;
  public selectedTeacherId: string;

  public showForm: boolean;
  public showTeachers: boolean;
  public showDateTime: boolean;
  public showUpdateError: boolean;
  public showNoSubs: boolean;

  public pickerDate: FormControl;
  public minDate = new Date();
  public dateFilter = (d: Date): boolean => {
    const day = d.getDay();
    return day !== 0 && day !== 6;
  }

  public hours = Hours;
  public startHour: number;
  public startMinute: number;
  public startMeridiem = Meridiems.find(m => m.key == 0);
  public endHour: number;
  public endMeridiem = Meridiems.find(m => m.key == 1);
  public endMinute: number;
  public minutes: any[] = [];

  constructor(private dataService: DataService, private route: ActivatedRoute, private router: Router, private fb: FormBuilder) { }

  ngOnInit() {
    this.dataService.setHeaders();
    this.getSchool();
    for (let i = 0; i < 60; i++) {
      this.minutes.push({ key: i, value: `${i}`.padStart(2, '0') });
    }
    this.pickerDate = new FormControl(new Date());
    this.job = new JobCreateModel();
    //this.job = new Job();
  }

  getSchool() {
    this.dataService.schoolsListGet()
      .subscribe(resp => {
        this.schools = resp;
      })
  }

  onSchoolSelected() {
    this.dataService.teacherGetBySchoolId(this.selectedSchoolId)
      .subscribe(resp => {
        this.teachers = resp;
        this.showTeachers = true;
      })
  }

  onTeacherSelected(){
    this.showDateTime = true;
  }

  populateForm() {
    this.showForm = true;
  }

  toggleStartMeridiem() {
    this.startMeridiem = this.startMeridiem.key == 0 ?
      Meridiems.find(m => m.key == 1) : Meridiems.find(m => m.key == 0);
  }

  toggleEndMeridiem() {
    this.endMeridiem = this.endMeridiem.key == 0 ?
      Meridiems.find(m => m.key == 1) : Meridiems.find(m => m.key == 0);
  }

  onJobCreate() {
    let selectedDate = this.pickerDate.value as Date;
    console.log(selectedDate)
    //console.log('selected', selectedDate.getDate());
    let startDate = new Date(selectedDate.getFullYear(), selectedDate.getMonth(), selectedDate.getDate());
    let endDate = new Date(selectedDate.getFullYear(), selectedDate.getMonth(), selectedDate.getDate());

    let startHourCheck = this.startMeridiem.key > 0 ? this.startHour + 12 : this.startHour;
    let endHourCheck = this.endMeridiem.key > 0 ? this.endHour + 12 : this.endHour;

    startDate.setHours(startHourCheck, this.startMinute);
    endDate.setHours(endHourCheck, this.endMinute);

    this.job.teacherId = this.selectedTeacherId;
    this.job.startDate = startDate;
    this.job.endDate = endDate;
    this.job.subNeeded = true;
    this.job.schoolId = this.selectedSchoolId;

    console.log(this.job);
    this.dataService.createJob(this.job)
      .subscribe(resp => {
        
      }, err => {
        console.log(err);
        if(err.status == 409){
          this.showNoSubs = true;
        }
        else{
          this.showUpdateError = true;
        }
      });

  }

}
