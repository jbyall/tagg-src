import { Component, ViewChild } from '@angular/core';
import { AuthService } from './auth/auth.service';
import { MatSidenav } from '@angular/material';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  @ViewChild('menu') menu: MatSidenav;
  public isLoggedIn: boolean = false;
  public userType: string;

  constructor(public authService: AuthService, public router: Router) {
    this.router.events.subscribe((e) =>{
      if(!(e instanceof NavigationEnd)){
        return;
      }
      window.scrollTo(0, 0);
    })
  }

  open() {
    this.isLoggedIn = this.authService.isLoggedIn();
    if (this.isLoggedIn) {
      this.userType = this.authService.user.profile.api_access;
    }
    this.menu.open();
  }

  onSettings() {
    this.menu.close();
    if (this.userType === "0x3" || this.userType === "0x33") {
      this.router.navigate(['/admin', 'settings']);
    }
    else {
      this.router.navigate(['/teachers', 'settings']);
    }
  }

  onProfile() {
    this.menu.close();
    if (this.userType === "0x3") {
      this.router.navigate(['/admin', 'settings']);
    }
    else {
      this.router.navigate(['/teachers', 'profile']);
    }
  }

  onSignOut(){
    this.authService.signOut();
  }
}
