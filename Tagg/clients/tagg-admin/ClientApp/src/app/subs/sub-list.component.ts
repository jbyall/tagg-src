import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserDto, Substitute } from '../models/user';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { UserTypesDisplay, ApplicationStatus } from '../models/constants';

@Component({
  selector: 'app-sub-list',
  templateUrl: './sub-list.component.html',
  styleUrls: ['./sub-list.component.css']
})
export class SubListComponent implements OnInit {

  public subs: Substitute[];
  public dataSource = new MatTableDataSource();// new UserDataSource(this.dataService);
  public columns = ['firstName', 'lastName', 'email', 'enabled', 'status'];
  public selectedSub: Substitute;
  private range: string;

  public appStatus = ApplicationStatus;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private dataService: DataService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.dataService.setHeaders();
    this.range = this.route.snapshot.data['range'];
    this.getSubs();
  }

  getSubs(){
    this.dataService.subsListGet(this.range)
      .subscribe(resp => {
        this.subs = resp;
        this.dataSource = new MatTableDataSource(this.subs);
        this.dataSource.paginator = this.paginator;
        if(!this.selectedSub){
          this.selectedSub = this.subs[0];
        }
        else{
          this.onSelect(this.selectedSub.id);
        }
      })
  }

  applyFilter(filterValue: string){
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }

  onSelect(id: string){
    let newSelected = this.subs.find(s => s.id === id);
    if(newSelected){
      this.selectedSub = newSelected;
    }
  }

  onDisable(id: string){
    this.dataService.subDisable(id)
      .subscribe(resp => {
        this.getSubs();
      });
  }

  onEnable(id: string){
    this.dataService.subApprove(id)
      .subscribe(resp => {
        this.getSubs();
      })
  }

}
