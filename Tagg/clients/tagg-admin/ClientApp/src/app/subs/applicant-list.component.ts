import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserDto, Substitute } from '../models/user';
import { MatTableDataSource, MatPaginator } from '@angular/material';
import { UserTypesDisplay, ApplicationStatus, EducationDisplay } from '../models/constants';

@Component({
  selector: 'app-applicant-list',
  templateUrl: './applicant-list.component.html',
  styleUrls: ['./applicant-list.component.css']
})
export class ApplicantListComponent implements OnInit {

  public subs: Substitute[];
  public dataSource = new MatTableDataSource();// new UserDataSource(this.dataService);
  public columns = ['firstName', 'lastName', 'email', 'enabled', 'status', 'approvedDate'];
  public selectedSub: Substitute;

  public appStatus = ApplicationStatus;
  public educations = EducationDisplay;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private dataService: DataService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.dataService.setHeaders();
    this.getSubs();
  }

  getSubs(){
    this.dataService.applicantListGet()
      .subscribe(resp => {
        this.subs = resp;
        this.dataSource = new MatTableDataSource(this.subs);
        this.dataSource.paginator = this.paginator;
        if(!this.selectedSub){
          this.selectedSub = this.subs[0];
        }
        else{
          this.onSelect(this.selectedSub.id);
        }
      })
  }

  applyFilter(filterValue: string){
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }

  onSelect(id: string){
    let newSelected = this.subs.find(s => s.id === id);
    if(newSelected){
      this.selectedSub = newSelected;
    }
  }

  onApprove(id: string){
    this.dataService.subApprove(id)
      .subscribe(resp => {
        this.getSubs();
      });
  }

  onDisable(id: string){
    this.dataService.subDisable(id)
      .subscribe(resp => {
        this.getSubs();
      });
  }

}
