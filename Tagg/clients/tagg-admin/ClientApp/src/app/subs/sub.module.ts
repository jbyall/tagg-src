import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminGuard } from '../auth/admin.guard';
import { TaggCommonModule } from '../tagg-common.module';
import { SubListComponent } from './sub-list.component';
import { ApplicantListComponent } from './applicant-list.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            { path: 'active', component: SubListComponent, data:{ range: 'active'}, canActivate: [AdminGuard] },
            { path: 'disabled', component: SubListComponent, data:{ range: 'disabled'}, canActivate: [AdminGuard] },
            { path: 'applicants', component: ApplicantListComponent, canActivate: [AdminGuard] },
            { path: '', component: SubListComponent, data:{ range:'all' }, canActivate: [AdminGuard] },
        ]),
        TaggCommonModule
    ],
    declarations: [
        SubListComponent,
        ApplicantListComponent
    ],
    entryComponents: [],
    providers: [
    ],
})
export class SubModule { }