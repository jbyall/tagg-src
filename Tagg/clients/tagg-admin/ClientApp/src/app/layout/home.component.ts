import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from '../auth/auth.service';
import { DataService } from '../services/data.service';
import { DashModel } from '../models/dto';

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    public dashModel: DashModel;

    constructor(public authService: AuthService, private router: Router, private dataService: DataService) {
        this.dataService.setHeaders();
    }

    ngOnInit() {
        this.populateDashModel();
    }

    populateDashModel(){
        this.dataService.getDashData()
            .subscribe(resp => {
                this.dashModel = resp;
                console.log(this.dashModel);
            })
    }

    onSubsActive(){
        this.router.navigate(['/subs', 'active']);
    }

    onSubsDisabled(){
        this.router.navigate(['/subs', 'disabled']);
    }

    onSubCreate(){
        this.router.navigate(['/users', 'create', 0]);
    }

    onJobsAll(){
        this.router.navigate(['/jobs']);
    }

    onJobsUpcoming(){
        this.router.navigate(['/jobs', 'upcoming']);
    }

    onJobCreate(){
        this.router.navigate(['/jobs', 'create']);
    }

    onSchoolsAll(){
        this.router.navigate(['/schools']);
    }

    onSchoolCreate(){
        this.router.navigate(['/schools', 'create']);
    }

    onDistrictsAll(){
        this.router.navigate(['/districts']);
    }

    onDistrictCreate(){
        this.router.navigate(['/districts', 'create']);
    }

    onTeacherCreate(){
        this.router.navigate(['/users', 'create', 1]);
    }

    onUsersAll(){
        this.router.navigate(['/users']);
    }

    onUsersIncomplete(){
        this.router.navigate(['/users', 'incomplete']);
    }

    onUserCreate(){
        this.router.navigate(['/users', 'create']);
    }

    onApplications(){
        this.router.navigate(['/subs', 'applicants']);
    }

    onInquiries(){
        this.router.navigate(['/schools', 'inquiries']);
    }

}
