import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MatTableDataSource, MatPaginator, MatSnackBar, MatSort } from '@angular/material';
import { InquiryStatus, TransactionStatus } from '../models/constants';
import { SchoolInqury } from '../models/school';
import { Transaction } from '../models/payments';

@Component({
  selector: 'app-transaction-list',
  templateUrl: './transaction-list.component.html'
})
export class TransactionListComponent implements OnInit {
  public transactions: Transaction[];
  public dataSource = new MatTableDataSource();// new UserDataSource(this.dataService);
  public columns = ['jobDate', 'processOn', 'payRateUsd', 'totalAmountUsd', 'teacherName', 'subName', 'status', 'select'];
  public transactionStatus = TransactionStatus;
  public selectedTransaction: Transaction;
  public resultsLength = 0;
  public isLoadingResults = true;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private dataService: DataService, private route: ActivatedRoute, private router: Router, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.sort.sortChange.subscribe(() => 
    {
      this.paginator.pageIndex = 0;
      this.isLoadingResults = true;
      this.getTransactions(this.paginator.pageSize, 0, this.sort.active, this.sort.direction);
    });
    this.paginator.page.subscribe(() => {
      this.isLoadingResults = true;
      this.getTransactions(this.paginator.pageSize, this.paginator.pageSize * this.paginator.pageIndex, this.sort.active, this.sort.direction);
    });

    this.dataService.setHeaders();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.getTransactions(5, 0,  "jobDate", "desc");
  }

  getTransactions(fetch: number, skip: number, sort: string, direction: string){

    this.dataService.transactionListGet(fetch, skip, sort, direction)
      .subscribe(resp => {
        this.transactions = resp.transactions;
        this.dataSource = new MatTableDataSource(this.transactions);
        this.selectedTransaction = this.transactions[0];
        this.resultsLength = resp.totalCount;
        this.isLoadingResults = false;
      });
  }

  applyFilter(filterValue: string){
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }

  onSelect(id: string){
    let newSelected = this.transactions.find(u => u.id === id);
    if(newSelected){
      this.selectedTransaction = newSelected;
    }
  }

  cancelTransaction(transactionId: string){
    this.dataService.transactionCancel(transactionId)
      .subscribe(resp =>{
        this.getTransactions(10, 0,  this.sort.active, this.sort.direction);
        this.openSnackBar("Transaction Cancelled");
      })
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, 'Dismiss', {
      duration: 5000
    });
  }

}
