import { Component, OnInit, Input } from '@angular/core';
import { MessageService } from '../services/message.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {
  public hasNewMessage: boolean;

  constructor(public messageService: MessageService, public authService: AuthService) { 
    this.messageService.newMessage$
      .subscribe(resp => {
        this.hasNewMessage = resp;
        if(resp){
          console.log('SideNav: new message');
        }
      });

      this.messageService.newChannel$
        .subscribe(resp => {
          if(resp){
            console.log('SideNav: new channel');
            this.hasNewMessage = resp;
          }
        })
  }



  ngOnInit() {
    console.log('SideNav: get chat token');
      let test = this.messageService.getChatToken();
  }

}
