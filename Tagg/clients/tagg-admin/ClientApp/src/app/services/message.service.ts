import { Injectable } from '@angular/core';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { DataService } from './data.service';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class MessageService {
    public chatToken: any;
    public chatClient: any;
    public channelDescriptors: any[];
    public hasNewMessages: boolean;

    private suppressNavIcon: boolean;
    private _newChannel = new Subject<boolean>();
    private _newMessage = new Subject<boolean>();
    private _chatReady = new Subject<boolean>();

    private newMessageDelegate = (event) => this.getStatus();
    private newChannelDelegate = (event) => this.onNewChannel();

    constructor(private dataService: DataService) {
    }

    newChannel$ = this._newChannel.asObservable();
    newMessage$ = this._newMessage.asObservable();
    chatReady$ = this._chatReady.asObservable();


    // Get chat token from Tagg Server for the client
    public getChatToken(){
        if (!this.chatToken || !this.chatClient) {
            this.dataService.getChatToken()
                .subscribe(resp => {
                    this.chatToken = resp;
                    this.initChatClient();
                });
        }
        else{
            this.getStatus();
            this._chatReady.next(true);
        }
    }

    // Initialize the Chat client
    initChatClient() {
        Twilio.Chat.Client.create(this.chatToken)
            .then(client => {
                this.chatClient = client;
                this.loadChannels();
            });
    }


    loadChannels(setListeners: boolean = true){
        this.chatClient.getSubscribedChannels()
            .then(resp => {
                this.channelDescriptors = resp.items;
                this.getStatus();
                if(setListeners){
                    this.setClientListeners();
                }
            });
    }

    setClientListeners() {
        this.chatClient.addListener('channelAdded', this.newChannelDelegate);
        this.chatClient.addListener('messageAdded', this.newMessageDelegate);
        this._chatReady.next(true);
    }

    getStatus(){
        let foundUnreads = false;
        for(let i = 0; i < this.channelDescriptors.length; i++){
            let descriptor = this.channelDescriptors[i];
            if(descriptor.lastConsumedMessageIndex < descriptor.lastMessage.index || descriptor.lastConsumedMessageIndex == null){
                foundUnreads = true;   
                break;
            }
        }
        this.onNewMessage(foundUnreads);
    }

    onNewMessage(hasUnread: boolean){
        if(!this.suppressNavIcon){
            this._newMessage.next(hasUnread);
        }
        
    }

    onNewChannel(){
        this._newChannel.next(true);
    }

    public statusRecheck(){
        this.getStatus();
    }

    // public getLocalChannels(): Promise<number>{
    //     console.log('Service: soring channels');
    //     this.chatClient.getLocalChannels({criteria: 'lastMessage', order: 'descending'})
    //         .then(resp =>{
    //             return new Promise((resolve) =>{
    //                 resolve(resp.length);
    //             });
    //         });
    // }

    // public onSuppressNavIcon(): void{
    //     this._newMessage.next(false);
    //     this.suppressNavIcon = true;
    // }

    // public onResumeNavIcon(): void{
    //     this.suppressNavIcon = false;
    //     this.loadChannels(false);
    // }
}