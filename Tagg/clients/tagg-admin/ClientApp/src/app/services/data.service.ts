import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpErrorResponse, HttpResponse, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { AuthService } from '../auth/auth.service';
import { DistanceResult } from '../models/location';
import { Lookup, AutocompleteUser, AutocompleteSchool } from '../models/helpers';
import { RegisterModel } from '../models/register';
import { School, SchoolInqury, SchoolDetail, District } from '../models/school';
import { SchoolAdmin, UserCreateModel, IUser, UserDto, Substitute, Teacher } from '../models/user';
import { DashModel, SelectDto } from '../models/dto';
import { Job, JobContainer, JobCreateModel } from '../models/job';
import { Transaction, TransactionContainer } from '../models/payments';
import { MessageDto } from '../models/message';

@Injectable()
export class DataService {
  baseUrl = environment.apiUrl;

  // register
  private _registerPartialPost = this.baseUrl + "register/partial";
  private _registerEmailUrl = this.baseUrl + "register/email/confirm/";

  // dashboard
  private _dashDataGetUrl = environment.apiUrl + 'system/dash';

  // messages
  private _messageGetTokenUrl = this.baseUrl + "messages/token";
  private _messageCreateChannelUrl = this.baseUrl + "messages/channel/job/";
  private _messageSendPost = this.baseUrl + "system/send";

  // users
  private _userListGet = this.baseUrl + "users/list/";
  private _userCreateUrl = this.baseUrl + "users/create";
  private _userLockout = this.baseUrl + "users/lockout/";
  private _userUnlock = this.baseUrl + "users/unlock/";
  private _userDelete = this.baseUrl + "users/";
  private _userResendInvite = this.baseUrl + "users/invite/resend/";
  private _userInviteLinkGet = this.baseUrl + "users/invite/link/";

  // autocomplet
  private _autocompleteUser = this.baseUrl + "system/autocomplete/user/";
  private _autocompleteSchool = this.baseUrl + "system/autocomplete/school/";
  private _autocompleteDistrict = this.baseUrl + "system/autocomplete/district/";

  // jobs
  private _jobsListGet = this.baseUrl + "jobs/details/";
  private _jobCreate = this.baseUrl + "jobs/create";
  private _jobCancel = this.baseUrl + "jobs/cancel/";

  // school
  private _districtCreatePost = this.baseUrl + "schools/district";
  private _districtsListGet = this.baseUrl + "schools/district/all";
  private _schoolCreatePost = this.baseUrl + "schools/create";
  private _schoolInquiriesGet = this.baseUrl + "schools/inquiry/all";
  private _schoolInquirySingle = this.baseUrl + "schools/inquiry/";
  private _schoolsListGet = this.baseUrl + "schools/all";
  private _schoolsSelectGet = this.baseUrl + "system/schools";
  private _districtsSelectGet = this.baseUrl + "schools/district/all";
  private _schoolsByDistrictGet = this.baseUrl + "schools/district/";
  private _inquiryConvertPost = this.baseUrl + "schools/inquiry/convert/";

  // subs
  private _subsListGet = this.baseUrl + "subs/all/";
  private _applicantListGet = this.baseUrl + "subs/status/";
  private _subApproveGet = this.baseUrl + "subs/approve/";
  private _subDisable = this.baseUrl + "subs/disable/";

  // teachers
  private _teachersGetBySchool = this.baseUrl + "teachers/school/";

  // transactions
  private _transactionListGet = this.baseUrl + "customers/transactions/";
  private _transactionCancelPut = this.baseUrl + "customers/transactions/cancel/";


  public authHeaders = new HttpHeaders();
  public jobsForReview: string[] = [];

  constructor(private http: HttpClient, private authService: AuthService) {
    this.authService.userLoaded$
      .subscribe(resp => {
        this.setHeaders();
      })
  }

  public setHeaders() {
    this.authHeaders = this.authHeaders.set('Authorization', this.authService.getAuthorizationHeaderValue());
  }


  // register
  public register(user: RegisterModel): Observable<boolean>{
    return this.http.post<boolean>(this._registerPartialPost, user);
  }
  public requestEmailVerification(email: string): Observable<string> {
    return this.http.get<string>(this._registerEmailUrl + email);
  }

  // dash
  public getDashData(): Observable<DashModel>{
   return this.http.get<DashModel>(this._dashDataGetUrl, {headers: this.authHeaders});
  }


  // Users
  public createUser(user: UserCreateModel): Observable<string> {
    return this.http.post<string>(this._userCreateUrl, user, { headers: this.authHeaders });
  }
  public usersList(type?: number): Observable<UserDto[]>{
    return this.http.get<UserDto[]>(this._userListGet + type, {headers: this.authHeaders});
  }
  public autocompleteUser(value: string): Observable<AutocompleteUser[]>{
    return this.http.get<AutocompleteUser[]>(this._autocompleteUser + value, {headers: this.authHeaders});
  }
  public userLockout(userId: string): Observable<string>{
    return this.http.get<string>(this._userLockout + userId, {headers: this.authHeaders});
  }
  public userUnlock(userId: string): Observable<string>{
    return this.http.get<string>(this._userUnlock + userId, {headers: this.authHeaders});
  }
  public userDelete(userId: string): Observable<string>{
    return this.http.delete<string>(this._userDelete + userId, {headers: this.authHeaders});
  }

  public autocompleteSchool(value: string): Observable<AutocompleteSchool[]>{
    return this.http.get<AutocompleteSchool[]>(this._autocompleteSchool + value, {headers: this.authHeaders});
  }

  public autocompleteDistrict(value: string): Observable<AutocompleteSchool[]>{
    return this.http.get<AutocompleteSchool[]>(this._autocompleteDistrict + value, {headers: this.authHeaders});
  }

  public resendInvite(userId: string): Observable<string>{
    return this.http.get<string>(this._userResendInvite + userId, {headers: this.authHeaders});
  }

  public inviteLinkGet(userId: string): Observable<string>{
    return this.http.get<string>(this._userInviteLinkGet + userId, {headers: this.authHeaders});
  }

  //jobs
  public jobsList(limit:number = 100, skip:number = 0, sort:string = null, direction:string = null): Observable<JobContainer>{
    var url = this._jobsListGet + limit + '/' + skip;
    if (sort != null)
    {
      url += '/' + sort + '/' + direction;
    }
    return this.http.get<JobContainer>(url, {headers: this.authHeaders});
  }
  public createJob(job: JobCreateModel): Observable<string> {
    return this.http.post<string>(this._jobCreate, job, { headers: this.authHeaders });
  }
  public jobCancel(jobId: string):Observable<string>{
    return this.http.put<string>(this._jobCancel + jobId, null, {headers: this.authHeaders});
  }

  // messages
  public getChatToken(): Observable<any> {
    return this.http.get<any>(this._messageGetTokenUrl, { headers: this.authHeaders });
  }
  public getOrCreateJobChannel(jobId: string): Observable<string> {
    return this.http.post<string>(this._messageCreateChannelUrl + jobId, null, { headers: this.authHeaders });
  }
  public messageSendPost(message: MessageDto): Observable<boolean>{
    return this.http.post<boolean>(this._messageSendPost, message, {headers: this.authHeaders});
  }

  // school
  public districtCreate(name: string): Observable<boolean>{
    return this.http.post<boolean>(this._districtCreatePost, new SelectDto(null, name), {headers: this.authHeaders});
  }
  public districtsListGet(): Observable<District[]>{
    return this.http.get<District[]>(this._districtsListGet, {headers: this.authHeaders});
  }
  public schoolCreate(school: School): Observable<string> {
    return this.http.post<string>(this._schoolCreatePost, school, { headers: this.authHeaders });
  }
  public schoolInquiriesGet(): Observable<SchoolInqury[]>{
    return this.http.get<SchoolInqury[]>(this._schoolInquiriesGet, {headers: this.authHeaders});
  }
  public schoolInquirySingle(inquiryId: string): Observable<SchoolInqury>{
    return this.http.get<SchoolInqury>(this._schoolInquirySingle + inquiryId, {headers: this.authHeaders});
  }
  public schoolsListGet(): Observable<School[]>{
    return this.http.get<School[]>(this._schoolsListGet, {headers: this.authHeaders});
  }
  public schoolSelectGet(): Observable<SelectDto[]>{
    return this.http.get<SelectDto[]>(this._schoolsSelectGet, {headers: this.authHeaders});
  }
  public districtSelectGet(): Observable<SelectDto[]>{
    return this.http.get<SelectDto[]>(this._districtsSelectGet, {headers: this.authHeaders});
  }
  public schoolsByDistrictGet(districtId: string): Observable<School[]>{
    return this.http.get<School[]>(this._schoolsByDistrictGet + districtId, {headers: this.authHeaders});
  }
  public inquiryConvertPost(inquiryId: string, inquiry: SchoolInqury): Observable<boolean>{
   return this.http.post<boolean>(this._inquiryConvertPost + inquiryId, inquiry, {headers: this.authHeaders});
  }

  // subs
  public subsListGet(range: string): Observable<Substitute[]>{
    return this.http.get<Substitute[]>(this._subsListGet + range, {headers: this.authHeaders});
  }
  public applicantListGet(): Observable<Substitute[]>{
    return this.http.get<Substitute[]>(this._applicantListGet + 'Pending', {headers: this.authHeaders});
  }
  public subApprove(subId: string): Observable<string>{
    return this.http.get<string>(this._subApproveGet + subId, {headers: this.authHeaders});
  }
  public subDisable(subId: string): Observable<string>{
    return this.http.get<string>(this._subDisable + subId, {headers: this.authHeaders});
  }

  //teachers
  public teacherGetBySchoolId(schoolId: string): Observable<Teacher[]>{
    return this.http.get<Teacher[]>(this._teachersGetBySchool + schoolId, {headers: this.authHeaders});
  }

  //transactions
  public transactionListGet(limit:number = 100, skip:number = 0, sort:string = null, direction:string = null): Observable<TransactionContainer>{
    var url = this._transactionListGet + limit + '/' + skip;
    if (sort != null)
    {
      url += '/' + sort + '/' + direction;
    }
    return this.http.get<TransactionContainer>(url, {headers: this.authHeaders});
  }

  public transactionCancel(transactionId: string): Observable<boolean> {
    return this.http.put<boolean>(this._transactionCancelPut + transactionId, null, { headers: this.authHeaders });
  }

  // Error handler
  private handleError(error: Response): Observable<any> {
    console.log("error from data service:");
    console.error(error);
    return Observable.throw(error || "Server error");
  }

}
