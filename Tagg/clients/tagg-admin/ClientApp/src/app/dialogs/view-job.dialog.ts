import { Component, OnInit, Inject, EventEmitter, Output } from '@angular/core';
import { Router } from "@angular/router";
import { DataService } from '../services/data.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { SubjectDisplay } from '../models/constants';


@Component({
    selector: 'view-job-dialog',
    templateUrl: './view-job.dialog.html',
    styleUrls: ['./view-job.dialog.css']
  })
  export class ViewJobDialog {
    @Output() notify: EventEmitter<string> = new EventEmitter();
    public subjects = SubjectDisplay;
  
    constructor(
      public dialogRef: MatDialogRef<ViewJobDialog>,
      @Inject(MAT_DIALOG_DATA) public data: any, private dataService: DataService, private router: Router) { }
  
    onNoClick(): void {
      this.dialogRef.close();
    }

    onMessageSub(jobId: string){
      this.router.navigate(['/messages']);
      this.onNoClick();
    }

    onCancelJob(){
      // this.dataService.cancelJob(this.data.job.id)
      //   .subscribe(resp => {
      //     this.onNoClick();
      //   })
    }
  
  }