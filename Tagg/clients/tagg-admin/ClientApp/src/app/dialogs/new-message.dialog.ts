import { Component, OnInit, Inject, EventEmitter, Output } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from '../auth/auth.service';
import { DataService } from '../services/data.service';
import { User } from 'oidc-client';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { SubjectDisplay, AdminJobStatus } from '../models/constants';
import { SendToOption, AutocompleteUser, AutocompleteSchool } from '../models/helpers';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs/Observable';
import { map, startWith, distinctUntilChanged, switchMap, debounceTime } from 'rxjs/operators';
import { MessageDto } from '../models/message';


@Component({
  selector: 'new-message-dialog',
  templateUrl: './new-message.dialog.html',
})
export class NewMessageDialog implements OnInit {
  @Output() notify: EventEmitter<string> = new EventEmitter();
  public error: boolean;

  public sendToOptions: SendToOption[] = [
    { value: 1, display: "User" },
    { value: 2, display: "School" },
    { value: 3, display: "District" },
    { value: 4, display: "All Users" },
  ];

  public sendToOption: number;
  public messageSubject: string = "";
  public messageBody: string = "";

  public userControl = new FormControl();
  public schoolControl = new FormControl();
  public districtControl = new FormControl();

  public usersFiltered: Observable<AutocompleteUser[]>;
  public schoolsFiltered: Observable<AutocompleteSchool[]>;
  public districtsFiltered: Observable<AutocompleteSchool[]>;

  public showSpinner: boolean;

  constructor(
    public dialogRef: MatDialogRef<NewMessageDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, private dataService: DataService, private router: Router, private authService: AuthService) { }

  ngOnInit() {
    this.dataService.setHeaders();
    this.usersFiltered = this.userControl.valueChanges
      .pipe(
        startWith(null),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(val => {
          return this.filterUsers(val || 'x')
        })
      );

    this.schoolsFiltered = this.schoolControl.valueChanges
      .pipe(
        startWith(null),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(val => {
          return this.filterSchools(val || 'x')
        })
      );

    this.districtsFiltered = this.districtControl.valueChanges
      .pipe(
        startWith(null),
        debounceTime(200),
        distinctUntilChanged(),
        switchMap(val => {
          return this.filterDistricts(val || 'x')
        })
      );
  }

  private filterUsers(value: string): Observable<AutocompleteUser[]> {
    return this.dataService.autocompleteUser(value)
      .pipe(
        map(resp => resp.filter(option => {
          return resp;
        }))
      )
  }

  private filterSchools(value: string): Observable<AutocompleteSchool[]> {
    return this.dataService.autocompleteSchool(value)
      .pipe(
        map(resp => resp.filter(option => {
          return resp;
        }))
      )
  }

  private filterDistricts(value: string): Observable<AutocompleteSchool[]> {
    return this.dataService.autocompleteDistrict(value)
      .pipe(
        map(resp => resp.filter(option => {
          return resp;
        }))
      )
  }

  displayUser(value?: AutocompleteUser): string | undefined {
    return value ? value.firstName + " " + value.lastName : undefined;
  }

  displaySchool(value?: AutocompleteSchool): string | undefined {
    return value ? value.name : undefined;
  }

  displayDistrict(value?: AutocompleteSchool): string | undefined {
    return value ? value.name : undefined;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSendMessage() {
    this.showSpinner = true;
    let sendToResult = '';
    switch (this.sendToOption) {
      case 1:
        sendToResult = this.userControl.value.id;
        break;
      case 2:
        sendToResult = this.schoolControl.value.id;
        break;
      case 3:
        sendToResult = this.districtControl.value.id;
        break;
      default:
        break;
    }

    let newMessage = new MessageDto(
      this.authService.user.profile.sub,
      this.sendToOption,
      this.messageBody,
      this.messageSubject, sendToResult);

      this.dataService.messageSendPost(newMessage)
        .subscribe(resp => {
          this.showSpinner = false;
          this.onNoClick();
        }, err => {
          this.showSpinner = false;
          console.log(err);
          this.onNoClick();
        })
  }

}

