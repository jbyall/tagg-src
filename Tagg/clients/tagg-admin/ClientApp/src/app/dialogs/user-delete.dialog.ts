import { Component, OnInit, Inject, EventEmitter, Output } from '@angular/core';
import { Router } from "@angular/router";
import { DataService } from '../services/data.service';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { SubjectDisplay, AdminJobStatus } from '../models/constants';

@Component({
  selector: 'user-delete-dialog',
  templateUrl: './user-delete.dialog.html',

})
export class UserDeleteDialog {
  @Output() notify: EventEmitter<string> = new EventEmitter();
  public showError: boolean;
  public showSuccess: boolean;

  constructor(
    public dialogRef: MatDialogRef<UserDeleteDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, private dataService: DataService, private router: Router) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onDeleteConfirm(){
      this.dataService.userDelete(this.data.user.id)
        .subscribe(resp =>{
            this.showSuccess = true;
        }, err => {this.showError = true;})
  }

}