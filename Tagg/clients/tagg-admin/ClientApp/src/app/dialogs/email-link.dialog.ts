import { Component, Inject, EventEmitter, Output, ViewChild, ElementRef } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
    selector: 'email-link-dialog',
    templateUrl: './email-link.dialog.html',

})
export class EmailLinkDialog {
    @ViewChild('linkbox') linkBox: ElementRef;
    @ViewChild('selBox') selBox: ElementRef;
    @Output() notify: EventEmitter<string> = new EventEmitter();

    constructor(
        public dialogRef: MatDialogRef<EmailLinkDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any) { }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onCopyLink(){
        this.selBox.nativeElement.select();
        document.execCommand('copy');
    }
}