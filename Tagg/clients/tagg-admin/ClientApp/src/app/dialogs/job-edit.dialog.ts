import { Component, OnInit, Inject, EventEmitter, Output } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from '../auth/auth.service';
import { DataService } from '../services/data.service';
import { User } from 'oidc-client';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { SubjectDisplay, AdminJobStatus } from '../models/constants';
import { Job } from '../models/job';

@Component({
  selector: 'job-edit-dialog',
  templateUrl: './job-edit.dialog.html',

})
export class JobEditDialog {
  @Output() notify: EventEmitter<string> = new EventEmitter();
  public subjects = SubjectDisplay;
  public jobStatus = AdminJobStatus;
  public error: boolean;
  public job: Job;

  constructor(
    public dialogRef: MatDialogRef<JobEditDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, private dataService: DataService, private router: Router) { 
    }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onCancelJob(jobId: string) {
    // this.dataService.cancelJob(jobId)
    //   .subscribe(resp => {
    //     this.onNoClick();
    //   }, err => this.error = true);

  }

}