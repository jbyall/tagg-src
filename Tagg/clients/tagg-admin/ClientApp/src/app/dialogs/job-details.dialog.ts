import { Component, OnInit, Inject, EventEmitter, Output } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from '../auth/auth.service';
import { DataService } from '../services/data.service';
import { User } from 'oidc-client';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { SubjectDisplay, AdminJobStatus } from '../models/constants';

@Component({
  selector: 'job-details-dialog',
  templateUrl: './job-details.dialog.html',

})
export class JobDetailsDialog {
  @Output() notify: EventEmitter<string> = new EventEmitter();
  public subjects = SubjectDisplay;
  public jobStatus = AdminJobStatus;
  public error: boolean;

  constructor(
    public dialogRef: MatDialogRef<JobDetailsDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, private dataService: DataService, private router: Router) { }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onCancelJob(jobId: string) {
    // this.dataService.cancelJob(jobId)
    //   .subscribe(resp => {
    //     this.onNoClick();
    //   }, err => this.error = true);

  }

}