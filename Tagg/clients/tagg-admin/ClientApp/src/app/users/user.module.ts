import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminGuard } from '../auth/admin.guard';
import { TaggCommonModule } from '../tagg-common.module';
import { UserListComponent } from './user-list.component';
import { UserCreateComponent } from './user-create.component';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            { path: '', component: UserListComponent, canActivate: [AdminGuard] },
            { path: 'incomplete', component: UserListComponent, data:{range: 'incomplete'}, canActivate: [AdminGuard] },
            { path: 'create', component: UserCreateComponent, canActivate: [AdminGuard] },
            { path: 'create/:type', component: UserCreateComponent, canActivate: [AdminGuard] },
        ]),
        TaggCommonModule
    ],
    declarations: [
        UserListComponent,
        UserCreateComponent
    ],
    entryComponents: [],
    providers: [
    ],
})
export class UserModule { }