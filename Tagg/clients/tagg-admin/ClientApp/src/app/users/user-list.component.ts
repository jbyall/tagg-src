import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UserDto } from '../models/user';
import { MatTableDataSource, MatPaginator, MatDialog, MatSnackBar } from '@angular/material';
import { UserTypesDisplay } from '../models/constants';
import { NewMessageDialog } from '../dialogs/new-message.dialog';
import { SendToOption } from '../models/enums';
import { UserDeleteDialog } from '../dialogs/user-delete.dialog';
import { EmailLinkDialog } from '../dialogs/email-link.dialog';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {
  public users: UserDto[];
  public dataSource = new MatTableDataSource();// new UserDataSource(this.dataService);
  public columns = ['firstName', 'lastName', 'email', 'type', 'phone', 'enabled'];
  public userTypes = UserTypesDisplay;
  public selectedUser: UserDto;

  @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(private dataService: DataService, private route: ActivatedRoute, private router: Router, private dialog: MatDialog, private snackbar: MatSnackBar) { }

  ngOnInit() {
    this.dataService.setHeaders();
    this.getUserList();
  }

  getUserList(existingSelected: string = null) {
    this.dataService.usersList()
      .subscribe(resp => {
        this.users = resp;
        this.dataSource = new MatTableDataSource(this.users);
        this.dataSource.paginator = this.paginator;
        if (this.users.length) {
          let selectUserId = existingSelected ? existingSelected : this.users[0].id;
          this.onSelectUser(selectUserId);
        }
      });
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLocaleLowerCase();
  }

  onSelectUser(userId: string) {
    let newSelected = this.users.find(u => u.id === userId);
    if (newSelected) {
      this.selectedUser = newSelected;
    }
  }

  onResendInvite(userId: string) {
    this.dataService.resendInvite(userId)
      .subscribe(resp => {
        this.openSnackbar("Invite Sent to " + this.selectedUser.email);
      }, err => {
        // TODO: Show error message
        this.openSnackbar("Unable to resend invite.");
      });
  }

  onGetInviteLink(userId: string) {
    console.log('userId', userId);
    this.dataService.inviteLinkGet(userId)
      .subscribe(resp => {
        this.dialog.open(EmailLinkDialog, {
          width: '800px',
          data: { link: resp }
        })
      }, err => {
        this.openSnackbar("Unable to get invite link for user.");
      });

  }

  onSendMessage() {
    this.dialog.open(NewMessageDialog, {
      width: '800px',
      data: {
        toId: this.selectedUser.id,
        toName: this.selectedUser.firstName + ' ' + this.selectedUser.lastName,
        toType: SendToOption.User
      }
    })
  }

  onLockout(userId: string) {
    this.dataService.userLockout(this.selectedUser.id)
      .subscribe(resp => {
        this.getUserList(this.selectedUser.id);
      });
  }

  onUnlock(userId: string) {
    this.dataService.userUnlock(this.selectedUser.id)
      .subscribe(resp => {
        this.getUserList(this.selectedUser.id);
      });
  }

  onDeleteDialog() {
    // this.dialog.open(UserDeleteDialog, {
    //   width: '800px',
    //   data: {
    //     user: this.selectedUser
    //   }
    // })
  }

  openSnackbar(message: string) {
    this.snackbar.open(message, 'Dismiss', {
      duration: 5000
    });
  }
}

// export class UserDataSource extends MatTableDataSource<any>{
//   public users: UserDto[];
//   public retVal: Observable<UserDto[]>;

//   constructor(private dataService: DataService) {super();}

//   connect(): Observable<UserDto[]>{
//     return this.dataService.usersList().map(resp => {
//       this.users = resp;
//       return this.users;
//     })
//   }

//   disconnect() { }
// }
