import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import { UserCreateModel } from '../models/user';
import { FormGroup, AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { UserTypesSelect } from '../models/constants';
import { SelectDto } from '../models/dto';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-user-create',
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.css']
})
export class UserCreateComponent implements OnInit {
  public user: UserCreateModel;
  public userForm: FormGroup;
  public showSuccess: boolean;

  public typeSelect = UserTypesSelect;
  public userTypeParam: number;

  public requireSchool: boolean;
  public requireDistrict: boolean;

  public schoolSelect: SelectDto[];
  public schoolOrDistrictId: string;

  public districtSelect: SelectDto[];

  public showForm: boolean;
  public showUpdateError: boolean;

  constructor(private dataService: DataService, private route: ActivatedRoute, private router: Router, public fb: FormBuilder, private sb: MatSnackBar) {
    this.user = new UserCreateModel();
  }


  get firstName(): AbstractControl { return this.userForm.get('firstName') };
  get lastName(): AbstractControl { return this.userForm.get('lastName') };
  get phoneNumber(): AbstractControl { return this.userForm.get('phoneNumber') };
  get email(): AbstractControl { return this.userForm.get('email') };

  ngOnInit() {
    this.dataService.setHeaders();
  }

  onTypeSelected() {
    if (this.userForm) {
      this.hideForm();
    }

    switch (this.userTypeParam) {
      // sub
      case 0:
        this.requireDistrict = false;
        this.requireSchool = false;
        this.populateForm();
        break;
      // teacher
      case 1:
      // school admin
      case 2:
        this.dataService.schoolSelectGet()
          .subscribe(resp => {
            this.schoolSelect = resp;
            this.requireDistrict = false;
            this.requireSchool = true;
          });
        break;
      // district admin
      case 3:
        this.dataService.districtSelectGet()
          .subscribe(resp => {
            this.districtSelect = resp;
            this.requireSchool = false;
            this.requireDistrict = true;
          });
        break;
      // sys admin
      case 4:
      default:
        this.requireDistrict = false;
        this.requireSchool = false;
        this.populateForm();
        break;
    }
  }

  onSchoolSelected() {
    this.populateForm();
  }

  onSelectDistrict() {
    this.populateForm();
  }

  populateForm() {
    this.userForm = this.fb.group({
      firstName: [this.user.firstName, Validators.required],
      lastName: [this.user.lastName, Validators.required],
      email: [this.user.email, [Validators.required, Validators.email]],
      phoneNumber: this.user.phoneNumber,
    });
    this.showForm = true;
  }

  hideForm() {
    this.showForm = false;
    this.userForm = null;
    this.schoolOrDistrictId = null;
  }

  onSubmit() {
    if (this.userForm.dirty && this.userForm.valid) {
      this.user.email = this.email.value;
      this.user.firstName = this.firstName.value;
      this.user.lastName = this.lastName.value;
      this.user.userType = this.userTypeParam;
      this.user.phoneNumber = this.phoneNumber.value;
      this.user.schoolId = this.schoolOrDistrictId;

      this.dataService.createUser(this.user)
        .subscribe(resp =>{
          this.openSnackbar("User created successfully");
          this.router.navigate(['/users']);
        })
    }
  }

  openSnackbar(message: string){
    this.sb.open(message, 'Dismiss');
  }

}
