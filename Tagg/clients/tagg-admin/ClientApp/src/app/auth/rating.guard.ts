import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate,Router } from '@angular/router';

import { AuthService } from './auth.service';
import { DataService } from '../services/data.service';
import { Observable } from 'rxjs/Observable';
import { Job } from '../models/job';

@Injectable()
export class RatingGuard implements CanActivate {
    public unratedJobs: Job[];

    constructor(private authService: AuthService, private router: Router, private dataService: DataService) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if(this.authService.user.profile.api_access !== '0x2' || !this.dataService.jobsForReview.length){
            console.log('ratingGuard: returning true');
            return true;
        }
        else{
            console.log('ratingGuard: jobs before pop', this.dataService.jobsForReview);
            let jobToReview = this.dataService.jobsForReview.pop();
            console.log('ratingGuard: jobs after pop', this.dataService.jobsForReview);
            this.router.navigate(['/rate', jobToReview]);
            return false;
        }
    }

    
}
