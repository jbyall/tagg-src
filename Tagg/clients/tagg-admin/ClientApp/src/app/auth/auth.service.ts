// For dependency injection
import { Injectable } from '@angular/core';

// For calls to API/web service
import { UserManager, UserManagerSettings, User } from 'oidc-client';
import * as jwtModels from 'angular2-jwt';
import { environment } from '../../environments/environment';
import { IUser } from '../models/user';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AuthService {
    private manager = new UserManager(getClientSettings());
    public user: User = null;

    public userProfile: IUser;
    public redirectUrl: string;
    jwtHelper: jwtModels.JwtHelper = new jwtModels.JwtHelper();

    private _userLoaded = new Subject<boolean>();

    constructor() {
        this.manager.getUser().then(user => {
            this.user = user;
        });

        this.manager.events.addUserLoaded(test => {
            this.setUser();
        });
    }

    userLoaded$ = this._userLoaded.asObservable();

    public setRedirect(redirect: string) {
        localStorage.setItem("TaggSchoolRedirect", redirect);
    }

    isLoggedIn(): boolean {
        return this.user != null && !this.user.expired;
    }

    getClaims(): any {
        return this.user.profile;
    }

    getAuthorizationHeaderValue(): string {
        if (this.user) {
            return `${this.user.token_type} ${this.user.access_token}`;
        }
        return '';
    }

    startAuthentication(): Promise<void> {
        return this.manager.signinRedirect();
    }

    completeAuthentication(): Promise<void> {
        //let redirect = localStorage.getItem("TaggSchoolRedirect");
        return this.manager.signinRedirectCallback().then(user => {
            this.user = user;
        });
    }

    signOut(): Promise<void> {
        return this.manager.signoutRedirect({ id_token_hint: this.user.id_token })
            .then(resp => {
                this.manager.signoutRedirectCallback
            });
    }

    public setUser(){
        this.manager.getUser().then(user => {
            this.user = user;
            this._userLoaded.next(true);
        });
    }

    // isLoggedIn(): boolean {
    //     return this.setUser();
    // }

    getInviteData(token: string): IUser {
        var decodedToken = this.jwtHelper.decodeToken(token);
        let result = new IUser();
        result.email = decodedToken.email;
        return result;
    }

    getUserFromToken(token: string): IUser {
        var decodedToken = this.jwtHelper.decodeToken(token);
        let result = new IUser
        result.email = decodedToken.email;
        result.firstName = decodedToken.given_name;
        result.lastName = decodedToken.family_name;
        result.id = decodedToken.sub;
        result.phoneNumber = decodedToken.phone_number;
        result.school = decodedToken.school;
        result.apiAccess = decodedToken.api_access;
        return result;
    }

}

export function getClientSettings(): UserManagerSettings {
    //var authority = environment.
    return {
        authority: environment.stsAuthority,
        client_id: environment.stsClientId,
        redirect_uri: environment.stsRedirectUri,
        response_type: environment.stsResponseType,
        scope: environment.stsScope,
        loadUserInfo: true,
        post_logout_redirect_uri: environment.logoutRedirect,
        automaticSilentRenew: true,
        silent_redirect_uri: environment.silentRefreshUrl

        //filterProtocolClaims: true,
        //

        //post_logout_redirect_uri: 'http://localhost:4200/',

        //automaticSilentRenew: true,
        //includeIdTokenInSilentRenew
        //accessTokenExpiringNotificationTime
        //popupWindowFeatures
    };
}
