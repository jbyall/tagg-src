import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, CanLoad, Router, Route } from '@angular/router';

import { AuthService } from './auth.service';

@Injectable()
export class AdminGuard implements CanActivate {

    constructor(private authService: AuthService,
        private router: Router) { }

        canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
            if(this.authService.isLoggedIn() && this.authService.user.profile.api_access === '0x4'){
                return true;
            }
            else{
                localStorage.setItem('taggRedirect', state.url);
                this.authService.startAuthentication();
                return false;
            }
            
        }
}
