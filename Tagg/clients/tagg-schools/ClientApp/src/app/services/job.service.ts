import { Injectable } from '@angular/core';
import * as Models from "../shared/models";
import { DataService } from './data.service';
import { AuthService } from '../auth/auth.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from '../../../node_modules/rxjs/Observable';
import { Job } from '../shared/models';


@Injectable()
export class JobService {
    public job: Models.Job;
    public search: Models.SubSearch;
    public lastQuery: Date;

    public jobs: Models.Job[];
    public recentJobs: Models.Job[];
    public upcomingJobs: Models.Job[];

    private _jobsReady = new Subject<boolean>();

    constructor(private dataService: DataService, private authService: AuthService) { }

    jobsReady$ = this._jobsReady.asObservable();

    public getJobs(userType: string) {
        let now = new Date();
        if (!this.lastQuery || (now.getTime() - this.lastQuery.getTime()) > 60000) {
            switch (this.authService.user.profile.api_access) {
                case "0x2":
                    this.getTeacherJobs();
                    break;
                case "0x3":
                    this.getSchoolJobs();
                    break;
                case "0x33":
                    this.getSchoolJobs();
                    break;
            }
            this.lastQuery = new Date();
        }
        else{
            this._jobsReady.next(true);
        }
    }

    getTeacherJobs() {
        this.dataService.getTeacherJobs(this.authService.user.profile.sub, 5)
            .subscribe(resp => {
                this.jobs = resp;
                this.splitJobs();
                this._jobsReady.next(true);
            }, err =>{ })
    }

    getSchoolJobs() {
        this.dataService.getSchoolJobs(this.authService.user.profile.school, new Date(), 5)
            .subscribe(resp => {
                this.jobs = resp.upcoming;
                this.recentJobs = resp.recent;
                this.upcomingJobs = resp.upcoming;
                this.upcomingJobs = this.upcomingJobs.concat(resp.today);
                this._jobsReady.next(true);
            })
    }

    splitJobs() {
        if (this.jobs) {
            this.recentJobs = this.jobs.filter(j => j.status == Models.JobStatus.Complete);
            this.upcomingJobs = this.jobs.filter(j => j.status == Models.JobStatus.Accepted || j.status == Models.JobStatus.Open);
        }
    }

    getJobDetails(jobId: string): Observable<Job>{
        return this.dataService.getJobDetails(jobId);
    }


}