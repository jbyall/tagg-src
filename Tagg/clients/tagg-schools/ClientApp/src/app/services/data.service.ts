import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpErrorResponse, HttpResponse, HttpHeaders } from '@angular/common/http';

import { UserManager, UserManagerSettings, User } from 'oidc-client';
import * as Models from "../shared/models";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { RequestOptions } from '@angular/http';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class DataService {
  baseUrl = environment.apiUrl;

  // Registration endpoints
  private _locationCheckUrl = environment.apiUrl + "location/check/";
  private _getSubjectsUrl = this.baseUrl + "lookup/subjects";
  private _getStatesUrl = this.baseUrl + "lookup/states";
  private _registerPhoneUrl = this.baseUrl + "register/phone/verify/";
  private _registerEmailUrl = this.baseUrl + "register/email/confirm/";
  private _registerUpdateUrl = this.baseUrl + "register/update/";
  private _userPictureUpdateUrl = environment.apiUrl + "users/photo/";
  // School endpoints
  private _schoolUpdateUrl = this.baseUrl + "schools/update/";
  private _schoolGetUrl = this.baseUrl + "schools/";
  private _schoolInquiryUrl = this.baseUrl + "schools/inquiry";
  private _schoolsAdminGetUrl = environment.apiUrl + "schools/user";
  private _schoolCreateUrl = this.baseUrl + "schools/create";
  private _schoolDetailsUrl = this.baseUrl + "schools/details/";
  private _schoolAdminsGetUrl = this.baseUrl + "schools/admin/";
  private _adminEnableUrl = this.baseUrl + "schools/admin/enable/";
  private _adminDisableUrl = this.baseUrl + "schools/admin/disable/";
  private _schoolsSummaryAdminGet = this.baseUrl + "schools/admin/summary/";

  // Teacher endpoints
  private _teachersUploadUrl = this.baseUrl + "teachers/upload/";
  private _teachersSingleUrl = this.baseUrl + "teachers/";
  private _teachersAddUrl = this.baseUrl + "teachers/create/";
  private _teachersUpdateUrl = this.baseUrl + "teachers/update";
  private _teachersAddFavoriteUrl = this.baseUrl + "teachers/favorite/";
  private _teacherDetailsUrl = this.baseUrl + "teachers/details/";
  private _teacherUpdatePayUrl = this.baseUrl + "teachers/pay/";

  //Job endpoints
  private _jobsGetUrl = environment.apiUrl + "jobs/";
  private _jobGetDetailsUrl = environment.apiUrl + "jobs/details/";
  private _jobsSchoolUrl = this.baseUrl + "jobs/school/";
  private _jobsAdminUrl = this.baseUrl + "jobs/admin/";
  private _jobsTeacherUrl = this.baseUrl + "jobs/teacher/";
  private _jobCreateUrl = this.baseUrl + "jobs/create";
  private _jobOfferUrl = this.baseUrl + "jobs/offer";
  private _jobCancelUrl = this.baseUrl + "jobs/cancel/";
  private _jobLessonPlanUrl = this.baseUrl + "jobs/lessonplan/";
  private _jobsUnratedGetUrl = environment.apiUrl + "jobs/teacher/unrated/";
  private _jobsAddSubRatingUrl = environment.apiUrl + "jobs/rate/sub";

  // Calendar
  private _calendarAdminGet = environment.apiUrl + "calendar/admin/";
  private _calendarTeacherGet = this.baseUrl + "calendar/teachers/";
  public _calendarBlackoutAdd = this.baseUrl + "calendar/blackout/add";
  public _calendarBlackoutRemove = this.baseUrl + "calendar/blackout/remove";


  // Metrics
  private _getTeacherMetricsUrl = this.baseUrl + "metrics/teacher/";
  private _getSchoolMetricsUrl = this.baseUrl + "metrics/school/";
  private _getDistrictMetricsUrl = this.baseUrl + "metrics/district/";

  // Sub endpoints
  private _subSearchUrl = this.baseUrl + "subs/search";
  private _subSchoolSearchUrl = this.baseUrl + "subs/search/school/";
  private _subTeacherSearchUrl = this.baseUrl + "subs/search/teacher/";
  private _getSubUrl = this.baseUrl + "subs/";
  private _subExclude = this.baseUrl + "subs/exclude/";
  private _subEnable = this.baseUrl + "subs/exclude/remove/";
  private _subResumeGetUrl = environment.apiUrl + "subs/resume/";

  // Customer endpoints
  private _customerCreateUrl = this.baseUrl + "customers";
  private _customerBankVerifyUrl = this.baseUrl + "customers/verify";
  private _customerTransactionsUrl = this.baseUrl + "customers/transactions/";
  private _customerCreateChargeUrl = this.baseUrl + "customers/charge/";
  private _transactionCancelPut = this.baseUrl + "customers/transactions/cancel/";

  // messages
  private _messageGetTokenUrl = environment.apiUrl + "messages/token";
  private _messageCreateChannelUrl = environment.apiUrl + "messages/channel/job/";

  // users
  private _userCreateUrl = environment.apiUrl + "users/create";
  private _userGetUrl = environment.apiUrl + "users/";
  private _userUpdateUrl = environment.apiUrl + "users/";
  private _userChangeEmail = environment.apiUrl + "users/email/";
  private _userResendInvite = this.baseUrl + "users/invite/resend/";

  public authHeaders = new HttpHeaders();
  public jobsForReview: string[] = [];

  constructor(private http: HttpClient, private authService: AuthService) {
    this.authService.userLoaded$
      .subscribe(resp => {
        this.setHeaders();
      })
  }

  public setHeaders() {
    this.authHeaders = this.authHeaders.set('Authorization', this.authService.getAuthorizationHeaderValue());
  }

  public addJobsForReview(jobIds: string[]): void {
    this.jobsForReview = this.jobsForReview.concat(jobIds);
  }

  public removeJob(jobId: string): void {
    var index = this.jobsForReview.indexOf(jobId);
    this.jobsForReview = this.jobsForReview.splice(index, 1);
  }


  // Registration
  public checkLocation(postalCode: number): Observable<Models.DistanceResult[]> {
    return this.http.get<Models.DistanceResult[]>(this._locationCheckUrl + postalCode.toString())
  }
  public getSubjects(): Observable<Models.Lookup[]> {
    return this.http.get<Models.Lookup[]>(this._getSubjectsUrl);
  }
  public getStates(): Observable<Models.Lookup[]> {
    return this.http.get<Models.Lookup[]>(this._getStatesUrl, { headers: this.authHeaders });
  }
  public sendPhoneCode(number: string): Observable<string> {
    return this.http.get<string>(this._registerPhoneUrl + number, { headers: this.authHeaders });
  }
  public verifyCode(code: string, phoneNumber: string): Observable<string> {
    return this.http.post<string>(this._registerPhoneUrl, { "code": code, "phoneNumber": phoneNumber }, { headers: this.authHeaders });
  }
  public requestEmailVerification(email: string): Observable<string> {
    return this.http.get<string>(this._registerEmailUrl + email);
  }
  public register(model: Models.RegisterModel): Observable<boolean> {
    return this.http.post<boolean>(this._registerUpdateUrl, model);
  }
  public uploadPicture(data: any): Observable<string> {
    return this.http.post<string>(this._userPictureUpdateUrl + this.authService.user.profile.sub, data, { headers: this.authHeaders });
  }


  // School
  public updateSchool(school: Models.School): Observable<string> {
    return this.http.post<string>(this._schoolUpdateUrl + school.id, school, { headers: this.authHeaders });
  }
  public getSchool(id: string): Observable<Models.School> {
    return this.http.get<Models.School>(this._schoolGetUrl + id, { headers: this.authHeaders })
  }
  public sendInquiry(inquiry: Models.SchoolInqury): Observable<string> {
    return this.http.post<string>(this._schoolInquiryUrl, inquiry);
  }
  public getAdminSchools(): Observable<Models.SchoolDetail[]> {
    return this.http.get<Models.SchoolDetail[]>(this._schoolsAdminGetUrl, { headers: this.authHeaders });
  }
  public createSchool(school: Models.School): Observable<any> {
    return this.http.post<string>(this._schoolCreateUrl, school, { headers: this.authHeaders });
  }
  public getSchoolDetails(schoolId: string): Observable<Models.SchoolDetail> {
    return this.http.get<Models.SchoolDetail>(this._schoolDetailsUrl + schoolId, { headers: this.authHeaders });
  }
  public getSchoolAdmins(schoolId: string): Observable<Models.SchoolAdmin[]> {
    return this.http.get<Models.SchoolAdmin[]>(this._schoolAdminsGetUrl + schoolId, { headers: this.authHeaders });
  }
  public adminEnable(schoolId: string, adminId: string): Observable<string> {
    return this.http.put<string>(this._adminEnableUrl + schoolId + '/' + adminId, null, { headers: this.authHeaders });
  }
  public adminDisable(schoolId: string, adminId: string): Observable<string> {
    return this.http.put<string>(this._adminDisableUrl + schoolId + '/' + adminId, null, { headers: this.authHeaders });
  }
  public schoolsSummaryAdminGet(userId: string): Observable<Models.SimpleEntityDto[]> {
    return this.http.get<Models.SimpleEntityDto[]>(this._schoolsSummaryAdminGet + userId, { headers: this.authHeaders });
  }


  // Teachers
  public uploadTeachers(schoolId: string, teachers: File): Observable<string> {
    let formData: FormData = new FormData();
    formData.append('file', teachers, teachers.name);
    return this.http.post<string>(this._teachersUploadUrl + schoolId, formData, { headers: this.authHeaders });
  }
  public getTeacherProfile(teacherId: string): Observable<Models.Teacher> {
    return this.http.get<Models.Teacher>(this._teachersSingleUrl + teacherId, { headers: this.authHeaders });
  }
  public updateTeacher(teacher: Models.Teacher): Observable<Models.TeacherUpdateResult> {
    return this.http.post<Models.TeacherUpdateResult>(this._teachersUpdateUrl, teacher, { headers: this.authHeaders });
  }
  public addFavorite(teacherId: string, subId: string): Observable<string> {
    return this.http.get<string>(this._teachersAddFavoriteUrl + `${teacherId}/${subId}`, { headers: this.authHeaders });
  }
  public getTeacherDetails(teacherId: string): Observable<Models.TeacherDetails> {
    return this.http.get<Models.TeacherDetails>(this._teacherDetailsUrl + teacherId, { headers: this.authHeaders });
  }
  public updateTeacherPayRate(teacherId: string, payRate: number): Observable<any> {
    return this.http.put<any>(this._teacherUpdatePayUrl + teacherId + '/' + payRate, null, { headers: this.authHeaders });
  }


  // Jobs
  public getJob(jobId: string): Observable<Models.Job> {
    return this.http.get<Models.Job>(this._jobsGetUrl + jobId, { headers: this.authHeaders });
  }
  public getJobDetails(jobId: string): Observable<Models.Job> {
    return this.http.get<Models.Job>(this._jobGetDetailsUrl + jobId, { headers: this.authHeaders });
  }
  public getSchoolJobs(schoolId: string, date: Date, limit: number = 100): Observable<Models.SchoolJobsResult> {
    return this.http.get<Models.SchoolJobsResult>(this._jobsSchoolUrl + schoolId + '/' + date.toUTCString() + '/' + limit, { headers: this.authHeaders });
  }

  public getAdminJobs(): Observable<Models.SchoolJobsResult>{
    return this.http.get<Models.SchoolJobsResult>(this._jobsAdminUrl + '/1/2', {headers: this.authHeaders});
  }
  
  public getTeacherJobs(teacherId: string, limit: number = 100): Observable<Models.Job[]> {
    return this.http.get<Models.Job[]>(this._jobsTeacherUrl + teacherId + '/' + limit, { headers: this.authHeaders });
  }
  public uploadLessonPlan(jobId: string, data: any): Observable<string> {
    return this.http.post<string>(this._jobLessonPlanUrl + jobId, data, { headers: this.authHeaders });
  }
  public getLessonPlan(lessonPlanId: string): Observable<Blob> {
    return this.http.get(this._jobLessonPlanUrl + lessonPlanId, { headers: this.authHeaders, responseType: 'blob' });
  }

  public createJob(job: Models.Job): Observable<string> {
    return this.http.post<string>(this._jobCreateUrl, job, { headers: this.authHeaders });
  }

  public createOffer(offer: Models.JobOfferModel): Observable<string> {
    return this.http.post<string>(this._jobOfferUrl, offer, { headers: this.authHeaders });
  }

  public cancelJob(jobId: string): Observable<boolean> {
    return this.http.put<boolean>(this._jobCancelUrl + jobId, null, { headers: this.authHeaders });
  }
  // public getUnratedJobs(teacherId: string): Observable<Models.Job[]> {
  //   return this.http.get<Models.Job[]>(this._jobsUnratedGetUrl + teacherId, { headers: this.authHeaders });
  // }
  public getUnratedJobIds(teacherId: string): Observable<string[]> {
    return this.http.get<string[]>(this._jobsUnratedGetUrl + teacherId, { headers: this.authHeaders });
  }
  public addJobRating(rating: Models.JobRating): Observable<boolean> {
    return this.http.post<boolean>(this._jobsAddSubRatingUrl, rating, { headers: this.authHeaders });
  }

  // Dash
  public getTeacherCalendar(month: number, year: number): Observable<Models.CalendarDay[]> {
    return this.http.get<Models.CalendarDay[]>(this._calendarTeacherGet + month + "/" + year, { headers: this.authHeaders });
  }

  public calendarAdminGet(month: number, year: number): Observable<Models.CalendarDay[]> {
    return this.http.get<Models.CalendarDay[]>(this._calendarAdminGet + month + "/" + year, { headers: this.authHeaders });
  }

  public calendarBlackoutAdd(dto: Models.BlackoutAddDto): Observable<boolean> {
    return this.http.post<boolean>(this._calendarBlackoutAdd, dto, { headers: this.authHeaders });
  }
  public calendarBlackoutRemove(dto: Models.BlackoutAddDto): Observable<boolean> {
    return this.http.post<boolean>(this._calendarBlackoutRemove, dto, { headers: this.authHeaders });
  }

  // Subs
  public searchSubs(searchModel: Models.SubSearch): Observable<Models.SubSearchResult> {
    return this.http.post<Models.SubSearchResult>(this._subSearchUrl, searchModel, { headers: this.authHeaders });
  }
  public getSubsForSchool(schoolId: string): Observable<Models.Substitute[]> {
    return this.http.get<Models.Substitute[]>(this._subSchoolSearchUrl + schoolId, { headers: this.authHeaders });
  }
  public getSubsForTeacher(teacherId: string, schoolId: string): Observable<Models.Substitute[]> {
    return this.http.get<Models.Substitute[]>(this._subTeacherSearchUrl + teacherId + '/' + schoolId, { headers: this.authHeaders });
  }
  public subExclude(subId: string, schoolId: string): Observable<boolean> {
    return this.http.get<boolean>(this._subExclude + subId + '/' + schoolId, { headers: this.authHeaders });
  }
  public subEnable(subId: string, schoolId: string): Observable<boolean> {
    return this.http.get<boolean>(this._subEnable + subId + '/' + schoolId, { headers: this.authHeaders });
  }
  public getResume(resumeId: string): Observable<Blob> {
    //let customHeaders = this.authHeaders;
    return this.http.get(this._subResumeGetUrl + resumeId, { headers: this.authHeaders, responseType: 'blob' });
  }


  // Customer
  public createCustomer(customer: Models.CustomerCreateModel): Observable<string> {
    return this.http.post<string>(this._customerCreateUrl, customer, { headers: this.authHeaders });
  }
  public verifySchoolBank(model: Models.BankVerifyModel): Observable<string> {
    return this.http.post<string>(this._customerBankVerifyUrl, model, { headers: this.authHeaders });
  }
  public getTransactions(customerId: string): Observable<Models.Transaction[]> {
    return this.http.get<Models.Transaction[]>(this._customerTransactionsUrl + customerId, { headers: this.authHeaders });
  }
  public createCharge(transactionId: string): Observable<string> {
    return this.http.get<string>(this._customerCreateChargeUrl + transactionId, { headers: this.authHeaders });
  }
  public transactionCancel(transactionId: string): Observable<boolean> {
    return this.http.put<boolean>(this._transactionCancelPut + transactionId, null, { headers: this.authHeaders });
  }

  // messages
  public getChatToken(): Observable<any> {
    return this.http.get<any>(this._messageGetTokenUrl, { headers: this.authHeaders });
  }
  public getOrCreateJobChannel(jobId: string): Observable<string> {
    return this.http.post<string>(this._messageCreateChannelUrl + jobId, null, { headers: this.authHeaders });
  }

  // metrics
  public getTeacherMetrics(teacherId: string): Observable<Models.TeacherMetrics> {
    return this.http.get<Models.TeacherMetrics>(this._getTeacherMetricsUrl + teacherId, { headers: this.authHeaders });
  }

  public getSchoolMetrics(schoolId: string): Observable<Models.SchoolMetrics> {
    return this.http.get<Models.SchoolMetrics>(this._getSchoolMetricsUrl + schoolId, { headers: this.authHeaders });
  }

  // Note - schoolId is passed here, because the school is used to determine the district on the back-end
  public getDistrictMetrics(schoolId: string): Observable<Models.DistrictMetrics> {
    return this.http.get<Models.DistrictMetrics>(this._getDistrictMetricsUrl + schoolId, { headers: this.authHeaders });
  }

  // Users
  public createUser(user: Models.UserCreateModel): Observable<string> {
    return this.http.post<string>(this._userCreateUrl, user, { headers: this.authHeaders });
  }

  public getUser(userId: string): Observable<Models.IUser> {
    return this.http.get<Models.IUser>(this._userGetUrl + userId, { headers: this.authHeaders });
  }

  public updateUser(user: Models.IUser): Observable<Models.IUser> {
    return this.http.post<Models.IUser>(this._userUpdateUrl, user, { headers: this.authHeaders });
  }

  public userChangeEmail(currentEmail: string, newEmail: string): Observable<string> {
    return this.http.put<string>(this._userChangeEmail + currentEmail + '/' + newEmail, null, { headers: this.authHeaders });
  }
  public resendInvite(userId: string): Observable<string>{
    return this.http.get<string>(this._userResendInvite + userId, {headers: this.authHeaders});
  }


  // Error handler
  private handleError(error: Response): Observable<any> {
    console.error(error);
    return Observable.throw(error || "Server error");
  }

}
