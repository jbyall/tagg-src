import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as Models from "../shared/models";
import { ContactMethods } from '../shared/constants';
import { MatSnackBar, MatDialog } from '@angular/material';
import { environment } from '../../environments/environment.prod';
import { PhoneDialog } from './dialogs/phone.dialog';

@Component({
  selector: 'app-teacher-settings',
  templateUrl: './teacher-settings.component.html',
  styleUrls: ['./teacher-settings.component.css']
})
export class TeacherSettingsComponent implements OnInit {
  settingsForm: FormGroup;
  public currentUser: Models.IUser;
  public teacher: Models.Teacher;
  public contact = ContactMethods;
  public showForm: boolean;
  public passwordReset = environment.passwordResetUrl;
  public selectedContact: number[] =[];

  constructor(public _authService: AuthService, private _router: Router, private _dataService: DataService, private fb: FormBuilder, public snackBar: MatSnackBar, public dialog: MatDialog) {
    this.currentUser = _authService.userProfile;
    this._dataService.setHeaders();
  }

  ngOnInit() {
    this.getProfile();
  }

  getProfile(){
    this._dataService.getTeacherProfile(this._authService.user.profile.sub)
      .subscribe(resp => {
        this.teacher = resp;
        this.populateForm();
      }, err => {  })
  }

  populateForm() {
    this.settingsForm = this.fb.group({
      firstName: this.teacher.firstName,
      lastName: this.teacher.lastName,
      email: this.teacher.email,
      phoneNumber: this.teacher.phoneNumber,
    });

    if(this.teacher.receiveEmail){
      this.selectedContact.push(0);
    }
    if(this.teacher.receiveSMS){
      this.selectedContact.push(1);
    }
    if(this.teacher.receivePhone){
      this.selectedContact.push(2);
    }
    if(!this.teacher.receiveEmail && !this.teacher.receivePhone && !this.teacher.receiveSMS){
      this.openPhoneDialog();
    }
    if(this.teacher.receiveSMS && !this.teacher.phoneVerified){
      this.openPhoneDialog();
    }
    // if(!this.selectedContact.length){
      
    // }

    this.showForm = true;
  }

  onSubmit(){
    let r = this.settingsForm.value;
    this.teacher.firstName = r.firstName;
    this.teacher.lastName = r.lastName;
    this.teacher.phoneNumber = r.phoneNumber;

    this.teacher.receiveEmail = this.selectedContact.includes(0);
    this.teacher.receiveSMS = this.selectedContact.includes(1);
    this.teacher.receivePhone = this.selectedContact.includes(2);
  
    this.settingsForm.markAsPristine();

    this._dataService.updateTeacher(this.teacher)
      .subscribe(resp =>{
        this.teacher = resp.teacher;
        if(resp.verificationRequired){
          //this._router.navigate(['/verify-phone', resp.phoneNumber]);
          this.openPhoneDialog();
        }
        else{
          this.showSnackbar('Settings updated.');
        }
      }, err => this.showSnackbar('Unable to update your profile.'))
  }

  markDirty(){
    this.settingsForm.markAsDirty();
  }

  onChangeEmail(){
    this._router.navigate(['/change-email']);
  }

  openPhoneDialog(){
    let width = window.innerWidth < 992 ? '80%' : '600px';
    let dialogRef = this.dialog.open(PhoneDialog, {
      width: width,
      data: { teacher: this.teacher }
    });

    dialogRef.afterClosed().subscribe(result => {
      window.location.reload();
    });
  }
  

  showSnackbar(message: string){
    this.snackBar.open(message, 'Dismiss',{
      duration: 5000
    });
  }
}
