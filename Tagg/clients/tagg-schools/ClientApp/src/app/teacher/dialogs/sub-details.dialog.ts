import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Component, Inject, OnInit } from "@angular/core";
import { AuthService } from "../../auth/auth.service";
import { DataService } from "../../services/data.service";
import { Router } from "@angular/router";
import { environment } from "../../../environments/environment";

@Component({
    selector: 'sub-details-dialog',
    templateUrl: 'sub-details.dialog.html',
    styleUrls:['./sub-details.dialog.css']
  })
  export class SubDetailsDialog implements OnInit{
    public photos: string = environment.userPhotoEndpoint;
    public educationLevel: string = "";

    constructor(
      public dialogRef: MatDialogRef<SubDetailsDialog>,
      @Inject(MAT_DIALOG_DATA) public data: any, private _router: Router, public dataService: DataService, public _authService: AuthService) {
      this._router.events.subscribe(() => { this.onNoClick() });
    }

    ngOnInit(){
      let edArray = this.data.chosenSub.educationLevel.split(/(?=[A-Z])/);
      for(let i=0; i<edArray.length; i++){
        this.educationLevel += edArray[i] + " ";
      }
    }
  
    onNoClick(): void {
      this.dialogRef.close();
    }

    onDownloadResume(){
      this.dataService.getResume(this.data.chosenSub.resumeFile)
          .subscribe(resp => {
              window.open(window.URL.createObjectURL(resp));
          }, err => {});
  }

  onLinkedIn(){
    window.open(this.data.chosenSub.linkedInUrl);
  }
  
  }