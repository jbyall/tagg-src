import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Component, Inject, OnInit } from "@angular/core";
import { AuthService } from "../../auth/auth.service";
import { DataService } from "../../services/data.service";
import { Router } from "@angular/router";
import { environment } from "../../../environments/environment";
import { ContactMethods } from "../../shared/constants";
import { Teacher } from "../../shared/models";

@Component({
    selector: 'phone-dialog',
    templateUrl: 'phone.dialog.html',
    styleUrls: ['./phone.dialog.css']
})
export class PhoneDialog implements OnInit {
    public photos: string = environment.userPhotoEndpoint;
    public contactMethods = ContactMethods;
    public selectedContact: number[] = [];
    public phone: string;
    public code: string;
    public step: number = 1;
    

    constructor(
        public dialogRef: MatDialogRef<PhoneDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any, private _router: Router, public dataService: DataService, public _authService: AuthService) {
        this._router.events.subscribe(() => { this.onNoClick() });
    }

    ngOnInit() {
        if (this.data.teacher.receiveEmail) {
            this.selectedContact.push(0);
        }
        if (this.data.teacher.receiveSMS) {
            this.selectedContact.push(1);
        }
        if (this.data.teacher.receivePhone) {
            this.selectedContact.push(2);
        }
        if (!this.selectedContact.length) {
            this.selectedContact = [0, 1, 2];
        }
        this.phone = this.data.teacher.phoneNumber;
    }

    onSave() {
        this.data.teacher.receiveEmail = this.selectedContact.includes(0);
        this.data.teacher.receiveSMS = this.selectedContact.includes(1);
        this.data.teacher.receivePhone = this.selectedContact.includes(2);
        this.dataService.updateTeacher(this.data.teacher)
            .subscribe(resp => {
                if (this.selectedContact.includes(1)) {
                    this.step++;
                }
                else {
                    this.onNoClick();
                }
            })
    }

    onSendCode(){
        this.dataService.sendPhoneCode(this.phone)
            .subscribe(resp => {
                this.step++;
            })
    }

    onVerificationCode(){
        this.dataService.verifyCode(this.code, this.phone)
            .subscribe(resp => {
                this.onNoClick();
            })
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

}