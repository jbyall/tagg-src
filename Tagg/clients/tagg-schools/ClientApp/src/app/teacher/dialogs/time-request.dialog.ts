import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import * as Models from "../../shared/models";
import { DataService } from "../../services/data.service";
import { AuthService } from "../../auth/auth.service";
import { Router } from "@angular/router";
import { JobService } from "../../services/job.service";
import { Inject, Component } from "@angular/core";
import { Subjects } from "../../shared/constants";

@Component({
  selector: 'time-request-dialog',
  templateUrl: './time-request.dialog.html',
})
export class TimeRequestDialog {
  public school: Models.School;
  public today: Date = new Date();
  public job: Models.Job;
  public error: boolean = false;
  public durationSelection = 0;
  public startTime: Models.TimeSpan = new Models.TimeSpan();
  public endTime: Models.TimeSpan = new Models.TimeSpan();
  public subjectDisplay: string;

  constructor(
    public dialogRef: MatDialogRef<TimeRequestDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, private dataService: DataService, private jobService: JobService, private _router: Router, private authService: AuthService) {
    this.dataService.setHeaders();
    this.job = new Models.Job();
    this.dataService.getSchoolDetails(this.data.schoolId)
      .subscribe(resp => {
        this.school = resp.schoolBase;
        this.job.teacherName = this.data.teacher.firstName;
        this.job.teacherEmail = this.data.teacher.email;
        this.job.subject = this.data.teacher.subjects[0];
        this.job.startDate = this.data.date;
        this.job.endDate = this.data.date;

        this.subjectDisplay = Subjects.find(s => s.key == this.job.subject).value;

        let startTimeParts = this.school.startTime.split(':');
        this.startTime.hour = +startTimeParts[0];
        this.startTime.minute = +startTimeParts[1];

        let endTimeParts = this.school.endTime.split(':');
        this.endTime.hour = +endTimeParts[0];
        this.endTime.minute = +endTimeParts[1];
      });

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  get timeError(): boolean {
    let startDateCheck = this.calculateStartDate();
    let endDateCheck = this.calculateEndDate();
    return startDateCheck.getTime() >= endDateCheck.getTime();
  }

  onJobCreate(needSub: boolean) {
    if (this.timeError) {

    }
    else {
      this.job.startDate = this.calculateStartDate();
      this.job.endDate = this.calculateEndDate();
      this.job.subNeeded = needSub;
      this.job.teacherId = this.data.teacher.id;

      if (!needSub) {
        this.dataService.createJob(this.job)
          .subscribe(resp => {
            this.dialogRef.close();
          }, err => this.error = true);
      }
      else {
        this.jobService.job = this.job;
        let search = new Models.SubSearch();
        search.date = this.job.startDate;
        search.schoolId = this.authService.user.profile.school;
        search.teacherId = this.authService.user.profile.sub;
        this.jobService.search = search;
        this.dialogRef.close();
        this._router.navigate(['/teachers', 'request'])
      }
    }
  }

  calculateStartDate(): Date {
    if (this.startTime != null) {
      let startDate = new Date(
        this.job.startDate.getFullYear(),
        this.job.startDate.getMonth(),
        this.job.startDate.getUTCDate(),
        this.startTime.hour,
        this.startTime.minute
      );
      return startDate;
    }
  }
  calculateEndDate(): Date {
    if (this.endTime != null) {
      let endDate = new Date(
        this.job.endDate.getFullYear(),
        this.job.endDate.getMonth(),
        this.job.endDate.getUTCDate(),
        this.endTime.hour,
        this.endTime.minute
      );
      return endDate;
    }
  }

}