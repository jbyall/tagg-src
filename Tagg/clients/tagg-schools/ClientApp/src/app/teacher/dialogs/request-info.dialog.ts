import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Router } from "@angular/router";

@Component({
    selector: 'request-info-dialog',
    templateUrl: 'request-info.dialog.html',
    styleUrls: ['../job-request.component.css']
  })
  export class RequestInfoDialog {
    constructor(
      public dialogRef: MatDialogRef<RequestInfoDialog>,
      @Inject(MAT_DIALOG_DATA) public data: any, private _router: Router) {
      this._router.events.subscribe(() => { this.onNoClick() });
    }
  
    onNoClick(): void {
      this.dialogRef.close();
    }
  
  }