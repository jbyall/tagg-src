import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data.service';
import { JobService } from '../services/job.service';
import * as Models from "../shared/models";
import { environment } from '../../environments/environment';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { SubDetailsDialog } from './dialogs/sub-details.dialog';
import { RequestInfoDialog } from './dialogs/request-info.dialog';

@Component({
  selector: 'app-job-request',
  templateUrl: './job-request.component.html',
  styleUrls: ['./job-request.component.css']
})
export class JobRequestComponent implements OnInit {
  public job: Models.Job;
  public teacher: Models.Teacher;
  public showError: boolean;
  public searchResult: Models.SubSearchResult;
  public chosenSub: Models.Substitute;
  public selectedSubs: Models.Substitute[] = [];
  public showNoSubs: boolean;
  public favoriteClass: string;
  public allSubs: Models.Substitute[] = [];
  public photos: string = environment.userPhotoEndpoint;

  constructor(public _authService: AuthService, private _router: Router, private route: ActivatedRoute, private dataService: DataService, private jobService: JobService, public dialog: MatDialog) {
    this.dataService.setHeaders();
  }



  ngOnInit() {
    if (this.jobService.job) {
      this.populateSearch();
    }
    else {
      this._router.navigate(['/home']);
    }

  }

  populateSearch() {
    this.job = this.jobService.job;
    this.dataService.searchSubs(this.jobService.search)
      .subscribe(resp => {
        this.searchResult = resp;
        if (this.searchResult.total > 0) {
          this.allSubs = this.searchResult.favorites.concat(this.searchResult.others);
          // this.allSubs.forEach(s => s.sendRequest = true);
          this.chosenSub = this.allSubs[0];
        }
        else {
          this.showNoSubs = true;
        }

      }, err => {
        this.showNoSubs = true;
      });
  }

  onCreateOffer() {
    let offer = new Models.JobOfferModel();
    let jobCreate = new Models.JobCreateModel();

    jobCreate.teacherId = this.job.teacherId;
    jobCreate.startDate = this.job.startDate;
    jobCreate.endDate = this.job.endDate;
    jobCreate.subNeeded = true;
    offer.job = jobCreate;
    offer.selected = this.selectedSubs.map(s => s.id);
    offer.favorites = this.searchResult.favorites.map(s => s.id);
    offer.others = this.searchResult.others.map(s => s.id);
    this.dataService.createOffer(offer)
      .subscribe(resp => {
        if(this.job.teacherId == this._authService.user.profile.sub)
        {
          this._router.navigate(['/teachers', 'home']);
        }
        else{
          this._router.navigate(['/home']);
        }
        
      });
  }

  addToSelected(subEmail: string) {
    let selectedSub = this.allSubs.find(s => s.email === subEmail);
    this.chosenSub = selectedSub;
    this.selectedSubs.push(selectedSub);
    this.removeFromSearch(selectedSub);
  }

  removeFromSelected(subEmail: string) {
    let selection = this.selectedSubs.find(s => s.email === subEmail);
    let removeIndex = this.selectedSubs.indexOf(selection);
    this.selectedSubs.splice(removeIndex, 1);
    this.addToSearch(selection);
  }

  // Remove sub from favorites or other
  private removeFromSearch(sub: Models.Substitute) {
    if (sub.isFavorite) {
      let removeIndex = this.searchResult.favorites.indexOf(sub);
      let removedFavorite = this.searchResult.favorites.splice(removeIndex, 1);
    }
    else {
      let removeIndex = this.searchResult.others.indexOf(sub);
      let removedOther = this.searchResult.others.splice(removeIndex, 1);
    }
  }

  // Add sub to favorites or other
  private addToSearch(sub: Models.Substitute) {
    if (sub.isFavorite) {
      this.searchResult.favorites.push(sub);
    }
    else {
      this.searchResult.others.push(sub);
    }
  }

  // Add sub to favorite if not here
  // Remove sub from favorite if there
  moveFavorite(sub: Models.Substitute) {
    // If user is teacher
    if (this._authService.user.profile.api_access === '0x2') {
      // Do only if toggled favorite is NOT in selected array
      if (!this.selectedSubs.find(s => s.email == sub.email)) {
        if (sub.isFavorite) {
          let removeIndex = this.searchResult.others.indexOf(sub);
          this.searchResult.others.splice(removeIndex, 1);
          this.searchResult.favorites.push(sub);
        }
        else {
          let removeIndex = this.searchResult.favorites.indexOf(sub);
          this.searchResult.favorites.splice(removeIndex, 1);
          this.searchResult.others.push(sub);
        }
      }
    }

  }



  onViewProfile(subEmail: string, showModal: boolean = false) {
    let result = this.allSubs.find(s => s.email === subEmail);
    if (result) {
      this.chosenSub = result;
      if (window.innerWidth < 767 || showModal) {
        this.openDialog();
      }
    }

  }

  onViewInfo() {
    let modalWidth = '50%';
    if (window.innerWidth < 1000) { modalWidth = '85%' }
    let dialogRef = this.dialog.open(RequestInfoDialog, {
      width: modalWidth
    });
  }

  // PREP FOR TAGG-247
  // onExclude(subEmail: string){
  //   let exclusion = this.allSubs.find(s => s.email === subEmail);
  //   if(exclusion){

  //   }
  // }

  onToggleFavorite(subId: string) {
    if (this._authService.user.profile.api_access === '0x2') {
      let toggled = this.allSubs.find(s => s.id == subId);
      toggled.isFavorite = !toggled.isFavorite;

      this.dataService.addFavorite(this._authService.user.profile.sub, subId)
        .subscribe(resp => {
          this.moveFavorite(toggled);
        }, err => {  });
    }
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(SubDetailsDialog, {
      width: '50%',
      data: { chosenSub: this.chosenSub },
    });
    // dialogRef.afterClosed().subscribe(resp => {
    //   this.populateSearch();
    // })
  }

}




