import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';
import * as Models from "../shared/models";
import { FormBuilder, FormGroup, Validators, AbstractControl, FormArray } from '@angular/forms';
import { FileUploader } from 'ng2-file-upload';
import { environment } from '../../environments/environment';
import { Subjects } from '../shared/constants';
import { MatSnackBar, MatDialog } from '@angular/material';
import { PhoneDialog } from './dialogs/phone.dialog';

@Component({
  selector: 'app-teacher-profile',
  templateUrl: './teacher-profile.component.html',
  styleUrls: ['./teacher-profile.component.css']
})
export class TeacherProfileComponent implements OnInit {
  public currentUser: Models.IUser;
  public teacher: Models.Teacher;
  public showUpdateError: boolean;
  public imageFile: File = null;
  public fileReader: FileReader;
  public photos = environment.userPhotoEndpoint;
  public showImageError: boolean;
  public subjects = Subjects;
  public selectedSubjects: number[];
  public dirty: boolean;
  public showForm: boolean;

  constructor(public _authService: AuthService, private _router: Router, private _dataService: DataService, private fb: FormBuilder, public snackBar: MatSnackBar, public dialog: MatDialog) {
    this.currentUser = _authService.userProfile;
    this._dataService.setHeaders();
  }

  ngOnInit() {
    this._dataService.getTeacherProfile(this._authService.user.profile.sub)
      .subscribe(resp => {
        this.teacher = resp;
        this.populateForm();
      }, err => { })
  }

  populateForm(){
    this.selectedSubjects = this.teacher.subjects;
    this.showForm = true;
    if(!this.teacher.receiveEmail && !this.teacher.receivePhone && !this.teacher.receiveSMS){
      this.openPhoneDialog();
    }
    if(this.teacher.receiveSMS && !this.teacher.phoneVerified){
      this.openPhoneDialog();
    }
  }

  onSubmit() {
    this.teacher.subjects = this.selectedSubjects;
    this.dirty = false;
    this._dataService.updateTeacher(this.teacher)
      .subscribe(resp => {
        this.teacher = resp.teacher;
        this.showSnackbar('Profile updated.');
        // if (resp.verificationRequired) {
        //   this._router.navigate(['/verify-phone', resp.phoneNumber]);
        // }
        // else {
        //   this._router.navigate(['/home']);
        // }
      }, err => { this.showUpdateError = true; })
  }

  openPhoneDialog(){
    let width = window.innerWidth < 992 ? '80%' : '600px';
    let dialogRef = this.dialog.open(PhoneDialog, {
      width: width,
      data: { teacher: this.teacher }
    });

    dialogRef.afterClosed().subscribe(result => {
      window.location.reload();
    });
  }

  onImageUpload(images: FileList) {
    this.showImageError = false;
    let imageUpload = images.item(0);
    let fileType = imageUpload.type;
    if (fileType === "image/jpeg" || fileType === "image/png" || fileType === "image/gif") {
      var data = new FormData();
      data.append("file", imageUpload)
      this._dataService.uploadPicture(data)
        .subscribe(resp => {
          this.teacher.picture = resp;
        }, err => {});
    }
    else {
      this.showImageError = true;
    }
  }

  markDirty() {
    this.dirty = true;
  }

  showSnackbar(message: string){
    this.snackBar.open(message, 'Dismiss',{
      duration: 5000
    });
  }

}
