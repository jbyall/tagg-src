import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../auth/auth.service';
import { AuthGuard } from '../auth/auth.guard';
import { TaggCommonModule } from '../shared/tagg-common.module';
import { AdminGuard } from '../auth/admin.guard';
import { TeacherProfileComponent } from './teacher-profile.component';
import { TeacherCalendarComponent } from './teacher-calendar.component';
import { TeacherMetricsComponent } from './teacher-metrics.component';
import { JobRequestComponent } from './job-request.component';
import { TeacherJobsComponent } from './teacher-jobs.component';
import { TeacherSettingsComponent } from './teacher-settings.component';
import { RatingGuard } from '../auth/rating.guard';
import { TimeRequestDialog } from './dialogs/time-request.dialog';
import { RequestInfoDialog } from './dialogs/request-info.dialog';
import { SubDetailsDialog } from './dialogs/sub-details.dialog';
import { PhoneDialog } from './dialogs/phone.dialog';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            { path: 'home', component: TeacherCalendarComponent, canActivate:[AuthGuard, RatingGuard]},
            { path: 'profile', component: TeacherProfileComponent, canActivate:[AuthGuard, RatingGuard]},
            { path: 'settings', component: TeacherSettingsComponent, canActivate:[AuthGuard, RatingGuard]},
            { path: 'metrics', component: TeacherMetricsComponent, canActivate:[AuthGuard, RatingGuard]},
            { path: 'request', component: JobRequestComponent, canActivate:[AuthGuard, RatingGuard]},
            { path: 'jobs', component: TeacherJobsComponent, canActivate:[AuthGuard, RatingGuard]}
        ]),
        TaggCommonModule,
    ],
    declarations: [
    TeacherCalendarComponent,
    TeacherProfileComponent,
    TeacherMetricsComponent,
    TimeRequestDialog,
    RequestInfoDialog,
    JobRequestComponent,
    TeacherJobsComponent,
    TeacherSettingsComponent,
    PhoneDialog
    ],
    providers:[
    ],
    entryComponents:[TimeRequestDialog, SubDetailsDialog, RequestInfoDialog, PhoneDialog]
})
export class TeacherModule { }