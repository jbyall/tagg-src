import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';
import * as Models from "../shared/models";
import { ViewJobDialog } from '../shared/view-job.dialog';
import { JobService } from '../services/job.service';
import { Subjects, MonthMap } from '../shared/constants';
import { TimeRequestDialog } from './dialogs/time-request.dialog';

@Component({
  selector: 'app-teacher-calendar',
  templateUrl: './teacher-calendar.component.html',
  styleUrls: ['./teacher-calendar.component.css']
})
export class TeacherCalendarComponent implements OnInit {
  public currentUser: Models.IUser;
  public teacher: Models.Teacher;
  public showIncomplete: boolean;
  public closeResult: string;

  public calendar: Models.CalendarDay[];
  public selectedDate: Date;
  public monthLabel: string;
  public monthMap = MonthMap;

  constructor(public _authService: AuthService, private _router: Router, private dataService: DataService, private dialog: MatDialog) {
    this.currentUser = _authService.userProfile;
    this.dataService.setHeaders();
  }

  ngOnInit() {
    this.selectedDate = new Date();
    this.populateCalendar();
    this.dataService.getTeacherProfile(this._authService.user.profile.sub)
      .subscribe(resp => {
        this.teacher = resp;
        if (this.teacher.status !== 1) {
          this.showIncomplete = true;
        }
      }, err => { })
  }

  onPrevMonth() {
    this.selectedDate.setMonth(this.selectedDate.getMonth() - 1);
    this.populateCalendar();
  }

  onNextMonth() {
    let nextMonth = this.selectedDate.getMonth() + 1;
    this.selectedDate.setMonth(nextMonth, 1);
    this.populateCalendar();
  }

  populateCalendar() {
    this.monthLabel = this.monthMap[this.selectedDate.getMonth()] + ' ' + this.selectedDate.getFullYear();
    this.dataService.getTeacherCalendar(this.selectedDate.getMonth() + 1, this.selectedDate.getFullYear())
      .subscribe(resp => {
        this.calendar = resp;
      });
  }

  onDateSelect(monthIndex: number, dayIndex: number): void {
    let day = this.calendar.find(d => d.dayOfMonthIndex == dayIndex && d.monthIndex == monthIndex);
    let jobEvent = day.events.find(e => e.job !== null);
    let blackoutEvent = day.events.find(e => e.isBlackout);
    if (jobEvent) {
      this.displayJob(jobEvent.job.id);
    }
    else if (blackoutEvent) {
      // Do nothing
    }
    else {
      let jobDateParts = day.date.toString()
        .split('T')[0]
        .split('-');
      let jobDate = new Date(+jobDateParts[0], (+jobDateParts[1] - 1), +jobDateParts[2]);
      let now = new Date();
      // Prevent opening request dialog if date is in the past
      if(jobDate >= new Date(now.getFullYear(), now.getMonth(), now.getDate())){
        this.openRequestDialog(jobDate);
      }
      
    }

  }

  displayJob(jobId: string) {
    this.dataService.getJobDetails(jobId)
      .subscribe(resp => {
        this.openJobDialog(resp);
      }, err => { });
  }

  openRequestDialog(date: Date): void {
    let dialogRef = this.dialog.open(TimeRequestDialog, {
      width: '500px',
      data: { teacher: this.teacher, schoolId: this._authService.user.profile.school, date: date }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.populateCalendar();
    });
  }

  openJobDialog(selectedJob: Models.Job): void {
    let width = window.innerWidth < 992 ? '80%' : '600px';
    let dialogRef = this.dialog.open(ViewJobDialog, {
      width: width,
      data: { job: selectedJob }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.populateCalendar();
    });
  }

}
