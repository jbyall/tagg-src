import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';
import * as Models from "../shared/models";
import { SubjectDisplay } from '../shared/constants';
import { MatDialog } from '@angular/material';
import { ViewJobDialog } from '../shared/view-job.dialog';

@Component({
  selector: 'app-teacher-jobs',
  templateUrl: './teacher-jobs.component.html',
  styleUrls: ['./teacher-jobs.component.css']
})
export class TeacherJobsComponent implements OnInit {
  public jobs: Models.Job[];
  public recentJobs: Models.Job[];
  public upcomingJobs: Models.Job[];
  public todayJobs: Models.Job[];
  public showError: boolean;
  public subjectDisplay = SubjectDisplay;

  constructor(private _authService: AuthService, private _router: Router, private dataService: DataService, public dialog: MatDialog) { }

  ngOnInit() {
    this.dataService.setHeaders();
    this.getTeacherJobs();
  }

  getTeacherJobs() {
    this.dataService.getTeacherJobs(this._authService.user.profile.sub)
      .subscribe(resp => {
        this.jobs = resp;
        this.splitJobs();
      }, err => this.handleError(err))
  }

  onSelectAccepted(jobId: string){
    this.dataService.getJobDetails(jobId)
      .subscribe(resp => {
        this.openJobDialog(resp);
      })
    
  }

  onSelectPending(jobId: string){
    this.dataService.getJobDetails(jobId)
      .subscribe(resp => {
        this.openJobDialog(resp);
      })
  }

  onSelectRecent(jobId: string){
    this.dataService.getJobDetails(jobId)
      .subscribe(resp => {
        this.openJobDialog(resp);
      })
  }

  splitJobs(){
    if(this.jobs){
      this.recentJobs = this.jobs.filter(j => j.status == Models.JobStatus.Complete);
      this.todayJobs = this.jobs.filter(j => j.status == Models.JobStatus.Accepted);
      this.upcomingJobs = this.jobs.filter(j => j.status == Models.JobStatus.Open);
    }
  }

  openJobDialog(selectedJob: Models.Job): void {
    let width = window.innerWidth < 992 ? '80%' : '600px';
    let dialogRef = this.dialog.open(ViewJobDialog, {
      width: width,
      data: { job: selectedJob }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getTeacherJobs();
    });
  }

  handleError(err: any){
    this.showError = true;
  }

}
