import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { TeacherMetrics } from '../shared/models';

@Component({
  selector: 'app-teacher-metrics',
  templateUrl: './teacher-metrics.component.html',
  styleUrls: ['./teacher-metrics.component.css']
})
export class TeacherMetricsComponent implements OnInit {
  public metrics: TeacherMetrics;
  public showError: boolean;

  constructor(public _authService: AuthService, private _router: Router, private dataService: DataService) {
    this.dataService.setHeaders();
  }

  ngOnInit() {
    this.dataService.getTeacherMetrics(this._authService.user.profile.sub)
      .subscribe(resp => {
        this.metrics = resp;
      }, err => {
        this.showError = true;
      })
  }

  onRateJob() {
    this._router.navigate(['/rate', this.metrics.recentJob.id]);
  }

}
