import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from '../auth/auth.service';
import * as Models from "../shared/models";
import { DataService } from '../services/data.service';

@Component({
    selector: 'home',
    template:'<div></div>'
})
export class HomeComponent implements OnInit {
    public adminSchools: Models.School[];

    constructor(public _authService: AuthService, private _router: Router, private _dataService: DataService) {
        this._dataService.setHeaders();
    }

    ngOnInit() {

        switch (this._authService.user.profile.api_access) {
            case '0x33':
            case '0x3': {
                this._router.navigate(['/admin', 'home']);
                break;
            }
            default: {
                this._router.navigate(['/teachers', 'home']);
            }
        }
    }

}
