import { Component } from '@angular/core';
import { Router } from "@angular/router";

@Component({
    selector: 'about',
    templateUrl: './about.component.html',
    styleUrls: ['./about.component.css']
})
export class AboutComponent { 
    onNewTab(){
        window.open('http://taggeducation.com');
    }
}