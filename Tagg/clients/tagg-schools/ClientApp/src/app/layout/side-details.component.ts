import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { DataService } from '../services/data.service';
import * as Models from '../shared/models';
import { JobService } from '../services/job.service';
import { MatDialog } from '../../../node_modules/@angular/material';
import { ViewJobDialog } from '../shared/view-job.dialog';

@Component({
  selector: 'app-side-details',
  templateUrl: './side-details.component.html',
  styleUrls: ['./side-details.component.css']
})
export class SideDetailsComponent implements OnInit {

  // public jobs: Models.Job[];
  // public recentJobs: Models.Job[];
  // public upcomingJobs: Models.Job[];
  public showJobs: boolean;

  constructor(private router: Router, public authService: AuthService, private dataService: DataService, public jobService: JobService, public dialog: MatDialog) { 
    this.jobService.jobsReady$.subscribe(resp =>{
      if(resp){ this.showJobs = true; }
    })
  }

  ngOnInit() {
    this.jobService.getJobs(this.authService.user.profile.api_access);
  }

  onRateJob(jobId: string){
    let jobToRate = this.jobService.recentJobs.find(j => j.id === jobId);
    if(jobToRate && jobToRate.rating == 0){
      this.router.navigate(['/rate', jobId]);
    }
    
  }

  onViewJob(jobId: string){
    this.jobService.getJobDetails(jobId)
      .subscribe(resp => {
        this.openJobDialog(resp);
      })
  }

  openJobDialog(selectedJob: Models.Job): void {
    let width = window.innerWidth < 992 ? '80%' : '600px';
    let dialogRef = this.dialog.open(ViewJobDialog, {
      width: width,
      data: { job: selectedJob }
    });
  }

}
