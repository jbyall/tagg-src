import { Component, OnInit, Input } from '@angular/core';
import { MessageService } from '../services/message.service';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.css']
})
export class SideNavComponent implements OnInit {
  public hasNewMessage: boolean;

  constructor(public messageService: MessageService, public authService: AuthService) { 
    this.messageService.newMessage$
      .subscribe(resp => {
        this.hasNewMessage = resp;
        if(resp){
        }
      });

      this.messageService.newChannel$
        .subscribe(resp => {
          if(resp){
            this.hasNewMessage = resp;
          }
        })
  }



  ngOnInit() {
      let test = this.messageService.getChatToken();
  }

}
