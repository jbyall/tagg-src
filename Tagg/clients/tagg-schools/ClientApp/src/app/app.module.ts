// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from "./app-routing.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Services

// Components
import { AppComponent } from './app.component';
import { HomeComponent } from './layout/home.component';
import { PageNotFoundComponent } from "./layout/page-not-found.component";
import { AboutComponent } from './layout/about.component';
import { ContactComponent } from './layout/contact.component';
//import { AccountComponent } from './shared/account/account.component';
import { AccountModule } from './shared/account/account.module';
import { TaggCommonModule } from './shared/tagg-common.module';
import { CommonModule } from '@angular/common';
import { SchoolModule } from './school/school.module';
import { RegisterModel } from './shared/models';
import { DataService } from './services/data.service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AuthCallbackComponent } from './auth/auth-callback.component';
import { AdminGuard } from './auth/admin.guard';
import { AuthGuard } from './auth/auth.guard';
import { AuthService } from './auth/auth.service';
import { ViewJobDialog } from './shared/view-job.dialog';
import { JobService } from './services/job.service';
import { JobDetailsDialog } from './shared/job-details.dialog';
import { MessageService } from './services/message.service';
import { RatingGuard } from './auth/rating.guard';
import { ChangeEmailComponent } from './shared/account/change-email.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PageNotFoundComponent,
    AboutComponent,
    ContactComponent,
    AuthCallbackComponent,
    ViewJobDialog,
    JobDetailsDialog,
    ChangeEmailComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    CommonModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AccountModule,
    NgbModule.forRoot(),
    TaggCommonModule
  ],
  providers: [
    AuthGuard,
    AdminGuard,
    RatingGuard,
    AuthService,
    DataService,
    JobService,
    MessageService
  ],
  entryComponents:[ViewJobDialog, JobDetailsDialog],
  bootstrap: [AppComponent]
})
export class AppModule { }
