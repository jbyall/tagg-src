import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-auth-callback',
  templateUrl: './auth-callback.component.html'
})
export class AuthCallbackComponent implements OnInit {
private redirectPath: string;
  constructor(private authService: AuthService, private router: Router, private dataService: DataService) { }

  ngOnInit() {
    this.authService.completeAuthentication().then( resp => {
      this.dataService.setHeaders();
      this.redirectPath = localStorage.getItem('taggRedirect');
      this.getUnratedJobs();
    });
  }

  getUnratedJobs(){
    this.dataService.getUnratedJobIds(this.authService.user.profile.sub)
      .subscribe(resp => {
        if(resp.length > 0){
          this.dataService.addJobsForReview(resp);
        }
        this.router.navigate([this.redirectPath]);
      }, err => {});
  }

}
