import { Component, OnInit } from '@angular/core';
import { FormsModule, FormGroup, FormBuilder, Validators, AbstractControl, FormControl, ValidatorFn } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import * as Models from '../shared/models';

import 'rxjs/add/operator/debounceTime';
import { DataService } from '../services/data.service';
import { States } from '../shared/constants';

@Component({
    selector:'app-school-inquiry',
    templateUrl: './school-inquiry.component.html',
    styleUrls:['./school-inquiry.component.css']
})
export class SchoolInquiryComponent implements OnInit{ 
    public inquiryForm: FormGroup;
    public errorMessage: string;
    public showSuccess: boolean = false;
    public validators: Map<string,string>;
    //public states: Models.Lookup[];
    public states = States;
    public showForm: boolean;
    


    constructor(private fb: FormBuilder, private _route: ActivatedRoute, private _router: Router, private dataService: DataService) {
        this.validators = new Map<string, string>();
    }
    get firstName(): AbstractControl { return this.inquiryForm.get('firstName') };
    get lastName(): AbstractControl { return this.inquiryForm.get('lastName') };
    get email(): AbstractControl { return this.inquiryForm.get('email') };
    get phoneNumber(): AbstractControl { return this.inquiryForm.get('phoneNumber') };
    get schoolName(): AbstractControl { return this.inquiryForm.get('schoolName') };
    get districtName(): AbstractControl { return this.inquiryForm.get('districtName') };
    get title(): AbstractControl { return this.inquiryForm.get('title') };
    get city(): AbstractControl { return this.inquiryForm.get('city') };
    get state(): AbstractControl { return this.inquiryForm.get('state') };
    get postalCode(): AbstractControl { return this.inquiryForm.get('postalCode') };

    ngOnInit(){
        this.populateForm();
        // this.dataService.getStates()
        //     .subscribe(resp => {
        //         this.states = resp;
        //         this.populateForm();
        //     });
        
    }

    populateForm(){
        this.inquiryForm = this.fb.group({
            firstName:['', Validators.required],
            lastName:['' , Validators.required],
            email:['', [Validators.required, Validators.email]],
            phoneNumber:['', Validators.required],
            schoolName:['', Validators.required],
            districtName:['', Validators.required],
            title:'',
            city:['', Validators.required],
            state:['', Validators.required],
            postalCode:['', Validators.required],
        });
        this.showForm = true;
    }

    onSubmit(){
        let i = Object.assign({}, this.inquiryForm.value)
        this.dataService.sendInquiry(i)
            .subscribe(resp => {
                this.showSuccess = true;
            }, err => {
                this.errorMessage = "Oops...we ran into a problem. Please give us a call at ###.###.#### or email support@taggeducation.com";
            })
    }

    
}