import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { DataService } from '../../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';

import * as Models from "../../shared/models";
import { environment } from '../../../environments/environment';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDatepicker, MatTableDataSource } from '@angular/material';
import { GradeSubject, Grades, SubjectDisplay, Subjects, Meridiems, Hours } from '../../shared/constants';
import { JobService } from '../../services/job.service';
import { FormControl } from '@angular/forms';
import { DataSource } from '@angular/cdk/table';
import { Observer } from 'rxjs/Observer';
// import {merge, Observable, of as observableOf} from 'rxjs';
// import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.css']
})
export class AdminListComponent implements OnInit {
  public school: Models.SchoolDetail;
  public selectedAdmin: Models.SchoolAdmin;

  public photos: string = environment.userPhotoEndpoint;
  public testPhotoId: string = '5ae5ebe06e28e8058ccf71e6';
  public schoolId: string;

  public subjectDisp = SubjectDisplay;
  private isIniting: boolean = true;

  constructor(private authService: AuthService, private route: ActivatedRoute, private router: Router, private dataService: DataService, public snackBar: MatSnackBar, private dialog: MatDialog) {
    this.dataService.setHeaders();
  }

  ngOnInit() {
    this.schoolId = this.route.snapshot.paramMap.get('id');
    this.loadAdmins();
  }

  loadAdmins() {
    this.dataService.getSchoolDetails(this.schoolId)
      .subscribe(resp => {
        this.school = resp;
        if (this.school.schoolBase.admins.length > 0) {
          let selectedAdminId = this.selectedAdmin ? this.selectedAdmin.id : this.school.schoolBase.admins[0].id;
          this.onSelectAdmin(selectedAdminId);
        }
      })
  }

  onSelectAdmin(adminId: string) {
    this.selectedAdmin = this.school.schoolBase.admins.find(a => a.id === adminId);
  }

  onAddAdmin() {
    this.router.navigate(['/schools', this.schoolId, 'add', '2']);
  }

  onDisableAdmin(adminId: string) {
    if (adminId !== this.authService.user.profile.sub) {
      this.dataService.adminDisable(this.schoolId, adminId)
        .subscribe(resp => {
          this.loadAdmins();
        });
    }
    else{
      this.snackBar.open("You cannot disable yourself :)", "Dismiss");
    }

  }

  onEnableAdmin(adminId: string) {
    this.dataService.adminEnable(this.schoolId, adminId)
      .subscribe(resp => {
        this.loadAdmins();
      });
  }

}
