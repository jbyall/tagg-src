import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../auth/auth.service';
import { AuthGuard } from '../auth/auth.guard';

import { TaggCommonModule } from '../shared/tagg-common.module';
import { SchoolInquiryComponent } from './school-inquiry.component';
import { SchoolDetailComponent } from './school-detail.component';
import { SchoolEditComponent } from './school-edit.component';
import { UploadTeachersComponent } from './upload-teachers.component';
import { RegisterSchoolComponent } from '../register/register-school.component';
import { AdminGuard } from '../auth/admin.guard';
import { TeacherListComponent, RequestForTeacherDialog, TeacherDetailsDialog, PayRateDialog } from './teacher-list.component';
import { TeacherAddComponent } from './teacher-add.component';
import { SchoolPaymentsComponent } from './school-payments.component';
import { BankVerifyComponent } from './bank-verify.component';
import { SchoolTransactionsComponent } from './school-transactions.component';
import { SchoolMetricsComponent } from './school-metrics.component';
import { SubListComponent } from './subs/sub-list.component';
import { AdminSchoolsComponent } from '../admin/admin-schools.component';
import { AdminListComponent } from './admins/admin-list.component';
import { SubDetailsDialog } from '../teacher/dialogs/sub-details.dialog';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            { path: 'inquiry', component: SchoolInquiryComponent},
            { path: ':id', component: SchoolEditComponent, canActivate:[AdminGuard]},
            { path: ':id/edit', component: SchoolEditComponent, canActivate:[AdminGuard]},
            { path: ':id/upload', component: UploadTeachersComponent, canActivate:[AdminGuard]},
            { path: ':id/add/:type', component: TeacherAddComponent, canActivate:[AdminGuard]},
            { path: ':id/teachers', component: TeacherListComponent, canActivate:[AdminGuard]},
            { path: ':id/admins', component: AdminListComponent, canActivate:[AdminGuard]},
            { path: ':id/payments', component: SchoolPaymentsComponent, canActivate:[AdminGuard]},
            { path: ':id/verify', component: BankVerifyComponent, canActivate:[AdminGuard]},
            { path: ':id/transactions', component: SchoolTransactionsComponent, canActivate:[AdminGuard]},
            { path: ':id/metrics', component: SchoolMetricsComponent, canActivate:[AdminGuard]},
            { path: ':id/subs', component: SubListComponent, canActivate:[AuthGuard]},
            { path: '', redirectTo: '/admin/schools', pathMatch:'full'},
        ]),
        TaggCommonModule,
    ],
    exports:[
        SchoolEditComponent
    ],
    providers:[
    ],
    declarations: [
        SchoolInquiryComponent,
        SchoolDetailComponent,
        SchoolEditComponent,
        UploadTeachersComponent,
        TeacherListComponent,
        TeacherAddComponent,
        SchoolPaymentsComponent,
        BankVerifyComponent,
        SchoolTransactionsComponent,
        RequestForTeacherDialog,
        SchoolMetricsComponent,
        TeacherDetailsDialog,
        SubListComponent,
        PayRateDialog,
        AdminListComponent
    ],
    entryComponents:[RequestForTeacherDialog, TeacherDetailsDialog, PayRateDialog, SubDetailsDialog]
})
export class SchoolModule { }