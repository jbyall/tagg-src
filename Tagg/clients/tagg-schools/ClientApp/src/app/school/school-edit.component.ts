import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as Models from '../shared/models';
import { DataService } from '../services/data.service';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Grades, Hours, Meridiems, States } from '../shared/constants';
import { MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-school-edit',
  templateUrl: './school-edit.component.html',
  styleUrls: ['./school-edit.component.css']
})
export class SchoolEditComponent implements OnInit {
  public editForm: FormGroup;
  public school: Models.School;
  public showForm: boolean;
  public emailError: string;
  public errorMessage: string;
  public states = States;
  public costPerDay: number;

  public hours = Hours;
  public startHour: number;
  public startMinute: number;
  public startMeridiem = Meridiems.find(m => m.key == 0);


  public endHour: number;
  public endMeridiem = Meridiems.find(m => m.key == 1);

  public endMinute: number;
  public grades = Grades;
  public startGrade: any;
  public endGrade: any;
  public schoolType: string;

  public minutes: any[] = [];

  @Input() isNew: boolean = false;

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router, private dataService: DataService, public snackBar: MatSnackBar) {
    for (let i = 0; i < 60; i++) {
      // Populate array of minutes for school time drop-downs
      this.minutes.push({ key: i, value: `${i}`.padStart(2, '0') });
    }
  }


  get name(): AbstractControl { return this.editForm.get('name') };
  get contactPhone(): AbstractControl { return this.editForm.get('contactPhone') };
  get contactEmail(): AbstractControl { return this.editForm.get('contactEmail') };
  get url(): AbstractControl { return this.editForm.get('url') };
  get payRate(): AbstractControl { return this.editForm.get('payRate') };
  get street(): AbstractControl { return this.editForm.get('address').get('street1') };
  get city(): AbstractControl { return this.editForm.get('address').get('city') };
  get state(): AbstractControl { return this.editForm.get('address').get('state') };
  get postalCode(): AbstractControl { return this.editForm.get('address').get('postalCode') };

  ngOnInit() {
    if (!this.isNew) {
      this.dataService.setHeaders();
      let schoolId = this.route.snapshot.paramMap.get('id');
      this.dataService.getSchool(schoolId)
        .subscribe(resp => {
          this.school = resp;
          this.populateForm();
        }, err => {
          this.router.navigate(['/home']);
        });
    }
  }

  getStates() {
    this.dataService.getStates()
      .subscribe(resp => {
      })
  }

  populateForm() {
    this.editForm = this.fb.group({
      name: [this.school.name, Validators.required],
      url: [this.school.url, Validators.required],
      contactEmail: [this.school.contactEmail, [Validators.required, Validators.email]],
      contactPhone: [this.school.contactPhone, Validators.required],
      summary: this.school.summary,
      payRate: [this.school.payRate || '', Validators.required],
      address: this.fb.group({
        street1: [this.school.address.street1, Validators.required],
        street2: this.school.address.street2,
        city: [this.school.address.city, Validators.required],
        state: [this.school.address.state, Validators.required],
        postalCode: [this.school.address.postalCode, Validators.required]
      }),
    });

    let startArray = this.school.startTime.split(':');
    this.startHour = +startArray[0];
    this.startMinute = +startArray[1];
    this.startHour = this.startHour > 12 ? (this.startHour - 12) : this.startHour;

    let endArray = this.school.endTime.split(':');
    this.endHour = +endArray[0];
    this.endMinute = +endArray[1];

    this.endHour = this.endHour > 12 ? (this.endHour - 12) : this.endHour;
    this.schoolType = this.school.schoolType;

    if (this.school.grades) {
      let schoolGrades = this.school.grades.split('-');
      if (schoolGrades.length === 2) {
        this.startGrade = Grades.find(g => g.key === schoolGrades[0]).key;
        this.endGrade = Grades.find(g => g.key === schoolGrades[1]).key;
      }
      if(schoolGrades.length === 3){
        this.startGrade = Grades.find(g => g.key === schoolGrades[0] + '-' + schoolGrades[1]).key;
        this.endGrade = Grades.find(g => g.key === schoolGrades[2]).key;
      }

    }
    this.calculateCostPerDay(this.school.payRate);
    this.showForm = true;
  }

  toggleStartMeridiem() {
    this.startMeridiem = this.startMeridiem.key == 0 ?
      Meridiems.find(m => m.key == 1) : Meridiems.find(m => m.key == 0);
  }

  toggleEndMeridiem() {
    this.endMeridiem = this.endMeridiem.key == 0 ?
      Meridiems.find(m => m.key == 1) : Meridiems.find(m => m.key == 0);
  }

  calculateCostPerDay(pay: number){
    if(this.school){
      let serviceFeeFactor = .25;
      let transactionFeeFactor = .0124;
      let payRateUsd = pay;
      if(serviceFeeFactor * payRateUsd > 50){
        serviceFeeFactor = 50 / payRateUsd;
      }
      let result = payRateUsd / (1-serviceFeeFactor) / (1-transactionFeeFactor);
      this.costPerDay = result;
    }
  }

  onSubmit() {
    let s = Object.assign({}, this.editForm.value);
    this.school.schoolType = this.schoolType;
    this.school.grades = `${this.startGrade}-${this.endGrade}`;

    let startHour = this.startMeridiem.key > 0 ? this.startHour + 12 : this.startHour;
    let endHour = this.endMeridiem.key > 0 ? this.endHour + 12 : this.endHour;

    this.school.startTime = `${startHour}:${this.startMinute}:00`;
    this.school.endTime = `${endHour}:${this.endMinute}:00`;

    this.school.name = s.name;
    this.school.url = s.url;
    this.school.contactEmail = s.contactEmail;
    this.school.contactPhone = s.contactPhone;
    this.school.address = s.address;
    this.school.payRate = s.payRate;
    this.school.summary = s.summary;


    this.dataService.updateSchool(this.school)
      .subscribe(resp => {
        this.showSnackbar('School profile updated successfully!');
      });

    this.editForm.markAsPristine();
  }

  showSnackbar(message: string) {
    this.snackBar.open(message, 'Dismiss', {
      duration: 5000
    });
  }

  onCanDoPayment() {
    if (this.editForm.dirty) {
      if (this.editForm.valid && this.settingsValid()) {
        this.onSubmit();
        this.onAddPayment();
      }
      else {
        this.showSnackbar('Please make sure all required school information is complete and correct before continuing.');
      }
    }
    else {
      this.onAddPayment();
    }
  }

  onAddPayment() {
    if (this.school.customerId && this.school.accountStatus == 0) {
      this.router.navigate(['/schools', this.school.id, 'verify']);
    }
    if (!this.school.customerId || (this.school.customerId && this.school.accountStatus == 1)) {
      this.router.navigate(['/schools', this.school.id, 'payments']);
    }

  }

  settingsValid(): boolean {
    return this.schoolType != null && this.startGrade && this.endGrade && this.startHour > 0 && this.endHour > 0;
  }

  markDirty() {
    this.editForm.markAsDirty();
  }

  public getEmailError() {
    if (this.contactEmail.touched
      && this.contactEmail.dirty
      && this.contactEmail.invalid) {
      if (this.contactEmail.errors.email && !this.contactEmail.errors.required) {
        this.emailError = "Please enter a valid email";
        this.contactEmail.markAsUntouched;
      }
    }
  }
  public resetEmailError() {
    this.emailError = '';
  }

}
