import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { DataService } from '../../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';

import * as Models from "../../shared/models";
import { environment } from '../../../environments/environment';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDatepicker, MatTableDataSource } from '@angular/material';
import { GradeSubject, Grades, SubjectDisplay, Subjects, Meridiems, Hours } from '../../shared/constants';
import { JobService } from '../../services/job.service';
import { FormControl } from '@angular/forms';
import { DataSource } from '@angular/cdk/table';
import { Observer } from 'rxjs/Observer';
// import {merge, Observable, of as observableOf} from 'rxjs';
// import {catchError, map, startWith, switchMap} from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { SubDetailsDialog } from '../../teacher/dialogs/sub-details.dialog';

@Component({
  selector: 'app-sub-list',
  templateUrl: './sub-list.component.html',
  styleUrls: ['./sub-list.component.css']
})
export class SubListComponent implements OnInit {
  //public data: SubsDataSource;
  public data: MatTableDataSource<Models.Substitute> = new MatTableDataSource();
  displayedColumns = ['picture', 'firstName', 'lastName', 'test'];

  public school: Models.School;
  public subs: Models.Substitute[];
  public filteredSubs: Models.Substitute[] = [];
  public selectedSub: Models.Substitute;
  public showNoSubs: boolean = false;
  public photos: string = environment.userPhotoEndpoint;
  public testPhotoId: string = '5ae5ebe06e28e8058ccf71e6';
  public schoolId: string;
  public isExcluded: boolean;

  public subjectDisp = SubjectDisplay;
  private isIniting: boolean = true;
  constructor(public authService: AuthService, private route: ActivatedRoute, private router: Router, private dataService: DataService, public snackBar: MatSnackBar, private dialog: MatDialog) {
    this.dataService.setHeaders();
  }

  ngOnInit() {
    this.schoolId = this.route.snapshot.paramMap.get('id');
    this.loadSubs();
  }

  loadSubs() {
    if (this.authService.user.profile.api_access === '0x2') {
      this.dataService.getSubsForTeacher(this.authService.user.profile.sub, this.authService.user.profile.school)
        .subscribe(resp => {
          this.subs = resp;
          this.filteredSubs = this.subs;
          if (this.subs.length > 0) {
            this.onSelectSub(this.subs[0].id);
          }
        });
    }
    else {
      this.dataService.getSubsForSchool(this.schoolId)
        .subscribe(resp => {
          this.subs = resp;
          this.filteredSubs = this.subs;
          if (this.subs.length > 0) {
            this.onSelectSub(this.subs[0].id);
          }
        });
    }

  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();

    if (filterValue !== '') {
      this.filteredSubs = this.subs.filter(s => s.firstName.toLocaleLowerCase().startsWith(filterValue) ||
        s.lastName.toLowerCase().startsWith(filterValue));
    }
    else {
      this.filteredSubs = this.subs;
    }


    // this.data.filter = filterValue;
  }

  onSelectSub(subId: string) {
    this.selectedSub = this.subs.find(s => s.id === subId);
    this.isExcluded = this.selectedSub.exclusions.some(x => x === this.schoolId);
  }

  onExclude() {
    let exclusionId = '';
    if (this.authService.user.profile.api_access === '0x2') {
      exclusionId = this.authService.user.profile.sub;
    }
    else {
      exclusionId = this.schoolId;
    }

    this.dataService.subExclude(this.selectedSub.id, exclusionId)
      .subscribe(resp => {
        this.loadSubs();
      });
  }

  onEnable() {
    let exclusionId = '';
    if (this.authService.user.profile.api_access === '0x2') {
      exclusionId = this.authService.user.profile.sub;
    }
    else {
      exclusionId = this.schoolId;
    }
    this.dataService.subEnable(this.selectedSub.id, exclusionId)
      .subscribe(resp => {
        this.loadSubs();
      })
  }

  getExcluded(id: string): boolean {
    let subCheck = this.subs.find(s => s.id === id);
    if(this.authService.user.profile.api_access === '0x2'){
      return subCheck.exclusions.some(x => x === this.authService.user.profile.sub);
    }
    return subCheck.exclusions.some(x => x === this.schoolId);
  }

  getExcludedSchool(id: string): boolean {
    let subCheck = this.subs.find(s => s.id === id);
    return subCheck.exclusions.some(x => x === this.schoolId);
  }

  onViewProfile(){
      let dialogRef = this.dialog.open(SubDetailsDialog, {
        width: '50%',
        data: { chosenSub: this.selectedSub },
      });
      // dialogRef.afterClosed().subscribe(resp => {
      //   this.populateSearch();
      // })
    
  }

}



// export class SubsDataSource extends DataSource<any>{
//   subs: Models.Substitute[];
//   retVal: Observable<Models.Substitute[]>;

//   constructor(private dataService: DataService, public schoolId: string) {super();}

//   connect(): Observable<Models.Substitute[]>{
//     return this.dataService.getSubsForSchool(this.schoolId).map(resp =>{
//       this.subs = resp;
//       return this.subs;
//     })
//   }

//   disconnect(){}
// }
