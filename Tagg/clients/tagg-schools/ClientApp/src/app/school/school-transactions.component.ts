import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as Models from '../shared/models';
import { DataService } from '../services/data.service';
import { MatSnackBar } from '@angular/material';
import { TransactionStatusDisplay } from '../shared/constants';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-school-transactions',
  templateUrl: './school-transactions.component.html',
  styleUrls: ['./school-transactions.component.css']
})
export class SchoolTransactionsComponent implements OnInit {
  public transactions: Models.Transaction[];
  public showNoTransactions: boolean;
  public chosenTransaction: Models.Transaction;
  public statusDisplay = TransactionStatusDisplay;
  public schoolId: string;
  public idDisplay: string;
  public jobNumberSort: number = -1;
  public dateSort: number = -1;
  public paySort: number = -1;
  public totalSort: number = -1;
  public teacherSort: number = -1;
  public subSort: number = -1;
  public schools: any[] = [];

  constructor(private route: ActivatedRoute, private router: Router, private dataService: DataService, private snackBar: MatSnackBar, private authService: AuthService) { }

  ngOnInit() {
    this.dataService.setHeaders();
    this.dataService.getAdminSchools()
      .subscribe(resp => {
        this.schools = resp.map(s => {
          return {id: s.schoolBase.id, name: s.schoolBase.name};
        });
        this.schoolId = this.schools[0].id;
        this.getTransactions();
      })
    
  }

  getTransactions() {
    this.dataService.getTransactions(this.schoolId)
      .subscribe(resp => {
        this.transactions = resp;
        if (this.transactions.length) {
          this.showNoTransactions = false;
          this.selectTransaction(this.transactions[0].id);
        }
        else {
          this.showNoTransactions = true;
        }
      }, err => {});
  }

  selectTransaction(id: string) {
    this.chosenTransaction = this.transactions.find(t => t.id === id);
    this.idDisplay = this.chosenTransaction.id.substr(0, 6);
  }

  createCharge(transactionId: string) {
    this.dataService.createCharge(transactionId)
      .subscribe(resp => {
        this.getTransactions();
        this.openSnackBar("Transaction Approved!");
      }, err =>{
        this.getTransactions();
        this.openSnackBar("Oops...there was a problem processing transaction " + transactionId + ". Please contact support.");
      });
  }

  cancelTransaction(transactionId: string){
    this.dataService.transactionCancel(transactionId)
      .subscribe(resp =>{
        this.getTransactions();
        this.openSnackBar("Transaction Cancelled");
      })
  }

  openSnackBar(message: string) {
    this.snackBar.open(message, 'Dismiss', {
      duration: 5000
    });
  }

  sort(field: string) {
    let newArray: Models.Transaction[] = [];
    switch (field) {
      case 'jobNumber':
        newArray = this.transactions.sort((a, b): number => {
          if (a.jobNumber < b.jobNumber) return this.jobNumberSort * -1;
          if (a.jobNumber > b.jobNumber) return this.jobNumberSort;
          return 0;
        });
        this.jobNumberSort = this.jobNumberSort * -1;
        break;
      case 'date':
        this.transactions.sort((a, b): number => {
          if (a.jobDate < b.jobDate) return this.dateSort * -1;
          if (a.jobDate > b.jobDate) return this.dateSort;
          return 0;
        });
        this.dateSort = this.dateSort * -1;
        break;
      case 'pay':
        this.transactions.sort((a, b): number => {
          if (a.payRateUsd < b.payRateUsd) return this.paySort * -1;
          if (a.payRateUsd > b.payRateUsd) return this.paySort;
          return 0;
        });
        this.paySort = this.paySort * -1;
        break;
      case 'total':
        this.transactions.sort((a, b): number => {
          if (a.totalAmountUsd < b.totalAmountUsd) return this.totalSort * -1;
          if (a.totalAmountUsd > b.totalAmountUsd) return this.totalSort;
          return 0;
        });
        this.totalSort = this.totalSort * -1;
        break;
      case 'teacher':
        this.transactions.sort((a, b): number => {
          if (a.teacherName < b.teacherName) return this.teacherSort * -1;
          if (a.teacherName > b.teacherName) return this.teacherSort;
          return 0;
        });
        this.teacherSort = this.teacherSort * -1;
        break;
      case 'sub':
        this.transactions.sort((a, b): number => {
          if (a.subName < b.subName) return this.subSort * -1;
          if (a.subName > b.subName) return this.subSort;
          return 0;
        });
        this.subSort = this.subSort * -1;
        break;
      default: { break; }
    }

    this.transactions = newArray;
  }

}
