import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-upload-teachers',
  templateUrl: './upload-teachers.component.html'
})
export class UploadTeachersComponent implements OnInit {
  fileToUpload: File = null;
  schoolId: string;
  public showError: boolean;
  public showSuccess: boolean;
  public errorMessage: string;
  public showSpinner: boolean;
  public showNoFile: boolean;

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router, private dataService: DataService) { }

  ngOnInit() {
    this.schoolId = this.route.snapshot.paramMap.get('id');
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.showNoFile = false;
  }

  onSubmit() {
    if (this.fileToUpload) {
      this.showSpinner = true;
      this.dataService.uploadTeachers(this.schoolId, this.fileToUpload)
        .subscribe(resp => {
          this.showSuccess = true;
        }, err => {
          this.showError = true;
        });
    }
    else{
      this.showNoFile = true;
    }

  }

}
