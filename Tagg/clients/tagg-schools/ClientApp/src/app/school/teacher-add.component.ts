import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data.service';
import * as Models from "../shared/models";
import { FormBuilder, FormGroup, Validators, AbstractControl, FormArray } from '@angular/forms';
import { FileUploader } from 'ng2-file-upload';

@Component({
  selector: 'app-teacher-add',
  templateUrl: './teacher-add.component.html',
  styleUrls: ['./teacher-add.component.css']
})
export class TeacherAddComponent implements OnInit {
  public teacher: Models.UserCreateModel;
  public showIncomplete: boolean;
  public profileForm: FormGroup;
  public showForm: boolean;
  public showUpdateError: boolean;
  public userTypeParam: number;
  schoolId:string;
  public showSuccess: boolean;
  public routeData: any[] = [];

  constructor(public _authService: AuthService, private _router: Router, private _route: ActivatedRoute, private _dataService: DataService, private fb: FormBuilder) {
    this._dataService.setHeaders();
    this.schoolId = this._route.snapshot.paramMap.get('id');
    this.userTypeParam = +this._route.snapshot.paramMap.get('type');
    this.routeData = this._route.snapshot.data['linkTrail'];
  }

  get firstName(): AbstractControl { return this.profileForm.get('firstName') };
  get lastName(): AbstractControl { return this.profileForm.get('lastName') };
  get phoneNumber(): AbstractControl { return this.profileForm.get('phoneNumber') };
  get email(): AbstractControl {return this.profileForm.get('email') };

  ngOnInit() {
    this.teacher = new Models.UserCreateModel();
    this.teacher.schoolId = this.schoolId;
    this.teacher.userType = this.userTypeParam;
    this.populateForm();
  }

  populateForm() {
    this.profileForm = this.fb.group({
      firstName: [this.teacher.firstName, Validators.required],
      lastName: [this.teacher.lastName, Validators.required],
      email: [this.teacher.email, [Validators.required, Validators.email]],
      phoneNumber: this.teacher.phoneNumber,
    });
    this.showForm = true;
  }

  onSubmit() {
    this.teacher.firstName = this.firstName.value;
    this.teacher.lastName = this.lastName.value;
    this.teacher.email = this.email.value;
    this.teacher.phoneNumber = this.phoneNumber.value;
    this.teacher.sendInvitEmail = true;

    this._dataService.createUser(this.teacher)
      .subscribe(resp => {
        this._router.navigate(['/schools', this.schoolId, 'teachers']);
      }, err => { this.showUpdateError = true; })
  }

}
