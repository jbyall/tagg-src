import { Component, OnInit } from '@angular/core';
import * as Models from '../shared/models';
import { AuthService } from '../auth/auth.service';
import { DataService } from '../services/data.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-school-metrics',
  templateUrl: './school-metrics.component.html',
  styleUrls: ['./school-metrics.component.css']
})
export class SchoolMetricsComponent implements OnInit {
  public metrics: Models.SchoolMetrics
  public schoolId: string;

  constructor(private authService: AuthService, private dataService: DataService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.dataService.setHeaders();
    this.schoolId = this.route.snapshot.paramMap.get('id');

    this.dataService.getSchoolMetrics(this.schoolId)
      .subscribe(resp => {
        this.metrics = resp;
      });
  }

}
