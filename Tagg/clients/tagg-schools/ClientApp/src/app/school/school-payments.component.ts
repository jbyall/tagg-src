import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as Models from '../shared/models';
import { DataService } from '../services/data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-school-payments',
  templateUrl: './school-payments.component.html',
  styleUrls: ['./school-payments.component.css']
})
export class SchoolPaymentsComponent implements OnInit {

  public school: Models.School;
  public customerInfoForm: FormGroup;
  public accountInfo: Models.CustomerAccountInfo;

  constructor(private route: ActivatedRoute, private router: Router, private dataService: DataService, private fb: FormBuilder) { }

  get accountHolderName(){return this.customerInfoForm.get('accountHolderName')};
  get accountNumber(){return this.customerInfoForm.get('accountNumber')};
  get routingNumber(){return this.customerInfoForm.get('routingNumber')};

  ngOnInit() {
    this.dataService.setHeaders();
    let schoolId = this.route.snapshot.paramMap.get('id');
    this.dataService.getSchool(schoolId)
      .subscribe(resp => {
        this.school = resp;
        // TODO : retrieve the customer account information instead of the school
        this.accountInfo = new Models.CustomerAccountInfo();
        this.populateForm();
      }, err => {});
  }

  populateForm(){
    this.customerInfoForm = this.fb.group({
      country: this.accountInfo.country,
      currency: this.accountInfo.currency,
      routingNumber: [this.accountInfo.routingNumber, Validators.required],
      accountNumber: [this.accountInfo.accountNumber, Validators.required],
      accountHolderName: [this.accountInfo.accountHolderName, Validators.required],
      accountHolderType: [this.accountInfo.accountHolderType]
    });
  }

  async onUpdateAccount(){
    
    if(this.customerInfoForm.valid && this.customerInfoForm.dirty){
      let result = Object.assign({}, this.customerInfoForm.value);
      const {token, error} = await stripe.createToken('bank_account',{
        country: result.country,
        currency: result.currency,
        routing_number: result.routingNumber,
        account_number: result.accountNumber,
        account_holder_name: result.accountHolderName,
        account_holder_type: result.accountHolderType
      });

      if(error){
      }
      else{
        let customerModel = new Models.CustomerCreateModel();
        customerModel.sourceTokenId = token.id;
        customerModel.description = `Account for school ${this.school.name}`;
        customerModel.schoolId = this.school.id;
        this.dataService.createCustomer(customerModel)
          .subscribe(resp =>{
            this.router.navigate(['/schools', this.school.id]);
          })
      }
    }
    
  }

}
