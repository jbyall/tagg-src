import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as Models from '../shared/models';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-school-detail',
  templateUrl: './school-detail.component.html'
})
export class SchoolDetailComponent implements OnInit {

  public school: Models.School;
  constructor(private route: ActivatedRoute, private router: Router, private dataService: DataService) { }

  ngOnInit() {
    this.dataService.setHeaders();
    let schoolId = this.route.snapshot.paramMap.get('id');
    this.dataService.getSchool(schoolId)
      .subscribe(resp => {
        this.school = resp;
      }, err => {});
  }

}
