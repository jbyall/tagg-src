import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { DataService } from '../services/data.service';
import { Router, ActivatedRoute } from '@angular/router';

import * as Models from "../shared/models";
import { environment } from '../../environments/environment';
import { MatSnackBar, MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDatepicker } from '@angular/material';
import { GradeSubject, Grades, SubjectDisplay, Subjects, Meridiems, Hours } from '../shared/constants';
import { JobService } from '../services/job.service';
import { FormControl } from '@angular/forms';
// import * as _moment from 'moment';
// const moment = _moment;


@Component({
  selector: 'app-teacher-list',
  templateUrl: './teacher-list.component.html',
  styleUrls: ['./teacher-list.component.css']
})
export class TeacherListComponent implements OnInit {
  public school: Models.School;
  public teachers: Models.Teacher[];
  public selectedTeacher: Models.TeacherDetails;
  public showNoTeacher: boolean = false;
  public photos: string = environment.userPhotoEndpoint;
  public testPhotoId: string = '5ae5ebe06e28e8058ccf71e6';
  public schoolId: string;

  public subjectDisp = SubjectDisplay;
  private isIniting: boolean = true;

  constructor(private authService: AuthService, private route: ActivatedRoute, private router: Router, private dataService: DataService, public snackBar: MatSnackBar, private dialog: MatDialog) {
    this.dataService.setHeaders();
  }

  ngOnInit() {
    this.schoolId = this.route.snapshot.paramMap.get('id');
    this.loadTeachers();
  }

  loadTeachers() {
    this.dataService.getSchool(this.schoolId)
      .subscribe(resp => {
        this.school = resp;
        this.teachers = this.school.teachers;
        if (this.teachers.length > 0) {
          let selectTeacherid = this.selectedTeacher ? this.selectedTeacher.id : this.teachers[0].id
          this.onSelectTeacher(selectTeacherid);
        }
        else {
          this.router.navigate(['/schools', this.school.id, 'upload'])
        }
      });
  }

  onSelectTeacher(teacherId: string) {
    this.dataService.getTeacherDetails(teacherId)
      .subscribe(resp => {
        this.selectedTeacher = resp;
        if (window.innerWidth < 767 && !this.isIniting) {
          this.openTeacherDialog();
        }
        this.isIniting = false;
      })

  }

  onUpdatePay() {
    this.openPayRateDialog();
  }

  openTeacherDialog(): void {
    let dialogRef = this.dialog.open(TeacherDetailsDialog, {
      width: '80%',
      data: { teacher: this.selectedTeacher, school: this.school },
      closeOnNavigation: true
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        //this.loadTeachers();
      }
    })
  }

  openPayRateDialog(): void {
    let dialogRef = this.dialog.open(PayRateDialog, {
      width: '350px',
      data: { teacher: this.selectedTeacher, school: this.school },
      closeOnNavigation: true
    });

    dialogRef.afterClosed().subscribe(result => {
      this.loadTeachers();
    })

  }

  onAddTeacher() {
    this.router.navigate(['/schools', this.school.id, 'add', 1]);
  }

  onResendInvite(userId: string){
    this.dataService.resendInvite(userId)
      .subscribe(resp => {
        this.onOpenSnackBar('Invite resent to ' + this.selectedTeacher.email);
      }, err => {
        this.onOpenSnackBar('Unable to resend invite to ' + this.selectedTeacher.email);
      });
  }

  onOpenSnackBar(message: string){
    this.snackBar.open(message, 'Dismiss', {
      duration: 5000
    });
  }

  // onViewProfile() {
  //   this.snackBar.open('Coming soon :)', 'Dismiss', {
  //     duration: 5000
  //   });
  // }

  onRequestSub(): void {
    let date = new Date();
    let dialogRef = this.dialog.open(RequestForTeacherDialog, {
      width: '500px',
      data: { teacher: this.selectedTeacher, schoolId: this.schoolId, date: date }
    });
  }
}


@Component({
  selector: 'pay-rate-dialog',
  templateUrl: 'pay-rate.dialog.html',
})
export class PayRateDialog {
  public teacherPay: number;
  public payDisplay: number;


  constructor(
    public dialogRef: MatDialogRef<PayRateDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, private _router: Router, private dataService: DataService) {
    this._router.events.subscribe(() => { this.onNoClick() });
    if (this.data.teacher && this.data.school) {
      this.teacherPay = this.data.teacher.payRate > 0 ? this.data.teacher.payRate : this.data.school.payRate;
      this.payDisplay = this.teacherPay;
    }

  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onSavePay() {
    this.dataService.updateTeacherPayRate(this.data.teacher.id, this.teacherPay)
      .subscribe(resp => {
        this.onNoClick();
      })
  }
}



@Component({
  selector: 'teacher-details-dialog',
  templateUrl: 'teacher-details.dialog.html',
})
export class TeacherDetailsDialog {
  public subjectDisp = SubjectDisplay;
  constructor(
    public dialogRef: MatDialogRef<TeacherDetailsDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, private _router: Router) {
    this._router.events.subscribe(() => { this.onNoClick() });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onRequestSub() {
    this.dialogRef.close(true);
  }
}


@Component({
  selector: 'request-for-teacher-dialog',
  templateUrl: './request-for-teacher.dialog.html',
})
export class RequestForTeacherDialog {
  public school: Models.School;
  public job: Models.Job;
  public error: boolean = false;
  public subjectDisplay: string;
  public pickerDate: FormControl;
  public minDate = new Date();
  public dateFilter = (d: Date): boolean => {
    const day = d.getDay();
    return day !== 0 && day !== 6;
  }

  public hours = Hours;
  public startHour: number;
  public startMinute: number;
  public startMeridiem = Meridiems.find(m => m.key == 0);
  public endHour: number;
  public endMeridiem = Meridiems.find(m => m.key == 1);
  public endMinute: number;
  public minutes: any[] = [];

  constructor(
    public dialogRef: MatDialogRef<RequestForTeacherDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, private dataService: DataService, private jobService: JobService, private _router: Router, private authService: AuthService) {
    this._router.events.subscribe(() => { this.onNoClick() });
    this.dataService.setHeaders();
    this.job = new Models.Job();
    this.dataService.getSchoolDetails(this.data.schoolId)
      .subscribe(resp => {
        this.school = resp.schoolBase;
        this.job.teacherName = this.data.teacher.firstName + ' ' + this.data.teacher.lastName;
        this.job.teacherEmail = this.data.teacher.email;
        this.job.teacherId = this.data.teacher.id;
        this.job.subject = this.data.teacher.subjects[0];
        this.job.subNeeded = true;
        this.job.createdBy = this.authService.user.profile.sub;

        this.subjectDisplay = Subjects.find(s => s.key == this.job.subject).value;

        let startTimeParts = this.school.startTime.split(':');
        this.startHour = +startTimeParts[0] > 12 ? +startTimeParts[0] - 12 : +startTimeParts[0];
        this.startMinute = +startTimeParts[1];

        let endTimeParts = this.school.endTime.split(':');
        this.endHour = +endTimeParts[0] > 12 ? +endTimeParts[0] - 12 : +endTimeParts[0];
        this.endMinute = +endTimeParts[1];
      });
    for (let i = 0; i < 60; i++) {
      this.minutes.push({ key: i, value: `${i}`.padStart(2, '0') });
    }
    this.pickerDate = new FormControl(new Date());
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  get timeError(): boolean {
    let startDateCheck = this.calculateStartDate(this.pickerDate.value as Date);
    let endDateCheck = this.calculateEndDate(this.pickerDate.value as Date);
    return startDateCheck.getTime() >= endDateCheck.getTime();
  }
  
  onJobCreate() {
    // Set job start and end dates
    let selectedDate = this.pickerDate.value as Date;
    this.job.startDate = this.calculateStartDate(selectedDate);
    this.job.endDate = this.calculateEndDate(selectedDate);

    console.log('startDate > ', this.job.startDate.toISOString());
    console.log('endDate > ', this.job.endDate.toISOString());

    // Populate search data
    let search = new Models.SubSearch();
    search.date = this.job.startDate;
    search.schoolId = this.school.id;
    search.teacherId = this.job.teacherId;

    // Add job to job service to be used on the next page
    this.jobService.search = search;
    this.jobService.job = this.job;
    this._router.navigate(['/teachers', 'request']);

  }

  calculateStartDate(selectedDate: Date): Date{
    let startHourCheck = 0;
    // AM
    if (this.startMeridiem.key == 0){
      startHourCheck = this.startHour == 12 ? 0 : this.startHour;
    }
    // PM
    else{
      startHourCheck = this.startHour == 12 ? this.startHour : this.startHour + 12;
    }
    return new Date(selectedDate.getFullYear(), selectedDate.getMonth(), selectedDate.getUTCDate(), startHourCheck, this.startMinute, 0, 0);
  }
  calculateEndDate(selectedDate: Date): Date{
    let endHourCheck = 0;
    // AM
    if (this.endMeridiem.key == 0){
      endHourCheck = this.endHour == 12 ? 0 : this.endHour;
    }
    // PM
    else{
      endHourCheck = this.endHour == 12 ? this.endHour : this.endHour + 12;
    }
    return new Date(selectedDate.getFullYear(), selectedDate.getMonth(), selectedDate.getUTCDate(), endHourCheck, this.endMinute, 0, 0);
  }

  toggleStartMeridiem() {
    this.startMeridiem = this.startMeridiem.key == 0 ?
      Meridiems.find(m => m.key == 1) : Meridiems.find(m => m.key == 0);
  }

  toggleEndMeridiem() {
    this.endMeridiem = this.endMeridiem.key == 0 ?
      Meridiems.find(m => m.key == 1) : Meridiems.find(m => m.key == 0);
  }

}
