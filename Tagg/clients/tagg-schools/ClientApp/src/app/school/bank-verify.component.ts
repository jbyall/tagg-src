import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as Models from '../shared/models';
import { DataService } from '../services/data.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-bank-verify',
  templateUrl: './bank-verify.component.html',
  styleUrls: ['./bank-verify.component.css']
})
export class BankVerifyComponent implements OnInit {

  public school: Models.School;
  public verifyForm: FormGroup;
  public showError: boolean = false;
  constructor(private route: ActivatedRoute, private router: Router, private dataService: DataService, private fb: FormBuilder) { }

  get amountOne() { return this.verifyForm.get('amountOne') }
  get amountTwo() { return this.verifyForm.get('amountTwo') }

  ngOnInit() {
    this.dataService.setHeaders();
    let schoolId = this.route.snapshot.paramMap.get('id');
    this.dataService.getSchool(schoolId)
      .subscribe(resp => {
        this.school = resp;
        this.populateForm();
      }, err => { });
  }

  populateForm() {
    this.verifyForm = this.fb.group({
      amountOne: ['', Validators.required],
      amountTwo: ['', Validators.required],
      schoolId: this.school.id
    });
  }

  onVerify() {
    this.showError = false;
    let model = Object.assign({}, this.verifyForm.value);
    this.dataService.verifySchoolBank(model)
      .subscribe(resp => {
        this.router.navigate(['/schools', this.school.id]);
      }, err => {
        this.showError = true;
        this.verifyForm.markAsPristine;
      });
  }

}
