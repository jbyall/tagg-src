import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as Models from './models';
import { SubjectDisplay } from './constants';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-job-rating',
  templateUrl: './job-rating.component.html',
  styleUrls: ['./job-rating.component.css']
})
export class JobRatingComponent implements OnInit, OnDestroy {

  public jobId: string;
  public job: Models.Job;

  public ratingOne: number;
  public ratingTwo: number;
  public ratingThree: number;
  public ratingFour: number;
  public ratingFive: number;

  public subjects = SubjectDisplay;
  public ratingDto: Models.JobRating;

  constructor(private dataService: DataService, public route: ActivatedRoute, public router: Router, public authService: AuthService) { }

  ngOnInit() {
    this.getJob();
  }

  ngOnDestroy() {
  }

  getJob() {
    this.ratingDto = new Models.JobRating();
    if(this.dataService.jobsForReview.length){
      let jobId = this.dataService.jobsForReview.pop();
      this.dataService.getJobDetails(jobId)
      .subscribe(resp => {
        this.job = resp;
      })
    }
    else{
      this.router.navigate(['/home']);
    }
  }

  onSubmitRating() {

    this.ratingDto.substituteId = this.job.substituteId;
    this.ratingDto.jobId = this.job.id;
    this.ratingDto.schoolReviewerId = this.authService.user.profile.sub;
    this.ratingDto.teacherId = this.job.teacherId;

    this.ratingDto.ratings.push(new Models.RatingModel('Cleanliness', this.ratingOne));
    this.ratingDto.ratings.push(new Models.RatingModel('Promptness', this.ratingTwo));
    this.ratingDto.ratings.push(new Models.RatingModel('ClassroomManagement', this.ratingThree));
    this.ratingDto.ratings.push(new Models.RatingModel('Effectiveness', this.ratingFour));
    this.ratingDto.ratings.push(new Models.RatingModel('Professionalism', this.ratingFive));

    this.dataService.addJobRating(this.ratingDto)
      .subscribe(resp => {
        if (this.dataService.jobsForReview.length) {
          this.ratingDto = null;
          this.job = null;
          this.ratingOne = 0;
          this.ratingTwo = 0;
          this.ratingThree = 0;
          this.ratingFour = 0;
          this.ratingFive = 0;
          this.getJob();
        }
        else {
          this.router.navigate(['/home']);
        }
      }, err => {
        //this.router.navigate(['/teachers', 'home'])}
      });
  }

}
