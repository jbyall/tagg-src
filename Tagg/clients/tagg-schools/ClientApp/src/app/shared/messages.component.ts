import { Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewChecked, AfterViewInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { AuthService } from '../auth/auth.service';
import * as Models from '../shared/models';
import { ActivatedRoute } from '@angular/router';
import { SubjectDisplay } from '../shared/constants';
import { MessageService } from '../services/message.service';
import { MatDialog } from '@angular/material';
import { ViewJobDialog } from './view-job.dialog';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.css']
})
export class MessagesComponent implements OnInit, AfterViewChecked, AfterViewInit, OnDestroy {

  public showSpinner: boolean = true;
  public showError: boolean;

  public jobId: string;
  public job: Models.Job;
  public subjectDisplay = SubjectDisplay;

  public channels: any[] = [];
  //public channelSummary: any[] = [];

  public activeChannel: any;
  //public activeChannelSummary: any;

  public channelMessages: any[] = [];
  public newMessageContent: string;
  public currentUserId: string;

  public didManualScroll: boolean = false;
  public autoScroll: boolean = true;

  public isJobOffer: boolean = false;
  public showChannels: boolean;
  public messagesLoaded: boolean;

  public imConsuming: boolean;
  public chatInitialized: boolean;

  constructor(private dataService: DataService, public authService: AuthService, public messageService: MessageService, private _route: ActivatedRoute, private dialog: MatDialog) {
    this.messageService.chatReady$
      .subscribe(resp => {
        //console.log('Component: chat ready fired');
        if (!this.chatInitialized && resp) {
          this.chatInitialized = true;
          this.initChat();
        }
      });

      this.messageService.newChannel$
        .subscribe(resp => {
          //console.log('Component: new channel fired');
          if(resp){
            this.refreshChannels();
          }
        });
  }

  private print = (event) => this.printMessage(event);

  @ViewChild('messageContainer') messageContainer: ElementRef;
  @ViewChild('chatPanel') chatPanel: ElementRef;

  ngOnInit() {
    this.jobId = this._route.snapshot.paramMap.get('id');
    this.dataService.setHeaders();
    this.currentUserId = this.authService.user.profile.sub;

    // Keep autoscrolling until all messages are loaded
    // NOTE: 3 seconds is a conservative guess. Seems to work well
    setTimeout(() => this.autoScroll = false, 3000);

    // Check status of messages after all is loaded and stuff
    setTimeout(() => this.messageService.getStatus(), 5000);
  }

  ngAfterViewChecked() {
    if (!this.didManualScroll) {
      this.scrollToBottom();
    }

    if (!this.imConsuming
      && this.activeChannel
      && this.activeChannel.lastConsumedMessageIndex != this.activeChannel.lastMessage.index) {
      this.imConsuming = true;
      //console.log('activeChannelConsumed', this.activeChannel.lastConsumedMessageIndex);
      //console.log('lastMessage', this.activeChannel.lastMessage.index);
      this.activeChannel.updateLastConsumedMessageIndex(this.activeChannel.lastMessage.index)
        .then(resp => { this.imConsuming = false });
    }
  }

  ngAfterViewInit() {
    //console.log('Component: after veiw init');
  }

  ngOnDestroy() {
    //this.messageService.onResumeNavIcon();
  }

  initChat() {
    //console.log('Component: initting client');
    this.messageService.chatClient.getLocalChannels({criteria: 'lastMessage', order: 'descending'})
      .then(resp => {
        this.channels = resp;
        this.getChannel(this.channels[0].sid);
      })
      .catch(err => this.handleError(err));
  }

  refreshChannels(){
    this.messageService.chatClient.getLocalChannels({criteria: 'lastMessage', order: 'descending'})
      .then(resp => {
        this.channels = resp;
      });
  }


  // triggered on first load and from onGetChannel (see below)
  getChannel(sid: string) {
    if (!this.activeChannel || sid != this.activeChannel.sid) {
      //console.log('Component: getting channel');
      this.messageService.chatClient.getChannelBySid(sid)
        .then(resp => {
          this.activeChannel = resp;
          this.showChannels = true;
          this.showSpinner = false;
          //console.log('Component: got channel', this.activeChannel);
          this.loadMessages();
          this.setChannelListeners();
          let jobId = this.activeChannel.uniqueName.split('-')[0];
          if(jobId){
            this.getJob(jobId);
          }
        });
    }
    else {
      // if user clicks on active channel, just scroll to bottom
      //console.log('Component: getChannelElse');
      this.didManualScroll = false;
      this.scrollToBottom();
    }
  }

  // triggered when user clicks on a new channel
  onGetChannel(uniqueName: string) {
    if (uniqueName != this.activeChannel.uniqueName) {
      this.channelMessages = [];
      this.removeChannelListeners();
      this.getChannel(uniqueName);
    }

  }

  // EVENTS
  setChannelListeners() {
    this.activeChannel.addListener('messageAdded', this.print);
  }

  removeChannelListeners() {
    this.activeChannel.removeListener("messageAdded", this.print);
  }

  loadMessages() {
    //console.log('Component: loading messages');
    this.messagesLoaded = false;
    this.activeChannel.getMessages(50)
      .then(resp => {
        this.channelMessages = resp.items;
        //console.log('Component: got messages', resp);
        this.messagesLoaded = true;
        this.didManualScroll = false;
        this.scrollToBottom();
      });
  }

  sendMessage(event: any, sendButton: boolean = false) {
    if(this.newMessageContent !== '' && (sendButton || event.keyCode == 13)){
      //console.log('Component: sending message');
      let messageToSend = this.newMessageContent;
      this.newMessageContent = '';
      this.activeChannel.sendMessage(messageToSend, {fromName: this.authService.user.profile.given_name + ' ' + this.authService.user.profile.family_name});
      return false;
        //.then(resp => this.newMessageContent = '');
    }

    // if (event.keyCode == 13 && this.newMessageContent !== "") {
      
    // }
  }

  onSendButton() {
    //console.log('Component: sending message (button)');
    this.sendMessage(null, true);
    // if (this.newMessageContent !== '') {
    //   this.activeChannel.sendMessage(this.newMessageContent)
    //     .then(resp => this.newMessageContent = '');
    // }
  }

  printMessage(message: any) {
    //console.log('Component: printing message', message);
    this.channelMessages = this.channelMessages.concat(message);
    this.scrollToBottom();
    //this.onReadActiveChannel();
  }

  scrollToBottom() {
    if (this.chatPanel && !this.didManualScroll) {
      this.chatPanel.nativeElement.scrollTop = this.chatPanel.nativeElement.scrollHeight;
    }
  }

  setManualScroll() {
    // NOTE: Autoscroll is used for when messages page is first loading are loading
    // Without this, the scroll event on chatPanel is fired from this.scrollToBotom() (see above)
    if (!this.autoScroll) {
      let top = this.chatPanel.nativeElement.scrollTop;
      let height = this.chatPanel.nativeElement.scrollHeight;
      let panelHeight = this.chatPanel.nativeElement.offsetHeight;

      if ((height - top - panelHeight) > 1) {
        //console.log('Component: set manual scroll');
        this.didManualScroll = true;
      }
      else {
        this.didManualScroll = false;
      }
    }
  }

  getJob(jobId: string){
    this.dataService.getJobDetails(jobId)
      .subscribe(resp => {
        this.job = resp;
        //console.log('job', this.job);
      });
  }

  onJobDetails(){
    let jobNumber = this.activeChannel.uniqueName.split('-')[0];
    this.dataService.getJobDetails(jobNumber)
      .subscribe(resp => {
        this.openJobDialog(resp);
      })
  }

  openJobDialog(selectedJob: Models.Job): void{
    let dialogRef = this.dialog.open(ViewJobDialog,{
      width: '500px',
      data: {job: selectedJob}
    });
  }

  onLessonPlanUpload(lessonPlan: FileList){
    let lessonPlanUpload = lessonPlan.item(0);
    let fileType = lessonPlanUpload.type;

    if(fileType === "application/msword"
        || fileType === "application/pdf" 
        || fileType === "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
        {
        var data = new FormData();
        data.append("file", lessonPlanUpload);
        this.dataService.uploadLessonPlan(this.job.id ,data)
            .subscribe(resp => {
                this.job.lessonPlanId = resp;
            }, err => {});
            
    }
    else{
        // Do something
    }
}

onDownloadLessonPlan(){
  this.dataService.getLessonPlan(this.job.lessonPlanId)
    .subscribe(resp => {
      window.open(window.URL.createObjectURL(resp));
    })
}





  handleError(err: any) {
    this.showSpinner = false;
    this.showError = true;
  }

}