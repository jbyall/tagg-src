import {Component, OnInit} from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { ActivatedRoute } from '@angular/router';
import { MatStepper } from '@angular/material';
import { DataService } from '../../services/data.service';

@Component({
    selector: 'verify-phone',
    templateUrl: './verify-phone.component.html'
})
export class VerifyPhoneComponent implements OnInit{

    constructor(public dataService: DataService, private _route: ActivatedRoute){ }

    public phone: string;
    public code: string;
    public showVerify: boolean = false;
    public showError: boolean = false;
    public showSuccess: boolean = false;

    ngOnInit(){
        let phoneNumber = this._route.snapshot.paramMap.get('id');
        this.phone = phoneNumber;
        this.dataService.setHeaders();
    }

    onSendCode(){
        this.dataService.sendPhoneCode(this.phone)
            .subscribe(resp => {
                this.showVerify = true;
            }, err => {
                this.showError = true;
            });
    }

    onVerify(){
        this.dataService.verifyCode(this.code, this.phone)
            .subscribe(resp =>{
                this.showSuccess = true;
            }, err => {
                this.showError = true;
            });
    }
    onStep(stepper: MatStepper){
        stepper.next();
    }
}