import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
})
export class ProfileComponent implements OnInit {

  constructor(private authService: AuthService, private router: Router) {

  }

  ngOnInit() {
    let userType = this.authService.user.profile.api_access;
    if (this.authService.user.profile.api_access == "0x2") {
      this.router.navigate(['/teachers/profile']);
    }
  }

}
