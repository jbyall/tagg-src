// import { Component, Input, OnInit, ViewEncapsulation, OnChanges, Output, EventEmitter } from '@angular/core';
// import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl, ValidatorFn, FormArray } from "@angular/forms";
// import { ProfileModel, ApplyViewData } from '../models';
// import { AuthService } from '../../services/auth.service';


// @Component({
//     selector: 'profile-form',
//     templateUrl: './profile-form.component.html',
// })
// export class ProfileFormComponent implements OnChanges {
//     profileForm: FormGroup;
//     @Input() profile: ProfileModel;
//     @Input() viewData: ApplyViewData;
//     @Output() notify: EventEmitter<string> = new EventEmitter();
//     public yesNo = [{key:true, value:"Yes"}, {key:false, value:"No"}];
    
//     constructor(
//         private fb: FormBuilder,
//         private authService: AuthService) { 
//             this.profileForm = this.fb.group({
//                 education: ['', Validators.required],
//                 hasTranscripts: ['', Validators.required],
//                 hasLicense: ['', Validators.required],
//                 resumeText: '',
//                 linkedInUrl:''
//             });
//         }


//         ngOnChanges(){
//             if(this.profile){
//                 this.profileForm.patchValue({
//                     education: this.profile.education,
//                     hasTranscripts: this.profile.hasTranscripts,
//                     hasLicense: this.profile.hasLicense,
//                     resumeText: this.profile.resumeText,
//                     linkedInUrl: this.profile.linkedInUrl
//                 });
//                 this.profileForm.markAsDirty();
//             }
//         }

//         onProfileSubmit(){
//             this.notify.emit("profile");
//             if(this.profileForm.dirty && this.profileForm.touched && this.profileForm.valid){
//                 let p = new ProfileModel();
//                 Object.assign(p, this.profileForm.value);
//                 // this.authService.saveProfile(p)
//                 //     .subscribe(resp =>{
//                 //         this.profileForm.markAsUntouched();
//                 //     })
                    
//             }
//         }

    
// }