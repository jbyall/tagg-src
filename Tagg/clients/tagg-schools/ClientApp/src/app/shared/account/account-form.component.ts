// import { Component, Input, OnInit, ViewEncapsulation, OnChanges, Output, EventEmitter } from '@angular/core';
// import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl, ValidatorFn, FormArray } from "@angular/forms";
// import { AccountModel } from '../models';
// import { AuthService } from '../../services/auth.service';


// @Component({
//     selector: 'account-form',
//     templateUrl: './account-form.component.html',
// })
// export class AccountFormComponent implements OnChanges {
//     accountForm: FormGroup;
//     @Input() accountModel: AccountModel;
//     @Output() notify: EventEmitter<string> = new EventEmitter();

//     constructor(
//         private fb: FormBuilder,
//         private authService: AuthService) { 
//             this.accountForm = this.fb.group({
//                 street1: ['', Validators.required],
//                 street2: '',
//                 city: ['', Validators.required],
//                 state: ['', Validators.required],
//                 postalCode: ['', Validators.required],
//                 phoneNumber: ['', Validators.required],
//                 firstName: '',
//                 lastName: '',
//             });
//             if(this.accountModel){
//                 this.populateForm();
//             }
//         }

//         onAccountSubmit(){
//             this.notify.emit('account');
//             if(this.accountForm.dirty && this.accountForm.touched && this.accountForm.valid){
//                 let a = new AccountModel();
//                 Object.assign(a, this.accountForm.value);
//                 // this.authService.saveAccount(a)
//                 //     .subscribe(resp =>{
//                 //         this.accountForm.markAsUntouched();
//                 //     });
//             }
//         }

//         ngOnChanges(){
//             this.populateForm();
//         }

//         populateForm(){
//             if(this.accountModel){
//                 // TODO: check to see if you can pass in the whole object
//                 this.accountForm.patchValue({
//                     street1: this.accountModel.street1,
//                     street2: this.accountModel.street2,
//                     city: this.accountModel.city,
//                     state: this.accountModel.state,
//                     postalCode: this.accountModel.postalCode,
//                     phoneNumber: this.accountModel.phoneNumber,
//                     firstName: this.accountModel.firstName,
//                     lastName: this.accountModel.lastName
//                 });
//                 this.accountForm.markAsDirty();
//             }
//         }

    
// }