// import { Component, Input, OnInit, ViewEncapsulation, OnChanges, Output, EventEmitter } from '@angular/core';
// import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl, ValidatorFn, FormArray } from "@angular/forms";
// import { ApplyViewData, JobPreferencesModel } from '../models';
// import { AuthService } from '../../services/auth.service';


// @Component({
//     selector: 'preferences-form',
//     templateUrl: './preferences-form.component.html',
// })
// export class PreferencesFormComponent implements OnChanges {
//     preferencesForm: FormGroup;
//     @Input() preferences: JobPreferencesModel;
//     @Output() notify: EventEmitter<string> = new EventEmitter();
//     public yesNo = [{key:true, value:"Yes"}, {key:false, value:"No"}];
    
//     constructor(
//         private fb: FormBuilder,
//         private authService: AuthService) { 
//             this.preferencesForm = this.fb.group({
//                 minPay: ['', Validators.required],
//                 maxDistance: ['', Validators.required],
//                 elementarySubjects: this.fb.array([]),
//                 middleSchoolSubjects: this.fb.array([]),
//                 highSchoolSubjects: this.fb.array([]),
//             });
//         }
//         get elementarySubjects(): FormArray{
//             return <FormArray>this.preferencesForm.get('elementarySubjects');
//         }

//         get middleSchoolSubjects(): FormArray{
//             return <FormArray>this.preferencesForm.get('middleSchoolSubjects');
//         }

//         get highSchoolSubjects(): FormArray{
//             return <FormArray>this.preferencesForm.get('highSchoolSubjects');
//         }

        

//         ngOnChanges(){
//             if(this.preferences){
//                 this.preferencesForm.patchValue({
//                     minPay: this.preferences.minPay,
//                     maxDistance: this.preferences.maxDistance,
//                 });
    
//                 let elementaryArray = this.preferences.subjects.slice(0, 7);
//                 const elemSubjectGroups = elementaryArray.map(subject => this.fb.group(subject));
//                 const elemFormArray = this.fb.array(elemSubjectGroups);
//                 this.preferencesForm.setControl('elementarySubjects', elemFormArray);
    
//                 let msArray = this.preferences.subjects.slice(7, 12);
//                 const msSubjectGroups = msArray.map(subject => this.fb.group(subject));
//                 const msFormArray = this.fb.array(msSubjectGroups);
//                 this.preferencesForm.setControl('middleSchoolSubjects', msFormArray);
                
//                 let hsArray = this.preferences.subjects.slice(12);
//                 const hsSubjectGroups = hsArray.map(subject => this.fb.group(subject));
//                 const hsFormArray = this.fb.array(hsSubjectGroups);
//                 this.preferencesForm.setControl('highSchoolSubjects', hsFormArray);
//                 this.preferencesForm.markAsDirty();
//             }
//         }

//         onPreferencesSubmit(){
//             this.notify.emit('account');
//             if(this.preferencesForm.dirty && this.preferencesForm.touched && this.preferencesForm.valid){
//                 let p = new JobPreferencesModel();
//                 p.minPay = this.preferencesForm.controls["minPay"].value;
//                 p.maxDistance = this.preferencesForm.controls["maxDistance"].value;
//                 p.subjects = p.subjects
//                         .concat(this.elementarySubjects.value)
//                         .concat(this.middleSchoolSubjects.value)
//                         .concat(this.highSchoolSubjects.value);
//                 // this.authService.savePreferences(p)
//                 //     .subscribe(resp =>{
//                 //         this.preferencesForm.markAsUntouched();
//                 //     })
                    
//             }
//         }

    
// }