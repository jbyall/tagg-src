// Modules
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Components
import { TaggCommonModule } from '../tagg-common.module';
// import { AccountFormComponent } from './account-form.component';
// import { ProfileFormComponent } from './profile-form.component';
// import { PreferencesFormComponent } from './preferences-form.component';
// import { ContactFormComponent } from './contact-form.component';
// import { AccountComponent, AccountDialog } from './account.component';
import { VerifyPhoneComponent } from './verify-phone.component';
import { ProfileComponent } from './profile.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TaggCommonModule
    ],
    declarations: [
        // AccountFormComponent,
        // ProfileFormComponent,
        // PreferencesFormComponent,
        // ContactFormComponent,
        // AccountComponent,
        // AccountDialog,
        VerifyPhoneComponent,
        ProfileComponent
    ],
    exports: [
        // AccountFormComponent,
        // ProfileFormComponent,
        // PreferencesFormComponent,
        // ContactFormComponent,
        VerifyPhoneComponent,
        ProfileComponent
    ],
    // entryComponents:[AccountDialog]
})
export class AccountModule { }