// import { Component, OnInit, Inject } from '@angular/core';
// import { AuthService } from '../../services/auth.service';
// import { SubUserModel, AccountModel, ProfileModel, ApplyViewData, AccountSection, JobPreferencesModel, ContactPreferencesModel } from '../models';
// import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
// import { AccountFormComponent } from './account-form.component';

// @Component({
//     selector: 'app-account',
//     templateUrl: './account.component.html'
// })
// export class AccountComponent implements OnInit {
//     user: SubUserModel;
//     public accountStatus = { icon:'error_outline', color:'list-group-item list-group-item-warning', panel:'panel panel-warning' };
//     public profileStatus = { icon:'error_outline', color:'list-group-item list-group-item-warning', panel: 'panel panel-warning' };
//     public preferencesStatus = { icon:'error_outline', color:'list-group-item list-group-item-warning', panel:'panel panel-warning' };
//     public contactStatus = { icon:'error_outline', color:'list-group-item list-group-item-warning', panel:'panel panel-warning' };
//     public applicationComplete: boolean = false;

//     constructor(public _authService: AuthService, public dialog: MatDialog) { }

//     ngOnInit() {
//         this.getUser();
//     }

//     onAccountEdit() {
//         let accountDialog = this.dialog.open(AccountDialog, {
//             width: '1000px',
//             data: { section: AccountSection.Account, account: this.user.account },
//         });
//         accountDialog.afterClosed().subscribe(result => {
//             this.getUser();
//         });
//     }

//     onProfileEdit() {
//         let profileDialog = this.dialog.open(AccountDialog, {
//             width: '1000px',
//             data: { section: AccountSection.Profile, profile: this.user.profile, viewData: this.user.viewData },
//         });
//         profileDialog.afterClosed().subscribe(result => {
//             this.getUser();
//         });
//     }

//     onPreferencesEdit() {
//         let preferencesDialog = this.dialog.open(AccountDialog, {
//             width: '1000px',
//             data: { section: AccountSection.Preferences, preferences: this.user.preferences },
//         });
//         preferencesDialog.afterClosed().subscribe(result => {
//             this.getUser();
//         });
//     }

//     onContactEdit() {
//         let contactDialog = this.dialog.open(AccountDialog, {
//             width: '1000px',
//             data: { section: AccountSection.Contact, contact: this.user.contact },
//         });
//         contactDialog.afterClosed().subscribe(result => {
//             this.getUser();
//         });
//     }

//     getUser() {
//         // this._authService.getAccount()
//         //     .subscribe(resp => {
//         //         this.user = resp;
//         //         this.getUserStatus();
//         //     })
//     }

//     getUserStatus(){
//         if(this.user.account.isComplete && this.user.profile.isComplete &&
//         this.user.preferences.isComplete && this.user.contact.isComplete){
//             this.applicationComplete = true;
//         }
//         else{ this.applicationComplete = false;}
//         if(this.user.account.isComplete){
//             this.accountStatus.icon = "done";
//             this.accountStatus.color = 'list-group-item list-group-item-success';
//             this.accountStatus.panel = 'panel panel-default';
//         }
//         if(this.user.profile.isComplete){
//             this.profileStatus.icon = "done";
//             this.profileStatus.color = 'list-group-item list-group-item-success';
//             this.profileStatus.panel = "panel panel-default";
//         }
//         if(this.user.preferences.isComplete){
//             this.preferencesStatus.icon = "done";
//             this.preferencesStatus.color = 'list-group-item list-group-item-success';
//             this.preferencesStatus.panel = 'panel panel-default';
//         }
//         if(this.user.contact.isComplete){
//             this.contactStatus.icon = "done";
//             this.contactStatus.color = 'list-group-item list-group-item-success';
//             this.contactStatus.panel = 'panel panel-default';
//         }
//     }
// }

// @Component({
//     selector: 'account-dialog',
//     template: `
//     <div *ngIf="section===1">
//         <account-form [accountModel]="account" (notify)="onAccountUpdated()"></account-form>
//     </div>
//     <div *ngIf="section===2">
//         <profile-form [profile]="profile" [viewData]="viewData" (notify)="onAccountUpdated()"></profile-form>
//     </div>
//     <div *ngIf="section===3">
//         <preferences-form [preferences]="preferences" (notify)="onAccountUpdated()"></preferences-form>
//     </div>
//     <div *ngIf="section===4">
//         <contact-form [contact]="contact" (notify)="onAccountUpdated()"></contact-form>
//     </div>
//     `
// })
// export class AccountDialog {
//     // TODO: Trigger ngOnChanges to get view data to populate
//     account: AccountModel;
//     profile: ProfileModel;
//     preferences: JobPreferencesModel;
//     contact: ContactPreferencesModel;
//     viewData: ApplyViewData;
//     section: AccountSection;

//     constructor(public dialogRef: MatDialogRef<AccountDialog>, @Inject(MAT_DIALOG_DATA) public data: any) {
//         this.section = data.section;
//         switch (this.section) {
//             case AccountSection.Account: {
//                 this.account = data.account;
//                 break;
//             }
//             case AccountSection.Profile: {
//                 this.profile = data.profile;
//                 this.viewData = data.viewData;
//                 break;
//             }
//             case AccountSection.Preferences: {
//                 this.preferences = data.preferences;
//                 break;
//             }
//             case AccountSection.Contact: {
//                 this.contact = data.contact;
//                 break;
//             }
//             default:{ break;}
//         }
//     }
//     onAccountUpdated(message: string, stepper: any): void {
//         this.dialogRef.close();
//     }

// }

