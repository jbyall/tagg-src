import { Component, OnInit, Inject, EventEmitter, Output } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from '../auth/auth.service';
import * as Models from "../shared/models";
import { DataService } from '../services/data.service';
import { User } from 'oidc-client';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { SubjectDisplay } from './constants';

@Component({
    selector: 'view-job-dialog',
    templateUrl: './view-job.dialog.html',
    styleUrls: ['./view-job.dialog.css']
  })
  export class ViewJobDialog {
    @Output() notify: EventEmitter<string> = new EventEmitter();
    public subjects = SubjectDisplay;
    public showUploadSuccess: boolean;
    public showCancelConfirm: boolean;
    public showPay: boolean;
  
    constructor(
      public dialogRef: MatDialogRef<ViewJobDialog>,
      @Inject(MAT_DIALOG_DATA) public data: any, private dataService: DataService, private router: Router, public authService: AuthService) { 
        if(this.authService.user.profile.api_access !== '0x2'){
          this.showPay = true;
        }
      }
  
    onNoClick(): void {
      this.dialogRef.close();
    }

    onMessageSub(jobId: string){
      this.router.navigate(['/messages']);
      this.onNoClick();
    }

    onCancelConfirm(){
      this.showCancelConfirm = true;
    }

    onCancelJob(){
      this.dataService.cancelJob(this.data.job.id)
        .subscribe(resp => {
          this.onNoClick();
        })
    }

    onLessonPlanUpload(lessonPlan: FileList){
      let lessonPlanUpload = lessonPlan.item(0);
      let fileType = lessonPlanUpload.type;
  
      if(fileType === "application/msword"
          || fileType === "application/pdf" 
          || fileType === "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
          {
          var data = new FormData();
          data.append("file", lessonPlanUpload);
          this.dataService.uploadLessonPlan(this.data.job.id ,data)
              .subscribe(resp => {
                  this.data.job.lessonPlanId = resp;
                  this.showUploadSuccess = true;
              }, err => {});
              
      }
      else{
          // Do something
      }
  }
  
  }