import { Time } from "@angular/common";

export class TimeSpan {
    public hour: number;
    public minute: number;
}

export class CalendarDay {
    public date: Date;
    public year: number;
    public monthIndex: number;
    public dayOfMonthIndex: number;
    public dayOfWeekIndex: number;
    public dayOfWeek: string;
    public events: CalendarEvent[];
}

export class CalendarEvent {
    public description: string;
    public job: Job;
    public availability: UserAvailability;
    public isBlackout: boolean;
    public schoolId: string;
}

export class UserAvailability {
    public startDate: Date;
    public endDate: Date;
}

export class SubSearch {
    public date: Date;
    public schoolId: string;
    public teacherId: string;
}

export class SubSearchResult {
    public favorites: Substitute[];
    public others: Substitute[];
    public total: number;
}

export class SchoolJobsResult {
    public recent: Job[];
    public upcoming: Job[];
    public today: Job[];
}

export class CustomerAccountInfo {
    public country: string = 'US';
    public currency: string = 'usd';
    public routingNumber: string;
    public accountNumber: string;
    public accountHolderName: string;
    public accountHolderType: string = 'individual';
}

export class CustomerCreateModel {
    public sourceTokenId: string;
    public description: string;
    public schoolId: string;
}

export class BankVerifyModel {
    public amountOne: number;
    public amountTwo: number;
    public schoolId: string;
}


export class Substitute {
    constructor(
        public educationLevel: string = '',
        public hasTeachingLicense: boolean = false,
        public hasTranscripts: boolean = false,
        public resumeFile: string = '',
        public applicationComplete: boolean = false,
        //public approvalDate: Date = new Date(),
        public linkedInUrl: string = '',
        public minPayAmount: 0,
        public maxDistance: 0,
        public location: null,
        public subjects: string[] = [],
        public id: string = '',
        public userType: 0,
        public firstName: string = '',
        public lastName: string = '',
        public email: string = '',
        public phoneNumber: string = '',
        public picture: null,
        public bio: string = '',
        public title: string = '',
        public receivePhone: boolean = true,
        public receiveSMS: boolean = true,
        public receiveEmail: boolean = true,
        public enabled: boolean = false,
        public status: number = 0,
        public interests: string = '',
        public address: Address = new Address(),
        public isFavorite: boolean = false,
        public sendRequest: boolean = true,
        public rating: number = 0,
        public subbedAtSchool: boolean = false,
        public ratingCount: number = 0,
        public exclusions: string[] = []
    ) { }
}

export class UserCreateModel {
    public firstName: string;
    public lastName: string;
    public email: string;
    public phoneNumber: string;
    public sendInvitEmail: boolean = true;
    public schoolId: string;
    // Default to teacher
    public userType: number;
}

export class IUser {
    public email: string;
    public id: string;
    public firstName: string;
    public lastName: string;
    public phoneNumber: string;
    public school: string;
    public type: string;
    public apiAccess: string;
}


export class AdminRegisterModel {
    constructor(
        public firstName: string = '',
        public lastName: string = '',
        public password: string = '',
        public confirmPassword: string = '',
        public role: string = '',
    ) { }
}

export class SchoolInqury {
    public id: string;
    public firstName: string;
    public lastName: string;
    public email: string;
    public phoneNumber: string;
    public schoolName: string;
    public districtName: string;
    public title: string;
    public city: string;
    public state: string;
    public postalCode: string;
    public status: string;
}

// Model for register form
export class RegisterModel {
    constructor(
        public firstName: string = '',
        public lastName: string = '',
        public email: string = '',
        public password: string = '',
        public confirmPassword: string = '',
        public postalCode: string = '',
        public apiAccessLevel: string = ''
    ) { }
}

export class School {
    public id: string;
    public name: string;
    public url: string;
    public schoolType: string;
    public contactEmail: string;
    public contactPhone: string;
    public address: Address;
    public startTime: string;
    public endTime: string;
    public summary: string;
    public payRate: number;
    public status: string;
    public teachers: Teacher[];
    public grades: string;
    public customerId: string;
    public accountStatus: number;
    public admins: SchoolAdmin[];
}

export class SchoolDetail {
    public schoolBase: School;
    public availableSubCount: number;
}

export class SchoolAdmin {
    public id: string;
    public firstName: string;
    public lastName: string;
    public fullName: string;
    public email: string;
    public enabled: boolean;
    public status: number;

    // Not currently used
    public phoneNumber: string;
    public picture: string;
    public bio: string;
    public title: string;
    public receivePhone: boolean;
    public receiveSMS: boolean;
    public receiveEmail: boolean;

}

export class Message {
    public to: string;
    public from: string;
    public subject: string;
    public body: string;
    public readOn?: Date;
    public id: string;
    public created: Date;
}

export class MessageThread {
    public id: string;
    public subject: string;
    public messages: Message[];
    public audience: string[];
    public jobId: string;
}

export class Transaction {
    public id: string;
    public jobId: string;
    public jobNumber: string;
    public status: number;
    public customerId: string;
    public processOn: Date;
    public payRateUsd: number;
    public totalAmountUsd: number;
    public teacherName: string;
    public subName: string;
    public jobDate: Date;
}

export enum JobStatus {
    Open = 0,
    Accepted = 1,
    Cancelled = 2,
    Complete = 3
}

export enum JobModalType {
    Admin = 0,
    AdminRecent = 1,
    Pending = 1,
    Accepted = 2,
    Recent = 3
}

export class Job {
    public id: string;
    public teacherId: string;
    public subject: string;
    public payAmount: number;
    public chargeAmount: number;
    public startDate: Date;
    public endDate: Date;
    public teacherName: string;
    public teacherEmail: string;
    public schoolName: string;
    public status: number;
    public subNeeded: boolean;
    public substituteId: string;
    public substituteName: string;
    public lessonPlanId: string;
    public rating: number;
    public createdBy: string;
    public subRating: number;
    public ratingCount: number;
    public subbedAtSchool: boolean;
}

export class JobCreateModel {
    public schoolId: string;
    public teacherId: string;
    public startDate: Date;
    public endDate: Date;
    public subNeeded: boolean;
}

export class JobRating {
    public substituteId: string;
    public jobId: string;
    public schoolReviewerId: string;
    public teacherId: string;
    public ratings: RatingModel[] = [];
}

export class RatingModel {
    constructor(
        public category: string = "",
        public rating: number = 5
    ) { }

}

export class JobOfferModel {
    public job: JobCreateModel;
    public selected: string[];
    public favorites: string[];
    public others: string[];
}

export class Teacher {
    public id: string;
    public userType: number;
    public schoolId: string;
    public firstName: string;
    public lastName: string;
    public email: string;
    public phoneNumber: string;
    public picture: string;
    public bio: string;
    public title: string;
    public receivePhone: boolean;
    public receiveSMS: boolean;
    public receiveEmail: boolean;
    public enabled: boolean;
    public status: number;
    public address: Address;
    public fullName: string;
    public subjects: number[];
    public jobs: string[];
    public favorites: string[];
    public payRate: number;
    public phoneVerified: boolean;
}

export class TeacherDetails {
    public id: string;
    public userType: number;
    public schoolId: string;
    public firstName: string;
    public lastName: string;
    public email: string;
    public phoneNumber: string;
    public picture: string;
    public bio: string;
    public title: string;
    public receivePhone: boolean;
    public receiveSMS: boolean;
    public receiveEmail: boolean;
    public enabled: boolean;
    public status: number;
    public address: Address;
    public fullName: string;
    public subjects: number[];
    public jobs: string[];
    public favorites: string[];
    public hoursTaken: number;
    public hoursCovered: number;
    public payRate: number;
}

export class TeacherMetrics {
    public timeOff: number;
    public timeCovered: number;
    public subsHired: number;
    public recentJob: Job;
    public favoriteSub: string;
}

export class SchoolMetrics {
    public subDaysMonth: number;
    public filledSytdPercent: number;
    public teacherDaysMonth: number;
    public teacherDaysSytd: number;
    public subDaysSytd: number;
    public avgRating: number;
    public popularSubs: number;
}

export class DistrictMetrics {
    public teacherDaysMonth: number;
    public teacherDaysSytd: number;
    public subDaysMonth: number;
    public subDaysSytd: number;
    public jobsFilledToday: number;
    public jobsUnfilledToday: number;
    public jobsFilledSytdPercent: number;
    public avgRating: number;
}

export class TeacherUpdateResult {
    public phoneNumber: string;
    public verificationRequired: boolean;
    public teacher: Teacher;
}

export class Address {

    public street1: string;
    public street2: string;
    public city: string;
    public state: string;
    public postalCode: string;

}

// Result from distance calculation
export class DistanceResult {
    constructor(
        public text: string,
        public value: number,
        public serviceLocation: Location,
        public inServiceArea: boolean
    ) { }
}

//
export class Location {
    public latitude: number;
    public longitude: number;
}

export class LoginModel {
    constructor(
        public email: string,
        public password: string
    ) { }
}

export class TokenResponse {
    constructor(
        public token: string = "",
        public expiration: Date = new Date()
    ) { }
}

export class Lookup {
    constructor(
        public key: string,
        public value: string,
        public id: number,
        public type: string
    ) { }
}

export class MultiSelect {
    constructor(
        public key: string = '',
        public value: string = '',
        public selected: boolean = false
    ) { }
}

export class SimpleEntityDto {
    public id: string;
    public name: string;
}

export class BlackoutAddDto {
    constructor(
        public day: Date,
        public schools: string[]
    ) { }

}

