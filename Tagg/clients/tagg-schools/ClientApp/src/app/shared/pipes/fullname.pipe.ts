import { Pipe, PipeTransform } from '@angular/core';
@Pipe({name: 'flastNameSplit'})
export class FLastNameSplitPipe implements PipeTransform {
  transform(value: string): string {
    var nameParts = value.split(' ');
    if(nameParts.length > 1){
        return nameParts[0].slice(0,1) + '. ' + nameParts[1];
    }
    else{
        return value;
    }
  }
}