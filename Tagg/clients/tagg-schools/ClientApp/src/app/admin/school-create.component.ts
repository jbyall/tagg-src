import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as Models from '../shared/models';
import { DataService } from '../services/data.service';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { Grades, Hours, Meridiems, States } from '../shared/constants';

@Component({
  selector: 'app-school-create',
  templateUrl: './school-create.component.html',
  styleUrls: ['./school-create.component.css']
})
export class SchoolCreateComponent implements OnInit {
  test
  public editForm: FormGroup;
  public school: Models.School;
  public showForm: boolean;
  public emailError: string;
  public errorMessage: string;

  public hours = Hours;
  public startHour: number;
  public startMinute: number;
  public startMeridiem = Meridiems.find(m => m.key == 0);


  public endHour: number;
  public endMeridiem = Meridiems.find(m => m.key == 1);

  public endMinute: number;
  public grades = Grades;
  public startGrade: any;
  public endGrade: any;
  public schoolType: string;
  public states = States;

  public minutes: any[] = [];

  @Input() isNew: boolean = false;

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private router: Router, private dataService: DataService) {
    for (let i = 0; i < 60; i++) {
      this.minutes.push({ key: i, value: `${i}`.padStart(2, '0') });
    }
    this.school = new Models.School();
  }


  get name(): AbstractControl { return this.editForm.get('name') };
  get contactPhone(): AbstractControl { return this.editForm.get('contactPhone') };
  get contactEmail(): AbstractControl { return this.editForm.get('contactEmail') };
  get url(): AbstractControl { return this.editForm.get('url') };
  get payRate(): AbstractControl { return this.editForm.get('payRate') };
  get street(): AbstractControl { return this.editForm.get('address').get('street1') };
  get city(): AbstractControl { return this.editForm.get('address').get('city') };
  get state(): AbstractControl { return this.editForm.get('address').get('state') };
  get postalCode(): AbstractControl { return this.editForm.get('address').get('postalCode') };

  ngOnInit() {
    if (!this.isNew) {
      this.dataService.setHeaders();
      this.populateForm();
    }
  }

  populateForm() {
    this.editForm = this.fb.group({
      name: ['', Validators.required],
      url: ['', Validators.required],
      contactEmail: ['', [Validators.required, Validators.email]],
      contactPhone: ['', Validators.required],
      summary: '',
      payRate: ['', Validators.required],
      address: this.fb.group({
        street1: ['', Validators.required],
        street2: '',
        city: ['', Validators.required],
        state: ['', Validators.required],
        postalCode: ['', Validators.required]
      }),
    });

    // let startArray = this.school.startTime.split(':');
    // this.startHour = +startArray[0];
    // this.startMinute = +startArray[1];
    // this.startHour = this.startHour > 12 ? (this.startHour - 12) : this.startHour;

    // let endArray = this.school.endTime.split(':');
    // this.endHour = +endArray[0];
    // this.endMinute = +endArray[1];

    // this.endHour = this.endHour > 12 ? (this.endHour - 12) : this.endHour;
    // this.schoolType = this.school.schoolType;

    // if (this.school.grades) {
    //   let schoolGrades = this.school.grades.split('-');
    //   this.startGrade = Grades.find(g => g.key === schoolGrades[0]).key;
    //   this.endGrade = Grades.find(g => g.key === schoolGrades[1]).key;
    // }

    this.showForm = true;
  }

  toggleStartMeridiem() {
    this.startMeridiem = this.startMeridiem.key == 0 ?
      Meridiems.find(m => m.key == 1) : Meridiems.find(m => m.key == 0);
  }

  toggleEndMeridiem() {
    this.endMeridiem = this.endMeridiem.key == 0 ?
      Meridiems.find(m => m.key == 1) : Meridiems.find(m => m.key == 0);
  }

  onSubmit() {
    let s = Object.assign({}, this.editForm.value);
    this.school.schoolType = this.schoolType;
    this.school.grades = `${this.startGrade}-${this.endGrade}`;

    let startHour = this.startMeridiem.key > 0 ? this.startHour + 12 : this.startHour;
    let endHour = this.endMeridiem.key > 0 ? this.endHour + 12 : this.endHour;

    this.school.startTime = `${startHour}:${this.startMinute}:00`;
    this.school.endTime = `${endHour}:${this.endMinute}:00`;

    this.school.name = s.name;
    this.school.url = s.url;
    this.school.contactEmail = s.contactEmail;
    this.school.contactPhone = s.contactPhone;
    this.school.address = s.address;
    this.school.payRate = s.payRate;
    this.school.summary = s.summary;

    this.dataService.createSchool(this.school)
      .subscribe(resp => {
        this.router.navigate(['/schools', resp.id, 'edit']);
      });

    this.editForm.markAsPristine();
  }

  onAddPayment(){
    this.router.navigate(['/schools', this.school.id, 'payments']);
  }

  settingsValid(): boolean {
    return this.schoolType != null && this.startGrade && this.endGrade && this.startHour > 0 && this.endHour > 0;
  }

  markDirty() {
    this.editForm.markAsDirty();
  }

  public getEmailError() {
    if (this.contactEmail.touched
      && this.contactEmail.dirty
      && this.contactEmail.invalid) {
      if (this.contactEmail.errors.email && !this.contactEmail.errors.required) {
        this.emailError = "Please enter a valid email";
        this.contactEmail.markAsUntouched;
      }
    }
  }
  public resetEmailError() {
    this.emailError = '';
  }

}
