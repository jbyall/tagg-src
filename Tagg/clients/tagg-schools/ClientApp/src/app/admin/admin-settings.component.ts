import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { MatSnackBar } from '@angular/material';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as Models from "../shared/models";
import { environment } from '../../environments/environment';
import { ContactMethods } from '../shared/constants';

@Component({
  selector: 'app-admin-settings',
  templateUrl: './admin-settings.component.html',
  styleUrls: ['./admin-settings.component.css']
})
export class AdminSettingsComponent implements OnInit {
  settingsForm: FormGroup;
  public currentUser: Models.IUser;
  public showForm: boolean;
  public passwordReset = environment.passwordResetUrl;

  constructor(public _authService: AuthService, private _router: Router, private _dataService: DataService, private fb: FormBuilder, public snackBar: MatSnackBar) {
    this.currentUser = this._authService.user.profile;
    this._dataService.setHeaders();
  }

  ngOnInit() {
    this._dataService.getUser(this._authService.user.profile.sub)
      .subscribe(resp => {
        this.currentUser = resp;
        this.populateForm();
      })
    
  }

  populateForm(){
    this.settingsForm = this.fb.group({
      firstName: this.currentUser.firstName,
      lastName: this.currentUser.lastName,
      email: this.currentUser.email,
      phoneNumber: this.currentUser.phoneNumber
    });
    this.showForm = true;
  }
  onSubmit(){
    this.showSnackbar('Coming soon :)');
  }

  onChangeEmail(){
    this._router.navigate(['/change-email']);
  }

  showSnackbar(message: string){
    this.snackBar.open(message, 'Dismiss',{
      duration: 5000
    });
  }

}
