import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TaggCommonModule } from '../shared/tagg-common.module';
import { AdminSchoolsComponent, SchoolDetailsDialog } from './admin-schools.component';
import { SchoolCreateComponent } from './school-create.component';
import { SchoolModule } from '../school/school.module';
import { AdminGuard } from '../auth/admin.guard';
import { AdminJobsComponent } from './admin-jobs.component';
import { AdminSettingsComponent } from './admin-settings.component';
import { ViewJobDialog } from '../shared/view-job.dialog';
import { AdminCalendarComponent } from './admin-calendar.component';
import { DistrictMetricsComponent } from './district-metrics.component';
import { AdminCalendarDialog } from './dialogs/admin-calendar.dialog';
import { AdminEventsDialog } from './dialogs/admin-events.dialog';
import { RatingGuard } from '../auth/rating.guard';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            { path: 'home', component: AdminCalendarComponent, canActivate:[AdminGuard, RatingGuard]},
            { path: 'jobs', component: AdminJobsComponent, canActivate:[AdminGuard, RatingGuard]},
            { path: 'schools', component: AdminSchoolsComponent, canActivate:[AdminGuard, RatingGuard]},
            { path: 'school/create', component: SchoolCreateComponent, canActivate:[AdminGuard, RatingGuard]},
            { path: 'settings', component: AdminSettingsComponent, canActivate:[AdminGuard, RatingGuard]},
            { path: 'calendar', component: AdminCalendarComponent, canActivate:[AdminGuard, RatingGuard]},
            { path: 'metrics', component: DistrictMetricsComponent, canActivate:[AdminGuard, RatingGuard]}
        ]),
        TaggCommonModule
    ],
    declarations: [
        AdminSchoolsComponent,
        SchoolCreateComponent,
        AdminJobsComponent,
        AdminSettingsComponent,
        SchoolDetailsDialog,
        AdminCalendarComponent,
        AdminCalendarDialog,
        AdminEventsDialog,
        DistrictMetricsComponent
    ],
    entryComponents:[SchoolDetailsDialog, AdminCalendarDialog, AdminEventsDialog],
    providers:[
    ],
})
export class AdminModule { }