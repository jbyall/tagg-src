import { Component, OnInit } from '@angular/core';
import * as Models from '../shared/models';
import { AuthService } from '../auth/auth.service';
import { DataService } from '../services/data.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-district-metrics',
  templateUrl: './district-metrics.component.html',
  styleUrls: ['./district-metrics.component.css']
})
export class DistrictMetricsComponent implements OnInit {
  public metrics: Models.DistrictMetrics

  constructor(private authService: AuthService, private dataService: DataService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {
    this.dataService.setHeaders();

    this.dataService.getDistrictMetrics(this.authService.user.profile.school)
      .subscribe(resp => {
        this.metrics = resp;
      });
  }

}
