import { Component, OnInit, Inject } from '@angular/core';
import { Router } from "@angular/router";
import * as Models from "../shared/models";
import { DataService } from '../services/data.service';
import { AuthService } from '../auth/auth.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-admin-schools',
  templateUrl: './admin-schools.component.html',
  styleUrls: ['./admin-schools.component.css']
})
export class AdminSchoolsComponent implements OnInit {
  public currentUser: Models.IUser;
  public schools: Models.SchoolDetail[];
  public selectedSchool: Models.SchoolDetail;
  public schoolAdmin: string;

  constructor(public _authService: AuthService, private _router: Router, private _dataService: DataService, public dialog: MatDialog, public sb: MatSnackBar) {
      //this.currentUser = _authService.userProfile;
      this._dataService.setHeaders();
  }

  ngOnInit() {
      this._dataService.getAdminSchools()
          .subscribe(resp => {
              this.schools = resp;
              if(this.schools.length == 1 && this.schools[0].schoolBase.grades == null){
                this._router.navigate(['/schools', this.schools[0].schoolBase.id, 'edit']);
              }
              this.selectedSchool = this.schools[0];
          }, err => { });
  }

  onSelectSchool(schoolId: string){
    this.selectedSchool = this.schools.find(s => s.schoolBase.id === schoolId);
    if(window.innerWidth < 767){
      this.openDialog();
    }
  }

  onUpload(){
    this._router.navigate(['/schools', this.selectedSchool.schoolBase.id, 'upload']);
  }

  onAddTeacher(){
    if(!this.selectedSchool.schoolBase.customerId && environment.production){
      this.sb.open('Please add and confirm your bank account before adding teachers.', 'Dismiss');
    }
    else{
      this._router.navigate(['/schools', this.selectedSchool.schoolBase.id, 'add', '1'])
    }
  }

  openDialog(): void{
    let dialogRef = this.dialog.open(SchoolDetailsDialog,{
      width: '80%',
      data: {school: this.selectedSchool},
      closeOnNavigation: true
    })
  }

}

@Component({
  selector: 'school-details-dialog',
  templateUrl: 'school-details.dialog.html',
})
export class SchoolDetailsDialog{
  constructor(
    public dialogRef: MatDialogRef<SchoolDetailsDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, private _router: Router) {
      this._router.events.subscribe(() =>{this.onNoClick()});
    }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
