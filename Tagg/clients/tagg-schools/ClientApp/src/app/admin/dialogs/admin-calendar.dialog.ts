import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { DataService } from "../../services/data.service";
import { JobService } from "../../services/job.service";
import { Inject, Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "../../auth/auth.service";
import { School, SimpleEntityDto, MultiSelect, BlackoutAddDto, CalendarEvent } from "../../shared/models";

@Component({
  selector: 'admin-calendar-dialog',
  templateUrl: 'admin-calendar.dialog.html',
})
export class AdminCalendarDialog implements OnInit {
  public adminSchools: SimpleEntityDto[];
  public schoolsInEvent: SimpleEntityDto[];

  public schoolOptions: MultiSelect[] = [];
  public selectedSchools: string[];
  public showForm: boolean;
  public showError: boolean;
  public dateString: string;
  public haveEvents: boolean;

  constructor(
    public dialogRef: MatDialogRef<AdminCalendarDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, private dataService: DataService, private jobService: JobService, private _router: Router, private authService: AuthService) {
    this.dataService.setHeaders();
  }

  ngOnInit() {
    this.dateString = this.data.date;
    this.getUserSchools();
    this.haveEvents = this.data.events.length > 0;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  getUserSchools() {
    this.dataService.schoolsSummaryAdminGet(this.authService.user.profile.sub)
      .subscribe(resp => {
        this.adminSchools = resp;
        this.populateOptions();
      }, err => {
        this.showError = true;
      })
  }

  populateOptions() {
    if (!this.haveEvents) {
      for (let i = 0; i < this.adminSchools.length; i++) {
        this.schoolOptions.push(new MultiSelect(this.adminSchools[i].id, this.adminSchools[i].name));
      }
    }
    else{
      this.schoolsInEvent = this.adminSchools.filter(x => this.data.events.some(e => e.schoolId === x.id));
    }
    this.showForm = true;
  }

  onAddBlackout() {
    if (this.selectedSchools.length) {
      let blackoutModel = new BlackoutAddDto(this.data.date, this.selectedSchools);
      this.dataService.calendarBlackoutAdd(blackoutModel)
        .subscribe(resp => {
          this.onNoClick();
        }, err => { this.showError = true; })
    }
  }
  onRemoveBlackout(){
    if (this.schoolsInEvent.length) {
      let blackoutModel = new BlackoutAddDto(this.data.date, this.schoolsInEvent.map(z => z.id));
      this.dataService.calendarBlackoutRemove(blackoutModel)
        .subscribe(resp => {
          this.onNoClick();
        }, err => { 
          this.showError = true; 
        })
    }
  }

}