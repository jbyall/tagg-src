import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { DataService } from "../../services/data.service";
import { JobService } from "../../services/job.service";
import { Inject, Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { AuthService } from "../../auth/auth.service";
import { CalendarDay, CalendarEvent, Job, JobStatus } from "../../shared/models";
import { AdminJobStatus } from "../../shared/constants";

@Component({
  selector: 'admin-events-dialog',
  templateUrl: 'admin-events.dialog.html',
})
export class AdminEventsDialog implements OnInit {
  public day: CalendarDay;
  public events: CalendarEvent[];
  public selectedEvent: CalendarEvent;
  public totalJobs: number;
  public currentIndex: number;
  public jobStatus = AdminJobStatus;

  public showCancelError: boolean;
  public showCancelSuccess: boolean;
  public showWarning: boolean;

  constructor(
    public dialogRef: MatDialogRef<AdminEventsDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, private dataService: DataService, private jobService: JobService, private _router: Router, private authService: AuthService) {
    this.dataService.setHeaders();
  }

  ngOnInit() {
    this.events = this.data.day.events;
    this.currentIndex = 0;
    this.selectEvent(this.currentIndex);
  }

  selectEvent(index: number) {
    // TODO: This will error if there are no events
    this.selectedEvent = this.events[index];
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onNext() {
    if (this.currentIndex < (this.events.length - 1) ) {
      this.currentIndex++;
      this.selectEvent(this.currentIndex);
    }
  }

  onPrevious() {
    if (this.currentIndex > 0) {
      this.currentIndex--;
      this.selectEvent(this.currentIndex);
    }
  }

  onCancelWarning(){
    this.showWarning = true;
  }

  dontCancel(){
    this.showWarning = false;
  }

  onCancelJob(){
    this.showWarning = false;
    this.dataService.cancelJob(this.selectedEvent.job.id)
      .subscribe(resp => {
        this.showCancelSuccess = true;
      }, err => {
        this.showCancelError = true;
      });
  }

}