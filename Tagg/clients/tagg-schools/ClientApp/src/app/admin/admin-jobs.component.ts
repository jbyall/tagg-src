import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from '../auth/auth.service';
import * as Models from "../shared/models";
import { DataService } from '../services/data.service';
import { User } from 'oidc-client';
import { AdminJobStatus } from '../shared/constants';
import { MatDialog } from '@angular/material';
import { ViewJobDialog } from '../shared/view-job.dialog';
import { JobDetailsDialog } from '../shared/job-details.dialog';

@Component({
  selector: 'app-admin-jobs',
  templateUrl: './admin-jobs.component.html',
  styleUrls: ['./admin-jobs.component.css']
})
export class AdminJobsComponent implements OnInit {
  public jobsResult: Models.SchoolJobsResult;
  public allJobs: Models.Job[] = [];
  public recentJobs: Models.Job[];
  public upcomingJobs: Models.Job[];
  public todayJobs: Models.Job[];
  public jobStatus = AdminJobStatus;
  constructor(private _authService: AuthService, private _router: Router, private dataService: DataService, private dialog: MatDialog) { }

  ngOnInit() {
    this.dataService.setHeaders();
    this.getSchoolJobs();
  }

  getSchoolJobs(){
    this.dataService.getAdminJobs()
      .subscribe(resp => {
        this.jobsResult = resp;
      });
  }

  onSelectRecent(jobId: string){
    let selectedJob = this.jobsResult.recent.find(j => j.id === jobId);
    this.openJobDialog(selectedJob, Models.JobModalType.AdminRecent);
  }

  onSelectUpcoming(jobId: string){
    let selectedJob = this.jobsResult.upcoming.find(j => j.id === jobId);
    this.openJobDialog(selectedJob, Models.JobModalType.Admin);
  }

  onSelectToday(jobId: string){
    let selectedJob = this.jobsResult.today.find(j => j.id === jobId);
    this.openJobDialog(selectedJob, Models.JobModalType.Admin);
  }

  openJobDialog(selectedJob: Models.Job, modalType: Models.JobModalType): void{
    let dialogRef = this.dialog.open(JobDetailsDialog,{
      width: '500px',
      data: {job: selectedJob, modalType: modalType}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getSchoolJobs();
    })
  }

}
