import { Component, OnInit, Inject } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatDialogConfig } from '@angular/material';
import * as Models from "../shared/models";
import { ViewJobDialog } from '../shared/view-job.dialog';
import { JobService } from '../services/job.service';
import { Subjects, MonthMap } from '../shared/constants';
import { AdminCalendarDialog } from './dialogs/admin-calendar.dialog';
import { AdminEventsDialog } from './dialogs/admin-events.dialog';

@Component({
  selector: 'app-admin-calendar',
  templateUrl: './admin-calendar.component.html',
  styleUrls: ['./admin-calendar.component.css']
})
export class AdminCalendarComponent implements OnInit {
  public currentUser: Models.IUser;
  public showIncomplete: boolean;
  public closeResult: string;

  public calendar: Models.CalendarDay[];
  public selectedDate: Date;
  public monthLabel: string;
  public monthMap = MonthMap;

  public userId: string;
  public isDistrict: boolean;
  public isSchool: boolean;

  constructor(public authService: AuthService, private _router: Router, private dataService: DataService, private dialog: MatDialog) {
    this.currentUser = authService.userProfile;
    this.dataService.setHeaders();
    this.userId = this.authService.user.profile.sub;
    this.isDistrict = this.authService.user.profile.api_access === '0x3';
    this.isSchool = !this.isDistrict;
  }

  ngOnInit() {
    this.selectedDate = new Date();
    this.populateCalendar();
  }

  onPrevMonth() {
    this.selectedDate.setMonth(this.selectedDate.getMonth() - 1);
    this.populateCalendar();
  }

  onNextMonth() {
    let nextMonth = this.selectedDate.getMonth() + 1;
    this.selectedDate.setMonth(nextMonth, 1);
    this.populateCalendar();
  }

  populateCalendar() {
    this.monthLabel = this.monthMap[this.selectedDate.getMonth()] + ' ' + this.selectedDate.getFullYear();
    this.dataService.calendarAdminGet(this.selectedDate.getMonth() + 1, this.selectedDate.getFullYear())
      .subscribe(resp => {
        this.calendar = resp;
        console.log('calendar > ', this.calendar);
      });
  }

  onDateSelect(monthIndex: number, dayIndex: number): void {
    let day = this.calendar.find(d => d.dayOfMonthIndex == dayIndex && d.monthIndex == monthIndex);
    let jobEvent = day.events.find(e => e.job !== null);
    let blackoutEvents = day.events.filter(e => e.isBlackout);

    let jobDateParts = day.date.toString()
      .split('T')[0]
      .split('-');

    let selectedDt = new Date(+jobDateParts[0], (+jobDateParts[1] - 1), +jobDateParts[2]);
    let now = new Date();

    if (jobEvent) {
      this.openEventDialog(day);
    }
    else if(blackoutEvents){
      this.openBlackoutDialog(selectedDt, blackoutEvents);
    }
    else {
      if (selectedDt.getTime() > now.getTime()) {
        this.openBlackoutDialog(selectedDt);
      }
      else {
        
      }
    }
  }

  openEventDialog(day: Models.CalendarDay): void {
    let dialogRef = this.dialog.open(AdminEventsDialog, {
      width: '500px',
      data: { day: day }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.populateCalendar();
    });
  }

  openBlackoutDialog(date: Date, events: Models.CalendarEvent[] = []): void {
    let dialogRef = this.dialog.open(AdminCalendarDialog, {
      width: '500px',
      data: { date: date, events: events }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.populateCalendar();
    });
  }

  openDialog(date: Date): void {

  }

}


