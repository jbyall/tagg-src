﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from "@angular/common";
//import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { RegisterComponent } from "./register.component";
import { AuthService } from '../auth/auth.service';
import { AuthGuard } from '../auth/auth.guard';
import { RegisterSchoolComponent } from './register-school.component';
import { TaggCommonModule } from '../shared/tagg-common.module';
import { RegisterVerifyComponent } from './register-verify.component';
import { RegisterAdminComponent } from './register-admin.component';
import { RegisterUploadComponent } from './register-upload.component';
import { SchoolModule } from '../school/school.module';
import { AdminGuard } from '../auth/admin.guard';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            {
                path: '', component: RegisterComponent,
                children: [
                    { path: 'school', component: RegisterSchoolComponent, canActivate:[AdminGuard] },
                    { path: 'verify/:id', component: RegisterVerifyComponent },
                    { path: 'admin/:id', component: RegisterAdminComponent},
                    
                    // { path: 'upload', component: RegisterUploadComponent },
                ]
            },
        ]),
        TaggCommonModule
    ],
    exports:[
        RegisterSchoolComponent
    ],
    providers:[
    ],
    declarations: [
        RegisterVerifyComponent,
        RegisterSchoolComponent,
        RegisterAdminComponent,
        RegisterUploadComponent,
        RegisterComponent,
    ]
})
export class RegisterModule { }