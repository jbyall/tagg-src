import { Component, OnInit } from '@angular/core';
import { FormsModule, FormGroup, FormBuilder, Validators, AbstractControl, FormControl, ValidatorFn } from "@angular/forms";
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import * as Models from '../shared/models';
import { DataService } from '../services/data.service';

@Component({
    selector:'app-school-register',
    templateUrl: './register-school.component.html'
})
export class RegisterSchoolComponent implements OnInit{ 
    
    constructor(private fb: FormBuilder, private _router: Router, private authService: AuthService, private dataService: DataService) {
        this.dataService.setHeaders();
    }

    ngOnInit(){
        if(this.authService.user.profile.school){
            this._router.navigate(['schools', this.authService.user.profile.school, 'edit']);
        }
        else{
            this._router.navigate(['admin', 'school', 'create']);
        }
        
        
    }
}