import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { FormsModule, FormGroup, FormBuilder, Validators, AbstractControl, FormControl, ValidatorFn } from "@angular/forms";
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';

@Component({
    selector: 'app-register-upload',
    templateUrl: './register-upload.component.html'
})
export class RegisterUploadComponent implements OnInit {
    public form: FormGroup;
    loading: boolean = false;
    errorMessage: string;
    dataSource: any[]=[];
    showTable: boolean = false;
    //@ViewChild('fileInput') fileInput: ElementRef
    constructor(private fb: FormBuilder, private _router: Router, private authService: AuthService) {
        this.form = this.fb.group({
            name: '',
            avatar: null
        });
    }

    ngOnInit() {

    }

    onFileChange(event) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            //reader.readAsDataURL(file);
            reader.readAsText(file);
            reader.onload = () => {
                this.dataSource = reader.result.split('\r\n')
                this.form.get('avatar').setValue({
                    filename: file.name,
                    filetype: file.type,
                    value: reader.result.split('\r\n')
                })
            };
            this.showTable = true;
        }
    }

    onSubmit() {
        const formModel = this.form.value;
        this.loading = true;
        // In a real-world app you'd have a http request / service call here like
        // this.http.post('apiUrl', formModel)
        setTimeout(() => {
          alert('done!');
          this.loading = false;
        }, 1000);
      }
    
      clearFile() {
        this.form.get('avatar').setValue(null);
        //this.fileInput.nativeElement.value = '';
      }
}

export interface CsvRow{
    
}