import { Component, OnInit } from '@angular/core';
import { FormsModule, FormGroup, FormBuilder, ReactiveFormsModule, Validators, AbstractControl, FormControl, ValidatorFn } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import * as Models from "../shared/models";

import 'rxjs/add/operator/debounceTime';
import { DataService } from '../services/data.service';

@Component({
    selector:'app-register-admin',
    templateUrl: './register-admin.component.html',
    styleUrls: ['./register-admin.component.css']
})
export class RegisterAdminComponent implements OnInit{ 
    token: string;
    public registerForm: FormGroup;
    public user: Models.IUser;

    public showForm: boolean = false;
    public errorMessage: string;
    public confirmPasswordErrorMessage: string;
    public emailErrorMessage: string;
    
    constructor(private fb: FormBuilder, 
        private _route: ActivatedRoute,
         private _router: Router, 
         private authService: AuthService, private dataService: DataService) {
        this.token = this._route.snapshot.params['id'];
    }
    validationMessages = {
        email: 'Please enter a valid email address',
        match: 'Passwords must match'
    }

    get firstName(): AbstractControl { return this.registerForm.get('firstName') };
    get lastName(): AbstractControl { return this.registerForm.get('lastName') };
    get email(): AbstractControl { return this.registerForm.get('email') };
    get postalCode(): AbstractControl { return this.registerForm.get('postalCode') };
    get password(): AbstractControl { return this.registerForm.get('passwordGroup').get('password') };
    get confirmPassword(): AbstractControl { return this.registerForm.get('passwordGroup').get('confirmPassword') };
    get passwordGroup(): AbstractControl { return this.registerForm.get('passwordGroup') };

    ngOnInit(){
        this.user = this.authService.getUserFromToken(this.token);

        this.registerForm = this.fb.group({
            firstName:[this.user.firstName, Validators.required],
            lastName:[this.user.lastName, Validators.required],
            email:[this.user.email, Validators.required],
            postalCode:'',
            agreeToTerms: [false, Validators.requiredTrue],
            passwordGroup: this.fb.group({
                password: ['', Validators.required],
                confirmPassword: ['', [Validators.required]]
            }, { validator: passwordMatchValidator })
        });

        const pwGroupControl = this.passwordGroup;
        pwGroupControl.valueChanges.debounceTime(1000).subscribe(value => this.setConfirmPasswordMessage(pwGroupControl));

        this.registerForm.markAsPristine();
        this.registerForm.markAsUntouched();
        this.showForm = true;
    };

    setConfirmPasswordMessage(control: AbstractControl): void {
        this.confirmPasswordErrorMessage = '';
        const confirmControl = control.get('confirmPassword');
        if ((confirmControl.touched || confirmControl.dirty) && !control.valid) {
            this.confirmPasswordErrorMessage = Object.keys(control.errors).map(key => {
                return this.validationMessages[key]
            }).join(' ');
        }
        else {
            this.confirmPasswordErrorMessage = '';
        }
    };

    onTerms(){
        window.open('http://www.taggeducation.com/school-terms-of-service');
    }

    onRegister() {
        if (this.registerForm.dirty && this.registerForm.valid) {
            let p = new Models.RegisterModel();
            Object.assign(p, this.registerForm.value, this.registerForm.value.passwordGroup);
            this.dataService.register(p)
                .subscribe(resp => {
                    if(this.user.apiAccess === "0x2"){
                        this._router.navigate(['/teachers', 'profile']);
                    }
                    else{
                        this._router.navigate(['/register/school']);
                    }
                });
        }
    };
}

export function passwordMatchValidator(c: AbstractControl): { [key: string]: boolean } | null {
    let passwordControl = c.get('password');
    let confirmPasswordControl = c.get('confirmPassword');

    if (passwordControl.pristine || confirmPasswordControl.pristine) {
        return null;
    }

    if (passwordControl.value === confirmPasswordControl.value) {
        return null;
    }

    return { 'match': true };
}