// Modules
import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";
// Services

// Components
import { HomeComponent } from './layout/home.component';
import { PageNotFoundComponent } from "./layout/page-not-found.component";
import { AboutComponent } from './layout/about.component';
import { ContactComponent } from './layout/contact.component';
import { VerifyPhoneComponent } from './shared/account/verify-phone.component';
import { ProfileComponent } from './shared/account/profile.component';
import { AuthGuard } from './auth/auth.guard';
import { AuthCallbackComponent } from './auth/auth-callback.component';
import { MessagesComponent } from './shared/messages.component';
import { JobRatingComponent } from './shared/job-rating.component';
import { ChangeEmailComponent } from './shared/account/change-email.component';


@NgModule({
    imports: [
        RouterModule.forRoot([
            { path: 'home', component: HomeComponent, canActivate: [AuthGuard] },
            {
                path: 'teachers',
                loadChildren: './teacher/teacher.module#TeacherModule'
            },
            {
                path: 'admin',
                loadChildren: './admin/admin.module#AdminModule',
            },
            {
                path: 'register',
                loadChildren: './register/register.module#RegisterModule',
            },
            {
                path: 'schools',
                loadChildren: './school/school.module#SchoolModule'
            },
            { path: 'rate', component: JobRatingComponent, canActivate: [AuthGuard] },
            { path: 'messages', component: MessagesComponent, canActivate: [AuthGuard] },
            { path: 'messages/:id', component: MessagesComponent, canActivate: [AuthGuard] },
            { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
            { path: 'verify-phone/:id', component: VerifyPhoneComponent },
            { path: 'change-email', component: ChangeEmailComponent, canActivate: [AuthGuard] },
            { path: 'contact', component: ContactComponent },
            { path: 'about', component: AboutComponent },
            { path: 'auth-callback', component: AuthCallbackComponent },
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: '**', component: PageNotFoundComponent }
        ])
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
