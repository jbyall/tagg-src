export const environment = {
  // DEPLOYMENT-CHECK
  //************PRODUCTION ******************************/
  production: true,
  apiUrl: 'https://connect.taggeducation.com/api/',
  userPhotoEndpoint: 'https://connect.taggeducation.com/api/users/photo/',
  stsAuthority: 'https://connect.taggeducation.com/',
  stsRedirectUri:'https://schools.taggeducation.com/auth-callback',
  passwordResetUrl: 'https://connect.taggeducation.com/account/ForgotPassword',
  stsClientId:'schoolsclient',
  stsResponseType:'id_token token',
  stsScope: 'openid profile api_access taggapi',
  logoutRedirect: 'https://schools.taggeducation.com',
  silentRefreshUrl: 'https://schools.taggeducation.com/silent-refresh.html'

  //************ STAGING ******************************/
  // production: true,
  // apiUrl: 'https://tagg-api-staging.azurewebsites.net/api/',
  // userPhotoEndpoint: 'https://tagg-api-staging.azurewebsites.net/api/users/photo/',
  // stsAuthority: 'https://tagg-api-staging.azurewebsites.net/',
  // stsRedirectUri:'https://tagg-schools-staging.azurewebsites.net/auth-callback',
  // passwordResetUrl: 'https://tagg-api-staging.azurewebsites.net/account/ForgotPassword',
  // stsClientId:'schoolsstageclient',
  // stsResponseType:'id_token token',
  // stsScope: 'openid profile api_access taggapi',
  // logoutRedirect: 'https://tagg-schools-staging.azurewebsites.net',
  // silentRefreshUrl: 'https://tagg-schools-staging.azurewebsites.net/silent-refresh.html'
};