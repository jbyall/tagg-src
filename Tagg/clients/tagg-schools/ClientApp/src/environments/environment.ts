// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  apiUrl: 'http://localhost:50706/api/',
  userPhotoEndpoint: 'http://localhost:50706/api/users/photo/',
  stsAuthority: 'http://localhost:50706/',
  passwordResetUrl: 'http://localhost:50706/account/ForgotPassword',
  stsRedirectUri:'http://localhost:56329/auth-callback',
  stsClientId:'schoolslocalclient',
  stsResponseType:'id_token token',
  stsScope: 'openid profile api_access taggapi',
  logoutRedirect: 'http://localhost:56329',
  silentRefreshUrl: 'http://localhost:56329/silent-refresh.html'
  
};
