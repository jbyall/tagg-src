// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,

  //************ STAGING SERVER******************************/
  // apiUrl: 'https://tagg-api-staging.azurewebsites.net/api/',
  // userPhotoEndpoint: 'https://tagg-api-staging.azurewebsites.net/api/users/photo/',
  // stsAuthority: 'https://tagg-api-staging.azurewebsites.net/',
  // passwordResetUrl: 'https://tagg-api-staging.azurewebsites.net/account/ForgotPassword',
  // stripeCallbackUrl: 'https://tagg-api-staging.azurewebsites.net/api/payments/callback',

  //************ LOCAL SERVER ******************************/
  apiUrl: 'http://localhost:50706/api/',
  userPhotoEndpoint: 'http://localhost:50706/api/users/photo/',
  stsAuthority: 'http://localhost:50706/',
  passwordResetUrl: 'http://localhost:50706/account/ForgotPassword',
  stripeCallbackUrl: 'http://localhost:50706/api/payments/callback',


  // LOCAL CLIENT
  stsClientId:'subslocalclient',
  stsResponseType:'id_token token',
  stsScope: 'openid profile api_access taggapi',
  stsRedirectUri:'http://localhost:62790/auth-callback',
  stripeKey: 'pk_test_1s9CuCVfWUwCuo45rPGqCpvX',
  stripeClientId: 'ca_Cl9Zrqu0zRiQaznT04owy0DpRwv9AdKf',
  logoutRedirect: 'http://localhost:62790',
  silentRefreshUrl: 'http://localhost:62790/silent-refresh.html'
};
