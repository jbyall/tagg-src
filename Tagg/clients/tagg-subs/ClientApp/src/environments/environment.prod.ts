export const environment = {
  // DEPLOYMENT-CHECK
  // PROD
  production: true,
  apiUrl: 'https://connect.taggeducation.com/api/',
  userPhotoEndpoint: 'https://connect.taggeducation.com/api/users/photo/',
  stsAuthority: 'https://connect.taggeducation.com/',
  stsRedirectUri:'https://subs.taggeducation.com/auth-callback',
  passwordResetUrl: 'https://connect.taggeducation.com/account/ForgotPassword',
  stsClientId:'subsclient',
  stsResponseType:'id_token token',
  stsScope: 'openid profile api_access taggapi',
  stripeKey: 'pk_live_Jx1MU1SdE8kfkxBVVzfVod43',
  stripeClientId: 'ca_Cl9ZkaThD04dAHJdsKztAJ8gPonULGmt',
  stripeCallbackUrl: 'https://connect.taggeducation.com/api/payments/callback',
  logoutRedirect: 'https://subs.taggeducation.com',
  silentRefreshUrl: 'https://subs.taggeducation.com/silent-refresh.html'

  // DEV/TEST
  // production: true,
  // apiUrl: 'https://tagg-api-staging.azurewebsites.net/api/',
  // userPhotoEndpoint: 'https://tagg-api-staging.azurewebsites.net/api/users/photo/',
  // stsAuthority: 'https://tagg-api-staging.azurewebsites.net/',
  // stsRedirectUri:'https://tagg-subs-staging.azurewebsites.net/auth-callback',
  // passwordResetUrl: 'https://tagg-api-staging.azurewebsites.net/account/ForgotPassword',
  // stsClientId:'subsstageclient',
  // stsResponseType:'id_token token',
  // stsScope: 'openid profile api_access taggapi',
  // stripeKey: 'pk_test_1s9CuCVfWUwCuo45rPGqCpvX',
  // stripeClientId: 'ca_Cl9Zrqu0zRiQaznT04owy0DpRwv9AdKf',
  // stripeCallbackUrl: 'https://tagg-api-staging.azurewebsites.net/api/payments/callback',
  // logoutRedirect: 'https://tagg-subs-staging.azurewebsites.net/',
  // silentRefreshUrl: 'https://tagg-subs-staging.azurewebsites.net/silent-refresh.html'
};
