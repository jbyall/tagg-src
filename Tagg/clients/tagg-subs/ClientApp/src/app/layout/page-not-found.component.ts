﻿import { Component } from '@angular/core';

@Component({
    template: `
    <div class="row">
        <div class="col-md-4 col-md-offset-4">
            <h1>404 - Sorry we got lost and couldn't find our way to the page you were looking for.</h1>
        </div>
    </div>
    `
})
export class PageNotFoundComponent { }