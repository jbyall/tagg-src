import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import { DataService } from '../services/data.service';
import * as Models from '../shared/models';
import { SubjectDisplay } from '../shared/constants';
import { JobService } from '../services/job.service';
import { MatDialog } from '../../../node_modules/@angular/material';
import { ViewJobDialog } from '../dash/jobs.component';

@Component({
  selector: 'app-side-details',
  templateUrl: './side-details.component.html',
  styleUrls: ['./side-details.component.css']
})
export class SideDetailsComponent implements OnInit {

  public subj = SubjectDisplay;
  public showJobs: boolean;

  constructor(private router: Router, public authService: AuthService, public dataService: DataService, public jobService: JobService, public dialog: MatDialog) { 
    this.jobService.jobsReady$.subscribe(resp =>{
      if(resp){ this.showJobs = true; }
    })
  }

  ngOnInit() {
    this.jobService.getJobs();
  }

  onViewJob(jobId: string){
    this.jobService.getJobDetails(jobId)
      .subscribe(resp => {
        this.openJobDialog(resp);
      })
  }

  openJobDialog(selectedJob: Models.Job): void {
    let width = window.innerWidth < 992 ? '80%' : '600px';
    let dialogRef = this.dialog.open(ViewJobDialog, {
      width: width,
      data: { job: selectedJob }
    });
  }

  // getJobs() {
  //   this.dataService.getJobSummary()
  //     .subscribe(resp => {
  //       this.jobs = resp;
  //     });
  // }

}
