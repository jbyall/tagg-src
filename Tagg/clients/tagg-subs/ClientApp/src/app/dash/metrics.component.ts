import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Substitute, SubMetrics } from '../shared/models';
import { SubjectDisplay } from '../shared/constants';

@Component({
  selector: 'app-metrics',
  templateUrl: './metrics.component.html',
  styleUrls: ['./metrics.component.css']
})
export class MetricsComponent implements OnInit {
  
  public metrics: SubMetrics;
  public subjects = SubjectDisplay;

  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.getMetrics();
  }

  getMetrics(){
    this.dataService.getMetrics()
      .subscribe(resp => {
        this.metrics = resp;
      });
  }

  onPaymentLogin() {
    this.dataService.getPaymentsLogin()
      .subscribe(resp => {
        window.location.href = resp;
      });
  }

}
