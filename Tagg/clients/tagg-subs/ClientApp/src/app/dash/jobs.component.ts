import { Component, OnInit, Inject, EventEmitter, Output } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from '../auth/auth.service';
import * as Models from "../shared/models";
import { DataService } from '../services/data.service';
import { User } from 'oidc-client';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { SubjectDisplay } from '../shared/constants';

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html'
})
export class JobsComponent implements OnInit {
  currentUser: User;
  public jobs: Models.AllJobsResponse;
  public subjectDisplay = SubjectDisplay;
  public showConflict: boolean;

  constructor(private _authService: AuthService, private _router: Router, private dataService: DataService, private dialog: MatDialog) {
    this.currentUser = _authService.user;
    this.dataService.setHeaders();
  }

  ngOnInit() {
    this.getJobs();
  }
  getJobs() {
    this.dataService.getAllJobs()
      .subscribe(resp => {
        this.jobs = resp;
      });
  }
  onViewOffered(jobId: string) {
    this.displayJob(jobId);
  }
  onViewAccepted(jobId: string) {
    this.displayJob(jobId);    
  }
  onViewRecent(jobId: string) {
    this.displayJob(jobId);
  }

  displayJob(jobId: string) {
    this.dataService.getJobDetails(jobId)
      .subscribe(resp => {
        this.openJobDialog(resp);
      }, err => {
          this.openJobDialogConflict("Job is no longer available");
        });
  }

  openJobDialog(selectedJob: Models.Job): void {
    let dialogRef = this.dialog.open(ViewJobDialog, {
      width: '500px',
      data: { job: selectedJob }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getJobs();
    });
  }

  openJobDialogConflict(message: string){
    let dialogRef = this.dialog.open(ViewJobDialog, {
        width: '500px',
        data: { error: 'Job is no longer available' }
    });

    dialogRef.afterClosed().subscribe(result => {
        this.getJobs();
    });
}
}

@Component({
  selector: 'view-job-dialog',
  templateUrl: './view-job.dialog.html',
})
export class ViewJobDialog {
  @Output() notify: EventEmitter<string> = new EventEmitter();

  public subjectDisplay = SubjectDisplay;
  public showCancelConfirm: boolean;
  public showCancelError: boolean;
  public showConflict: boolean;
  public debounce: boolean;

  constructor(
    public dialogRef: MatDialogRef<ViewJobDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any, private dataService: DataService) {
    if (!data.job) {
      this.showConflict = true;
    }
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onAccept(id: string) {
    this.debounce = true;
    this.dataService.jobAccept(id)
      .subscribe(resp => {
        this.dialogRef.close();
      }, err => {
      });
  }

  onDecline(id: string) {
    this.debounce = true;
    this.dataService.jobDecline(id)
      .subscribe(resp => {
        this.dialogRef.close();
      })
  }

  onCancelConfirm() {
    this.showCancelConfirm = true;
  }

  onResetCancel() {
    this.showCancelConfirm = false;
  }

  onCancel(id: string) {
    this.dataService.jobCancel(id)
      .subscribe(resp => {
        this.dialogRef.close();
      }, err => {
        this.showCancelConfirm = false;
        this.showCancelError = true;
      })
  }

}
