import { Component, OnInit, OnDestroy } from '@angular/core';
import { DataService } from '../services/data.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as Models from '../shared/models';
import { SubjectDisplay } from '../shared/constants';

@Component({
  selector: 'app-job-rating',
  templateUrl: './job-rating.component.html',
  styleUrls: ['./job-rating.component.css']
})
export class JobRatingComponent implements OnInit, OnDestroy {

  public jobId: string;
  public job: Models.Job;

  public ratingOne: number;
  public ratingTwo: number;
  public ratingThree: number;
  public subComments: string = '';

  public subjects = SubjectDisplay;
  public ratingDto: Models.JobRating;

  constructor(private dataService: DataService, public route: ActivatedRoute, public router: Router) { }

  ngOnInit() {
    this.getJob();
  }

  ngOnDestroy() {
    
  }

  getJob() {
    this.ratingDto = new Models.JobRating();
    if(this.dataService.jobsForReview.length){
      let jobId = this.dataService.jobsForReview.pop();
      // todo
      this.dataService.getJobDetails(jobId)
      .subscribe(resp => {
        this.job = resp;
      }, err =>{
        
      });
    }
    else{
      this.router.navigate(['/home']);
    }
  }

  onSubmitRating() {
    this.ratingDto.substituteId = this.job.substituteId;
    this.ratingDto.jobId = this.job.id;
    this.ratingDto.teacherId = this.job.teacherId;

    this.ratingDto.ratings.push(new Models.RatingModel('Cleanliness', this.ratingOne));
    this.ratingDto.ratings.push(new Models.RatingModel('Promptness', this.ratingTwo));
    this.ratingDto.ratings.push(new Models.RatingModel('ClassroomManagement', this.ratingThree));
    this.ratingDto

    this.dataService.addJobRating(this.ratingDto)
      .subscribe(resp => {
        if (this.dataService.jobsForReview.length) {
          this.ratingDto = null;
          this.job = null;
          this.ratingOne = 0;
          this.ratingTwo = 0;
          this.ratingThree = 0;
          this.subComments = '';
          this.getJob();
        }
        else {
          this.router.navigate(['/home']);
        }
      }, err => {
        //this.router.navigate(['/teachers', 'home'])}
      });
  }

}
