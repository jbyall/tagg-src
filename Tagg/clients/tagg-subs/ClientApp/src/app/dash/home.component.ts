import { Component, OnInit, Inject } from '@angular/core';
import { Router } from "@angular/router";
import { AuthService } from '../auth/auth.service';
import * as Models from "../shared/models";
import { DataService } from '../services/data.service';
import { User } from 'oidc-client';
import { MAT_DIALOG_DATA, MatDialogRef, MatDialog } from '@angular/material';
import { ViewJobDialog } from './jobs.component';
import { SubjectDisplay } from '../shared/constants';

@Component({
    selector: 'home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
    currentUser: User;
    public jobs: Models.Job[];
    public calendar: Models.CalendarDay[];
    public selectedDate: Date;
    public monthLabel: string;
    public subjects = SubjectDisplay;

    public monthMap: any = {
        0: 'Jan',
        1: 'Feb',
        2: 'March',
        3: 'April',
        4: 'May',
        5: 'June',
        6: 'July',
        7: 'Aug',
        8: 'Sept',
        9: 'Oct',
        10: 'Nov',
        11: 'Dec'
    }


    constructor(private _authService: AuthService, private _router: Router, private dataService: DataService, private dialog: MatDialog) {
        this.currentUser = _authService.user;
        this.dataService.setHeaders();
    }

    ngOnInit() {
        this.selectedDate = new Date();
        this.populateCalendar();
    }

    onPrevMonth() {
        this.selectedDate.setMonth(this.selectedDate.getMonth() - 1);
        this.selectedDate.setDate(1);
        this.populateCalendar();
    }

    onNextMonth() {
        let nextMonth = this.selectedDate.getMonth() + 1;
        this.selectedDate.setMonth(nextMonth, 1);
        this.populateCalendar();
    }

    populateCalendar() {
        this.monthLabel = this.monthMap[this.selectedDate.getMonth()] + ' ' + this.selectedDate.getFullYear();
        this.dataService.getSubCalendar(this.selectedDate.getMonth() + 1, this.selectedDate.getFullYear())
            .subscribe(resp => {
                this.calendar = resp;
            });
    }

    onDateSelect(date: Date): void {
        let day = this.calendar.find(d => d.date == date);
        if (day.year > 0) {
            if (day.events.length < 2) {
                let jobEvent = day.events.find(e => e.job !== null);
                if (jobEvent) {
                    this.displayJob(jobEvent.job.id);
                }
                else {
                    this.openAvailabilityDialog(date);
                }
            }

        }
    }

    displayJob(jobId: string) {
        this.dataService.getJobDetails(jobId)
            .subscribe(resp => {
                this.openJobDialog(resp);
            }, err => {
                    this.openJobDialogConflict("Job is no longer available");
                });
    }

    openAvailabilityDialog(date: Date): void {
        let dialogRef = this.dialog.open(AvailabilityDialog, {
            width: '500px',
            data: { email: this._authService.user.profile.email, date: date }
        });

        dialogRef.afterClosed().subscribe(result => {
            this.populateCalendar();
        });
    }

    openJobDialog(selectedJob: Models.Job): void {
        let dialogRef = this.dialog.open(ViewJobDialog, {
            width: '500px',
            data: { job: selectedJob }
        });

        dialogRef.afterClosed().subscribe(result => {
            this.populateCalendar();
        });
    }

    openJobDialogConflict(message: string) {
        let dialogRef = this.dialog.open(ViewJobDialog, {
            width: '500px',
            data: { error: 'Job is no longer available' }
        });

        dialogRef.afterClosed().subscribe(result => {
            this.populateCalendar();
        });
    }
}

@Component({
    selector: 'availability-dialog',
    templateUrl: './availability.dialog.html',
})
export class AvailabilityDialog {

    public today: Date = new Date();
    public job: Models.Job;
    public error: boolean = false;

    constructor(
        public dialogRef: MatDialogRef<AvailabilityDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any, private dataService: DataService) {

    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onToggleAvailability() {
        this.dataService.updateAvailability(this.data.date)
            .subscribe(resp => {
                this.dialogRef.close();
            }, err => {
                this.error = true
            });
    }

}