import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { EventEmitter } from "events";
import { Output, Inject, Component } from "@angular/core";

@Component({
    selector: 'conflict-dialog',
    templateUrl: './conflict.dialog.html',
  })
  export class ConflictDialog {
  
    constructor(
      public dialogRef: MatDialogRef<ConflictDialog>,
      @Inject(MAT_DIALOG_DATA) public data: any) { }
  
    onNoClick(): void {
      this.dialogRef.close();
    }
  
  }