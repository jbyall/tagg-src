import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate,Router } from '@angular/router';

import { AuthService } from './auth.service';
import { DataService } from '../services/data.service';
import { Job } from '../shared/models';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class RatingGuard implements CanActivate {
    public unratedJobs: Job[];

    constructor(private authService: AuthService, private router: Router, private dataService: DataService) {

    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if(!this.dataService.jobsForReview.length){
            return true;
        }
        else{
            this.router.navigate(['/rate']);
            return false;
        }
    }

    
}