import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate, CanLoad, Router, Route, ActivatedRoute } from '@angular/router';

import { AuthService } from './auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
    redirectUrl: string;
    constructor(private authService: AuthService, private router: Router) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
        if(this.authService.isLoggedIn()){
            return true;
        }
        else{
            localStorage.setItem('taggRedirect', state.url);
            this.authService.startAuthentication();
            return false;
        }
        
    }
}
