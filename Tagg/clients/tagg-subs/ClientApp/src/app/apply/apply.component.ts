import { Component, Input, OnInit, ViewEncapsulation, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl, ValidatorFn, FormArray } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { PasswordMatch } from "../shared/validators";
import 'rxjs/add/operator/debounceTime';
import { AuthService } from '../auth/auth.service';
import * as Models from "../shared/models";
import { MatStepper } from '@angular/material';

import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { DataService } from '../services/data.service';
import { AccountService } from '../services/account.service';

@Component({
    selector: 'app-apply',
    templateUrl: './apply.component.html',
    providers: [AccountService]
})
export class ApplyComponent implements OnInit {
    isLinear = false;
    // accountForm: FormGroup;
    // profileForm: FormGroup;
    // preferencesForm: FormGroup;
    // contactForm: FormGroup;
    substitute: Models.Substitute;
    viewData: Models.AccountViewData;
    stepNumber: number;
    public viewReady: boolean;
    @ViewChild('stepper') stepper: MatStepper;

    constructor(
        private fb: FormBuilder,
        private _route: ActivatedRoute, private _router: Router,
        private authService: AuthService, private dataService: DataService,
        private accountService: AccountService) {
        this.accountService.step$.subscribe(
            resp => this.setStep(resp)
        );

        this.accountService.sub$.subscribe(
            resp => {
                this.substitute = resp
                this.getViewReady();
            });

        this.accountService.viewData$.subscribe(
            resp => {
                this.viewData = resp;
                this.getViewReady();
            });
    }

    ngOnInit() {
        this.dataService.setHeaders();
        this.accountService.getSubAccount();
        this.accountService.getViewData();
        this.stepNumber = 0;
    }

    getViewReady() {
        if (this.viewData && this.substitute) {
            this.viewReady = true;
        }
    }

    onStepNext(){
        this.accountService.goToStep(this.stepNumber + 1);
    }

    onStepPrevious(){
        this.accountService.goToStep(this.stepNumber - 1);
    }

    setStep(index: Models.ApplyStep) {
        this.stepNumber = index;
        if (index == Models.ApplyStep.Complete) {
            if (this.substitute.receiveSMS) {
                let phone = this.substitute.phoneNumber;
                this._router.navigate(['/verify-phone', phone]);
            }
            else {
                this._router.navigate(['account']);
            }

        }
        else {
            this.stepper.selectedIndex = index;
        }

    }

}

