﻿// Modules
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Components
import { ApplyComponent } from './apply.component';
import { AuthService } from '../auth/auth.service';
import { TaggCommonModule } from '../shared/tagg-common.module';
import { AccountModule } from '../shared/account/account.module';
//import { LayoutModule } from '@angular/cdk/layout';

@NgModule({
    imports: [
        CommonModule,
        //HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        TaggCommonModule,
        AccountModule,
        //RouterModule
        //LayoutModule
    ],
    declarations: [
        ApplyComponent,
    ],
})
export class ApplyModule { }