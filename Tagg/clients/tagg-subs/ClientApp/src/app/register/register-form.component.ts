import { Component, Input, OnInit } from '@angular/core';
import { FormsModule, FormGroup, FormBuilder, Validators, AbstractControl, FormControl, ValidatorFn } from "@angular/forms";
import * as Models from "../shared/models";
import { ActivatedRoute, Router } from "@angular/router";
import { PasswordMatch } from "../shared/validators";
import 'rxjs/add/operator/debounceTime';
import { DataService } from '../services/data.service';
import { AuthService } from '../auth/auth.service';

@Component({
    selector: 'register-form',
    templateUrl: './register-form.component.html',
    styleUrls: ['./register-form.component.css']
})
export class RegisterFormComponent implements OnInit {
    model: Models.RegisterModel;
    public registerForm: FormGroup;
    locationResult: Models.DistanceResult[];
    emailErrorMessage: string;
    duplicateErrorMessage: string;
    confirmPasswordErrorMessage: string;
    showSuccess: boolean = false;
    showSpinner: boolean = false;
    public outOfArea: boolean;
    public showPasswordInvalid: boolean;

    constructor(private fb: FormBuilder,
        private _route: ActivatedRoute,
        private _router: Router,
        private authService: AuthService,
        private dataService: DataService) {
        this.registerForm = this.fb.group({
            firstName: ['', Validators.required], // [{value: '', disabled:true/false}, Validators or [array of validation rules]] or 
            lastName: ['', Validators.required],
            email: ['', [Validators.required, Validators.email]],
            postalCode: ['', Validators.required],
            agreeToTerms: [false, Validators.requiredTrue],
            passwordGroup: this.fb.group({
                password: ['', Validators.required],
                confirmPassword: ['', [Validators.required]]
            }, { validator: passwordMatchValidator })
        });
    }

    validationMessages = {
        email: 'Please enter a valid email address',
        match: 'Passwords do not match'
    }

    get firstName() { return this.registerForm.get('firstName') };
    get lastName() { return this.registerForm.get('lastName') };
    get email() { return this.registerForm.get('email') };
    get postalCode() { return this.registerForm.get('postalCode') };
    get password() { return this.registerForm.get('passwordGroup').get('password') };
    get confirmPassword() { return this.registerForm.get('passwordGroup').get('confirmPassword') };
    get passwordGroup() { return this.registerForm.get('passwordGroup') };


    ngOnInit() {
        const emailControl = this.email;
        emailControl.valueChanges.debounceTime(1000).subscribe(value => this.setEmailMessage(emailControl));

        const pwGroupControl = this.passwordGroup;
        pwGroupControl.valueChanges.debounceTime(1000).subscribe(value => this.setConfirmPasswordMessage(pwGroupControl));
    }

    onRegister() {
        if (this.passwordIsValid()) {
            if (this.registerForm.dirty && this.registerForm.valid) {
                this.showSpinner = true;
                this.dataService.checkLocation(this.postalCode.value)
                    .subscribe(resp => {
                        for (var i = 0; i < resp.length; i++) {
                            if (resp[i].inServiceArea) {
                                this.submitRegister();
                            }
                            else {
                                this.outOfArea = true;
                                this.showSpinner = false;
                            }
                        }
                    });

            }
        }
        else{
            this.showPasswordInvalid = true;
        }

    }

    submitRegister() {
        let p = new Models.RegisterModel();
        Object.assign(p, this.registerForm.value, this.registerForm.value.passwordGroup);
        this.dataService.register(p)
            .subscribe(resp => {
                this.showSuccess = true;
            }, err => {
                if (err.status === 409) {
                    this.duplicateErrorMessage = "This email address has been taken.";
                }
                this.showSpinner = false;
            });
    }

    setEmailMessage(control: AbstractControl): void {
        this.emailErrorMessage = '';
        this.duplicateErrorMessage = '';
        if (!control.valid && (control.touched || control.dirty)) {
            this.emailErrorMessage = Object.keys(control.errors).map(key =>
                this.validationMessages[key]).join(' ');
        }
    }

    setConfirmPasswordMessage(control: AbstractControl): void {
        this.confirmPasswordErrorMessage = '';
        const confirmControl = control.get('confirmPassword');
        if ((confirmControl.touched || confirmControl.dirty) && !control.valid) {
            this.confirmPasswordErrorMessage = Object.keys(control.errors).map(key => {
                return this.validationMessages[key]
            }).join(' ');
        }
        else {
            this.confirmPasswordErrorMessage = '';
        }
    }

    passwordIsValid(): boolean {
        let hasUpper = this.password.value.match(/(?=[A-Z])/) != undefined;
        let hasNumber = this.password.value.match(/(?=[0-9])/) != undefined;
        let hasLength = this.password.value.length > 7;
        let isValid = hasUpper && hasNumber && hasLength;
        if (isValid) {
            this.showPasswordInvalid = false;
        }
        return isValid;
    }
}


export function passwordMatchValidator(c: AbstractControl): { [key: string]: boolean } | null {
    let passwordControl = c.get('password');
    let confirmPasswordControl = c.get('confirmPassword');

    if (passwordControl.pristine || confirmPasswordControl.pristine) {
        return null;
    }

    if (passwordControl.value === confirmPasswordControl.value) {
        return null;
    }

    return { 'match': true };
}

