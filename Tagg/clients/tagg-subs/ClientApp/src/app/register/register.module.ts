﻿import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from "@angular/common";
//import { HttpClientModule } from "@angular/common/http";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { RegisterFormComponent } from "./register-form.component";
import { RegisterComponent } from "./register.component";
import { ConfirmEmailComponent } from "./confirm-email.component";
import { AuthService } from '../auth/auth.service';
import { AuthGuard } from '../auth/auth.guard';
import { TaggCommonModule } from '../shared/tagg-common.module';
import { RegisterVerifyComponent } from './register-verify.component';
import { RegisterStepsComponent } from './register-steps.component';
import { RegisterPasswordComponent } from './register-password.component';


@NgModule({
    imports: [
        CommonModule,
        //HttpClientModule,
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forChild([
            { path: '', component: RegisterFormComponent },
            { path: 'confirm/:id', component: RegisterPasswordComponent },
            { path: 'confirm', component: ConfirmEmailComponent },
            { path: 'verify/:id', component: RegisterVerifyComponent },
            { path: 'steps', component: RegisterStepsComponent, canActivate:[AuthGuard] },
            // {
            //     path: '', component: RegisterComponent, redirectTo:'/main',
            //     children: [
            //         { path: 'area', component: RegisterAreaComponent },
            //         { path: 'confirm', component: ConfirmEmailComponent },
            //         { path: 'main', component: RegisterFormComponent}
            //     ]
            // },
            // { path: ':id', component: RegisterFormComponent }
        ]),
        TaggCommonModule
    ],
    providers: [
    ],
    declarations: [
        RegisterFormComponent,
        RegisterComponent,
        ConfirmEmailComponent,
        RegisterVerifyComponent,
        RegisterStepsComponent,
        RegisterPasswordComponent,
    ]
})
export class RegisterModule { }