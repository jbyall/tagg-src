import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { FileUploader } from 'ng2-file-upload';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { validateConfig } from '@angular/router/src/config';
import { DataService } from '../services/data.service';
import { AccountService } from '../services/account.service';
import * as Models from '../shared/models';
import { Subjects, ContactMethods, PayAmounts, Distances, States } from '../shared/constants';
import { environment } from '../../environments/environment.prod';

@Component({
  selector: 'app-register-steps',
  templateUrl: './register-steps.component.html',
  styleUrls: ['./register-steps.component.css']
})
export class RegisterStepsComponent implements OnInit {
  public user: Models.Substitute;
  public settingsForm: FormGroup;
  public preferenceForm: FormGroup;
  public showForm: boolean;

  public contactMethods = ContactMethods;
  public payAmounts = PayAmounts;
  public distances = Distances;
  public subjects = Subjects;
  public states = States;

  public selectedSubjects: number[];
  public selectedContact: number[] = [];
  public doVerifyPhone: boolean;
  public showMessage: true;

  public stepNumber: number;

  public phone: string;
  public code: string;
  public showVerify: boolean = false;
  public showError: boolean = false;
  public showSuccess: boolean = false;

  constructor(public dataService: DataService, private accountService: AccountService, private router: Router, public fb: FormBuilder) { }

  ngOnInit() {
    this.dataService.setHeaders();
    this.dataService.getAccount()
      .subscribe(resp => {
        this.user = resp;
        if(this.user.enabled){
          this.router.navigate(['/home']);
        }
        this.populateForm();
      });
  }

  populateForm() {
    // For phone verification step
    this.phone = this.user.phoneNumber;
    if(!this.user.address){
      let address = new Models.Address();
      this.user.address = address;
    }

    this.settingsForm = this.fb.group({
      firstName: [this.user.firstName, Validators.required],
      lastName: [this.user.lastName, Validators.required],
      email: [this.user.email, Validators.required],
      phoneNumber: [this.user.phoneNumber, Validators.required],
      street1: [this.user.address.street1, Validators.required],
      street2: this.user.address.street2,
      city: [this.user.address.city, Validators.required],
      state: [this.user.address.state, Validators.required],
      postalCode: [this.user.address.postalCode, Validators.required]
    });

    this.preferenceForm = this.fb.group({
      maxDistance: this.user.maxDistance,
      minPay: this.user.minPayAmount,
    });

    this.selectedSubjects = this.user.subjects.length ? this.user.subjects : [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
    if (this.user.receiveEmail) {
      this.selectedContact.push(0);
    }
    if (this.user.receiveSMS) {
      this.selectedContact.push(1);
    }
    if (this.user.receivePhone) {
      this.selectedContact.push(2);
    }
    if (!this.selectedContact.length) {
      this.selectedContact = [0, 1, 2];
    }

    this.stepNumber = 1;
    this.showForm = true;

  }

  markDirty() {
    this.preferenceForm.markAsDirty();
  }

  onNext() {
    if (this.settingsForm.dirty || this.preferenceForm.dirty) {
      let phoneNumber = this.settingsForm.get('phoneNumber').value;
      let receiveSMS = this.selectedContact.includes(1);

      this.user.receiveEmail = this.selectedContact.includes(0);
      this.user.receiveSMS = this.selectedContact.includes(1);
      this.user.receivePhone = this.selectedContact.includes(2);

      this.user.address.street1 = this.settingsForm.get('street1').value;
      this.user.address.street2 = this.settingsForm.get('street2').value;
      this.user.address.city = this.settingsForm.get('city').value;
      this.user.address.state = this.settingsForm.get('state').value;
      this.user.address.postalCode = this.settingsForm.get('postalCode').value;

      this.user.phoneNumber = this.settingsForm.get('phoneNumber').value;
      this.phone = this.user.phoneNumber;
      this.user.firstName = this.settingsForm.get('firstName').value;
      this.user.lastName = this.settingsForm.get('lastName').value;

      this.user.subjects = this.selectedSubjects;
      this.user.minPayAmount = this.preferenceForm.get('minPay').value;
      this.user.maxDistance = this.preferenceForm.get('maxDistance').value;

      this.dataService.saveAccount(this.user)
        .subscribe(resp => {
          this.user = resp;
          this.goToStep(this.stepNumber + 1);
          this.settingsForm.markAsPristine();
          this.preferenceForm.markAsPristine();
        });

    }
    else {
      this.goToStep(this.stepNumber + 1);
    }
  }

  goToStep(stepNumber: number) {
    this.stepNumber = stepNumber;
  }

  onSendCode() {
    this.dataService.sendPhoneCode(this.phone)
      .subscribe(resp => {
        this.showVerify = true;
      }, err => {
        this.showError = true;
      });
  }

  onVerify() {
    this.dataService.verifyCode(this.code, this.phone)
      .subscribe(resp => {
        this.showSuccess = true;
      }, err => {
        this.showError = true;
      });
  }

  onAddPayment() {
    window.location.href = `https://connect.stripe.com/express/oauth/authorize?redirect_uri=${environment.stripeCallbackUrl}&client_id=${environment.stripeClientId}&state=a1b2c3`
  }

}
