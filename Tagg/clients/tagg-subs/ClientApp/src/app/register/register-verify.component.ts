import { Component, OnInit } from '@angular/core';
import { FormsModule, FormGroup, FormBuilder, ReactiveFormsModule, Validators, AbstractControl, FormControl, ValidatorFn } from "@angular/forms";
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth/auth.service';
import * as Models from "../shared/models";
import { DataService } from '../services/data.service';

@Component({
    selector: 'app-register-verify',
    templateUrl: './register-verify.component.html',
    styleUrls: ['./register-verify.component.css']
})
export class RegisterVerifyComponent implements OnInit {
    token: string;
    public errorMessage: string;
    public showForm: boolean;
    public showEmail: boolean;
    public user: Models.IUser;
    public showSpinner: boolean = false;
    public displayEmail: string;

    constructor(private fb: FormBuilder, private _route: ActivatedRoute, private _router: Router, private dataService: DataService, private authService: AuthService) {
        this.token = this._route.snapshot.params['id'];
    }

    ngOnInit() {
        this.user = this.authService.getInviteData(this.token);
        let emailParts = this.user.email.split('@');
        let hiddenPart = emailParts[0].slice(0,1);
        this.displayEmail = hiddenPart.concat('-'.repeat(emailParts[0].length-1)) + '@' + emailParts[1];
        this.showForm = true;
    }

    onVerify() {
        this.showForm = false;
        this.showSpinner = true;
        this.dataService.requestEmailVerification(this.user.email)
            .subscribe(resp => {
                this.showEmail = true;
            }, err => {
                this.errorMessage = "Oops, we ran into a problem. Please contact support.";
            });
    }

}
