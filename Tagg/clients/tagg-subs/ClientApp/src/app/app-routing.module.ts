// Modules
import { NgModule } from '@angular/core';
import { RouterModule } from "@angular/router";
// Services
import { AuthGuard } from "./auth/auth.guard";
// Components
import { HomeComponent } from './dash/home.component';
import { PageNotFoundComponent } from "./layout/page-not-found.component";
import { AboutComponent } from './layout/about.component';
import { ContactComponent } from './layout/contact.component';
import { ApplyComponent } from './apply/apply.component';
import { ApplyModule } from './apply/apply.module';
import { ProfileComponent } from './shared/account/profile.component';
import { VerifyPhoneComponent } from './shared/account/verify-phone.component';
import { AuthCallbackComponent } from './auth/auth-callback.component';
import { JobsComponent } from './dash/jobs.component';
import { MessagesComponent } from './dash/messages.component';
import { MetricsComponent } from './dash/metrics.component';
import { SettingsComponent } from './shared/account/settings.component';
import { RatingGuard } from './auth/rating.guard';
import { JobRatingComponent } from './dash/job-rating.component';
import { ChangeEmailComponent } from './shared/account/change-email.component';



@NgModule({
    imports: [
        ApplyModule,
        RouterModule.forRoot([
            { path: 'home', component: HomeComponent, canActivate: [AuthGuard,RatingGuard] },
            { path: 'jobs', component: JobsComponent, canActivate: [AuthGuard, RatingGuard] },
            { path: 'messages', component: MessagesComponent, canActivate: [AuthGuard, RatingGuard] },
            { path: 'metrics', component: MetricsComponent, canActivate: [AuthGuard, RatingGuard] },
            { path: 'rate', component: JobRatingComponent, canActivate: [AuthGuard] },
            {
                path: 'register',
                loadChildren: './register/register.module#RegisterModule'
            },
            {
                path: 'apply', component: ApplyComponent, canActivate: [AuthGuard]
                //loadChildren: './apply/apply.module#ApplyModule'
            },
            { path: 'verify-phone/:id', component: VerifyPhoneComponent },
            { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
            { path: 'settings', component: SettingsComponent, canActivate: [AuthGuard] },
            { path: 'account', component: SettingsComponent, canActivate: [AuthGuard] },
            { path: 'change-email', component: ChangeEmailComponent, canActivate: [AuthGuard] },
            { path: 'auth-callback', component: AuthCallbackComponent },
            { path: 'contact', component: ContactComponent },
            { path: 'about', component: AboutComponent },
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: '**', component: PageNotFoundComponent }
        ])
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
