// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from "./app-routing.module";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Services
import { AuthService } from "./auth/auth.service";
import { AuthGuard } from "./auth/auth.guard";
import { DataService } from './services/data.service';

// Components
import { AppComponent } from './app.component';
import { HomeComponent, AvailabilityDialog } from './dash/home.component';
import { PageNotFoundComponent } from "./layout/page-not-found.component";
import { AboutComponent } from './layout/about.component';
import { ContactComponent } from './layout/contact.component';
import { ProfileComponent } from './shared/account/profile.component';
import { AccountModule } from './shared/account/account.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TaggCommonModule } from './shared/tagg-common.module';
import { AuthCallbackComponent } from './auth/auth-callback.component';
import { JobsComponent, ViewJobDialog } from './dash/jobs.component';
import { MessagesComponent } from './dash/messages.component';
import { MetricsComponent } from './dash/metrics.component';
import { AccountService } from './services/account.service';
import { MessageService } from './services/message.service';
import { ConflictDialog } from './dash/conflict.dialog';
import { JobService } from './services/job.service';
import { JobRatingComponent } from './dash/job-rating.component';
import { RatingGuard } from './auth/rating.guard';
import { ChangeEmailComponent } from './shared/account/change-email.component';



@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PageNotFoundComponent,
    AboutComponent,
    ContactComponent,
    AuthCallbackComponent,
    JobsComponent,
    MessagesComponent,
    MetricsComponent,
    JobRatingComponent,
    ChangeEmailComponent,
    ViewJobDialog,
    AvailabilityDialog,
    ConflictDialog
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    NgbModule.forRoot(),
    AccountModule,
    TaggCommonModule,
    
  ],
  providers: [
    AuthGuard,
    RatingGuard,
    AuthService,
    DataService,
    AccountService,
    MessageService,
    JobService
  ],
  entryComponents: [ViewJobDialog, AvailabilityDialog, ConflictDialog],
  bootstrap: [AppComponent]
})
export class AppModule { }
