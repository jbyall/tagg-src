export const ContactMethods: any[] = [
    { key: 0, value: "Email" },
    { key: 1, value: "Text" },
    { key: 2, value: "Phone" }
]

export const PayAmounts: any[] = [
    { key: 0, value: "$0" },
    { key: 10, value: "$10" },
    { key: 20, value: "$20" },
    { key: 30, value: "$30" },
    { key: 40, value: "$40" },
    { key: 50, value: "$50" },
    { key: 60, value: "$60" },
    { key: 70, value: "$70" },
    { key: 80, value: "$80" },
    { key: 90, value: "$90" },
    { key: 100, value: "$100" },
    { key: 110, value: "$110" },
    { key: 120, value: "$120" },
    { key: 130, value: "$130" },
    { key: 140, value: "$140" },
    { key: 150, value: "$150" },
]

export const Distances: any[] = [
    { key: 10, value: "10" },
    { key: 20, value: "20" },
    { key: 30, value: "30" },
    { key: 40, value: "40" },
    { key: 50, value: "50" },
    { key: 60, value: "60" },
    { key: 70, value: "70" },
    { key: 80, value: "80" },
    { key: 90, value: "90" },
    { key: 100, value: "100" }
]

export const Subjects: any[] = [
    { key: 0, value: "Pre-K" },
    { key: 1, value: "Kindergarten" },
    { key: 2, value: "1st Grade" },
    { key: 3, value: "2nd Grade" },
    { key: 4, value: "3rd Grade" },
    { key: 5, value: "4th Grade" },
    { key: 6, value: "5th/6th Grade" },
    { key: 7, value: "Grades 6-8 Math" },
    { key: 8, value: "Grades 6-8 English" },
    { key: 9, value: "Grades 6-8 Social Studies" },
    { key: 10, value: "Grades 6-8 Science" },
    { key: 11, value: "Grades 6-8 Electives" },
    { key: 12, value: "Grades 9-12 Math" },
    { key: 13, value: "Grades 9-12 English" },
    { key: 14, value: "Grades 9-12 Social Studies" },
    { key: 15, value: "Grades 9-12 Science" },
    { key: 16, value: "Grades 9-12 Electives" },
]

export const SubjectDisplay: any[]=[
    "Pre-K",
    "Kindergarten",
    "1st Grade",
    "2nd Grade",
    "3rd Grade",
    "4th Grade",
    "5th/6th Grade",
    "Grades 6-8 Math",
    "Grades 6-8 English",
    "Grades 6-8 Social Studies",
    "Grades 6-8 Science",
    "Grades 6-8 Electives",
    "Grades 9-12 Math",
    "Grades 9-12 English",
    "Grades 9-12 Social Studies",
    "Grades 9-12 Science",
    "Grades 9-12 Electives",
];

export const States: any[] = [
    {
        "key": "AL",
        "value": "Alabama",
        "order": 0
    },
    {
        "key": "AK",
        "value": "Alaska",
        "order": 0
    },
    {
        "key": "AZ",
        "value": "Arizona",
        "order": 0
    },
    {
        "key": "AR",
        "value": "Arkansas",
        "order": 0
    },
    {
        "key": "CA",
        "value": "California",
        "order": 0
    },
    {
        "key": "CO",
        "value": "Colorado",
        "order": 0
    },
    {
        "key": "CT",
        "value": "Connecticut",
        "order": 0
    },
    {
        "key": "DE",
        "value": "Delaware",
        "order": 0
    },
    {
        "key": "DC",
        "value": "District of Columbia",
        "order": 0
    },
    {
        "key": "FL",
        "value": "Florida",
        "order": 0
    },
    {
        "key": "GA",
        "value": "Georgia",
        "order": 0
    },
    {
        "key": "HI",
        "value": "Hawaii",
        "order": 0
    },
    {
        "key": "ID",
        "value": "Idaho",
        "order": 0
    },
    {
        "key": "IL",
        "value": "Illinois",
        "order": 0
    },
    {
        "key": "IN",
        "value": "Indiana",
        "order": 0
    },
    {
        "key": "IA",
        "value": "Iowa",
        "order": 0
    },
    {
        "key": "KS",
        "value": "Kansas",
        "order": 0
    },
    {
        "key": "KY",
        "value": "Kentucky",
        "order": 0
    },
    {
        "key": "LA",
        "value": "Louisiana",
        "order": 0
    },
    {
        "key": "ME",
        "value": "Maine",
        "order": 0
    },
    {
        "key": "MT",
        "value": "Montana",
        "order": 0
    },
    {
        "key": "NE",
        "value": "Nebraska",
        "order": 0
    },
    {
        "key": "NV",
        "value": "Nevada",
        "order": 0
    },
    {
        "key": "NH",
        "value": "New Hampshire",
        "order": 0
    },
    {
        "key": "NJ",
        "value": "New Jersey",
        "order": 0
    },
    {
        "key": "NM",
        "value": "New Mexico",
        "order": 0
    },
    {
        "key": "NY",
        "value": "New York",
        "order": 0
    },
    {
        "key": "NC",
        "value": "North Carolina",
        "order": 0
    },
    {
        "key": "ND",
        "value": "North Dakota",
        "order": 0
    },
    {
        "key": "OH",
        "value": "Ohio",
        "order": 0
    },
    {
        "key": "OK",
        "value": "Oklahoma",
        "order": 0
    },
    {
        "key": "OR",
        "value": "Oregon",
        "order": 0
    },
    {
        "key": "MD",
        "value": "Maryland",
        "order": 0
    },
    {
        "key": "MA",
        "value": "Massachusetts",
        "order": 0
    },
    {
        "key": "MI",
        "value": "Michigan",
        "order": 0
    },
    {
        "key": "MN",
        "value": "Minnesota",
        "order": 0
    },
    {
        "key": "MS",
        "value": "Mississippi",
        "order": 0
    },
    {
        "key": "MO",
        "value": "Missouri",
        "order": 0
    },
    {
        "key": "PA",
        "value": "Pennsylvania",
        "order": 0
    },
    {
        "key": "RI",
        "value": "Rhode Island",
        "order": 0
    },
    {
        "key": "SC",
        "value": "South Carolina",
        "order": 0
    },
    {
        "key": "SD",
        "value": "South Dakota",
        "order": 0
    },
    {
        "key": "TN",
        "value": "Tennessee",
        "order": 0
    },
    {
        "key": "TX",
        "value": "Texas",
        "order": 0
    },
    {
        "key": "UT",
        "value": "Utah",
        "order": 0
    },
    {
        "key": "VT",
        "value": "Vermont",
        "order": 0
    },
    {
        "key": "VA",
        "value": "Virginia",
        "order": 0
    },
    {
        "key": "WA",
        "value": "Washington",
        "order": 0
    },
    {
        "key": "WV",
        "value": "West Virginia",
        "order": 0
    },
    {
        "key": "WI",
        "value": "Wisconsin",
        "order": 0
    },
    {
        "key": "WY",
        "value": "Wyoming",
        "order": 0
    }
];