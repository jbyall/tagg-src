export class CalendarDay {
    public date: Date;
    public year: number;
    public monthIndex: number;
    public dayOfMonthIndex: number;
    public dayOfWeekIndex: number;
    public dayOfWeek: string;
    public events: CalendarEvent[];
}

export class CalendarEvent {
    public description: string;
    public job: Job;
    public availability: UserAvailability;
}

export class UserAvailability {
    public startDate: Date;
    public endDate: Date;
}

export class IUser {
    [x: string]: any;
    public email: string;
    public id: string;
    public firstName: string;
    public lastName: string;
    public phoneNumber: string;
    public school: string;
    public type: string;
}

export class Substitute {
    
        public educationLevel: string = '';
        public hasTeachingLicense: boolean = false;
        public hasTranscripts: boolean = false;
        public resumeFile: string = '';
        public applicationComplete: boolean = false;
        //public approvalDate: Date = new Date();
        public linkedInUrl: string = '';
        public minPayAmount: 0;
        public maxDistance: 0;
        public location: null;
        public subjects: number[] = [];
        public id: string = '';
        public userType: 0;
        public firstName: string = '';
        public lastName: string = '';
        public email: string = '';
        public phoneNumber: string = '';
        public picture: string;
        public bio: string = '';
        public title: string = '';
        public receivePhone: boolean = true;
        public receiveSMS: boolean = true;
        public receiveEmail: boolean = true;
        public enabled: boolean = false;
        public status: number = 0;
        public interests: string = '';
        public address: Address = new Address();
        public paymentId: string;
        public rating: number;
        public ratingCount: number;
        public deactivated: boolean;
}

export class ImageUploadModel{
    public imageFile: File;
    public contentType: string;
    public length: number;
    public name: string;
    public fileName: string;
}

export class Job {
    public id: string;
    public schoolId: string;
    public schoolName: string;
    public schoolAddress: Address;
    public teacherId: string;
    public teacherName: string;
    public substituteId: string;
    public substituteName: string;
    public subject: string;
    public payAmount: number;
    public status: number;
    public startDate: Date;
    public endDate: Date;
    public lessonPlanId: string;
}

export class JobRating {
    public substituteId: string;
    public jobId: string;
    public schoolReviewerId: string;
    public teacherId: string;
    public ratings: RatingModel[] = [];
    public subComments: string;
}

export class RatingModel {
    constructor(
        public category: string = "",
        public rating: number = 5
    ) { }

}

export class Message {
    public to: string;
    public from: string;
    public subject: string;
    public body: string;
    public readOn: Date;
    public headers: any[];
    public id: string;
    public created: Date;
}

export class MessageThread{
    public id: string;
    public subject: string;
    public messages: Message[];
    public audience: string[];
    public jobId: string;
}

export class AllJobsResponse {
    public accepted: Job[];
    public offered: Job[];
    public recent: Job[];
}

export class Address {
    constructor(
        public street1: string = '',
        public street2: string = '',
        public city: string = '',
        public state: string = '',
        public postalCode: string = ''
    ) { }
}

// Model for register form
export class RegisterModel {
    constructor(
        public firstName: string = '',
        public lastName: string = '',
        public email: string = '',
        public password: string = '',
        public confirmPassword: string = '',
        public postalCode: string = '',
        public apiAccessLevel: string = '0x1'
    ) { }
}

export class AccountViewData {
    public states: Lookup[];
    public educationLevels: Lookup[];
    public subjects: Lookup[];
}

export enum ApplyStep {
    Account = 0,
    Profile = 1,
    Preferences = 2,
    Contact = 3,
    Phone = 4,
    Complete = 5,
}

export class PaymentInfo{
    public country: string = 'US';
    public currency: string = 'usd';
    public routingNumber: string;
    public accountNumber: string;
    public accountHolderName: string;
    public accountHolderType: string;
}

export class CustomerCreateModel{
    public sourceTokenId: string;
    public description: string;
}

// export enum AccountSection {
//     Account = 1,
//     Profile,
//     Preferences,
//     Contact
// }



// Result from distance calculation
export class DistanceResult {
    constructor(
        public text: string,
        public value: number,
        public serviceLocation: Location,
        public inServiceArea: boolean
    ) { }
}

//
export class Location {
    public latitude: number;
    public longitude: number;
}

export class LoginModel {
    constructor(
        public email: string,
        public password: string
    ) { }
}

export class TokenResponse {
    constructor(
        public token: string = "",
        public expiration: Date = new Date()
    ) { }
}

export class Lookup {
    constructor(
        public key: string,
        public value: string,
        public id?: number,
        public type?: string
    ) { }
}

export class MultiSelect {
    constructor(
        public key: string = '',
        public value: string = '',
        public selected: boolean = false
    ) { }
}

export class Constants{
    
}

export class ChatChannelSummary{
    public attributes: any;
    public createdBy: string;
    public dateCreated: Date;
    public dateUpdated: Date;
    public descriptor: any;
    public friendlyName: string;
    public isPrivate: boolean;
    public lastConsumedMessageIndex: number;
    public membersCount: number;
    public messageCount: number;
    public sid: string;
    public status: any;
    public type: any;
    public uniqueName: string;
}

export class ChatChannel{
    public attributes: any;
    public createdBy: string;
    public dateCreated: Date;
    public dateUpdated: Date;
    public friendlyName: string;
    public isPrivate: boolean;
    public lastConsumedMessageIndex: number;
    public lastMessage: any;
    public sid: string;
    public status: any;
    public type: any;
    public uniqueName: string;
}

export class SubMetrics{
    public earningsLastMonth: number;
    public earningsAvgPerMonth: number;
    public rating: number;
    public daysSubbing: number;
    public totalSchools: number;
    public favoriteSchool: string;
    public favoriteGrade: number;
    public sub: Substitute;
}



// export class SubUserModel {
//     constructor(
//         public account: AccountModel,
//         public profile: ProfileModel,
//         public preferences: JobPreferencesModel,
//         public contact: ContactPreferencesModel,
//         public viewData: ApplyViewData,
//         public userProfileId: number
//     ) { }
// }

// export class AccountModel {
//     constructor(
//         public firstName: string = '',
//         public lastName: string = '',
//         public email: string = '',
//         public postalCode: string = '',
//         public street1: string = '',
//         public street2: string = '',
//         public city: string = '',
//         public state: string = '',
//         public phoneNumber: string = '',
//         public userProfileId: number = 0,
//         public isComplete: boolean = false
//     ) { }
// }

// export class ProfileModel {
//     constructor(
//         public education: string = '',
//         public hasLicense: boolean = false,
//         public hasTranscripts: boolean = false,
//         public linkedInUrl: string = '',
//         public resumeText: string = '',
//         public interests: string = '',
//         public userProfileId: number = 0,
//         public isComplete: boolean = false
//     ) { }
// }

// export class JobPreferencesModel {
//     constructor(
//         public minPay: number = 0,
//         public maxDistance: number = 0,
//         public subjects: MultiSelect[] = [],
//         public userProfileId: number = 0,
//         public isComplete: boolean = false
//     ) { }
// }

// export class ContactPreferencesModel {
//     constructor(
//         public receivePhone: MultiSelect = new MultiSelect(),
//         public receiveText: MultiSelect = new MultiSelect(),
//         public receiveEmail: MultiSelect = new MultiSelect(),
//         public phoneVerified: boolean = false,
//         public emailVerified: boolean = false,
//         public userProfileId: number = 0,
//         public isComplete: boolean = false
//     ) { }
// }

// export class ApplyViewData {
//     constructor(
//         public states: Lookup[],
//         public educations: Lookup[],
//         public subjects: Lookup[],
//     ) { }
// }



