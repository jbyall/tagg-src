import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import * as Models from '../models';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { AccountFormComponent } from './account-form.component';
import { DataService } from '../../services/data.service';
import { FileUploader, FileUploaderOptions } from 'ng2-file-upload';
import { AccountService } from '../../services/account.service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
    selector: 'app-profile',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
    public educationLevels: any[] = [
        { key: 0, value: "None" },
        { key: 1, value: "Associates Degree" },
        { key: 2, value: "Bachelors Degree" },
        { key: 3, value: "Masters Degree" },
        { key: 4, value: "Doctoral Degree" }
    ]

    public yesNo: any[] = [
        { key: 0, value: "No" },
        { key: 1, value: "Yes" },
    ]

    user: Models.Substitute;
    viewData: Models.AccountViewData;
    showForm: boolean;
    profileForm: FormGroup;
    public photos: string = environment.userPhotoEndpoint;
    public fileReader: FileReader;
    public showImageError: boolean;
    public showResumeError: boolean;

    public accountStatus = { icon: 'error_outline', color: 'list-group-item list-group-item-warning', panel: 'panel panel-warning' };
    public profileStatus = { icon: 'error_outline', color: 'list-group-item list-group-item-warning', panel: 'panel panel-warning' };
    public preferencesStatus = { icon: 'error_outline', color: 'list-group-item list-group-item-warning', panel: 'panel panel-warning' };
    public contactStatus = { icon: 'error_outline', color: 'list-group-item list-group-item-warning', panel: 'panel panel-warning' };
    public applicationComplete: boolean = false;
    public isApproved: boolean = false;
    public uploader: FileUploader;

    constructor(public dataService: DataService, private accountService: AccountService, private router: Router, private fb: FormBuilder, public snackBar: MatSnackBar) {
    }

    ngOnInit() {
        this.dataService.setHeaders();
        this.dataService.getAccount()
            .subscribe(resp => {
                this.user = resp;
                
                this.populateForm();
                this.getUserStatus();
            });
    }
    populateForm() {
        this.profileForm = this.fb.group({
            bio: this.user.bio,
            interests: this.user.interests,
            education: this.user.educationLevel,
            license: this.user.hasTeachingLicense,
            transcripts: this.user.hasTranscripts,
            linkedIn: this.user.linkedInUrl
        });
        let hasLicense = this.user.hasTeachingLicense ? 1 : 0;
        let hasTranscripts = this.user.hasTranscripts ? 1 : 0;

        this.profileForm.controls["education"].setValue(this.user.educationLevel);
        this.profileForm.controls["transcripts"].setValue(hasTranscripts);
        this.profileForm.controls["license"].setValue(hasLicense);
        this.uploader = new FileUploader({url: 'http://localhost:50706/api/subs/account/resume/' + this.user.id})
    }

    onImageUpload(images: FileList) {
        this.showImageError = false;
        let imageUpload = images.item(0);
        let fileType = imageUpload.type;
        if (fileType === "image/jpeg" || fileType === "image/png" || fileType === "image/gif") {
            var data = new FormData();
            data.append("file", imageUpload)
            this.dataService.uploadPicture(data)
                .subscribe(resp => {
                    this.user.picture = resp;
                }, err => console.log('image upload error', err));
        }
        else {
            this.showImageError = true;
        }
    }

    onResumeUpload(resume: FileList){
        this.showResumeError = false;
        let resumeUpload = resume.item(0);
        let fileType = resumeUpload.type;
        if(fileType === "application/msword"
            || fileType === "application/pdf" 
            || fileType === "application/vnd.openxmlformats-officedocument.wordprocessingml.document")
            {
            var data = new FormData();
            data.append("file", resumeUpload);
            this.dataService.uploadResume(data)
                .subscribe(resp => {
                    this.user.resumeFile = resp;
                    console.log(resp);
                }, err => console.log('resume upload error', err));
        }
        else{
            this.showResumeError = true;
        }
    }

    onDownloadResume(){
        this.dataService.getResume(this.user.resumeFile)
            .subscribe(resp => {
                window.open(window.URL.createObjectURL(resp));
            }, err => console.log(err));
    }


    onSubmit() {
        let bio = this.profileForm.get('bio').value;
        let interests = this.profileForm.get('interests').value;
        let education = this.profileForm.get('education').value;
        let license = this.profileForm.get('license').value;
        let transcripts = this.profileForm.get('transcripts').value;
        let linkedIn = this.profileForm.get('linkedIn').value;

        this.user.bio = bio;
        this.user.interests = interests;
        this.user.educationLevel = education;
        this.user.hasTeachingLicense = license;
        this.user.hasTranscripts = transcripts;
        this.user.linkedInUrl = linkedIn;

        this.dataService.saveAccount(this.user)
            .subscribe(resp => {
                this.user = resp;
                this.profileForm.markAsPristine();
            });
    }

    onAddPayment() {
        window.location.href = `https://connect.stripe.com/express/oauth/authorize?redirect_uri=${environment.stripeCallbackUrl}&client_id=${environment.stripeClientId}&state=a1b2c3`
    }

    onPaymentLogin() {
        this.dataService.getPaymentsLogin()
            .subscribe(resp => {
                window.location.href = resp;
            });
    }



    getUserStatus() {
        if (this.user.status == 1) {
            this.isApproved = true;
        }
        this.applicationComplete = this.user.address !== undefined
            && this.user.subjects.length > 0
            && (this.user.receiveEmail || this.user.receivePhone || this.user.receiveSMS)
            && this.user.paymentId !== '';

    }
}

