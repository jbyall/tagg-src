import { Component, OnInit} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, AbstractControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { AuthService } from '../../auth/auth.service';
import { DataService } from '../../services/data.service';


@Component({
  selector: 'app-change-email',
  templateUrl: './change-email.component.html',
  styleUrls: ['./change-email.component.css']
})
export class ChangeEmailComponent implements OnInit {
    public emailForm: FormGroup;
    public showForm: boolean = false;
    public showError: boolean;
    public showSuccess: boolean;
    
    constructor(private fb: FormBuilder, 
        private _route: ActivatedRoute,
         private _router: Router, 
         private authService: AuthService, private dataService: DataService) {
    }

    get currentEmail(): AbstractControl { return this.emailForm.get('currentEmail') };
    get newEmail(): AbstractControl { return this.emailForm.get('newEmail') };
    

    ngOnInit(){
        this.dataService.setHeaders();
        this.emailForm = this.fb.group({
            currentEmail:this.authService.user.profile.email,
            newEmail:["", [Validators.required, Validators.email]],
        });
        this.showForm = true;
    };

    onSubmit() {
        if (this.emailForm.dirty && this.emailForm.valid) {
            this.dataService.userChangeEmail(this.currentEmail.value, this.newEmail.value)
                .subscribe(resp => {
                    this.showSuccess = true;
                }, err => {this.showError = true;})
        }
    };
}