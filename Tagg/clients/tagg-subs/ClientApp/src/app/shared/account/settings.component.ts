import { Component, OnInit, Inject, ViewChild } from '@angular/core';
import { AuthService } from '../../auth/auth.service';
import * as Models from '../models';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA, MatSnackBar } from '@angular/material';
import { AccountFormComponent } from './account-form.component';
import { DataService } from '../../services/data.service';
import { FileUploader } from 'ng2-file-upload';
import { AccountService } from '../../services/account.service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ContactMethods, PayAmounts, Distances, Subjects } from '../constants';
import { validateConfig } from '@angular/router/src/config';
import { DeactivateDialog } from './dialogs/deactivate.dialog';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  user: Models.Substitute;
  settingsForm: FormGroup;
  preferenceForm: FormGroup;
  public applicationComplete: boolean = false;
  public isApproved: boolean = false;
  public showForm: boolean;
  public passwordReset = environment.passwordResetUrl;

  public contactMethods = ContactMethods;
  public payAmounts = PayAmounts;
  public distances = Distances;
  public subjects = Subjects;

  public selectedSubjects: number[];
  public selectedContact: number[] =[];
  public doVerifyPhone: boolean;
  public showMessage: true;

  constructor(public dataService: DataService, private accountService: AccountService, private router: Router, private fb: FormBuilder, public snackBar: MatSnackBar, public dialog: MatDialog) {
  }

  ngOnInit() {
    this.dataService.setHeaders();
    this.getAccount();
  }

  getAccount(){
    this.dataService.getAccount()
      .subscribe(resp => {
        this.user = resp;
        this.populateForm();
        this.getUserStatus();
      });
  }

  populateForm() {
    this.settingsForm = this.fb.group({
      firstName: [this.user.firstName, Validators.required],
      lastName: [this.user.lastName, Validators.required],
      email: [this.user.email, Validators.required],
      phoneNumber: [this.user.phoneNumber, Validators.required],
      street1: [this.user.address.street1, Validators.required],
      street2: this.user.address.street2,
      city: [this.user.address.city, Validators.required],
      state: [this.user.address.state, Validators.required],
      postalCode: [this.user.address.postalCode, Validators.required]
    });

    this.preferenceForm = this.fb.group({
      maxDistance: this.user.maxDistance,
      minPay: this.user.minPayAmount,
    });

    this.selectedSubjects = this.user.subjects.length ? this.user.subjects : [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16];
    if(this.user.receiveEmail){
      this.selectedContact.push(0);
    }
    if(this.user.receiveSMS){
      this.selectedContact.push(1);
    }
    if(this.user.receivePhone){
      this.selectedContact.push(2);
    }
    if(!this.selectedContact.length){
      this.selectedContact = [0,1,2];
    }
    this.showForm = true;

  }

  markDirty(){
    this.preferenceForm.markAsDirty();
  }

  onChangeEmail(){
    this.router.navigate(['/change-email']);
  }

  onSubmit() {
    let phoneNumber = this.settingsForm.get('phoneNumber').value;
    let receiveSMS = this.selectedContact.includes(1);

    if(receiveSMS && (!this.user.receiveSMS || phoneNumber !== this.user.phoneNumber)){
      this.doVerifyPhone = true;
    }

    this.user.receiveEmail = this.selectedContact.includes(0);
    this.user.receiveSMS = this.selectedContact.includes(1);
    this.user.receivePhone = this.selectedContact.includes(2);

    this.user.address.street1 = this.settingsForm.get('street1').value;
    this.user.address.street2 = this.settingsForm.get('street2').value;
    this.user.address.city = this.settingsForm.get('city').value;
    this.user.address.state = this.settingsForm.get('state').value;
    this.user.address.postalCode = this.settingsForm.get('postalCode').value;

    this.user.phoneNumber = this.settingsForm.get('phoneNumber').value;
    this.user.firstName = this.settingsForm.get('firstName').value;
    this.user.lastName = this.settingsForm.get('lastName').value;

    this.user.subjects = this.selectedSubjects;
    this.user.minPayAmount = this.preferenceForm.get('minPay').value;
    this.user.maxDistance = this.preferenceForm.get('maxDistance').value;

    this.dataService.saveAccount(this.user)
      .subscribe(resp => {
        this.user = resp;
        if(this.doVerifyPhone){
          this.router.navigate(['/verify-phone', phoneNumber]);
        }
        else{
          this.settingsForm.markAsPristine();
          this.preferenceForm.markAsPristine();
        }
        
      });
  }

  getUserStatus() {
    if (this.user.status == 1) {
        this.isApproved = true;
    }
    // let accountDone = this.user.address !== undefined;
    // let profileDone = this.user.educationLevel.length > 0;
    // let preferencesDone = this.user.subjects.length > 0;
    // let contactDone = this.user.receiveEmail || this.user.receivePhone || this.user.receiveSMS;
    this.applicationComplete = this.user.address !== undefined 
    && this.user.subjects.length > 0
    && (this.user.receiveEmail || this.user.receivePhone || this.user.receiveSMS)
    && this.user.paymentId !== '';
  
}

  onAddPayment() {
    window.location.href = `https://connect.stripe.com/express/oauth/authorize?redirect_uri=${environment.stripeCallbackUrl}&client_id=${environment.stripeClientId}&state=a1b2c3`
  }

  onPaymentLogin() {
    this.dataService.getPaymentsLogin()
      .subscribe(resp => {
        window.location.href = resp;
      });
  }

  showDeactivateDialog(){
    let width = window.innerWidth < 992 ? '80%' : '600px';
    let dialogRef = this.dialog.open(DeactivateDialog, {
      width: width,
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result){
        this.snackBar.open('Your account has been successfully deactivated.', 'Dismiss');
      }
      this.getAccount();
    });
  }

  onReactivate(){
    this.dataService.accountReactivate(this.user.id)
      .subscribe(resp => {
        this.getAccount();
      });
  }
}
