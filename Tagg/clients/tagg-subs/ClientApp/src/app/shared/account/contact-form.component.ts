import { Component, Input, OnInit, ViewEncapsulation, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl, ValidatorFn, FormArray } from "@angular/forms";
import * as Models from '../models';
import { AuthService } from '../../auth/auth.service';
import { DataService } from '../../services/data.service';
import { AccountService } from '../../services/account.service';


@Component({
    selector: 'contact-form',
    templateUrl: './contact-form.component.html',
})
export class ContactFormComponent implements OnInit {
    @Input() model: Models.Substitute;
    showForm: boolean;

    constructor(
        private fb: FormBuilder,
        private dataService: DataService, private accountService: AccountService) {
    }

    public checkedPhone: boolean;
    public checkedEmail: boolean;
    public checkedText: boolean;

    ngOnInit() {
        this.populateForm();
        // this.accountService.sub$.subscribe(
        //     resp => {
        //         this.model = resp;
        //         this.populateForm();
        //     });


    }

    populateForm() {
        var allTrue = !this.model.receiveEmail && !this.model.receivePhone && !this.model.receiveSMS
        this.checkedEmail = this.model.receiveEmail || allTrue;
        this.checkedPhone = this.model.receivePhone || allTrue;
        this.checkedText = this.model.receiveSMS || allTrue;
        this.showForm = true;
    }

    onContactSubmit() {
        let c = Object.assign({}, this.model)
        c.receiveEmail = this.checkedEmail;
        c.receivePhone = this.checkedPhone;
        c.receiveSMS = this.checkedText;

        this.accountService.updateContact(c);
    }


}