// Modules
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

// Components
import { TaggCommonModule } from '../tagg-common.module';
import { AccountFormComponent } from './account-form.component';
import { ProfileFormComponent } from './profile-form.component';
import { PreferencesFormComponent } from './preferences-form.component';
import { ContactFormComponent } from './contact-form.component';
import { ProfileComponent } from './profile.component';
import { VerifyPhoneComponent } from './verify-phone.component';
import { FileUploadModule } from 'ng2-file-upload';
import { SettingsComponent } from './settings.component';
import { DeactivateDialog } from './dialogs/deactivate.dialog';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        TaggCommonModule,
        FileUploadModule,
        RouterModule
    ],
    declarations: [
        AccountFormComponent,
        ProfileFormComponent,
        PreferencesFormComponent,
        ContactFormComponent,
        ProfileComponent,
        VerifyPhoneComponent,
        SettingsComponent,
        DeactivateDialog
    ],
    exports: [
        AccountFormComponent,
        ProfileFormComponent,
        PreferencesFormComponent,
        ContactFormComponent,
        VerifyPhoneComponent,
    ],
    entryComponents:[DeactivateDialog]
})
export class AccountModule { }