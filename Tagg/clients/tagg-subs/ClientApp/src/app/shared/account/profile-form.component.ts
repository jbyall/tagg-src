import { Component, Input, OnInit, ViewEncapsulation, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl, ValidatorFn, FormArray } from "@angular/forms";
import * as Models from '../models';
import { AuthService } from '../../auth/auth.service';
import { DataService } from '../../services/data.service';
import { AccountService } from '../../services/account.service';


@Component({
    selector: 'profile-form',
    templateUrl: './profile-form.component.html',
})
export class ProfileFormComponent implements OnInit {
    @Input() model: Models.Substitute;
    @Input() viewData: Models.AccountViewData;
    profileForm: FormGroup;
    public yesNo = [{ key: true, value: "Yes" }, { key: false, value: "No" }, { key: false, value: "" }];
    message: string;
    showForm: boolean;

    constructor(
        private fb: FormBuilder,
        private dataService: DataService, private accountService: AccountService) {

    }

    ngOnInit() {
        this.populateForm();
        // this.accountService.sub$.subscribe(
        //     resp => {
        //         this.model = resp;
        //         this.populateForm();
        //     });

        // this.accountService.viewData$.subscribe(
        //     resp => {
        //         this.viewData = resp;
        //         if(!this.showForm && this.model){
        //             this.populateForm();
        //         }
        //     });
    }

    populateForm(){
        this.profileForm = this.fb.group({
            educationLevel: [this.model.educationLevel, Validators.required],
            hasTranscripts: [this.model.hasTranscripts, Validators.required],
            hasTeachingLicense: [this.model.hasTeachingLicense, Validators.required],
            bio: this.model.bio,
            linkedInUrl: this.model.linkedInUrl,
            interestes: this.model.interests
        });
        if(this.viewData){
            this.showForm = true;
        }
    }



    onProfileSubmit() {
        if (this.profileForm.dirty && this.profileForm.touched && this.profileForm.valid) {
            let p = Object.assign({}, this.model, this.profileForm.value);
            this.accountService.updateProfile(p);
        }
        else{
            this.accountService.goToStep(Models.ApplyStep.Preferences);
        }
    }


}