import { Component, Input, OnInit, ViewEncapsulation, OnChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl, ValidatorFn, FormArray } from "@angular/forms";
import * as Models from '../models';
import { AuthService } from '../../auth/auth.service';
import { all } from 'q';
import { DataService } from '../../services/data.service';
import { AccountService } from '../../services/account.service';


@Component({
    selector: 'preferences-form',
    templateUrl: './preferences-form.component.html',
})
export class PreferencesFormComponent implements OnInit {
    preferencesForm: FormGroup;
    @Input() model: Models.Substitute;
    @Input() viewData: Models.AccountViewData;
    showForm: boolean;
    public yesNo = [{ key: true, value: "Yes" }, { key: false, value: "No" }];
    public elementaryArray: Models.MultiSelect[] = [];
    public middleSchoolArray: Models.MultiSelect[] = [];
    public highSchoolArray: Models.MultiSelect[] = [];
    //public showForm = false;
    message: string;

    constructor(
        private fb: FormBuilder,
        private dataService: DataService, private accountService: AccountService) {
        this.preferencesForm = this.fb.group({
            minPayAmount: ['', Validators.required],
            maxDistance: ['', Validators.required],
            elementarySubjects: this.fb.array([]),
            middleSchoolSubjects: this.fb.array([]),
            highSchoolSubjects: this.fb.array([]),
        });
    }
    get elementarySubjects(): FormArray {
        return <FormArray>this.preferencesForm.get('elementarySubjects');
    }

    get middleSchoolSubjects(): FormArray {
        return <FormArray>this.preferencesForm.get('middleSchoolSubjects');
    }

    get highSchoolSubjects(): FormArray {
        return <FormArray>this.preferencesForm.get('highSchoolSubjects');
    }

    ngOnInit() {
        this.populateForm();
        // this.accountService.sub$.subscribe(
        //     resp => {
        //         this.model = resp;
        //         this.populateForm();
        //     }
        // );

        // this.accountService.viewData$.subscribe(
        //     resp => {
        //         this.viewData = resp;
        //         this.populateForm();
        //     });
    }

    populateForm() {
        if (this.viewData && this.model) {
            this.preferencesForm.patchValue({
                minPayAmount: this.model.minPayAmount,
                maxDistance: this.model.maxDistance,
            });

            if (this.highSchoolArray.length < 1) {
                let allTrue = this.model.subjects.length < 1;
                // this.viewData.subjects.slice(0, 7).forEach(element => {
                //     let val = allTrue || this.model.subjects.find(function (obj) { return obj === element.key }) !== undefined;
                //     this.elementaryArray.push(new Models.MultiSelect(element.key, element.value, val));
                // });
                // this.viewData.subjects.slice(7, 12).forEach(element => {
                //     let val = allTrue || this.model.subjects.find(function (obj) { return obj === element.key }) !== undefined;
                //     this.middleSchoolArray.push(new Models.MultiSelect(element.key, element.value, val));
                // });
                // this.viewData.subjects.slice(12).forEach(element => {
                //     let val = allTrue || this.model.subjects.find(function (obj) { return obj === element.key }) !== undefined;
                //     this.highSchoolArray.push(new Models.MultiSelect(element.key, element.value, val));
                // });

                // let elementaryArray = this.viewData.subjects.slice(0, 7);
                const elemSubjectGroups = this.elementaryArray.map(subject => this.fb.group(subject));
                const elemFormArray = this.fb.array(elemSubjectGroups);
                this.preferencesForm.setControl('elementarySubjects', elemFormArray);

                // let msArray = this.viewData.subjects.slice(7, 12);
                const msSubjectGroups = this.middleSchoolArray.map(subject => this.fb.group(subject));
                const msFormArray = this.fb.array(msSubjectGroups);
                this.preferencesForm.setControl('middleSchoolSubjects', msFormArray);

                // let hsArray = this.viewData.subjects.slice(12);
                const hsSubjectGroups = this.highSchoolArray.map(subject => this.fb.group(subject));
                const hsFormArray = this.fb.array(hsSubjectGroups);
                this.preferencesForm.setControl('highSchoolSubjects', hsFormArray);
                this.preferencesForm.markAsDirty();
            }
            this.showForm = true;
        }
    }

    onPreferencesSubmit() {

        if (this.preferencesForm.dirty && this.preferencesForm.touched && this.preferencesForm.valid) {
            let p = Object.assign({}, this.model)
            p.minPayAmount = this.preferencesForm.get("minPayAmount").value;
            p.maxDistance = this.preferencesForm.get("maxDistance").value;
            p.subjects = [];
            this.elementarySubjects.controls.forEach(element => {
                if (element.value.selected) {
                    p.subjects.push(element.value.key);
                }
            });
            this.middleSchoolSubjects.controls.forEach(element => {
                if (element.value.selected) {
                    p.subjects.push(element.value.key);
                }
            });
            this.highSchoolSubjects.controls.forEach(element => {
                if (element.value.selected) {
                    p.subjects.push(element.value.key);
                }
            });
            this.accountService.updatePreferences(p);
        }
        else {
            this.accountService.goToStep(Models.ApplyStep.Contact);
        }

    }


}