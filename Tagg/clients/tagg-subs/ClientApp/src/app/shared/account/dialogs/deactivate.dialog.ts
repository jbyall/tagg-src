import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material";
import { Router } from "@angular/router";
import { AuthService } from "../../../auth/auth.service";
import { DataService } from "../../../services/data.service";

@Component({
    selector: 'deactivate-dialog',
    templateUrl: 'deactivate.dialog.html',
})
export class DeactivateDialog {

    public showError: boolean;
    public showSuccess: boolean;
    public didDeactivate: boolean;

    constructor(
        public dialogRef: MatDialogRef<DeactivateDialog>,
        @Inject(MAT_DIALOG_DATA) public data: any, private _router: Router, private authService: AuthService, private dataService: DataService) {
        this._router.events.subscribe(() => { this.onNoClick() });
    }

    onNoClick(): void {
        this.dialogRef.close(this.didDeactivate);
    }

    onDeactivateAccount() {
        this.dataService.accountDeactivate(this.authService.user.profile.sub)
            .subscribe(resp => {
                this.showSuccess = true;
                this.didDeactivate = true;
            }, err => {
                this.showError = true;
            })
    }

}