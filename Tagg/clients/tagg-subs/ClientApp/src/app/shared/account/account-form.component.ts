import { Component, OnInit, ViewEncapsulation, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, FormControl, ValidatorFn, FormArray } from "@angular/forms";
import * as Models from '../models';
import { AuthService } from '../../auth/auth.service';
import { DataService } from '../../services/data.service';
import { AccountService } from '../../services/account.service';


@Component({
    selector: 'account-form',
    templateUrl: './account-form.component.html',
    styleUrls: ['./account-form.component.css']
})
export class AccountFormComponent implements OnInit {
    @Input() model: Models.Substitute;
    @Input() viewData: Models.AccountViewData;
    accountForm: FormGroup;
    message: string;
    step: Models.ApplyStep;
    showForm: boolean;

    constructor(private fb: FormBuilder, private accountService: AccountService, private dataService: DataService) {

    }

    ngOnInit() {
        this.populateForm();
    }

    getViewData() {
        this.accountService.viewData$.subscribe(
            resp => {
                this.viewData = resp;
                this.populateForm();
            });
    }

    populateForm() {
        this.accountForm = this.fb.group({
            address: this.fb.group({
                street1: [this.model.address.street1, Validators.required],
                street2: this.model.address.street2,
                city: [this.model.address.city, Validators.required],
                state: [this.model.address.state, Validators.required],
                postalCode: [this.model.address.postalCode, Validators.required]
            }),
            phoneNumber: [this.model.phoneNumber, Validators.required],
        });
    }

    onAccountSubmit() {
        if (this.accountForm.dirty && this.accountForm.touched && this.accountForm.valid) {
            let a = Object.assign({}, this.model);
            Object.assign(a.address, this.accountForm.get('address').value)
            a.phoneNumber = this.accountForm.get('phoneNumber').value;
            this.accountService.updateAccount(a);
        }
        else {
            this.accountService.goToStep(Models.ApplyStep.Profile);
        }
    }
}