// For dependency injection
import { Injectable } from '@angular/core';

// For calls to API/web service
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

// For exception handling
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/map';

import { Subject } from 'rxjs/Subject';

import * as Models from "../shared/models";
import { environment } from '../../environments/environment';
import { DataService } from './data.service';

@Injectable()
export class AccountService {
    // Observable sources
    private _sub = new Subject<Models.Substitute>();
    private _viewData = new Subject<Models.AccountViewData>();
    private step = new Subject<Models.ApplyStep>();
    
    public subAccount: Models.Substitute;


    constructor(private dataService: DataService) {
        this.dataService.setHeaders();
    }

    step$ = this.step.asObservable();
    sub$ = this._sub.asObservable();
    viewData$ = this._viewData.asObservable();



    public getSubAccount(){
        this.dataService.setHeaders();
        this.dataService.getAccount()
            .subscribe(resp =>{
                this._sub.next(resp);
            });
    }

    public getViewData(){
        this.dataService.setHeaders();
        this.dataService.getAccountViewData()
            .subscribe(resp => {
                this._viewData.next(resp);
            })
    }

    public updateAccount(sub: Models.Substitute) {
        this.dataService.saveAccount(sub)
            .subscribe(resp => {
                this.subAccount = resp;
                this._sub.next(resp);
                this.step.next(Models.ApplyStep.Profile);
            });
    }

    public updateProfile(sub: Models.Substitute) {
        this.dataService.saveAccount(sub)
            .subscribe(resp => {
                this.subAccount = resp;
                this._sub.next(resp);
                this.step.next(Models.ApplyStep.Preferences);
            });
    }

    public updatePreferences(sub: Models.Substitute) {
        this.dataService.saveAccount(sub)
            .subscribe(resp => {
                this.subAccount = resp;
                this._sub.next(resp);
                this.step.next(Models.ApplyStep.Contact);
            });
    }

    public updateContact(sub: Models.Substitute) {
        this.dataService.saveAccount(sub)
            .subscribe(resp => {
                this.subAccount = resp;
                this._sub.next(resp);
                this.step.next(Models.ApplyStep.Complete);
            });
    }

    public goToStep(index: number){
        if(index >= 0){
            this.step.next(index);
        }
        
    }
}

