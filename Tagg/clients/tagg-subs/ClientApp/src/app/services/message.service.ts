import { Injectable } from '@angular/core';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { DataService } from './data.service';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class MessageService {
    public chatToken: any;
    public chatClient: any;
    public channelDescriptors: any[];
    public hasNewMessages: boolean;

    private suppressNavIcon: boolean;
    private _newChannel = new Subject<boolean>();
    private _newMessage = new Subject<boolean>();
    private _chatReady = new Subject<boolean>();
    private _channelRemoved = new Subject<boolean>();

    private newMessageDelegate = (event) => this.getStatus();
    private newChannelDelegate = (event) => this.onNewChannel();
    private channelRemovedDelegate = (event) => this.onChannelRemoved();

    constructor(private dataService: DataService) {
        //console.log('Service: constructor called');
    }

    newChannel$ = this._newChannel.asObservable();
    newMessage$ = this._newMessage.asObservable();
    chatReady$ = this._chatReady.asObservable();
    channelRemoved$ = this._channelRemoved.asObservable();


    // Get chat token from Tagg Server for the client
    public getChatToken(){
        if (!this.chatToken || !this.chatClient) {
            //console.log('Service: getting chat token');
            this.dataService.getChatToken()
                .subscribe(resp => {
                    //console.log('Service: got token', resp);
                    this.chatToken = resp;
                    this.initChatClient();
                });
        }
        else{
            //console.log('Service: else happened CHAT READY');
            this.getStatus();
            this._chatReady.next(true);
        }
    }

    // Initialize the Chat client
    initChatClient() {
        //console.log('Service: initting chat client')
        Twilio.Chat.Client.create(this.chatToken)
            .then(client => {
                //console.log('Service: got client');
                this.chatClient = client;
                this.loadChannels();
            });
    }


    loadChannels(setListeners: boolean = true){
        //console.log('Service: loading channels');
        this.chatClient.getSubscribedChannels()
            .then(resp => {
                //console.log('Service: channels loaded', resp);
                this.channelDescriptors = resp.items;
                this.getStatus();
                if(setListeners){
                    //console.log('Service: set listeners true');
                    this.setClientListeners();
                }
            });
    }

    setClientListeners() {
       // console.log('Service: setting listeners');
        this.chatClient.addListener('channelAdded', this.newChannelDelegate);
        this.chatClient.addListener('messageAdded', this.newMessageDelegate);
        //console.log('Service: listeners set, CHAT READY');
        this._chatReady.next(true);
    }

    getStatus(){
        //console.log('Service: getting status');
        let foundUnreads = false;
        for(let i = 0; i < this.channelDescriptors.length; i++){
            let descriptor = this.channelDescriptors[i];
            //console.log('Service: channel from status', descriptor);
            //console.log('consumed', descriptor.lastConsumedMessageIndex);
            //console.log('messages', descriptor.lastMessage.index);
            if(descriptor.lastConsumedMessageIndex < descriptor.lastMessage.index || descriptor.lastConsumedMessageIndex == null){
                foundUnreads = true;   
                break;
            }
        }
        this.onNewMessage(foundUnreads);
    }

    onNewMessage(hasUnread: boolean){
        if(!this.suppressNavIcon){
           // console.log('Service: notify next message', hasUnread);
            this._newMessage.next(hasUnread);
        }
        
    }

    onNewChannel(){
        //console.log('Chat Service: notify new channel');
        this._newChannel.next(true);
    }

    onChannelRemoved(){
       // console.log('Chat Service: channel removed');
        this._channelRemoved.next(true);
    }

    public statusRecheck(){
        //console.log('Service: status recheck');
        this.getStatus();
    }
}