import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient, HttpErrorResponse, HttpResponse, HttpHeaders } from '@angular/common/http';
import { UserManager, UserManagerSettings, User } from 'oidc-client';
import * as Models from "../shared/models";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import { RequestOptions, ResponseContentType } from '@angular/http';
import { AuthService } from '../auth/auth.service';

@Injectable()
export class DataService {
    currentUser: Models.IUser;
    redirectUrl: string;

    // Register & account endpoints
    private _lookupAccountViewDataUrl = environment.apiUrl + "lookup/accountviewdata";
    private _locationCheckUrl = environment.apiUrl + "location/check/";
    private _registerPhoneVerifyUrl = environment.apiUrl + "register/phone/verify/";
    private _registerUrl = environment.apiUrl + "register";
    private _registerPartialUrl = environment.apiUrl + "register/partial";
    private _registerEmailUrl = environment.apiUrl + "register/email/confirm/";
    private _accountGetUrl = environment.apiUrl + "subs/account";
    private _accountSaveUrl = environment.apiUrl + "subs/account/save";
    private _userPictureUpdateUrl = environment.apiUrl + "users/photo/";
    private _subResumeUpdateUrl = environment.apiUrl + "subs/account/resume/";
    private _subResumeGetUrl = environment.apiUrl + "subs/resume/";
    private _accountDeactivate = environment.apiUrl + "subs/deactivate/";
    private _accountReactivate = environment.apiUrl + "subs/activate/";

    // job endpoints
    private _jobGetUrl = environment.apiUrl + "jobs/";
    private _jobGetDetailsUrl = environment.apiUrl + "jobs/details/";
    private _jobsOpenUrl = environment.apiUrl + "jobs/subs/open";
    private _jobsAllUrl = environment.apiUrl + "jobs/subs/";
    private _jobSummaryUrl = environment.apiUrl + "jobs/subs/summary/";
    private _jobDeclineUrl = environment.apiUrl + "jobs/decline/";
    private _jobAcceptUrl = environment.apiUrl + "jobs/accept/";
    private _jobLessonPlanUrl = environment.apiUrl + "jobs/lessonplan/";
    private _jobCancelUrl = environment.apiUrl + "jobs/cancel/sub/";
    private _jobsUnratedGet = environment.apiUrl + "jobs/sub/unrated/";
    private _jobsAddSchoolRatingUrl = environment.apiUrl + "jobs/rate/school";

    // calendar endpoints
    private _calendarGetUrl = environment.apiUrl + "calendar/subs/";
    private _calendarAvailAddUrl = environment.apiUrl + "calendar/subs/";
    // payments endpoints
    private _paymentsCustomerCreateUrl = environment.apiUrl + "payments/Customer";
    private _paymentsLoginUrl = environment.apiUrl + "payments/login";
    // messages
    private _messageGetTokenUrl = environment.apiUrl + "messages/token";
    private _messageCreateChannelUrl = environment.apiUrl + "messages/channel/job/";

    // metrics
    private _metricsGetUrl = environment.apiUrl + "metrics/sub/";

    // user endpoints
    private _usersChangeEmail = environment.apiUrl + "users/email/";

    authHeaders = new HttpHeaders();
    public jobsForReview: string[] = [];

    constructor(private http: HttpClient, private authService: AuthService) {
        this.authService.userLoaded$
            .subscribe(resp => {
                this.setHeaders();
            })
    }

    public setHeaders() {
        this.authHeaders = this.authHeaders.set('Authorization', this.authService.getAuthorizationHeaderValue());
    }

    public addJobsForReview(jobIds: string[]): void {
        this.jobsForReview = this.jobsForReview.concat(jobIds);
    }

    public removeJob(jobId: string): void {
        var index = this.jobsForReview.indexOf(jobId);
        this.jobsForReview = this.jobsForReview.splice(index, 1);
    }

    // Register & account
    public register(model: Models.RegisterModel): Observable<boolean> {
        return this.http.post<boolean>(this._registerUrl, model);
    }
    public partialRegister(model: Models.RegisterModel): Observable<boolean> {
        return this.http.post<boolean>(this._registerPartialUrl, model);
    }
    public getAccountViewData(): Observable<Models.AccountViewData> {
        return this.http.get<Models.AccountViewData>(this._lookupAccountViewDataUrl, { headers: this.authHeaders });
    }
    public checkLocation(postalCode: number): Observable<Models.DistanceResult[]> {
        return this.http.get<Models.DistanceResult[]>(this._locationCheckUrl + postalCode.toString())
    }
    public sendPhoneCode(number: string): Observable<string> {
        return this.http.get<string>(this._registerPhoneVerifyUrl + number, { headers: this.authHeaders });
    }
    public verifyCode(code: string, phoneNumber: string): Observable<string> {
        return this.http.post<string>(this._registerPhoneVerifyUrl, { "code": code, "phoneNumber": phoneNumber }, { headers: this.authHeaders });
    }
    public getAccount(): Observable<Models.Substitute> {
        return this.http.get<Models.Substitute>(this._accountGetUrl, { headers: this.authHeaders });
    }
    public saveAccount(account: Models.Substitute): Observable<Models.Substitute> {
        return this.http.post<Models.Substitute>(this._accountSaveUrl, account, { headers: this.authHeaders });
    }
    public uploadPicture(data: any): Observable<string> {
        return this.http.post<string>(this._userPictureUpdateUrl + this.authService.user.profile.sub, data, { headers: this.authHeaders });
    }
    public uploadResume(data: any): Observable<string> {
        return this.http.post<string>(this._subResumeUpdateUrl + this.authService.user.profile.sub, data, { headers: this.authHeaders });
    }
    public getResume(resumeId: string): Observable<Blob> {
        //let customHeaders = this.authHeaders;
        return this.http.get(this._subResumeGetUrl + resumeId, { headers: this.authHeaders, responseType: 'blob' });
    }

    public requestEmailVerification(email: string): Observable<string> {
        return this.http.get<string>(this._registerEmailUrl + email);
    }

    public accountDeactivate(userId: string): Observable<boolean> {
        return this.http.put<boolean>(this._accountDeactivate + userId, null, { headers: this.authHeaders });
    }

    public accountReactivate(userId: string): Observable<boolean> {
        return this.http.put<boolean>(this._accountReactivate + userId, null, { headers: this.authHeaders });
    }

    // Job
    public getJob(jobId: string): Observable<Models.Job> {
        return this.http.get<Models.Job>(this._jobGetUrl + jobId, { headers: this.authHeaders });
    }
    public getJobDetails(jobId: string): Observable<Models.Job> {
        return this.http.get<Models.Job>(this._jobGetDetailsUrl + jobId, { headers: this.authHeaders });
    }
    public getOpenJobs(): Observable<Models.Job[]> {
        return this.http.get<Models.Job[]>(this._jobsOpenUrl, { headers: this.authHeaders });
    }
    public getAllJobs(limit: number = 100): Observable<Models.AllJobsResponse> {
        return this.http.get<Models.AllJobsResponse>(this._jobsAllUrl + limit, { headers: this.authHeaders });
    }
    public jobCancel(id: string): Observable<boolean> {
        return this.http.put<boolean>(this._jobCancelUrl + id, null, { headers: this.authHeaders });
    }
    public getUnratedJobIds(subId: string): Observable<string[]> {
        return this.http.get<string[]>(this._jobsUnratedGet + subId, { headers: this.authHeaders });
    }
    public addJobRating(rating: Models.JobRating): Observable<boolean> {
        return this.http.post<boolean>(this._jobsAddSchoolRatingUrl, rating, { headers: this.authHeaders });
    }

    // Used for side details
    public getJobSummary(limit: number = 9): Observable<Models.AllJobsResponse> {
        return this.http.get<Models.AllJobsResponse>(this._jobSummaryUrl + limit, { headers: this.authHeaders });
    }

    public jobDecline(id: string): Observable<string> {
        return this.http.get<string>(this._jobDeclineUrl + id + '/' + this.authService.user.profile.sub, { headers: this.authHeaders });
    }
    public jobAccept(id: string): Observable<string> {
        return this.http.get<string>(this._jobAcceptUrl + id + '/' + this.authService.user.profile.sub, { headers: this.authHeaders });
    }

    public getLessonPlan(lessonPlanId: string): Observable<Blob> {
        return this.http.get(this._jobLessonPlanUrl + lessonPlanId, { headers: this.authHeaders, responseType: 'blob' });
    }



    // Calendar
    public getSubCalendar(month: number, year: number): Observable<Models.CalendarDay[]> {
        return this.http.get<Models.CalendarDay[]>(this._calendarGetUrl + month + "/" + year, { headers: this.authHeaders });
    }
    public updateAvailability(date: Date): Observable<string> {
        return this.http.get<string>(this._calendarAvailAddUrl + date, { headers: this.authHeaders });
    }

    // Payments
    public createCustomer(customer: Models.CustomerCreateModel): Observable<string> {
        return this.http.post<string>(this._paymentsCustomerCreateUrl, customer, { headers: this.authHeaders });
    }
    public getPaymentsLogin(): Observable<string> {
        return this.http.get<string>(this._paymentsLoginUrl, { headers: this.authHeaders });
    }

    // messages
    public getChatToken(): Observable<any> {
        return this.http.get<any>(this._messageGetTokenUrl, { headers: this.authHeaders });
    }

    public getOrCreateJobChannel(jobId: string): Observable<string> {
        return this.http.post<string>(this._messageCreateChannelUrl + jobId, null, { headers: this.authHeaders });
    }

    // metrics
    public getMetrics(): Observable<Models.SubMetrics> {
        return this.http.get<Models.SubMetrics>(this._metricsGetUrl + this.authService.user.profile.sub, { headers: this.authHeaders });
    }

    // users
    public userChangeEmail(currentEmail: string, newEmail: string): Observable<string> {
        return this.http.put<string>(this._usersChangeEmail + currentEmail + '/' + newEmail, null, { headers: this.authHeaders });
    }

    private handleError(error: Response): Observable<any> {
        console.error(error);
        return Observable.throw(error || "Server error");
    }



}
