import { Injectable } from '@angular/core';
import * as Models from "../shared/models";
import { DataService } from './data.service';
import { AuthService } from '../auth/auth.service';
import { Subject } from 'rxjs/Subject';
import { Observable } from '../../../node_modules/rxjs/Observable';


@Injectable()
export class JobService {
    public lastQuery: Date;
    public jobs: Models.AllJobsResponse;
    private _jobsReady = new Subject<boolean>();

    constructor(private dataService: DataService, private authService: AuthService) { }

    jobsReady$ = this._jobsReady.asObservable();

    public getJobs() {
        let now = new Date();
        if (!this.lastQuery || (now.getTime() - this.lastQuery.getTime()) > 60000) {
            this.dataService.getJobSummary()
                .subscribe(resp =>{
                    this.jobs = resp;
                    this._jobsReady.next(true);
                });
            this.lastQuery = new Date();
        }
        else{
            this._jobsReady.next(true);
        }
    }

    public getJobDetails(jobId: string): Observable<Models.Job>{
        return this.dataService.getJobDetails(jobId);
    }
}